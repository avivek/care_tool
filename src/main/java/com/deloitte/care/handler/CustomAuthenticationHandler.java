package com.deloitte.care.handler;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

public class CustomAuthenticationHandler extends
		SimpleUrlAuthenticationSuccessHandler {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws ServletException, IOException {
		if(request.getSession().getAttribute("userid")==null){
			request.getSession().setAttribute("userid", authentication.getCredentials());
		}
		Set<String> roles = AuthorityUtils.authorityListToSet(authentication
				.getAuthorities());
		if (roles.contains("ROLE_RECRUITER")) {
			getRedirectStrategy().sendRedirect(request, response, "/recruiter");
		} else if (roles.contains("ROLE_PANELIST")) {
			getRedirectStrategy()
					.sendRedirect(request, response, "/panelistlanding");
		} else if (roles.contains("ROLE_ADMIN")) {
			getRedirectStrategy()
					.sendRedirect(request, response, "/administrator");
		} else {
			super.onAuthenticationSuccess(request, response, authentication);
			return;
		}
	}
}