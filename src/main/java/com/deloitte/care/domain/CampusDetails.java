package com.deloitte.care.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CAMPUS_DETAILS")
public class CampusDetails {
	
	@Id
	@Column(name="ID")
	@GeneratedValue
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	private String placementOfficer;
	private int talentPool;
	private int winPercentage;
	private String brandingEvents;
	private String competitiononCampus;
	private float avgSal;
	private String slots;

	private int offeredStudents;
	private float conversionRate;
	private int winRate;
	private int slot;
	private int sample;
	private int interns;

	private int pastvisitstoCampus;
	private int deloitteTier;
	private String serviceLineVisiting;

	private String aluminiDetails;
	private String otherDetails;

	private String ourExperience;
	
	//imran
	private String ppd;
		
	
	public String getPpd() {
			return ppd;
	}

	public void setPpd(String ppd) {
			this.ppd = ppd;
	}

	public String getPlacementOfficer() {
		return placementOfficer;
	}

	public void setPlacementOfficer(String placementOfficer) {
		this.placementOfficer = placementOfficer;
	}

	public int getTalentPool() {
		return talentPool;
	}

	public void setTalentPool(int talentPool) {
		this.talentPool = talentPool;
	}

	public int getWinPercentage() {
		return winPercentage;
	}

	public void setWinPercentage(int winPercentage) {
		this.winPercentage = winPercentage;
	}

	public String getBrandingEvents() {
		return brandingEvents;
	}

	public void setBrandingEvents(String brandingEvents) {
		this.brandingEvents = brandingEvents;
	}

	public String getCompetitiononCampus() {
		return competitiononCampus;
	}

	public void setCompetitiononCampus(String competitiononCampus) {
		this.competitiononCampus = competitiononCampus;
	}

	public float getAvgSal() {
		return avgSal;
	}

	public void setAvgSal(float avgSal) {
		this.avgSal = avgSal;
	}

	public String getSlots() {
		return slots;
	}

	public void setSlots(String slots) {
		this.slots = slots;
	}

	public int getOfferedStudents() {
		return offeredStudents;
	}

	public void setOfferedStudents(int offeredStudents) {
		this.offeredStudents = offeredStudents;
	}

	public float getConversionRate() {
		return conversionRate;
	}

	public void setConversionRate(float conversionRate) {
		this.conversionRate = conversionRate;
	}

	public int getWinRate() {
		return winRate;
	}

	public void setWinRate(int winRate) {
		this.winRate = winRate;
	}

	public int getSlot() {
		return slot;
	}

	public void setSlot(int slot) {
		this.slot = slot;
	}

	public int getSample() {
		return sample;
	}

	public void setSample(int sample) {
		this.sample = sample;
	}

	public int getInterns() {
		return interns;
	}

	public void setInterns(int interns) {
		this.interns = interns;
	}

	public int getPastvisitstoCampus() {
		return pastvisitstoCampus;
	}

	public void setPastvisitstoCampus(int pastvisitstoCampus) {
		this.pastvisitstoCampus = pastvisitstoCampus;
	}

	public int getDeloitteTier() {
		return deloitteTier;
	}

	public void setDeloitteTier(int deloitteTier) {
		this.deloitteTier = deloitteTier;
	}

	public String getServiceLineVisiting() {
		return serviceLineVisiting;
	}

	public void setServiceLineVisiting(String serviceLineVisiting) {
		this.serviceLineVisiting = serviceLineVisiting;
	}

	public String getAluminiDetails() {
		return aluminiDetails;
	}

	public void setAluminiDetails(String aluminiDetails) {
		this.aluminiDetails = aluminiDetails;
	}

	public String getOtherDetails() {
		return otherDetails;
	}

	public void setOtherDetails(String otherDetails) {
		this.otherDetails = otherDetails;
	}

	public String getOurExperience() {
		return ourExperience;
	}

	public void setOurExperience(String ourExperience) {
		this.ourExperience = ourExperience;
	}

	@Override
	public String toString() {
		return "CampusDetails [id=" + id + ", placementOfficer="
				+ placementOfficer + ", talentPool=" + talentPool
				+ ", winPercentage=" + winPercentage + ", brandingEvents="
				+ brandingEvents + ", competitiononCampus="
				+ competitiononCampus + ", avgSal=" + avgSal + ", slots="
				+ slots + ", offeredStudents=" + offeredStudents
				+ ", conversionRate=" + conversionRate + ", winRate=" + winRate
				+ ", slot=" + slot + ", sample=" + sample + ", interns="
				+ interns + ", pastvisitstoCampus=" + pastvisitstoCampus
				+ ", deloitteTier=" + deloitteTier + ", serviceLineVisiting="
				+ serviceLineVisiting + ", aluminiDetails=" + aluminiDetails
				+ ", otherDetails=" + otherDetails + ", ourExperience="
				+ ourExperience + "]";
	}

}
