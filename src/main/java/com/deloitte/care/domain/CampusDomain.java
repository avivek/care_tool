package com.deloitte.care.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "campus")
public class CampusDomain implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "campus_id")
	private int id;

	@Column(name = "campus_name")
	private String campusName;

	@Column(name = "description")
	private String campusDesc;

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdDt", updatable = false)
	private Date createdDt;

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updatedDt", insertable = false)
	private Date updatedDt;

	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "updatedBy")
	private String updatedBy;

	
//	@OneToOne(mappedBy = "campusdomain", cascade = CascadeType.ALL)
//	private CampusDetailsDomain campusdetailsdomain;
//
//	public CampusDetailsDomain getCampusdetailsdomain() {
//		return campusdetailsdomain;
//	}
	
//
//	
//
//	public void setCampusdetailsdomain(CampusDetailsDomain campusdetailsdomain) {
//		this.campusdetailsdomain = campusdetailsdomain;
//	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCampusName() {
		return campusName;
	}

	public void setCampusName(String campusName) {
		this.campusName = campusName;
	}

	public String getCampusDesc() {
		return campusDesc;
	}

	public void setCampusDesc(String campusDesc) {
		this.campusDesc = campusDesc;
	}

}
