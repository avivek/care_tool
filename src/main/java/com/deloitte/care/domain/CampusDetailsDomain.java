package com.deloitte.care.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CAMPUS_DETAILS")
public class CampusDetailsDomain implements java.io.Serializable  {

	@Id
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "placementOfficer")
	private String placementOfficer;

	@Column(name = "talentPool")
	private int talentPool;

	@Column(name = "winPercentage")
	private int winPercentage;

	@Column(name = "brandingEvents")
	private String brandingEvents;

	@Column(name = "competitiononCampus")
	private String competitiononCampus;

	@Column(name = "avgSal")
	private float avgSal;

	@Column(name = "slots")
	private String slots;

	@Column(name = "pastvisitstoCampus")
	private int pastvisitstoCampus;

	@Column(name = "deloitteTier")
	private int deloitteTier;

	@Column(name = "serviceLineVisiting")
	private String serviceLineVisiting;

	@Column(name = "aluminiDetails")
	private String aluminiDetails;

	@Column(name = "otherDetails")
	private String otherDetails;

	@Column(name = "ourExperience")
	private String ourExperience = "experience";

	@OneToOne(fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private CampusDomain campusdomain;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "updatedBy")
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdDt", updatable = false)
	private Date createdDt;

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updatedDt", insertable = false)
	private Date updatedDt;
	
	@Column(name = "candidateFreeze")
	private String candidateFreeze;
	
	@Column(name = "panelistFreeze")
	private String panelistFreeze;
	
	@Column(name = "groupDistributionFreeze")
	private String groupDistributionFreeze;
	
	@Column(name = "consensusFreeze")
	private String consensusFreeze;
	

	@Column(name = "interviewFreeze")
	private String interviewFreeze;
	
	public String getInterviewFreeze() {
		return interviewFreeze;
	}

	public void setInterviewFreeze(String interviewFreeze) {
		this.interviewFreeze = interviewFreeze;
	}

	
	//Imran
	@Column(name = "ppd")
	private String ppd;
	
	public String getPpd() {
		return ppd;
	}

	public void setPpd(String ppd) {
		this.ppd = ppd;
	}
	public String getConsensusFreeze() {
		return consensusFreeze;
	}

	public void setConsensusFreeze(String consensusFreeze) {
		this.consensusFreeze = consensusFreeze;
	}

	public String getCandidateFreeze() {
		return candidateFreeze;
	}

	public void setCandidateFreeze(String candidateFreeze) {
		this.candidateFreeze = candidateFreeze;
	}

	public String getPanelistFreeze() {
		return panelistFreeze;
	}

	public void setPanelistFreeze(String panelistFreeze) {
		this.panelistFreeze = panelistFreeze;
	}

	public String getGroupDistributionFreeze() {
		return groupDistributionFreeze;
	}

	public void setGroupDistributionFreeze(String groupDistributionFreeze) {
		this.groupDistributionFreeze = groupDistributionFreeze;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public CampusDomain getCampusdomain() {
		return campusdomain;
	}

	public void setCampusdomain(CampusDomain campusdomain) {
		this.campusdomain = campusdomain;
	}
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPlacementOfficer() {
		return placementOfficer;
	}

	public void setPlacementOfficer(String placementOfficer) {
		this.placementOfficer = placementOfficer;
	}

	public int getTalentPool() {
		return talentPool;
	}

	public void setTalentPool(int talentPool) {
		this.talentPool = talentPool;
	}

	public int getWinPercentage() {
		return winPercentage;
	}

	public void setWinPercentage(int winPercentage) {
		this.winPercentage = winPercentage;
	}

	public String getBrandingEvents() {
		return brandingEvents;
	}

	public void setBrandingEvents(String brandingEvents) {
		this.brandingEvents = brandingEvents;
	}

	public String getCompetitiononCampus() {
		return competitiononCampus;
	}

	public void setCompetitiononCampus(String competitiononCampus) {
		this.competitiononCampus = competitiononCampus;
	}

	public float getAvgSal() {
		return avgSal;
	}

	public void setAvgSal(float avgSal) {
		this.avgSal = avgSal;
	}

	public String getSlots() {
		return slots;
	}

	public void setSlots(String slots) {
		this.slots = slots;
	}


	public int getPastvisitstoCampus() {
		return pastvisitstoCampus;
	}

	public void setPastvisitstoCampus(int pastvisitstoCampus) {
		this.pastvisitstoCampus = pastvisitstoCampus;
	}

	public int getDeloitteTier() {
		return deloitteTier;
	}

	public void setDeloitteTier(int deloitteTier) {
		this.deloitteTier = deloitteTier;
	}

	public String getServiceLineVisiting() {
		return serviceLineVisiting;
	}

	public void setServiceLineVisiting(String serviceLineVisiting) {
		this.serviceLineVisiting = serviceLineVisiting;
	}

	public String getAluminiDetails() {
		return aluminiDetails;
	}

	public void setAluminiDetails(String aluminiDetails) {
		this.aluminiDetails = aluminiDetails;
	}

	public String getOtherDetails() {
		return otherDetails;
	}

	public void setOtherDetails(String otherDetails) {
		this.otherDetails = otherDetails;
	}

	public String getOurExperience() {
		return ourExperience;
	}

	public void setOurExperience(String ourExperience) {
		this.ourExperience = ourExperience;
	}

}
