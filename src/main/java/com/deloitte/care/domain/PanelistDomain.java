package com.deloitte.care.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "panelist")
public class PanelistDomain implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "panelist_name")
	private String panelist_name;


	@Column(name = "panelist_email_id")
	private String panelist_email_id;
	
	@Column(name = "designation")
	private String designation;
	
	@Column(name = "serviceLine")
	private String serviceLine;
	
	@ManyToOne
	@JoinColumn(name="USER_ID")
	private UserDomain userDomain;

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdDt", updatable = false)
	private Date createdDt;

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updatedDt", insertable = false)
	private Date updatedDt;

	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "updatedBy")
	private String updatedBy;
	
	@OneToMany(fetch = FetchType.EAGER , targetEntity=PanelistMappingDomain.class, cascade=CascadeType.ALL)
	@JoinColumn(name="panelist_id")
	private List<PanelistMappingDomain> mappings;

	public List<PanelistMappingDomain> getMappings() {
		return mappings;
	}

	public void setMappings(List<PanelistMappingDomain> mappings) {
		this.mappings = mappings;
	}

	@Override
    public boolean equals(Object obj) {
        return ((obj instanceof PanelistDomain) && (id == ((PanelistDomain) obj).id)   );
    }

    @Override
    public int hashCode() {
        return id;
    }
    
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getPanelist_name() {
		return panelist_name;
	}

	public void setPanelist_name(String panelist_name) {
		this.panelist_name = panelist_name;
	}

	public String getPanelist_email_id() {
		return panelist_email_id;
	}

	public void setPanelist_email_id(String panelist_email_id) {
		this.panelist_email_id = panelist_email_id;
	}
	
	public UserDomain getUserDomain() {
		return userDomain;
	}

	public void setUserDomain(UserDomain userDomain) {
		this.userDomain = userDomain;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getServiceLine() {
		return serviceLine;
	}

	public void setServiceLine(String serviceLine) {
		this.serviceLine = serviceLine;
	}

}
