package com.deloitte.care.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "interview_details")
public class InterviewDetailsDomain implements java.io.Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "interviewName")
	private String interviewName;

	@Column(name = "interviewStartDate")
	private Date interviewStartDate;

	@Column(name = "interviewEndDate")
	private Date interviewEndDate;
	
	@Column(name = "position")
	private String position;
	@Column(name = "service")
	private String service;
	@Column(name = "communication")
	private String communication;
	@Column(name = "management")
	private String management;
	@Column(name = "leadership")
	private String leadership;
	@Column(name = "qualification")
	private String qualification;
	@Column(name = "motivators")
	private String motivators;
	@Column(name = "objectives")
	private String objectives;
	@Column(name = "shift")
	private char shift;
	@Column(name = "coursecompletiondate")
	private Date coursecompletiondate;
	@Column(name = "applied")
	private char applied;
	@Column(name = "comments")
	private String comments;
	@ManyToOne
	@JoinColumn(name="campus_id")
	private CampusDomain campusdomain;
	
	@ManyToOne
	@JoinColumn(name="candidate_id")
	private CandidateDetailsDomain candidateDetailsDomain;

	@ManyToOne
	@JoinColumn(name="panelist_id")
	private PanelistDomain panelistDomain;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdDt", updatable = false)
	private Date createdDt;

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updatedDt", insertable = false)
	private Date updatedDt;

	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "updatedBy")
	private String updatedBy;
	
	@Column(name = "interviewerDecision")
	private String interviewerDecision;

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getInterviewName() {
		return interviewName;
	}

	public void setInterviewName(String interviewName) {
		this.interviewName = interviewName;
	}

	public Date getInterviewStartDate() {
		return interviewStartDate;
	}

	public void setInterviewStartDate(Date interviewStartDate) {
		this.interviewStartDate = interviewStartDate;
	}

	public Date getInterviewEndDate() {
		return interviewEndDate;
	}

	public void setInterviewEndDate(Date interviewEndDate) {
		this.interviewEndDate = interviewEndDate;
	}

	public CampusDomain getCampusdomain() {
		return campusdomain;
	}

	public void setCampusdomain(CampusDomain campusdomain) {
		this.campusdomain = campusdomain;
	}

	public CandidateDetailsDomain getCandidateDetailsDomain() {
		return candidateDetailsDomain;
	}

	public void setCandidateDetailsDomain(
			CandidateDetailsDomain candidateDetailsDomain) {
		this.candidateDetailsDomain = candidateDetailsDomain;
	}

	public PanelistDomain getPanelistDomain() {
		return panelistDomain;
	}

	public void setPanelistDomain(PanelistDomain panelistDomain) {
		this.panelistDomain = panelistDomain;
	}

	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getCommunication() {
		return communication;
	}
	public void setCommunication(String communication) {
		this.communication = communication;
	}
	public String getManagement() {
		return management;
	}
	public void setManagement(String management) {
		this.management = management;
	}
	public String getLeadership() {
		return leadership;
	}
	public void setLeadership(String leadership) {
		this.leadership = leadership;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getMotivators() {
		return motivators;
	}
	public void setMotivators(String motivators) {
		this.motivators = motivators;
	}
	public String getObjectives() {
		return objectives;
	}
	public void setObjectives(String objectives) {
		this.objectives = objectives;
	}
	public char getShift() {
		return shift;
	}
	public void setShift(char shift) {
		this.shift = shift;
	}
	public Date getCoursecompletiondate() {
		return coursecompletiondate;
	}
	public void setCoursecompletiondate(Date coursecompletiondate) {
		this.coursecompletiondate = coursecompletiondate;
	}
	public char getApplied() {
		return applied;
	}
	public void setApplied(char applied) {
		this.applied = applied;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getInterviewerDecision() {
		return interviewerDecision;
	}
	public void setInterviewerDecision(String interviewerDecision) {
		this.interviewerDecision = interviewerDecision;
	}
}
