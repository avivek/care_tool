package com.deloitte.care.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "campus_hiring_details")
public class CampusHiringDetailsDomain implements java.io.Serializable  {
	
	@Id
	@Column(name = "id")
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "offeredStudents")
	private int offeredStudents;

	@Column(name = "conversionRate")
	private float conversionRate;

	@Column(name = "winRate")
	private int winRate;

	@Column(name = "slots")
	private int slot;

	@Column(name = "sample")
	private int sample;

	@Column(name = "interns")
	private int interns;
	
	@Column(name = "yearStat")
	private Integer year;

	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "updatedBy")
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdDt", updatable = false)
	private Date createdDt;

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updatedDt", insertable = false)
	private Date updatedDt;

	public int getOfferedStudents() {
		return offeredStudents;
	}

	public void setOfferedStudents(int offeredStudents) {
		this.offeredStudents = offeredStudents;
	}

	public float getConversionRate() {
		return conversionRate;
	}

	public void setConversionRate(float conversionRate) {
		this.conversionRate = conversionRate;
	}

	public int getWinRate() {
		return winRate;
	}

	public void setWinRate(int winRate) {
		this.winRate = winRate;
	}

	public int getSlot() {
		return slot;
	}

	public void setSlot(int slot) {
		this.slot = slot;
	}

	public int getSample() {
		return sample;
	}

	public void setSample(int sample) {
		this.sample = sample;
	}

	public int getInterns() {
		return interns;
	}

	public void setInterns(int interns) {
		this.interns = interns;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}
}
