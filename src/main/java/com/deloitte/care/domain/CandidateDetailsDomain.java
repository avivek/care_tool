package com.deloitte.care.domain;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "CANDIDATE_DETAILS")
public class CandidateDetailsDomain implements java.io.Serializable{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "candidate_id")
	private Integer candidateId;
	@ManyToOne
	@JoinColumn(name="campus_id")
	private CampusDomain campusdomain;
	@Column(name = "amcatID")
	private BigInteger amcatID;	 //Unique
	@Column(name = "firstName")
	private String firstName;
	@Column(name = "lastName")
	private String lastName;
	@Column(name = "middleName")
	private String middleName;
	@Column(name = "emailID")
	private String emailID;	 
	@Column(name = "alternateEmailID")
	private String alternateEmailID;
	@Column(name = "mobileNumber")
	private BigInteger mobileNumber;
	@Column(name = "landLine")
	private BigInteger landLine;
	@Column(name = "fatherMobileNo")
	private BigInteger fatherMobileNo;
	@Column(name = "dob")
	private Date dob;
	@Column(name = "gender")
	private String gender;
	@Column(name = "pAddress")
	private String pAddress;
	@Column(name = "pCity")
	private String pCity;
	@Column(name = "pState")
	private String pState;
	@Column(name = "pZipCode")
	private Integer pZipCode;
	@Column(name = "cAddress")
	private String cAddress;
	@Column(name = "cCity")
	private String cCity;
	@Column(name = "cState")
	private String cState;
	@Column(name = "cZipCode")
	private Integer cZipCode;
	@Column(name = "boardName10")
	private String boardName10;
	@Column(name = "passingYear10")
	private Integer passingYear10;
	@Column(name = "percentage10")
	private BigDecimal percentage10;
	@Column(name = "boardName12")
	private String boardName12;	
	@Column(name = "passingYea12")
	private Integer passingYea12;
	@Column(name = "percentage12")
	private BigDecimal percentage12;
	@Column(name = "completionYear")
	private Integer completionYear;
	@Column(name = "collegePecentage")
	private BigDecimal collegePecentage;
	@Column(name = "collegName")
	private String collegName;
	@Column(name = "collegeCity")
	private String collegeCity;
	@Column(name = "collegeState")
	private String collegeState;
	@Column(name = "degreeName")
	private String degreeName;
	@Column(name = "subjectName")
	private String subjectName;
	@Column(name = "gradUniversityRollNo")
	private String gradUniversityRollNo;
	@Column(name = "eduMode")
	private String eduMode;	
	@Column(name = "backPaper")
	private Integer backPaper;
	@Column(name = "affiliatedTo")
	private String affiliatedTo;
	@Column(name = "testAttemptTime")
	private String testAttemptTime;
	@Column(name = "englishScore")
	private BigDecimal englishScore;
	@Column(name = "englishPercentile")
	private BigDecimal englishPercentile;
	@Column(name = "vocabulary")
	private BigDecimal vocabulary;
	@Column(name = "grammar")
	private BigDecimal grammar;
	@Column(name = "comprehension")
	private BigDecimal comprehension;
	@Column(name = "quantitativeAbilityScore")
	private BigDecimal quantitativeAbilityScore;
	@Column(name = "quantitativeAbilityPercentage")
	private BigDecimal quantitativeAbilityPercentage;
	@Column(name = "basicMathematics")
	private BigDecimal basicMathematics;
	@Column(name = "engineeringMathematics")
	private BigDecimal engineeringMathematics;
	@Column(name = "appliedMathematics")
	private BigDecimal appliedMathematics;
	@Column(name = "logicalAbilityScore")
	private BigDecimal logicalAbilityScore;
	@Column(name = "logicalAbilityPercentage")
	private BigDecimal logicalAbilityPercentage;
	@Column(name = "inductiveReasoning")
	private BigDecimal inductiveReasoning;
	@Column(name = "deductiveReasoning")
	private BigDecimal deductiveReasoning;
	@Column(name = "abductiveReasoning")
	private BigDecimal abductiveReasoning;
	@Column(name = "parentTitle")
	private String parentTitle;
	@Column(name = "parentName")
	private String parentName;
	@Column(name = "pref1Function")
	private String pref1Function;
	@Column(name = "prefOfGurgaon1")
	private String prefOfGurgaon1;
	@Column(name = "prefOfHyderabad1")
	private String prefOfHyderabad1;
	@Column(name = "prefOfMumbai1")
	private String prefOfMumbai1;
	@Column(name = "prefOfBengaluru")
	private String prefOfBengaluru;
	@Column(name = "pref2Function")
	private String pref2Function;
	@Column(name = "prefOfGurgaon2")
	private String prefOfGurgaon2;
	@Column(name = "prefOfMumbai2")
	private String prefOfMumbai2;
	@Column(name = "prefOfHyderabad2")
	private String prefOfHyderabad2;
	@Column(name = "prefOfBengaluru2")
	private String prefOfBengaluru2;
	@Column(name = "function")
	private String function;
	@Column(name = "panelists")
	private String panelists;
	@Column(name = "interviewers")
	private String interviewers;
	@Column(name = "observers")
	private String observers;
	@Column(name = "inactive")
	private Boolean inactive;
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdDt", updatable = false)
	private Date createdDt;
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updatedDt", insertable = false)
	private Date updatedDt;
	@Column(name = "createdBy")
    private String createdBy;
	@Column(name = "updatedBy")
    private String updatedBy;
	
	@Column(name = "groupDiscussionResult")
    private String groupDiscussionResult;
	@Column(name = "interviewResult")
    private String interviewResult;
	@Column(name = "consensusResult")
    private String consensusResult;
	
	@Column(name = "consensusComments")
	private String consensusComments;
	
	@Column(name = "finalizedForGd")
	private String finalizedForGd;
	
	public String getFinalizedForGd() {
		return finalizedForGd;
	}

	public void setFinalizedForGd(String finalizedForGd) {
		this.finalizedForGd = finalizedForGd;
	}

	public String getConsensusComments() {
		return consensusComments;
	}

	public void setConsensusComments(String consensusComments) {
		this.consensusComments = consensusComments;
	}

	public String getGroupDiscussionResult() {
		return groupDiscussionResult;
	}

	public void setGroupDiscussionResult(String groupDiscussionResult) {
		this.groupDiscussionResult = groupDiscussionResult;
	}

	public String getInterviewResult() {
		return interviewResult;
	}

	public void setInterviewResult(String interviewResult) {
		this.interviewResult = interviewResult;
	}

	public String getConsensusResult() {
		return consensusResult;
	}

	public void setConsensusResult(String consensusResult) {
		this.consensusResult = consensusResult;
	}

	public BigInteger getAmcatID() {
		return amcatID;
	}

	public void setAmcatID(BigInteger amcatID) {
		this.amcatID = amcatID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public String getAlternateEmailID() {
		return alternateEmailID;
	}

	public void setAlternateEmailID(String alternateEmailID) {
		this.alternateEmailID = alternateEmailID;
	}

	public BigInteger getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(BigInteger mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public BigInteger getLandLine() {
		return landLine;
	}

	public void setLandLine(BigInteger landLine) {
		this.landLine = landLine;
	}

	public BigInteger getFatherMobileNo() {
		return fatherMobileNo;
	}

	public void setFatherMobileNo(BigInteger fatherMobileNo) {
		this.fatherMobileNo = fatherMobileNo;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getpAddress() {
		return pAddress;
	}

	public void setpAddress(String pAddress) {
		this.pAddress = pAddress;
	}

	public String getpCity() {
		return pCity;
	}

	public void setpCity(String pCity) {
		this.pCity = pCity;
	}

	public String getpState() {
		return pState;
	}

	public void setpState(String pState) {
		this.pState = pState;
	}

	public Integer getpZipCode() {
		return pZipCode;
	}

	public void setpZipCode(Integer pZipCode) {
		this.pZipCode = pZipCode;
	}

	public String getcAddress() {
		return cAddress;
	}

	public void setcAddress(String cAddress) {
		this.cAddress = cAddress;
	}

	public String getcCity() {
		return cCity;
	}

	public void setcCity(String cCity) {
		this.cCity = cCity;
	}

	public String getcState() {
		return cState;
	}

	public void setcState(String cState) {
		this.cState = cState;
	}

	public Integer getcZipCode() {
		return cZipCode;
	}

	public void setcZipCode(Integer cZipCode) {
		this.cZipCode = cZipCode;
	}

	public String getBoardName10() {
		return boardName10;
	}

	public void setBoardName10(String boardName10) {
		this.boardName10 = boardName10;
	}

	public Integer getPassingYear10() {
		return passingYear10;
	}

	public void setPassingYear10(Integer passingYear10) {
		this.passingYear10 = passingYear10;
	}

	public BigDecimal getPercentage10() {
		return percentage10;
	}

	public void setPercentage10(BigDecimal percentage10) {
		this.percentage10 = percentage10;
	}

	public String getBoardName12() {
		return boardName12;
	}

	public void setBoardName12(String boardName12) {
		this.boardName12 = boardName12;
	}

	public Integer getPassingYea12() {
		return passingYea12;
	}

	public void setPassingYea12(Integer passingYea12) {
		this.passingYea12 = passingYea12;
	}

	public BigDecimal getPercentage12() {
		return percentage12;
	}

	public void setPercentage12(BigDecimal percentage12) {
		this.percentage12 = percentage12;
	}

	public Integer getCompletionYear() {
		return completionYear;
	}

	public void setCompletionYear(Integer completionYear) {
		this.completionYear = completionYear;
	}

	public BigDecimal getCollegePecentage() {
		return collegePecentage;
	}

	public void setCollegePecentage(BigDecimal collegePecentage) {
		this.collegePecentage = collegePecentage;
	}

	public String getCollegName() {
		return collegName;
	}

	public void setCollegName(String collegName) {
		this.collegName = collegName;
	}

	public String getCollegeCity() {
		return collegeCity;
	}

	public void setCollegeCity(String collegeCity) {
		this.collegeCity = collegeCity;
	}

	public String getCollegeState() {
		return collegeState;
	}

	public void setCollegeState(String collegeState) {
		this.collegeState = collegeState;
	}

	public String getDegreeName() {
		return degreeName;
	}

	public void setDegreeName(String degreeName) {
		this.degreeName = degreeName;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getGradUniversityRollNo() {
		return gradUniversityRollNo;
	}

	public void setGradUniversityRollNo(String gradUniversityRollNo) {
		this.gradUniversityRollNo = gradUniversityRollNo;
	}

	public String getEduMode() {
		return eduMode;
	}

	public void setEduMode(String eduMode) {
		this.eduMode = eduMode;
	}

	public Integer getBackPaper() {
		return backPaper;
	}

	public void setBackPaper(Integer backPaper) {
		this.backPaper = backPaper;
	}

	public String getAffiliatedTo() {
		return affiliatedTo;
	}

	public void setAffiliatedTo(String affiliatedTo) {
		this.affiliatedTo = affiliatedTo;
	}

	public String getTestAttemptTime() {
		return testAttemptTime;
	}

	public void setTestAttemptTime(String testAttemptTime) {
		this.testAttemptTime = testAttemptTime;
	}

	public BigDecimal getEnglishScore() {
		return englishScore;
	}

	public void setEnglishScore(BigDecimal englishScore) {
		this.englishScore = englishScore;
	}

	public BigDecimal getEnglishPercentile() {
		return englishPercentile;
	}

	public void setEnglishPercentile(BigDecimal englishPercentile) {
		this.englishPercentile = englishPercentile;
	}

	public BigDecimal getVocabulary() {
		return vocabulary;
	}

	public void setVocabulary(BigDecimal vocabulary) {
		this.vocabulary = vocabulary;
	}

	public BigDecimal getGrammar() {
		return grammar;
	}

	public void setGrammar(BigDecimal grammar) {
		this.grammar = grammar;
	}

	public BigDecimal getComprehension() {
		return comprehension;
	}

	public void setComprehension(BigDecimal comprehension) {
		this.comprehension = comprehension;
	}

	public BigDecimal getQuantitativeAbilityScore() {
		return quantitativeAbilityScore;
	}

	public void setQuantitativeAbilityScore(BigDecimal quantitativeAbilityScore) {
		this.quantitativeAbilityScore = quantitativeAbilityScore;
	}

	public BigDecimal getQuantitativeAbilityPercentage() {
		return quantitativeAbilityPercentage;
	}

	public void setQuantitativeAbilityPercentage(
			BigDecimal quantitativeAbilityPercentage) {
		this.quantitativeAbilityPercentage = quantitativeAbilityPercentage;
	}

	public BigDecimal getBasicMathematics() {
		return basicMathematics;
	}

	public void setBasicMathematics(BigDecimal basicMathematics) {
		this.basicMathematics = basicMathematics;
	}

	public BigDecimal getEngineeringMathematics() {
		return engineeringMathematics;
	}

	public void setEngineeringMathematics(BigDecimal engineeringMathematics) {
		this.engineeringMathematics = engineeringMathematics;
	}

	public BigDecimal getAppliedMathematics() {
		return appliedMathematics;
	}

	public void setAppliedMathematics(BigDecimal appliedMathematics) {
		this.appliedMathematics = appliedMathematics;
	}

	public BigDecimal getLogicalAbilityScore() {
		return logicalAbilityScore;
	}

	public void setLogicalAbilityScore(BigDecimal logicalAbilityScore) {
		this.logicalAbilityScore = logicalAbilityScore;
	}

	public BigDecimal getLogicalAbilityPercentage() {
		return logicalAbilityPercentage;
	}

	public void setLogicalAbilityPercentage(BigDecimal logicalAbilityPercentage) {
		this.logicalAbilityPercentage = logicalAbilityPercentage;
	}

	public BigDecimal getInductiveReasoning() {
		return inductiveReasoning;
	}

	public void setInductiveReasoning(BigDecimal inductiveReasoning) {
		this.inductiveReasoning = inductiveReasoning;
	}

	public BigDecimal getDeductiveReasoning() {
		return deductiveReasoning;
	}

	public void setDeductiveReasoning(BigDecimal deductiveReasoning) {
		this.deductiveReasoning = deductiveReasoning;
	}

	public BigDecimal getAbductiveReasoning() {
		return abductiveReasoning;
	}

	public void setAbductiveReasoning(BigDecimal abductiveReasoning) {
		this.abductiveReasoning = abductiveReasoning;
	}

	public String getParentTitle() {
		return parentTitle;
	}

	public void setParentTitle(String parentTitle) {
		this.parentTitle = parentTitle;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getPref1Function() {
		return pref1Function;
	}

	public void setPref1Function(String pref1Function) {
		this.pref1Function = pref1Function;
	}

	public String getPrefOfGurgaon1() {
		return prefOfGurgaon1;
	}

	public void setPrefOfGurgaon1(String prefOfGurgaon1) {
		this.prefOfGurgaon1 = prefOfGurgaon1;
	}

	public String getPrefOfHyderabad1() {
		return prefOfHyderabad1;
	}

	public void setPrefOfHyderabad1(String prefOfHyderabad1) {
		this.prefOfHyderabad1 = prefOfHyderabad1;
	}

	public String getPrefOfMumbai1() {
		return prefOfMumbai1;
	}

	public void setPrefOfMumbai1(String prefOfMumbai1) {
		this.prefOfMumbai1 = prefOfMumbai1;
	}

	public String getPrefOfBengaluru() {
		return prefOfBengaluru;
	}

	public void setPrefOfBengaluru(String prefOfBengaluru) {
		this.prefOfBengaluru = prefOfBengaluru;
	}

	public String getPref2Function() {
		return pref2Function;
	}

	public void setPref2Function(String pref2Function) {
		this.pref2Function = pref2Function;
	}

	public String getPrefOfGurgaon2() {
		return prefOfGurgaon2;
	}

	public void setPrefOfGurgaon2(String prefOfGurgaon2) {
		this.prefOfGurgaon2 = prefOfGurgaon2;
	}

	public String getPrefOfMumbai2() {
		return prefOfMumbai2;
	}

	public void setPrefOfMumbai2(String prefOfMumbai2) {
		this.prefOfMumbai2 = prefOfMumbai2;
	}

	public String getPrefOfHyderabad2() {
		return prefOfHyderabad2;
	}

	public void setPrefOfHyderabad2(String prefOfHyderabad2) {
		this.prefOfHyderabad2 = prefOfHyderabad2;
	}

	public String getPrefOfBengaluru2() {
		return prefOfBengaluru2;
	}

	public void setPrefOfBengaluru2(String prefOfBengaluru2) {
		this.prefOfBengaluru2 = prefOfBengaluru2;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getPanelists() {
		return panelists;
	}

	public void setPanelists(String panelists) {
		this.panelists = panelists;
	}

	public String getInterviewers() {
		return interviewers;
	}

	public void setInterviewers(String interviewers) {
		this.interviewers = interviewers;
	}

	public String getObservers() {
		return observers;
	}

	public void setObservers(String observers) {
		this.observers = observers;
	}

	public Boolean getInactive() {
		return inactive;
	}

	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(Integer candidateId) {
		this.candidateId = candidateId;
	}

	public CampusDomain getCampusdomain() {
		return campusdomain;
	}

	public void setCampusdomain(CampusDomain campusdomain) {
		this.campusdomain = campusdomain;
	}

	@Override
	public String toString() {
		return "CandidateDetailsDomain [candidateId=" + candidateId
				+ ", campusdomain=" + campusdomain + ", amcatID=" + amcatID
				+ ", firstName=" + firstName + ", lastName=" + lastName
				+ ", middleName=" + middleName + ", emailID=" + emailID
				+ ", alternateEmailID=" + alternateEmailID + ", mobileNumber="
				+ mobileNumber + ", landLine=" + landLine + ", fatherMobileNo="
				+ fatherMobileNo + ", dob=" + dob + ", gender=" + gender
				+ ", pAddress=" + pAddress + ", pCity=" + pCity + ", pState="
				+ pState + ", pZipCode=" + pZipCode + ", cAddress=" + cAddress
				+ ", cCity=" + cCity + ", cState=" + cState + ", cZipCode="
				+ cZipCode + ", boardName10=" + boardName10
				+ ", passingYear10=" + passingYear10 + ", percentage10="
				+ percentage10 + ", boardName12=" + boardName12
				+ ", passingYea12=" + passingYea12 + ", percentage12="
				+ percentage12 + ", completionYear=" + completionYear
				+ ", collegePecentage=" + collegePecentage + ", collegName="
				+ collegName + ", collegeCity=" + collegeCity
				+ ", collegeState=" + collegeState + ", degreeName="
				+ degreeName + ", subjectName=" + subjectName
				+ ", gradUniversityRollNo=" + gradUniversityRollNo
				+ ", eduMode=" + eduMode + ", backPaper=" + backPaper
				+ ", affiliatedTo=" + affiliatedTo + ", testAttemptTime="
				+ testAttemptTime + ", englishScore=" + englishScore
				+ ", englishPercentile=" + englishPercentile + ", vocabulary="
				+ vocabulary + ", grammar=" + grammar + ", comprehension="
				+ comprehension + ", quantitativeAbilityScore="
				+ quantitativeAbilityScore + ", quantitativeAbilityPercentage="
				+ quantitativeAbilityPercentage + ", basicMathematics="
				+ basicMathematics + ", engineeringMathematics="
				+ engineeringMathematics + ", appliedMathematics="
				+ appliedMathematics + ", logicalAbilityScore="
				+ logicalAbilityScore + ", logicalAbilityPercentage="
				+ logicalAbilityPercentage + ", inductiveReasoning="
				+ inductiveReasoning + ", deductiveReasoning="
				+ deductiveReasoning + ", abductiveReasoning="
				+ abductiveReasoning + ", parentTitle=" + parentTitle
				+ ", parentName=" + parentName + ", pref1Function="
				+ pref1Function + ", prefOfGurgaon1=" + prefOfGurgaon1
				+ ", prefOfHyderabad1=" + prefOfHyderabad1 + ", prefOfMumbai1="
				+ prefOfMumbai1 + ", prefOfBengaluru=" + prefOfBengaluru
				+ ", pref2Function=" + pref2Function + ", prefOfGurgaon2="
				+ prefOfGurgaon2 + ", prefOfMumbai2=" + prefOfMumbai2
				+ ", prefOfHyderabad2=" + prefOfHyderabad2
				+ ", prefOfBengaluru2=" + prefOfBengaluru2 + ", function="
				+ function + ", panelists=" + panelists + ", interviewers="
				+ interviewers + ", observers=" + observers + ", inactive="
				+ inactive + ", createdDt=" + createdDt + ", updatedDt="
				+ updatedDt + ", createdBy=" + createdBy + ", updatedBy="
				+ updatedBy + "]";
	}

	
}
