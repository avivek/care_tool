package com.deloitte.care.controller;

//import org.dozer.DozerBeanMapper;
//import org.dozer.Mapper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.domain.PanelistDomain;
import com.deloitte.care.domain.PanelistMappingDomain;
import com.deloitte.care.exception.CareException;
import com.deloitte.care.service.CampusDetailsService;
import com.deloitte.care.service.PanelistService;
import com.deloitte.care.util.DataTablesData;
import com.google.gson.Gson;

@Controller
@RequestMapping("/panelist")
public class PanelistController {
	
	static final Logger logger = Logger.getLogger(PanelistController.class);
	Gson gson= new Gson();
	@Autowired
	private PanelistService panelistService;
	
	
	@Autowired
	private CampusDetailsService campusDetailsService;
	
	@RequestMapping(value = "/finalize/{campusId}", method = RequestMethod.POST)
	@ResponseBody
	public void panelistFinalize(@PathVariable Integer campusId, HttpServletRequest request, HttpServletResponse response) throws CareException, JSONException{
		
		String jsonString = request.getParameter("jsondata") ;

		JSONArray jsonArray = new JSONArray(jsonString);
		
		for (int i =0 ; i < jsonArray.length(); i++){
			
			/*PanelistMappingDomain pmd = new PanelistMappingDomain();
			pmd.setCampus_id(0);
			pmd.setCreatedDt(null);
			pmd.setCreatedDt(null);
			pmd.setPanelist_id(0);
			panelistService.addPanelistMapping(pmd);*/
		}
		
		CampusDetailsDomain tempCampusdomain = campusDetailsService.getCampusDetails(campusId);
		tempCampusdomain.setPanelistFreeze("Y");
		campusDetailsService.saveOrUpdateCampusDetails(tempCampusdomain);
		System.out.println(jsonString);
		

	}
	
	@RequestMapping(value = "/addPanelistToCampus", method = RequestMethod.POST)
	public @ResponseBody String addPanelist(HttpServletRequest request, HttpServletResponse response) throws CareException{
		
		int campus_id = Integer.parseInt(request.getParameter("campus_id"));
		int panelist_id = Integer.parseInt(request.getParameter("panelist_id"));
		PanelistMappingDomain pmd = new PanelistMappingDomain();
		pmd.setCampus_id(campus_id);
		pmd.setPanelist_id(panelist_id);
		pmd.setBatch(Calendar.getInstance().get(Calendar.YEAR));
		pmd.setCreatedDt(new Date(System.currentTimeMillis()));
		pmd.setUpdatedDt(new Date(System.currentTimeMillis()));
		panelistService.addPanelistMapping(pmd);
		return "success";
		
	}
	
	@RequestMapping(value = "/deletePanelistFromCampus", method = RequestMethod.POST)
	public @ResponseBody String deletePanelist(HttpServletRequest request, HttpServletResponse response) throws CareException{
		
		int campus_id = Integer.parseInt(request.getParameter("campus_id"));
		int panelist_id = Integer.parseInt(request.getParameter("panelist_id"));
		PanelistMappingDomain pmd = new PanelistMappingDomain();
		pmd.setCampus_id(campus_id);
		pmd.setPanelist_id(panelist_id);
		panelistService.deletePanelistMapping(pmd);
		return "success";
		
	}
	
	@RequestMapping(value = "/availablePanelist/{campusID}", method = RequestMethod.GET)
	public @ResponseBody String availablePanelist(@PathVariable Integer campusID, HttpSession session) throws CareException{
		
		List<PanelistDomain> listofPanelist = new ArrayList<PanelistDomain>();
		
		listofPanelist = panelistService.listPanelist();
		System.out.println("Size of the list before removal" + listofPanelist.size());
		listofPanelist.removeAll(panelistService.listCampusPanelistDetails(campusID)) ;
		System.out.println("Size of the list after removal" + listofPanelist.size());
		
		DataTablesData gridData = new DataTablesData(listofPanelist);
		String availablePanelist = gson.toJson(gridData);
		return availablePanelist;
		
	}
	
	@RequestMapping(value = "/sendNotificationMail", method = RequestMethod.POST)
	public @ResponseBody String sendNotification(HttpServletRequest request, HttpServletResponse response) throws CareException{
		
		int campus_id = Integer.parseInt(request.getParameter("campus_id"));
		/*System.out.println(campus_id);
		System.out.println(request.getParameter("txt"));
		System.out.println(request.getParameter("txtmsg"));
		System.out.println(request.getParameter("emailId"));*/
		
		Properties props = new Properties();
	    props.put("mail.smtp.host", "smtp.us.deloitte.com");
	    props.put("mail.from", "srseetharam@deloitte.com");
	    props.put("mail.smtp.port", "25");
	    Session session = Session.getInstance(props, null);
	    String subject = request.getParameter("txt");
	    String mailBody = request.getParameter("txtmsg");
	    String toList = "srseetharam@deloitte.com,"+request.getParameter("emailId");
	    try
	    {
	      MimeMessage msg = new MimeMessage(session);
	      msg.setFrom();
	      msg.setRecipients(Message.RecipientType.TO, toList);
	      msg.setSubject(subject);
	      msg.setSentDate(new Date());
	      msg.setText(mailBody);
	      Transport.send(msg);
	    } catch (MessagingException mex) {
	      System.out.println("send failed, exception: " + mex);
	    }
		
		return "success";
		
	}	
}
