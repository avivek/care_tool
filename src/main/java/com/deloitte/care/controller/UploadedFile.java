package com.deloitte.care.controller;

import org.springframework.web.multipart.MultipartFile;  

public class UploadedFile {  
  
 private MultipartFile myfile;  
  
 public MultipartFile getFile() {  
  return myfile;  
 }  
  
 public void setFile(MultipartFile myfile) {  
  this.myfile = myfile;  
 }  
} 
