package com.deloitte.care.controller;

import java.util.List;

import javax.annotation.Resource;

import com.deloitte.care.exception.CareException;
import com.deloitte.care.form.CampusDetails;
import com.deloitte.care.form.Person;
import com.deloitte.care.service.CampusDetailsService;
import com.deloitte.care.service.PersonService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles and retrieves person request
 */
@Controller
@RequestMapping("/main")
public class MainController {
	
	@Autowired  
    private CampusDetailsService campusService;  
	//MapperFactory mapperFactory ;//new DefaultMapperFactory.Builder().build();

	/** ########## NEW METHODS */

	/**
	 * Handles and retrieves all persons and show it in a JSP page
	 * 
	 * @return the name of the JSP page
	 */
	@RequestMapping(value = "/campusdetails", method = RequestMethod.GET)
	public String getCampusDetails(@RequestParam(value="campusID", required=false) String campusID, Model model) {

		System.out.println("Called Get Campus Details::::"+campusID);

		// Attach persons to the Model
		model.addAttribute("campusAttribute", new CampusDetails());

		// This will resolve to /WEB-INF/jsp/personspage.jsp
		return "campusdetails";
	}
	
	
	/** ########## NEW METHODS */

	@RequestMapping(value = "/campusdetails/get/{id}", method = RequestMethod.GET)
	public ModelAndView getCampusDetailsForm(@PathVariable Integer id) throws CareException {

		System.out.println("Called Get ID Campus Details::::"+id);
		
		ModelAndView modelAndView = new ModelAndView("campusdetails");
		com.deloitte.care.domain.CampusDetailsDomain campusDetailsDO = campusService.getCampusDetails(id);
		CampusDetails campusDetails = new CampusDetails();
		
		campusDetails.setPlacementOfficer(campusDetailsDO.getPlacementOfficer());
		campusDetails.setAvgSal(campusDetailsDO.getAvgSal());
		
        modelAndView.addObject("campusAttribute",campusDetails); 
		
		// Attach persons to the Model
		//model.addAttribute("campusAttribute", new CampusDetails());

		// This will resolve to /WEB-INF/jsp/personspage.jsp
		return modelAndView;
	}

	@RequestMapping(value = "/campusdetails/add", method = RequestMethod.POST)
	public @ResponseBody
	String addCampusDetails(
			@ModelAttribute("campusAttribute") CampusDetails campusDetails) {
		
		

		System.out.println("Received request to show all persons");

		// Attach persons to the Model
		System.out.println("test:::" + campusDetails.getPlacementOfficer());
		System.out.println("test:::" + campusDetails.getTalentPool());
		
		

		// This will resolve to /WEB-INF/jsp/personspage.jsp
		return "campusdetails";
	}

	/** ########## TO BE REMOVED AFTER FIRST EXHAUST DROP */

	protected static Logger logger = LoggerFactory
			.getLogger(MainController.class);

	@Resource(name = "personService")
	private PersonService personService;

	/**
	 * Handles and retrieves all persons and show it in a JSP page
	 * 
	 * @return the name of the JSP page
	 */
	@RequestMapping(value = "/persons", method = RequestMethod.GET)
	public String getPersons(Model model) {

		logger.debug("Received request to show all persons");

		// Retrieve all persons by delegating the call to PersonService
		List<Person> persons = personService.getAll();

		// Attach persons to the Model
		model.addAttribute("persons", persons);

		// This will resolve to /WEB-INF/jsp/personspage.jsp
		return "personspage";
	}

	/**
	 * Retrieves the add page
	 * 
	 * @return the name of the JSP page
	 */
	@RequestMapping(value = "/persons/add", method = RequestMethod.GET)
	public String getAdd(Model model) {
		logger.debug("Received request to show add page");

		// Create new Person and add to model
		// This is the formBackingOBject
		model.addAttribute("personAttribute", new Person());

		// This will resolve to /WEB-INF/jsp/addpage.jsp
		return "addpage";
	}

	/**
	 * Adds a new person by delegating the processing to PersonService. Displays
	 * a confirmation JSP page
	 * 
	 * @return the name of the JSP page
	 */
	@RequestMapping(value = "/persons/add", method = RequestMethod.POST)
	public String add(@ModelAttribute("personAttribute") Person person) {
		logger.debug("Received request to add new person");

		// The "personAttribute" model has been passed to the controller from
		// the JSP
		// We use the name "personAttribute" because the JSP uses that name

		// Call PersonService to do the actual adding
		personService.add(person);

		// This will resolve to /WEB-INF/jsp/addedpage.jsp
		return "addedpage";
	}

	/**
	 * Deletes an existing person by delegating the processing to PersonService.
	 * Displays a confirmation JSP page
	 * 
	 * @return the name of the JSP page
	 */
	@RequestMapping(value = "/persons/delete", method = RequestMethod.GET)
	public String delete(
			@RequestParam(value = "id", required = true) Integer id, Model model) {

		logger.debug("Received request to delete existing person");

		// Call PersonService to do the actual deleting
		personService.delete(id);

		// Add id reference to Model
		model.addAttribute("id", id);

		// This will resolve to /WEB-INF/jsp/deletedpage.jsp
		return "deletedpage";
	}

	/**
	 * Retrieves the edit page
	 * 
	 * @return the name of the JSP page
	 */
	@RequestMapping(value = "/persons/edit", method = RequestMethod.GET)
	public String getEdit(
			@RequestParam(value = "id", required = true) Integer id, Model model) {
		logger.debug("Received request to show edit page");

		// Retrieve existing Person and add to model
		// This is the formBackingOBject
		model.addAttribute("personAttribute", personService.get(id));

		// This will resolve to /WEB-INF/jsp/editpage.jsp
		return "editpage";
	}

	/**
	 * Edits an existing person by delegating the processing to PersonService.
	 * Displays a confirmation JSP page
	 * 
	 * @return the name of the JSP page
	 */
	@RequestMapping(value = "/persons/edit", method = RequestMethod.POST)
	public String saveEdit(@ModelAttribute("personAttribute") Person person,
			@RequestParam(value = "id", required = true) Integer id, Model model) {
		logger.debug("Received request to update person");

		// The "personAttribute" model has been passed to the controller from
		// the JSP
		// We use the name "personAttribute" because the JSP uses that name

		// We manually assign the id because we disabled it in the JSP page
		// When a field is disabled it will not be included in the
		// ModelAttribute
		person.setId(id);

		// Delegate to PersonService for editing
		personService.edit(person);

		// Add id reference to Model
		model.addAttribute("id", id);

		// This will resolve to /WEB-INF/jsp/editedpage.jsp
		return "editedpage";
	}

}
