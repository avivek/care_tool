package com.deloitte.care.controller;

//import org.dozer.DozerBeanMapper;
//import org.dozer.Mapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.domain.CampusDomain;
import com.deloitte.care.domain.CampusHiringDetailsDomain;
import com.deloitte.care.domain.CandidateDetailsDomain;
import com.deloitte.care.domain.GroupDiscussionDetailsDomain;
import com.deloitte.care.domain.InterviewDetailsDomain;
import com.deloitte.care.domain.PanelistDomain;
import com.deloitte.care.exception.CareException;
import com.deloitte.care.form.CampusDetails;
import com.deloitte.care.form.CampusHiringDetails;
import com.deloitte.care.form.PanelistDetails;
import com.deloitte.care.service.CampusDetailsService;
import com.deloitte.care.service.CampusHiringDetailsService;
import com.deloitte.care.service.CandidateDetailsService;
import com.deloitte.care.service.InterviewDetailsService;
import com.deloitte.care.service.PanelistService;
import com.deloitte.care.util.DataTablesData;
import com.google.gson.Gson;

@Controller
@RequestMapping("/consensus")
public class ConsensusController {
	
	static final Logger logger = Logger.getLogger(ConsensusController.class);
	
	@Autowired
	private CandidateDetailsService candidateDetailsService;
	
	@Autowired
	private PanelistService panelistService;
	
	@Autowired
	private InterviewDetailsService interviewDetailsservice;
	
	@RequestMapping(value = "/getConsensusEligibleCandidates/{campusId}", method = RequestMethod.GET)
	@ResponseBody
	public List<CandidateDetailsDomain> getConsensusEligibleCandidates(@PathVariable Integer campusId, HttpSession session) throws CareException{

		List<CandidateDetailsDomain> listOfCandidates = new ArrayList<CandidateDetailsDomain>();
		listOfCandidates = candidateDetailsService.listCandidateDetails(campusId , "interviewResult");
		
		return listOfCandidates;
	}
	
	/*@RequestMapping(value = "/getConsensusEligibleCandidates/downloadFinal", method = RequestMethod.GET)
	@ResponseBody
	public List<CandidateDetailsDomain> downloadFinalCandidates(@PathVariable Integer campusId, HttpSession session) throws CareException{

		
		// uses the Super CSV API to generate CSV data from the model data
        ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
                CsvPreference.STANDARD_PREFERENCE);
		
		
		return listOfCandidates;
	}*/
	
	
	
}
