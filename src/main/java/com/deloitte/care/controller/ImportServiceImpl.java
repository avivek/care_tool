package com.deloitte.care.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;

public class ImportServiceImpl implements ImportService {
	
	@Override
    public void importFile(FileBean fileBean) {

        ByteArrayInputStream bis = new ByteArrayInputStream(fileBean.getFileData().getBytes());
        Workbook workbook = null;
        try {
            if (fileBean.getFileData().getOriginalFilename().endsWith("xls")) {
                workbook = new HSSFWorkbook(bis);
            } else if (fileBean.getFileData().getOriginalFilename().endsWith("xlsx")) {
              //  workbook = new XSSFWorkbook(bis);
            } else {
                throw new IllegalArgumentException("Received file does not have a standard excel extension.");
            }

            for (Row row : workbook.getSheetAt(0)) {
               if (row.getRowNum() == 0) {
                  Iterator<Cell> cellIterator = row.cellIterator();
                  while (cellIterator.hasNext()) {
                      Cell cell = cellIterator.next();
                      System.out.println("cell::::::::::"+cell);
                      //go from cell to cell and do create sql based on the content
                  }
               }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
