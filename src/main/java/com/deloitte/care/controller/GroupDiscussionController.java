package com.deloitte.care.controller;

//import org.dozer.DozerBeanMapper;
//import org.dozer.Mapper;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.domain.CampusDomain;
import com.deloitte.care.domain.CampusHiringDetailsDomain;
import com.deloitte.care.domain.CandidateDetailsDomain;
import com.deloitte.care.domain.GroupDiscussionDetailsDomain;
import com.deloitte.care.domain.PanelistDomain;
import com.deloitte.care.exception.CareException;
import com.deloitte.care.form.CampusDetails;
import com.deloitte.care.form.CampusHiringDetails;
import com.deloitte.care.form.CandidateDetails;
import com.deloitte.care.form.DiscussionGroups;
import com.deloitte.care.form.PanelistDetails;
import com.deloitte.care.service.CampusDetailsService;
import com.deloitte.care.service.CandidateDetailsService;
import com.deloitte.care.service.GroupDiscussionDetailsService;
import com.deloitte.care.service.PanelistService;
import com.deloitte.care.util.DataTablesData;
import com.google.gson.Gson;

@Controller
@RequestMapping("/groupdiscussion")
public class GroupDiscussionController {
	
	static final Logger logger = Logger.getLogger(GroupDiscussionController.class);
	Gson gson= new Gson();
	@Autowired
	private CandidateDetailsService candidateDetailsService;
	
	@Autowired
	private PanelistService panelistService;

	@Autowired
	private GroupDiscussionDetailsService groupDiscussionDetailsService;
	
	@Autowired
	private CampusDetailsService campusDetailsService;

	MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

	
	@RequestMapping(value = "/generateGdGroups", method = RequestMethod.GET)
	public void generateDiscussionGroups(HttpServletRequest request, HttpServletResponse response) throws CareException, JSONException, IOException{
				
		System.out.println("Inside delete metthod");
		int id = Integer.parseInt(request.getParameter("id"));
		int sizeofgroup = Integer.parseInt(request.getParameter("sizeofgroup"));
		
		List <CandidateDetails> candidateList = prepareListofBean((List<CandidateDetailsDomain>) candidateDetailsService.listCandidateDetails(id , "candidateFreeze"));
		
		//Fetching the list of Panelists
		List <PanelistDetails> campusPanelistList = prepareListofCampusPanelistBean((List<PanelistDomain>) panelistService.listCampusPanelistDetails(id));
		
		
		/* Creating dummy object
		 
		 */
		List <PanelistDetails> dummyCampusPanelistList = new ArrayList <PanelistDetails> ();
		
		 PanelistDetails dummyPanelist1 = new PanelistDetails();
		 dummyPanelist1.setId(1);
		 dummyPanelist1.setPanelist_email_id("test1@CRT.com");
		 dummyPanelist1.setPanelist_name("Panelist 1");
		
		 dummyCampusPanelistList.add(dummyPanelist1);
		
		 PanelistDetails dummyPanelist2 = new PanelistDetails();
		 dummyPanelist2.setId(2);
		 dummyPanelist2.setPanelist_email_id("test2@CRT.com");
		 dummyPanelist2.setPanelist_name("Panelist 2");
		 
		 dummyCampusPanelistList.add(dummyPanelist2);
		 
		 PanelistDetails dummyPanelist3 = new PanelistDetails();
		 dummyPanelist3.setId(3);
		 dummyPanelist3.setPanelist_email_id("test3@CRT.com");
		 dummyPanelist3.setPanelist_name("Panelist 3");

		 dummyCampusPanelistList.add(dummyPanelist3);
		 
		 PanelistDetails dummyPanelist4 = new PanelistDetails();
		 dummyPanelist4.setId(4);
		 dummyPanelist4.setPanelist_email_id("test4@CRT.com");
		 dummyPanelist4.setPanelist_name("Panelist 4");

		 dummyCampusPanelistList.add(dummyPanelist4);
		  
		  
		  /*
		   * Dummy Obj Creation complete
		   * 
		   * */
		 
		
				
		int numberOfGroups = ((candidateList.size())/sizeofgroup);
		if((candidateList.size())%sizeofgroup>0){numberOfGroups++;}
		
		JSONObject jsonObject = new JSONObject();

		List <CandidateDetails> remCandList = new ArrayList<CandidateDetails>();
		List <CandidateDetails> candidateListMaleBTech = new ArrayList<CandidateDetails>();
		List <CandidateDetails> candidateListMaleMtech = new ArrayList<CandidateDetails>();
		List <CandidateDetails> candidateListFemaleBTech = new ArrayList<CandidateDetails>();
		List <CandidateDetails> candidateListFemaleMTech = new ArrayList<CandidateDetails>();
		
		ArrayList<DiscussionGroups> discussionGroups = new ArrayList<DiscussionGroups>(numberOfGroups); 
		

		String gender = "";
		String degree = "";
		
		for (CandidateDetails candidate : candidateList){

			if(candidate.getGender()!=null){
				gender = candidate.getGender();
			}
			if(candidate.getDegreeName()!=null){
				degree= candidate.getDegreeName();
			}
			if(gender.equals("Male") && degree.equals("B.Tech/B.E.")){
				candidateListMaleBTech.add(candidate);
			}
			else if(gender.equals("Female") && degree.equals("B.Tech/B.E.")){
				candidateListFemaleBTech.add(candidate);
			}
			else if(gender.equals("Male") && degree.equals("MTech")){
				candidateListMaleMtech.add(candidate);
			}
			else if(gender.equals("Female") && degree.equals("MTech")){
				candidateListFemaleMTech.add(candidate);
			}
			else{
				remCandList.add(candidate);
			}
		}
		
		ArrayList<DiscussionGroups> tempListDiscussionGroups = new ArrayList<DiscussionGroups>(); 

		//Creating GD groups
		int j=0;
		do {
		
			DiscussionGroups tempDiscGrp = new DiscussionGroups();

		List<CandidateDetails> tempCandList = new ArrayList<CandidateDetails>(sizeofgroup);
		int i=0;
		do{
			if(!candidateListMaleBTech.isEmpty()){
			tempCandList.add(candidateListMaleBTech.get(0));
			candidateListMaleBTech.remove(0);
			if(i<sizeofgroup)
			i++;
			else
			break;
			}
			
			if(!candidateListMaleMtech.isEmpty()){
			tempCandList.add(candidateListMaleMtech.get(0));
			candidateListMaleMtech.remove(0);
			if(i<sizeofgroup)
			i++;
			else
			break;
			}
			
			if(!candidateListFemaleBTech.isEmpty()){
			tempCandList.add(candidateListFemaleBTech.get(0));
			candidateListFemaleBTech.remove(0);
			if(i<sizeofgroup)
			i++;
			else
			break;
			}
			
			if(!candidateListFemaleMTech.isEmpty()){
			tempCandList.add(candidateListFemaleMTech.get(0));
			candidateListFemaleMTech.remove(0);
			if(i<sizeofgroup)
			i++;
			else
			break;	
			}
			
			if(!remCandList.isEmpty()){
			tempCandList.add(remCandList.get(0));
			remCandList.remove(0);
			if(i<sizeofgroup)
			i++;
			else
			break;	
			}
			if(remCandList.isEmpty() && candidateListFemaleBTech.isEmpty() && candidateListFemaleMTech.isEmpty() && candidateListMaleBTech.isEmpty() && candidateListMaleMtech.isEmpty()){
				i=sizeofgroup;
			}
			
		}while(i<sizeofgroup);
		
		tempDiscGrp.setCandidateDetails(tempCandList);
		tempListDiscussionGroups.add(tempDiscGrp);
		j++;
		}while(j<numberOfGroups);
		
		discussionGroups = tempListDiscussionGroups;

		jsonObject = convertDiscussionGroupsToJSONStr(discussionGroups, campusPanelistList);
		
		//jsonObject = convertDiscussionGroupsToJSONStr(discussionGroups, campusPanelistList);
			
		//jsonObject = convertDiscussionGroupsToJSONStr(discussionGroups, dummyCampusPanelistList);
		
		handleJSONResponse(response, jsonObject);
		
}

	@RequestMapping(value = "/finalize", method = RequestMethod.POST)
	@ExceptionHandler({CareException.class})
	public void finalizeGd(HttpServletRequest request, HttpServletResponse response) throws CareException, JSONException, IOException {
		/*Enter finalize code here*/
		System.out.println("Finalized Group request is"+ request);
		String jsonString = request.getParameter("jsondata") ;

		JSONArray jsonArray = new JSONArray(jsonString);
		
		List<GroupDiscussionDetailsDomain> gdGroupList = new ArrayList<GroupDiscussionDetailsDomain>();
		int campusId  = 0 ;
		
		for (int i =0 ; i < jsonArray.length(); i++){
			GroupDiscussionDetailsDomain gdGroup = null ;
			
			JSONObject temp = jsonArray.getJSONObject(i);

			String gdName = (String) temp.get("gdName");
						
			campusId = Integer.parseInt((String) temp.get("campusID"));
			CampusDomain tempCampusdomain = new CampusDomain(); 
			tempCampusdomain.setId(campusId);	
			
			JSONArray listOfCandidates = (JSONArray) temp.get("listOfCandidates");
			JSONArray listOfPanelists = (JSONArray)temp.get("listOfPanelists");
			
			for(int j=0 ; j < listOfCandidates.length(); j++){
				
				int candidateId = listOfCandidates.getInt(j);
				CandidateDetailsDomain tempCandidateDetailsDomain = new CandidateDetailsDomain();
				tempCandidateDetailsDomain.setCandidateId(candidateId);
				
			
				for(int k=0 ; k < listOfPanelists.length(); k++){
					
					int panelistId = Integer.parseInt((String) listOfPanelists.get(k));
					PanelistDomain tempPanelistDomain = new PanelistDomain();
					tempPanelistDomain.setId(panelistId);
					
					gdGroup = new GroupDiscussionDetailsDomain();
					gdGroup.setGdName(gdName);
					gdGroup.setCampusdomain(tempCampusdomain);
					gdGroup.setCandidateDetailsDomain(tempCandidateDetailsDomain);
					gdGroup.setPanelistDomain(tempPanelistDomain);
					
					gdGroup.setCreatedDt(new Date(System.currentTimeMillis()));
					gdGroup.setUpdatedDt(new Date(System.currentTimeMillis()));
					
					gdGroupList.add(gdGroup);
				}
			
			
			}
			
		}
		
		groupDiscussionDetailsService.finalize(gdGroupList);
		CampusDetailsDomain tempCampusdomain = campusDetailsService.getCampusDetails(campusId);
		tempCampusdomain.setGroupDistributionFreeze("Y");
		campusDetailsService.saveOrUpdateCampusDetails(tempCampusdomain);
		//System.out.println(jsonString);
	}


	////////////////////////////////////////////////////////////////////////
	//Private Methods
	////////////////////////////////////////////////////////////////////////
	
	private List<CandidateDetails> prepareListofBean(List<CandidateDetailsDomain> candidateDetailsDomain) {
		List<CandidateDetails> beans = null;
		if (candidateDetailsDomain != null && !candidateDetailsDomain.isEmpty()) {
			beans = new ArrayList<CandidateDetails>();
			CandidateDetails bean = null;
			for (CandidateDetailsDomain candidateDomain : candidateDetailsDomain) {
				bean = new CandidateDetails();
				MapperFacade mapper = mapperFactory.getMapperFacade();
				// Mapper mapper = new DozerBeanMapper();
				mapper.map(candidateDomain, bean);
				beans.add(bean);
			}
		}
		return beans;
	}
	
	private List<PanelistDetails> prepareListofCampusPanelistBean(List<PanelistDomain> campusPanelistsDomain) {
		List<PanelistDetails> beans = null;

		if (campusPanelistsDomain != null && !campusPanelistsDomain.isEmpty()) {
			beans = new ArrayList<PanelistDetails>();
			PanelistDetails bean = null;
			for (PanelistDomain panelistDomain : campusPanelistsDomain) {
				bean = new PanelistDetails();
				MapperFacade mapper = mapperFactory.getMapperFacade();
				// Mapper mapper = new DozerBeanMapper();
				mapper.map(panelistDomain, bean);
				beans.add(bean);
			}
		}
		return beans;
		
	}

	
	
	/**
	 * Converts object to Json type
	 * 
	 * @param discussionGroups
	 * @return
	 * @throws JSONException
	 */
	private JSONObject convertDiscussionGroupsToJSONStr(
			List<DiscussionGroups> discussionGroups, List <PanelistDetails> campusPanelistList) throws JSONException {
		// LOGGER.info("Entering convertEntitlementToJSONStr method");
		JSONArray jsonarrayListofGdGrps = new JSONArray();
		JSONArray listOfPanelistjsonarray = new JSONArray();
		JSONObject jsonObj = new JSONObject();
		if (null != discussionGroups && discussionGroups.size() > 0) 
		{
			for (DiscussionGroups discGroup : discussionGroups) 
			{
				JSONArray listOfCandjsonarray = new JSONArray();
				JSONObject gdGroupjson = new JSONObject();
				for(CandidateDetails candidate : discGroup.getCandidateDetails()){
					JSONObject candidateJson = new JSONObject();
//					String productGroupCode = (productGroupVO.getProductGroupCode() == null) ? AdminConstants.BLANK
//							: productGroupVO.getProductGroupCode();
//					String productGroupDesc = (productGroupVO.getProductGroupDesc() == null) ? AdminConstants.BLANK
//							: productGroupVO.getProductGroupDesc();
					candidateJson.put("candidateName", (candidate.getFirstName()+candidate.getLastName()));
					candidateJson.put("candidateID", (candidate.getCandidateId()));
					listOfCandjsonarray.put(candidateJson);
				}
				gdGroupjson.put("listOfCandidates", listOfCandjsonarray);
				jsonarrayListofGdGrps.put(gdGroupjson);
			}

			jsonObj.put("Success", "True");
			jsonObj.put("listofGdGroups", jsonarrayListofGdGrps);

		} 
		else 
		{
			jsonObj.put("Success", "False");
		}
		
		for(PanelistDetails panelistDetail : campusPanelistList){
				JSONObject panelistJson = new JSONObject();
				panelistJson.put("panelistName", (panelistDetail.getPanelist_name()));
				panelistJson.put("panelistEmailId", (panelistDetail.getPanelist_email_id()));
				panelistJson.put("panelistID", (panelistDetail.getId()));
				listOfPanelistjsonarray.put(panelistJson);
			}
			
		jsonObj.put("listOfPanelists", listOfPanelistjsonarray);
		
		// LOGGER.info("Entering convertEntitlementToJSONStr method");
		return jsonObj;
	}

	private void handleJSONResponse(HttpServletResponse response,
			JSONObject json) throws JSONException,
			IOException {
		// LOGGER.info("Inside handleJSONResponse method");
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.print(json);
			out.flush();
		} catch (IOException ioException) {
		}
		out.close();
	}
	
	@RequestMapping(value = "/fetchFinalized/{campusID}", method = RequestMethod.GET)
	public void fetchFinalizedGroups(@PathVariable Integer campusID, HttpServletRequest request, HttpServletResponse response) throws CareException, JSONException, IOException{

		List<GroupDiscussionDetailsDomain> listGroupDiscussionDetails = groupDiscussionDetailsService.listGroupDiscussionDetails(campusID);
/*		for (int i=0 ; i < listGroupDiscussionDetails.size() ; i++){
			listGroupDiscussionDetails.get(i).setCampusdomain(null);
			listGroupDiscussionDetails.get(i).getCandidateDetailsDomain().setCampusdomain(null);
			listGroupDiscussionDetails.get(i).getPanelistDomain().setUserDomain(null);
			listGroupDiscussionDetails.get(i).getPanelistDomain().setMappings(null);
		}
		
		DataTablesData gridData = new DataTablesData(listGroupDiscussionDetails);
		String availablePanelist = gson.toJson(gridData);
		return availablePanelist;*/
		
		JSONArray jsonarrayListofGdDetails = new JSONArray();
		JSONObject jsonObj = new JSONObject();

		Map<String , List<GroupDiscussionDetailsDomain>> groupNametoInfoMap = new HashMap<String , List<GroupDiscussionDetailsDomain>>();
		
		for (GroupDiscussionDetailsDomain groupDiscussionDetailsDomain : listGroupDiscussionDetails) 
		{
			String gdName = groupDiscussionDetailsDomain.getGdName();
			
			if(groupNametoInfoMap.get(gdName) == null){
				List<GroupDiscussionDetailsDomain> list =  new ArrayList<GroupDiscussionDetailsDomain>(); 
				list.add(groupDiscussionDetailsDomain);
				groupNametoInfoMap.put(gdName, list);
			}
			else{
				groupNametoInfoMap.get(gdName).add(groupDiscussionDetailsDomain);
			}
		}

		for (Map.Entry<String , List<GroupDiscussionDetailsDomain>> entry : groupNametoInfoMap.entrySet())
		{
			JSONArray listofCandidates = new JSONArray();
			JSONArray listofPanelist = new JSONArray();
			JSONObject groupInfo = new JSONObject();
			groupInfo.put("gdName", entry.getKey());
			for (GroupDiscussionDetailsDomain groupDiscussionDetailsDomain : entry.getValue()) 
			{
				PanelistDomain pd = groupDiscussionDetailsDomain.getPanelistDomain();
				JSONObject panelistJSON = new JSONObject();
				panelistJSON.put("panelistName", (pd.getPanelist_name()));
				panelistJSON.put("panelistID", (pd.getId()));
				listofPanelist.put(panelistJSON);

				CandidateDetailsDomain cd = groupDiscussionDetailsDomain.getCandidateDetailsDomain();
				JSONObject candidateJson = new JSONObject();
				candidateJson.put("candidateName", (cd.getFirstName()+cd.getLastName()));
				candidateJson.put("candidateID", (cd.getCandidateId()));
				listofCandidates.put(candidateJson);

			}
			groupInfo.put("listOfCandidates", listofCandidates);
			groupInfo.put("listofPanelist", listofPanelist);
			jsonarrayListofGdDetails.put(groupInfo);
		}	
			
		jsonObj.put("Success", "True");
		jsonObj.put("listofGdGroups", jsonarrayListofGdDetails);

		
		handleJSONResponse(response, jsonObj);
		
	}
}

