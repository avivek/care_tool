package com.deloitte.care.controller;

import java.security.Principal;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.deloitte.care.domain.PanelistDomain;
import com.deloitte.care.domain.UserDomain;
import com.deloitte.care.form.CampusDetails;
import com.deloitte.care.form.CandidateDetails;
import com.deloitte.care.service.PanelistService;
import com.deloitte.care.service.UserService;

@Controller
@SessionAttributes("panelistdomain")
public class LoginController {
	
	static final Logger logger = Logger.getLogger(LoginController.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PanelistService panelistService;

	@RequestMapping(value = "/recruiter", method = RequestMethod.GET)
	public String printWelcome(ModelMap model, Principal principal) {
		System.out.println("In here111:::::");
		
		String name = principal.getName();
		CampusDetails campusdetails = new CampusDetails();
		logger.info("inside the logging method");
		logger.debug("username"+name);
		
		model.addAttribute("username", name);
		model.addAttribute("campusdetails", campusdetails);
		model.addAttribute("candidatedetails", new CandidateDetails());
		model.addAttribute("message", "Spring Security Custom Form example");
		return "recruiter";
   
	}
	
	@RequestMapping(value = "/administrator", method = RequestMethod.GET)
	public String printWelcome1(ModelMap model, Principal principal) {
		System.out.println("In here111:::::");
		
		String name = principal.getName();
		CampusDetails campusdetails = new CampusDetails();
		logger.info("inside the logging method");
		logger.debug("username"+name);
		
		model.addAttribute("username", name);
		model.addAttribute("campusdetails", campusdetails);
		model.addAttribute("candidatedetails", new CandidateDetails());
		model.addAttribute("message", "Spring Security Custom Form example");
		return "administrator";
   
	}
	
	@RequestMapping(value = "/panelistlanding", method = RequestMethod.GET)
	public String printWelcome2(ModelMap model, Principal principal) {
		System.out.println("In here111:::::");
		
		String name = principal.getName();
		CampusDetails campusdetails = new CampusDetails();
				
		logger.info("inside the logging method");
		logger.debug("username"+name);
		
		UserDomain userDomain = userService.getUser(name);
		List<PanelistDomain> panelistDomainList = panelistService.getPanelistDomain(userDomain.getUser_id());
		
		model.addAttribute("username", name);
		model.addAttribute("panelistdomain", panelistDomainList.get(0));
		model.addAttribute("candidatedetails", new CandidateDetails());
		model.addAttribute("message", "Spring Security Custom Form example");
		return "panelistlanding";
   
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(ModelMap model) {
		return "login";

	}

	@RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
	public String loginerror(ModelMap model) {

		model.addAttribute("error", "true");
		return "login";

	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(ModelMap model) {

		return "login";

	}
	
	//Jquery Header and Footer
	
	public void test()
	{
		
	}
	
	@RequestMapping(value = "/header", method = RequestMethod.GET)
    public String getHeader() {
		System.out.println("In Header1");
		System.out.println("In Header1");
		System.out.println("In Header1");
     return "header";
 }
}
