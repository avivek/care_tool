package com.deloitte.care.controller;

//import org.dozer.DozerBeanMapper;
//import org.dozer.Mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.deloitte.care.domain.CampusDomain;
import com.deloitte.care.domain.CandidateDetailsDomain;
import com.deloitte.care.domain.InterviewDetailsDomain;
import com.deloitte.care.domain.PanelistDomain;
import com.deloitte.care.exception.CareException;
import com.deloitte.care.service.CandidateDetailsService;
import com.deloitte.care.service.InterviewDetailsService;
import com.deloitte.care.service.PanelistService;
import com.google.gson.Gson;

@Controller
@RequestMapping("/interview")
public class InterviewController {
	
	static final Logger logger = Logger.getLogger(InterviewController.class);
	
	@Autowired
	private CandidateDetailsService candidateDetailsService;
	
	@Autowired
	private PanelistService panelistService;
	
	@Autowired
	private InterviewDetailsService interviewDetailsservice;
	
	MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
	
	@RequestMapping(value = "/getInterviewEligibleStudents/{campusId}", method = RequestMethod.GET)
	@ResponseBody
	public List<CandidateDetailsDomain> getInterviewEligibleStudents(@PathVariable Integer campusId, HttpSession session) throws CareException{

		List<CandidateDetailsDomain> listOfCandidates = new ArrayList<CandidateDetailsDomain>();
		listOfCandidates = candidateDetailsService.listCandidateDetails(campusId , "groupDiscussionResult");
		
		return listOfCandidates;
	}
	
	/*private List<PanelistDomain> prepareListofBean(List<PanelistDomain> campuses){
		List<Campus> beans = null;
		if(campuses != null && !campuses.isEmpty()){
			beans = new ArrayList<Campus>();
			Panelist bean = null;
			for(PanelistDomain campusdomain : campuses){
				bean = new Campus();
				MapperFacade mapper = mapperFactory.getMapperFacade();
				//Mapper mapper = new DozerBeanMapper();
				mapper.map(campusdomain, bean);
				beans.add(bean);
			}
		}
		return beans;
	}
	*/
	@RequestMapping(value = "/getInterviewEligiblePanelist/{campusId}", method = RequestMethod.GET)
	@ResponseBody
	public String getInterviewEligiblePanelist(@PathVariable Integer campusId, HttpSession session) throws CareException{

		List<PanelistDomain> listOfPanelist = new ArrayList<PanelistDomain>();
		listOfPanelist = panelistService.listCampusPanelistDetails(campusId);
		
		return new Gson().toJson(listOfPanelist);
	}
	
	@RequestMapping(value = "/saveInterviewPanel", method = RequestMethod.POST)
	public void saveInterviewPanel(HttpServletRequest request, HttpServletResponse response) throws CareException, JSONException{

		System.out.println("Save Interview Panel request ");
		String jsonString = request.getParameter("jsondata") ;
		
		InterviewDetailsDomain interviewDetailsDomain = null ; 
		
		JSONArray temp = new JSONArray(jsonString);
		/*JSONArray listOfPanelist = (JSONArray) temp.get("listOfPanelist");*/
		
		JSONObject object = temp.getJSONObject(0);
		int interviewId = object.getInt("interviewId");
		int campusId = object.getInt("campusID");
		int candidateId = Integer.parseInt((String) object.get("candidateId"));
		JSONArray listOfPanelists = object.getJSONArray("listOfPanelists");
		

		for(int j=0 ; j < listOfPanelists.length(); j++){
			
			interviewDetailsDomain = new InterviewDetailsDomain();
			
			interviewDetailsDomain.setInterviewName("Interview_"+candidateId);
			interviewDetailsDomain.setCreatedDt(new Date(System.currentTimeMillis()));
			interviewDetailsDomain.setUpdatedDt(new Date(System.currentTimeMillis()));
			interviewDetailsDomain.setCreatedBy(null);
			interviewDetailsDomain.setUpdatedBy(null);
			
			CampusDomain tempCampusDomain = new CampusDomain();
			tempCampusDomain.setId(campusId);
			CandidateDetailsDomain tempCandidateDetailsDomain = new CandidateDetailsDomain();
			tempCandidateDetailsDomain.setCandidateId(candidateId);
			interviewDetailsDomain.setCampusdomain(tempCampusDomain);
			interviewDetailsDomain.setCandidateDetailsDomain(tempCandidateDetailsDomain);
			
			int panelistId = Integer.parseInt( (String) listOfPanelists.getJSONObject(j).get("panelistId"));
			PanelistDomain tempPanelistDomain = new PanelistDomain();
			tempPanelistDomain.setId(panelistId);
			interviewDetailsDomain.setPanelistDomain(tempPanelistDomain);
			interviewDetailsDomain.setCreatedDt(new Date(System.currentTimeMillis()));
			interviewDetailsDomain.setUpdatedDt(new Date(System.currentTimeMillis()));
			interviewDetailsservice.addInterviewRecord(interviewDetailsDomain);
			
		}
		
	}
	
	@RequestMapping(value = "/getElgibleCountOnCutoff", method = RequestMethod.GET)
	public @ResponseBody Integer getElgibleCountOnCutoff(HttpServletRequest request, HttpServletResponse response) {
		
		String campusid = request.getParameter("campusid");
		String cutoff = request.getParameter("cutoff");
		Integer count  = interviewDetailsservice.getElgibleCountOnCutoff(campusid , cutoff);
		return count ;
	}
	
	@RequestMapping(value = "/setInterviewResultFlag", method = RequestMethod.GET)
	public @ResponseBody String setInterviewResultFlag(HttpServletRequest request, HttpServletResponse response) {
		
		String campusid = request.getParameter("campusid");
		String cutoff = request.getParameter("cutoff");
		String freeze = request.getParameter("interviewFreeze");
		interviewDetailsservice.setInterviewResultFlag(campusid , cutoff, freeze);
		return "success" ;
	}
	
	@RequestMapping(value = "/getInterviewFreezeFlag", method = RequestMethod.GET)
	public @ResponseBody String getInterviewFreezeFlag(HttpServletRequest request, HttpServletResponse response) {
		
		String campusid = request.getParameter("campusid");
		String interviewFreeze = interviewDetailsservice.getInterviewFreezeFlag(campusid);
		return interviewFreeze ;
	}
	
	
	
	
	
	
	
	
	
	
}
