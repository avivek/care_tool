package com.deloitte.care.controller;

//import org.dozer.DozerBeanMapper;
//import org.dozer.Mapper;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.domain.CampusDomain;
import com.deloitte.care.domain.CampusHiringDetailsDomain;
import com.deloitte.care.domain.CandidateDetailsDomain;
import com.deloitte.care.exception.CareException;
import com.deloitte.care.form.CampusDetails;
import com.deloitte.care.form.CampusHiringDetails;
import com.deloitte.care.service.CampusDetailsService;
import com.deloitte.care.service.CampusHiringDetailsService;
import com.deloitte.care.service.CampusService;
import com.deloitte.care.service.CandidateDetailsService;
import com.deloitte.care.service.PanelistService;
import com.deloitte.care.util.DataTablesData;
import com.google.gson.Gson;

@Controller
@RequestMapping("/campusdetails")
public class CampusDetailsController {
	
	static final Logger logger = Logger.getLogger(CampusDetailsController.class);
	
	Gson gson= new Gson();
	@Autowired
	private CampusService campusService;
	
	@Autowired
	private CampusDetailsService campusDetailsService;
	
	@Autowired
	private CampusHiringDetailsService campusHiringDetailsService;
	
	@Autowired
	private CandidateDetailsService candidateDetailsService;
	
	@Autowired
	private PanelistService panelistService;
	
	MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
	
	@ModelAttribute("hiringYears")
    public Map<String,String> populateHiringYears() {
            Map<String,String> hiringYears = new LinkedHashMap<String,String>();
            hiringYears.put("2012", "2012");
            hiringYears.put("2013", "2013");
            hiringYears.put("2014", "2014");
            return hiringYears;
    }

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ExceptionHandler({CareException.class})
	public @ResponseBody String saveCampusDetails(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws JSONException {
		String jsonString = "["+request.getParameter("jsondata")+"]";
		JSONArray campusJson = new JSONArray(jsonString);
		CampusDomain campusdomain = new CampusDomain();	
		CampusDetailsDomain campusDetailsDomain = new CampusDetailsDomain();
		try
		{			JSONArray tempArray = campusJson.getJSONObject(0).getJSONArray("campusDetails");
						/*JSONObject tempObj = (JSONObject) tempArray.get(i);*/
						campusdomain = campusService.getCampus(Integer.parseInt(request.getParameter("campusId")));
						campusDetailsDomain.setId(Integer.parseInt(tempArray.getJSONObject(13).getString("campusId")));
						campusDetailsDomain.setCampusdomain(campusdomain);
						campusDetailsDomain.setPlacementOfficer(tempArray.getJSONObject(14).getString("plcmtofficer"));
						campusDetailsDomain.setTalentPool(Integer.parseInt(tempArray.getJSONObject(15).getString("talentpool")));
						campusDetailsDomain.setWinPercentage(Integer.parseInt(tempArray.getJSONObject(16).getString("win")));
						campusDetailsDomain.setBrandingEvents(tempArray.getJSONObject(17).getString("brandingevents"));
						campusDetailsDomain.setCompetitiononCampus(tempArray.getJSONObject(18).getString("competition"));
						campusDetailsDomain.setAvgSal(Float.parseFloat(tempArray.getJSONObject(19).getString("avgsalary")));
						campusDetailsDomain.setPastvisitstoCampus(Integer.parseInt(tempArray.getJSONObject(20).getString("pastvisits")));
						campusDetailsDomain.setServiceLineVisiting(tempArray.getJSONObject(21).getString("slvisiting"));
						campusDetailsDomain.setAluminiDetails(tempArray.getJSONObject(6).getString("alumni"));
						campusDetailsDomain.setSlots(tempArray.getJSONObject(31).getString("slots"));
						campusDetailsDomain.setDeloitteTier(Integer.parseInt(tempArray.getJSONObject(32).getString("tier")));
						campusDetailsDomain.setOurExperience(tempArray.getJSONObject(34).getString("our-exp"));
						String username = (String)session.getAttribute("username");
						campusDetailsDomain.setCreatedBy(username);
						campusDetailsDomain.setUpdatedBy(username);
						campusDetailsDomain.setCreatedDt(new Date());
						campusDetailsDomain.setUpdatedDt(new Date());
						campusDetailsDomain.setCandidateFreeze("N");
						campusDetailsDomain.setGroupDistributionFreeze("N");
						campusDetailsDomain.setPanelistFreeze("N");
						campusDetailsDomain.setConsensusFreeze("N");
						//imran
						campusDetailsDomain.setPpd(tempArray.getJSONObject(7).getString("ppd"));
						
						campusDetailsService.saveOrUpdateCampusDetails(campusDetailsDomain);	
						return "success";
											
		}
			catch(CareException ce)
			{
				ce.printStackTrace();
				return "fail";
			}

		
	}
	@SuppressWarnings("null")
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ExceptionHandler({CareException.class})
	public void getCampusForm(@PathVariable Integer id, HttpServletRequest session, HttpServletResponse response) throws JSONException {
		// LOGGER.info("Entering convertEntitlementToJSONStr method");
		JSONArray campusDetailsArray = new JSONArray();
		JSONArray campusDetailsMaster = new JSONArray();
		JSONObject campusFormFieldList = new JSONObject();
		JSONObject campusFormFieldListObject = new JSONObject();
		JSONObject campusHiringObject = new JSONObject();
		CampusDetails campusdetails = new CampusDetails();
		try {
			campusdetails = prepareCampusDetailsBean(campusDetailsService.getCampusDetails(id));
		} catch (CareException e) {
			System.out.print("Error getting campus details from DB"+e.getExceptionMsg());
		}
		String campusDetailsToString = "";
		campusDetailsToString = campusdetails.toString();
		
		if (null != campusDetailsToString && !(campusDetailsToString.isEmpty())) 
		{
			campusFormFieldList.put("candidateId",campusdetails.getId());
			campusFormFieldList.put("placementOfficer",campusdetails.getPlacementOfficer());
			campusFormFieldList.put("talentPool",campusdetails.getTalentPool());
			campusFormFieldList.put("winPercentage",campusdetails.getWinPercentage());
			campusFormFieldList.put("brandingEvents",campusdetails.getBrandingEvents());
			campusFormFieldList.put("competitiononCampus",campusdetails.getCompetitiononCampus());
			campusFormFieldList.put("avgSal",campusdetails.getAvgSal());
			campusFormFieldList.put("slots",campusdetails.getSlots());
			campusFormFieldList.put("pastvisitstoCampus",campusdetails.getPastvisitstoCampus());
			campusFormFieldList.put("deloitteTier",campusdetails.getDeloitteTier());
			campusFormFieldList.put("serviceLineVisiting",campusdetails.getServiceLineVisiting());
			campusFormFieldList.put("aluminiDetails",campusdetails.getAluminiDetails());
			campusFormFieldList.put("otherDetails",campusdetails.getOtherDetails());
			campusFormFieldList.put("ourExperience",campusdetails.getOurExperience());
			//Imran
			campusFormFieldList.put("ppd", campusdetails.getPpd());
			CampusHiringDetails campusHiringDetails = (CampusHiringDetails) campusdetails.getCampusHiringDetails();
			if(null !=campusdetails.getCampusHiringDetails() && !(campusdetails.getCampusHiringDetails().isEmpty())){
				campusHiringObject.put("offeredStudents", campusHiringDetails.getOfferedStudents());
				campusHiringObject.put("conversionRate", campusHiringDetails.getOfferedStudents());
				campusHiringObject.put("winRate", campusHiringDetails.getOfferedStudents());
				campusHiringObject.put("sample", campusHiringDetails.getOfferedStudents());
				campusHiringObject.put("interns", campusHiringDetails.getOfferedStudents());
				campusHiringObject.put("year", campusHiringDetails.getOfferedStudents());
			}
			campusFormFieldListObject.put("campusDetails",campusFormFieldList);
			campusFormFieldListObject.put("success", true);
			campusFormFieldListObject.put("campusId", id);
			campusFormFieldListObject.put("campusHiring", campusHiringObject);
			campusFormFieldListObject.put("candidateFreeze", campusdetails.getCandidateFreeze());
			campusFormFieldListObject.put("groupDistributionFreeze", campusdetails.getGroupDistributionFreeze());
			campusFormFieldListObject.put("panelistFreeze", campusdetails.getPanelistFreeze());
			
		}		
		else 
		{
			campusFormFieldListObject.put("Success", "False");
		}
		try {
			handleJSONResponse(response, campusFormFieldListObject);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	
	
	@RequestMapping(value = "/getHiringStats/", method = RequestMethod.GET)
	@ResponseBody
	public void getHtmlForHiringStats(@RequestParam("campusId") String id,@RequestParam("hireYear") String year, HttpServletRequest request, HttpServletResponse response) throws CareException, JSONException{
		CampusHiringDetails campusHiringDetails = null;
		JSONObject campusHiringObject = new JSONObject();
		JSONArray campusHiringArray = new JSONArray();
		JSONObject campusHiringMasterObject = new JSONObject();
		CampusHiringDetailsDomain campusHiringDetailsDomain = null;
		if (id != null && year!= null){
			campusHiringDetailsDomain = campusHiringDetailsService.getCampusHiringDetails(Integer.parseInt(id), Integer.parseInt(year));
			if (campusHiringDetailsDomain != null){
				campusHiringDetails = prepareCampusHiringDetailsBean(campusHiringDetailsDomain);
				campusHiringObject.put("offeredStudents", campusHiringDetails.getId());
				campusHiringObject.put("conversionRate", campusHiringDetails.getConversionRate());
				campusHiringObject.put("winRate", campusHiringDetails.getWinRate());
				campusHiringObject.put("slot", campusHiringDetails.getSlot());
				campusHiringObject.put("sample", campusHiringDetails.getSample());
				campusHiringObject.put("interns", campusHiringDetails.getInterns());
				campusHiringObject.put("year", campusHiringDetails.getYear());
				campusHiringArray.put(campusHiringObject);
				campusHiringMasterObject.put("hiringStats", campusHiringArray);
				campusHiringMasterObject.put("success",true);
			}
			else {
				campusHiringMasterObject.put("success", false);
			}
				
		}
		else
		{
			campusHiringMasterObject.put("success", false);
		}
		try {
			handleJSONResponse(response, campusHiringMasterObject);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} 
	

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView deleteEmployee(@ModelAttribute("campusdetails") CampusDetails campusdetails,BindingResult result){
		ModelAndView modelAndView = null;
		try
		{
		
		CampusHiringDetailsDomain campusHiringdetailsdomain = new CampusHiringDetailsDomain();
		campusHiringdetailsdomain.setId(campusdetails.getId());
		campusHiringDetailsService.deleteCampusHiringDetails(campusHiringdetailsdomain);
		campusDetailsService.deleteCampusDetails(prepareModel(campusdetails));
		modelAndView = new ModelAndView("campusdetails/campusDetailsAdd");
		campusdetails.setSuccess("success");
		campusdetails.setSuccessMsg("Campus Details deleted successfully!");
		modelAndView.addObject("campusdetails", campusdetails);
		}
		catch(CareException e)
		{
			modelAndView = new ModelAndView("campusdetails/campusDetailsAdd");
			campusdetails.setError("error");
			campusdetails.setErrorMsg("Error while deleting Campus Details!");
			modelAndView.addObject("campusdetails", campusdetails);
		}
		return modelAndView;
	}
	
	@RequestMapping(value = "/editCampusDetailsdisplay", method = RequestMethod.GET)
	public ModelAndView editDisplayCampusDetails(@ModelAttribute("campusdetails") CampusDetails campusdetails,
			BindingResult result, HttpSession session) throws CareException{
		
		System.out.println("in edit display::::::::"+ campusdetails.getPlacementOfficer());
		
		CampusDetails temp = new CampusDetails();
		ModelAndView modelAndView = new ModelAndView("campusdetails/campusDetailsAdd");
		temp = prepareCampusDetailsBean(campusDetailsService.getCampusDetails(campusdetails.getId()));
		modelAndView.addObject("campusdetails", temp);
		return modelAndView;
	}
	
	@RequestMapping(value = "/get_panelist_list/{id}", method = RequestMethod.GET)
	public @ResponseBody String get_panelist_list(@PathVariable Integer id, HttpSession session) throws CareException{
		
		
		DataTablesData gridData = new DataTablesData(panelistService.listCampusPanelistDetails(id));
		String panelistData = gson.toJson(gridData);
		return panelistData;
		
	}
	

	
	////////////////////////////////////////////////////////////////////////
	//Private Methods
	////////////////////////////////////////////////////////////////////////

	private CampusDetailsDomain prepareModel(CampusDetails campusdetails) {
		CampusDetailsDomain campusdetailsdomain = new CampusDetailsDomain();
		MapperFacade mapper = mapperFactory.getMapperFacade();
		// Mapper mapper = new DozerBeanMapper();
		mapper.map(campusdetails, campusdetailsdomain);
		return campusdetailsdomain;
	}

	private List<CampusDetails> prepareListofBean(
			List<CampusDetailsDomain> campusdetailsdomain) {
		List<CampusDetails> beans = null;
		if (campusdetailsdomain != null && !campusdetailsdomain.isEmpty()) {
			beans = new ArrayList<CampusDetails>();
			CampusDetails bean = null;
			for (CampusDetailsDomain campusdomain : campusdetailsdomain) {
				bean = new CampusDetails();
				MapperFacade mapper = mapperFactory.getMapperFacade();
				// Mapper mapper = new DozerBeanMapper();
				mapper.map(campusdomain, bean);
				beans.add(bean);
			}
		}
		return beans;
	}

	private CampusDetails prepareCampusDetailsBean(
			CampusDetailsDomain campusdetailsdomain) {
		CampusDetails campusdetails = new CampusDetails();
		MapperFacade mapper = mapperFactory.getMapperFacade();
		if (campusdetailsdomain != null)
			mapper.map(campusdetailsdomain, campusdetails);
		return campusdetails;
	}
	
	
	private CampusHiringDetailsDomain prepareModel(CampusHiringDetails campusHiringdetails) {
		CampusHiringDetailsDomain campusHiringdetailsdomain = new CampusHiringDetailsDomain();
		MapperFacade mapper = mapperFactory.getMapperFacade();
		// Mapper mapper = new DozerBeanMapper();
		mapper.map(campusHiringdetails, campusHiringdetailsdomain);
		return campusHiringdetailsdomain;
	}

	private List<CampusHiringDetails> prepareListofBeanCampusHiring(
			List<CampusHiringDetailsDomain> campusHiringdetailsdomain) {
		List<CampusHiringDetails> beans = null;
		if (campusHiringdetailsdomain != null && !campusHiringdetailsdomain.isEmpty()) {
			beans = new ArrayList<CampusHiringDetails>();
			CampusHiringDetails bean = null;
			for (CampusHiringDetailsDomain campusHiringdomain : campusHiringdetailsdomain) {
				bean = new CampusHiringDetails();
				MapperFacade mapper = mapperFactory.getMapperFacade();
				// Mapper mapper = new DozerBeanMapper();
				mapper.map(campusHiringdomain, bean);
				beans.add(bean);
			}
		}
		return beans;
	}

	private CampusHiringDetails prepareCampusHiringDetailsBean(
			CampusHiringDetailsDomain campusHiringdetailsdomain) {
		CampusHiringDetails campusHiringdetails = new CampusHiringDetails();
		MapperFacade mapper = mapperFactory.getMapperFacade();
		if (campusHiringdetailsdomain != null)
			mapper.map(campusHiringdetailsdomain, campusHiringdetails);
		return campusHiringdetails;
	}
	private void handleJSONResponse(HttpServletResponse response,
			JSONObject json) throws JSONException,
			IOException {
		// LOGGER.info("Inside handleJSONResponse method");
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.print(json);
			out.flush();
		} catch (IOException ioException) {
		}
		out.close();
	}
	
	@RequestMapping(value = "/finalize/{finalizeParam}/{campusId}", method = RequestMethod.POST)
	public @ResponseBody String finalizeConsensus(@PathVariable Integer campusId , @PathVariable String finalizeParam ,
			HttpServletRequest request, HttpServletResponse response) {
		
		/*String id = request.getParameter("id");
		String recruiterName = request.getParameter("recruiterName");
		String recruiterEmail = request.getParameter("recruiter_email_id");
		
		RecruiterDomain recruiterdomain = new RecruiterDomain();
		recruiterdomain.setId(Integer.parseInt(id));
		recruiterdomain.setRecruiter_name(recruiterName);
		recruiterdomain.setRecruiter_email_id(recruiterEmail);
		recruiterdomain.setCreatedDt(new Date(System.currentTimeMillis()));
		recruiterdomain.setUpdatedDt(new Date(System.currentTimeMillis()));
		recruiterService.addRecruiter(recruiterdomain);*/
		
		try {
			CampusDetailsDomain cpd = campusDetailsService.getCampusDetails(campusId);
			if ("candidateFreeze".equals(finalizeParam))
				{
				cpd.setCandidateFreeze("Y");
				/*List<CandidateDetailsDomain> cdd = candidateDetailsService.listCandidatesOnCampus(cpd);
				candidateDetailsService.updateCandidateDetails(cdd);*/
				}
			if ("panelistFreeze".equals(finalizeParam))
				cpd.setPanelistFreeze("Y");
			if ("groupDistributionFreeze".equals(finalizeParam))
				cpd.setGroupDistributionFreeze("Y");
			
			campusDetailsService.saveOrUpdateCampusDetails(cpd);
			
			
		} catch (CareException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "success";
	}
	

}
