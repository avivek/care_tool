package com.deloitte.care.controller;

public interface ImportService {

    public void importFile(FileBean fileBean);
}
