package com.deloitte.care.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.format.CellDateFormatter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.domain.CampusDomain;
import com.deloitte.care.domain.CandidateDetailsDomain;
import com.deloitte.care.exception.CareException;
import com.deloitte.care.form.CandidateDetails;
import com.deloitte.care.service.CampusDetailsService;
import com.deloitte.care.service.CampusService;
import com.deloitte.care.service.CandidateDetailsService;
import com.deloitte.care.util.DataTablesData;
import com.google.gson.Gson;

@Controller
@RequestMapping("/candidatedetails")
public class CandidateDetailsController {
	Gson gson= new Gson();
	static final Logger logger = Logger
			.getLogger(CandidateDetailsController.class);

	@Autowired
	private CandidateDetailsService candidateDetailsService;
	@Autowired
	private CampusService campusService;
	
	@Autowired
	private CampusDetailsService campusDetailsService;
	
	MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

	// @InitBinder
	// public void initBinder(WebDataBinder webDataBinder) {
	// SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	// dateFormat.setLenient(false);
	// webDataBinder.registerCustomEditor(Date.class, new
	// CustomDateEditor(dateFormat, true));
	// }

	@ModelAttribute("genderTypes")
	public Map<String, String> populateGenderTypes() {
		Map<String, String> genderTypes = new LinkedHashMap<String, String>();
		genderTypes.put("Male", "Male");
		genderTypes.put("Female", "Female");
		return genderTypes;
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				new SimpleDateFormat("MM/dd/yyyy"), true));
	}

	@RequestMapping(value = "/get/{id}", method = RequestMethod.POST)
	@ResponseBody
	public String getCandidatesFromCollege(@PathVariable Integer id)
			/*@RequestParam("page") int page, @RequestParam("rows") int rows,
			@RequestParam("sidx") String sortColumnId,
			@RequestParam("sord") String sortDirection) */{

		DataTablesData gridData = null;
		try {
		/*	System.out.println("getCandidatesFromCollege:::::::::" + id);
			System.out.println("getCandidatesFromCollege:::::::::" + page);
			System.out.println("getCandidatesFromCollege:::::::::" + rows);
			System.out.println("getCandidatesFromCollege:::::::::"
					+ sortColumnId);
			System.out.println("getCandidatesFromCollege:::::::::"
					+ sortDirection);*/
			
			CampusDetailsDomain cd = campusDetailsService.getCampusDetails(id);	
			List<CandidateDetails> candidateList=null;
			if(cd!=null && cd.getCandidateFreeze().equals("Y")){
				 candidateList =  prepareListofBean(candidateDetailsService
						.listCandidateDetails(id , "candidateFreeze"));	
			}
			else{
				 candidateList =  prepareListofBean(candidateDetailsService
						.listCandidateDetails(id , null));
			}

			
			/*if (candidateList != null) {
				int totalNumberOfPages = GridUtils.getTotalNumberOfPages(
						candidateList, rows);
				int currentPageNumber = GridUtils.getCurrentPageNumber(
						candidateList, page, rows);
				int totalNumberOfRecords = candidateList.size();
				List<CandidateDetails> pageData = GridUtils.getDataForPage(
						candidateList, page, rows);

				gridData = new JqGridData<CandidateDetails>(totalNumberOfPages,
						currentPageNumber, totalNumberOfRecords, pageData);
			} else {
				List<CandidateDetails> pageData = new ArrayList<CandidateDetails>();
				gridData = new JqGridData<CandidateDetails>(0, 0, 0, pageData);
			}*/
			gridData=new DataTablesData(candidateList);

			/**
			 * int numberOfRows = candidateList.size(); int pageNumber = 1; //
			 * This example will only every show a single page. int
			 * totalNumberOfRecords = candidateList.size(); // All in there are
			 * 8 records in our dummy data object
			 * 
			 * JqGridData gridData = new JqGridData(numberOfRows, pageNumber,
			 * totalNumberOfRecords, candidateList);
			 */
		} catch (CareException ce) {

		}
		/*int sizeOfList = gridData.getAaData().size();
		gridData.setiTotalRecords(sizeOfList);*/
		//Message code 0 = null aAData
		String testData = null ;
		
		
		if (gridData.getAaData() == null)
		{
			testData =  "{aaData:[]}";
		}
		else
		{
			testData = gson.toJson(gridData);
		}
		System.out.println("JSONIZED List is"+testData);
		return testData;
	}
	
	@RequestMapping(value = "/freezeCandidates/{campusId}", method = RequestMethod.POST)
	public @ResponseBody String finalizeConsensus(@PathVariable Integer campusId, HttpServletRequest request, HttpServletResponse response) {
		
		BigDecimal aptMin = BigDecimal.valueOf(Double.parseDouble(request.getParameter("aptMin")));	
		BigDecimal engMin = BigDecimal.valueOf(Double.parseDouble(request.getParameter("engMin")));
		BigDecimal logMin = BigDecimal.valueOf(Double.parseDouble(request.getParameter("logMin")));
		
		try {

			candidateDetailsService.updateCandidateDetailsUsingCutOff(campusId,aptMin,engMin,logMin);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "success";
	}


	
	@RequestMapping(value = "/getCanDetail/{id}", method = RequestMethod.GET)
	public ModelAndView getCandidateDetails(@PathVariable Integer id,
			ModelMap model) throws CareException {
		System.out.println("getCandidateDetails::" + id);
		CandidateDetails canDetail = prepareCandidateDetailsBean(candidateDetailsService
				.getCandidateDetails(id));
		// model.addAttribute("candidateDetails", canDetail);
		ModelAndView modelNView = new ModelAndView(
				"candidatedetails/candidateDetailsShow", "candidatedetails",
				canDetail);
		// ModelAndView modelNView = new ModelAndView();
		return modelNView;
	}
	@RequestMapping(value = "/getCanDetailForEdit/{id}", method = RequestMethod.GET)
	public ModelAndView getCandidateDetailsForEdit(@PathVariable Integer id,
			ModelMap model) throws CareException {
		System.out.println("getCandidateDetails::" + id);
		CandidateDetails canDetail = prepareCandidateDetailsBean(candidateDetailsService
				.getCandidateDetails(id));
		// model.addAttribute("candidateDetails", canDetail);
		ModelAndView modelNView = new ModelAndView(
				"candidatedetails/candidateDetailsAdd", "candidatedetails",
				canDetail);
		// ModelAndView modelNView = new ModelAndView();
		return modelNView;
	}

	@RequestMapping(value = "/savepage", method = RequestMethod.GET)
	public ModelAndView getSaveCandidateDetails(
			@ModelAttribute("candidatedetails") CandidateDetails canDetails,
			BindingResult bResult) throws CareException {
		ModelAndView modelAndView = null;
		modelAndView = new ModelAndView("candidatedetails/candidateDetailsAdd");
		modelAndView.addObject("candidatedetails", canDetails);

		return modelAndView;
	}

	@RequestMapping(value = "/massupload", method = RequestMethod.POST)
	@ResponseBody
	public String massUploadCandidateDetails(
			MultipartHttpServletRequest request, HttpServletResponse response)
			throws CareException {
		MultipartHttpServletRequest mp = (MultipartHttpServletRequest) request;
		Iterator<String> itr = mp.getFileNames();
		MultipartFile mpf = null;
		HashMap<Integer, String> errorMap = new HashMap<Integer, String>();
		HashMap<Integer, String> successMap = new HashMap<Integer, String>();
		CandidateDetails candidateDetails = null;
		int id = Integer.parseInt(request.getParameter("id"));
		try {
			while (itr.hasNext()) {
				mpf = mp.getFile(itr.next());
				System.out.println(mpf.getOriginalFilename() + " uploaded! ");
				XSSFWorkbook workbook = new XSSFWorkbook(mpf.getInputStream());
				XSSFSheet sheet = workbook.getSheetAt(0);
				Iterator<Row> rowIterator = sheet.iterator();
				int i =0;
				while (rowIterator.hasNext()) {
					System.out.println("Current row"+i);
					Row row = null;
					candidateDetails = new CandidateDetails();
					StringBuffer sb = new StringBuffer();
					Boolean isError = false;
					if (i == 0)
						{
						 row = rowIterator.next();
						 row = rowIterator.next();
						}
					else
						{
							row = rowIterator.next();
						}
					String bottomString = "Shortlisted in AMCAT";
					if(bottomString.equalsIgnoreCase(row.getCell(1).toString())){
						break;
					}
					Double checksiNo = null;
					Double checkacmatId = null;
					String campusName = null;
					CampusDomain campusdomain = null;
					if (row.getCell(28) != null)
					{
						campusName = row.getCell(28).getStringCellValue();
						campusdomain = campusService.getCampus(id);
						System.out.println("College Name: "+campusName);
					}
					else
					{
						System.out.println("College Name is Missing in row: "+i);
					}
					if (row.getCell(0) != null)
					{
						checksiNo = row.getCell(0).getNumericCellValue();
						System.out.println("Serial Number is: "+checksiNo);
					}
					else
					{
						System.out.println("Serial number is missing in row: "+i);
					}
					if (row.getCell(1) != null)
					{
						checkacmatId = row.getCell(1).getNumericCellValue();
						System.out.println("AMCAT ID is: "+checkacmatId);
					}
					else
					{
						System.out.println("AMCAT ID is missing in row: "+i);
					}
					i++;
					if (campusdomain!=null && campusdomain.getCampusName() != null)
					{
					if (checksiNo!=null && checkacmatId != null)
					{
						if (checksiNo != 0.0 && checkacmatId != 0.0)
						{
						Iterator<Cell> cellIterator = row.cellIterator();
							while (cellIterator.hasNext()) {
								//Setting Campus ID
								candidateDetails.setCampusdomain(campusdomain);
								Cell cell = cellIterator.next();
								switch (cell.getColumnIndex()) {
								case 0:
									isError = validateDataNumericInt(cell);
									if (!isError)
									{
										String siNo = assignDataString(cell);
										System.out.println("Serial Number: "+siNo);
									}
									else
									{
										sb.append("siNo should contain only numericals/Length less than 20 numericals\n ");
									}
									break;
								case 1:
									isError = validateDataNumericBigInt(cell);
									if (!isError)
									{
										BigInteger amcatId = assignDataNumericBigInt(cell);
										candidateDetails.setAmcatID(amcatId);
										System.out.println("AMCAT ID is:"+amcatId);
									}
									else
									{
										sb.append("AMCAT ID should contain only numericals/Length less than 50 numericals\n ");
									}
									
									break;
								case 2:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String firstName = assignDataString(cell);
											candidateDetails.setFirstName(firstName);
											System.out.println("First Name: "+firstName);
										}
										else
										{
											sb.append("First Name has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 3:
									isError = validateDataString(cell);
									System.out.println("!!Care Error!! "+isError);
									if (isError != null)
									{
										if (!isError)
										{
											String middleName = assignDataString(cell);
											candidateDetails.setMiddleName(middleName);
											System.out.println("Middle Name: "+middleName);
										}
										else
										{
											sb.append("Middle Name has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 4:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String lastName = assignDataString(cell);
											candidateDetails.setLastName(lastName);
											System.out.println("Last Name: "+lastName);
										}
										else
										{
											sb.append("Last Name has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 5:
									isError = validateDataString(cell);
									if (isError !=null)
									{
										if (!isError)
										{
											String email = assignDataString(cell);
											candidateDetails.setEmailID(email);
											System.out.println("Email: "+email);
										}
										else
										{
											sb.append("Email has invalid characters/Greater than 256 characters ");
										}
									}
									break;	
								case 6:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String altemail = assignDataString(cell);
											candidateDetails.setAlternateEmailID(altemail);
											System.out.println("Alternate Email: "+altemail);
										}
										else
										{
											sb.append("Alt Email has invalid characters/Greater than 256 characters ");
										}
									}
									break;	
								case 7:
									isError = validateDataNumericBigInt(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigInteger mobileNo = assignDataNumericBigInt(cell);
											candidateDetails.setMobileNumber(mobileNo);
											System.out.println("Mobile Number: "+mobileNo);
										}
										else
										{
											sb.append("Mobile No should contain only numericals/Length less than 50 numericals\n ");
										}
									}
									break;	
								case 8:
									isError = validateDataNumericBigInt(cell);
									if (isError !=null)
									{
										if (!isError)
										{
											BigInteger landLine = assignDataNumericBigInt(cell);
											candidateDetails.setLandLine(landLine);
											System.out.println("Landline number: "+landLine);
										}
										else
										{
											sb.append("LandLine No should contain only numericals/Length less than 50 numericals\n ");
										}
									}
									break;
								case 9:
									isError = validateDataNumericBigInt(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigInteger fatherMob = assignDataNumericBigInt(cell);
											candidateDetails.setFatherMobileNo(fatherMob);
											System.out.println("Father's mobile number::"+fatherMob);
										}
										else
										{
											sb.append("fatherMob No should contain only numericals/Length less than 50 numericals\n ");
										}
									}
									break;
								case 10:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											try
											{
													if (cell.getCellType()== Cell.CELL_TYPE_NUMERIC)
													{
														double dv = cell.getNumericCellValue();
														if (HSSFDateUtil.isCellDateFormatted(cell)) 
														{
														    Date date = HSSFDateUtil.getJavaDate(dv);
	
														    String dateFmt = cell.getCellStyle().getDataFormatString();
	
														    String strValue = new CellDateFormatter(dateFmt).format(date);
														    System.out.println("Date of birth: "+strValue);
														    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
															Date date1 = sdf.parse(strValue);
															candidateDetails.setDob(date1);
															System.out.println("Date of birth (date format): "+date1);
														}
													}
													else
													{
														sb.append("Invalid Date");
													}
											}
											catch(ParseException pe)
											{
												System.out
														.println("pe"+pe);
												sb.append("Date parse exception");
											}
										}
										else
										{
											sb.append("Date field contains invalid data");
										}
									}
									break;
								case 11:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String gender = assignDataString(cell);
											candidateDetails.setGender(gender);
											System.out.println("Gender: "+gender);
										}
										else
										{
											sb.append("Gender has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 12:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String pAddress = assignDataString(cell);
											candidateDetails.setpAddress(pAddress);
											System.out.println("Permanent Address: "+pAddress);
										}
										else
										{
											sb.append("pAddress has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 13:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String pCity = assignDataString(cell);
											candidateDetails.setpCity(pCity);
											System.out.println("Permanent City::"+pCity);
										}
										else
										{
											sb.append("pCity has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 14:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String pState = assignDataString(cell);
											candidateDetails.setpState(pState);
											System.out.println("Permanent State: "+pState);
										}
										else
										{
											sb.append("Gender has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 15:
									isError = validateDataNumericInt(cell);
									if (!isError)
									{
										Integer pZipCode = assignDataNumericInt(cell);
										candidateDetails.setpZipCode(pZipCode);
										System.out.println("Pin Code: "+pZipCode);
									}
									else
									{
										sb.append("pZipCode should contain only numericals/Length less than 20 numericals\n ");
									}
									break;
								case 16:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String cAddress = assignDataString(cell);
											candidateDetails.setcAddress(cAddress);
											System.out.println("College Address: "+cAddress);
										}
										else
										{
											sb.append("cAddress has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 17:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String cCity = assignDataString(cell);
											candidateDetails.setcCity(cCity);
											System.out.println("College City: "+cCity);
										}
										else
										{
											sb.append("cCity has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 18:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String cState = assignDataString(cell);
											candidateDetails.setcState(cState);
											System.out.println("College State: "+cState);
										}
										else
										{
											sb.append("cState has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 19:
									isError = validateDataNumericInt(cell);
									if (!isError)
									{
										Integer cZipCode = assignDataNumericInt(cell);
										candidateDetails.setcZipCode(cZipCode);
										System.out.println("Pin Code: "+cZipCode);
									}
									else
									{
										sb.append("cZipCode should contain only numericals/Length less than 20 numericals\n ");
									}
									break;
								case 20:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String boardName10 = assignDataString(cell);
											candidateDetails.setBoardName10(boardName10);;
											System.out.println("boardName10::"+boardName10);
										}
										else
										{
											sb.append("boardName10 has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 21:
									isError = validateDataNumericInt(cell);
									if (isError != null)
									{
										if (!isError)
										{
											Integer passYear10 = assignDataNumericInt(cell);
											candidateDetails.setPassingYear10(passYear10);
											System.out.println("passYear10::"+passYear10);
										}
										else
										{
											sb.append("passYear10 has invalid characters/Size greater than 20 characters ");
										}
									}
									break;
								case 22:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal percentage10 = assignDataNumericBD(cell);
											candidateDetails.setPercentage10(percentage10);
											System.out.println("percentage10::"+percentage10);
										}
										else
										{
											sb.append("percentage10 has invalid characters/Size greater than 20 characters ");
										}
									}
									break;
								case 23:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String boardName12 = assignDataString(cell);
											candidateDetails.setBoardName12(boardName12);;
											System.out.println("boardName12::"+boardName12);
										}
										else
										{
											sb.append("boardName12 has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 24:
									isError = validateDataNumericInt(cell);
									if (isError != null)
									{
										if (!isError)
										{
											Integer passingYea12 = assignDataNumericInt(cell);
											candidateDetails.setPassingYea12(passingYea12);
											System.out.println("passingYea12::"+passingYea12);
										}
										else
										{
											sb.append("passingYear12 has invalid characters/Size greater than 20 characters ");
										}
									}
									break;
								case 25:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal percentage12 = assignDataNumericBD(cell);
											candidateDetails.setPercentage12(percentage12);
											System.out.println("percentage12::"+percentage12);
										}
										else
										{
											sb.append("percentage12 has invalid characters/Size greater than 20 characters ");
										}
									}
									break;
								case 26:
									isError = validateDataNumericInt(cell);
									if (isError != null)
									{
										if (!isError)
										{
											Integer completionYear = assignDataNumericInt(cell);
											candidateDetails.setCompletionYear(completionYear);
											System.out.println("completionYear::"+completionYear);
										}
										else
										{
											sb.append("completionYear has invalid characters/Size greater than 20 characters ");
										}
									}
									break;
								case 27:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal collegePercentage = assignDataNumericBD(cell);
											candidateDetails.setCollegePecentage(collegePercentage);
											System.out.println("collegePercentage::"+collegePercentage);
										}
										else
										{
											sb.append("collegePercentage has invalid characters/Size greater than 20 characters ");
										}
									}
									break;
								case 28:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String collegeName = assignDataString(cell);
											candidateDetails.setCollegName(collegeName);
											System.out.println("collegeName::"+collegeName);
										}
										else
										{
											sb.append("collegeName has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 29:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String collegeCity = assignDataString(cell);
											candidateDetails.setCollegeCity(collegeCity);
											System.out.println("collegeCity::"+collegeCity);
										}
										else
										{
											sb.append("collegeCity has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 30:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String collegeState = assignDataString(cell);
											candidateDetails.setCollegeState(collegeState);
											System.out.println("collegeState::"+collegeState);
										}
										else
										{
											sb.append("collegeState has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 31:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String degreeName = assignDataString(cell);
											candidateDetails.setDegreeName(degreeName);
											System.out.println("degreeName::"+degreeName);
										}
										else
										{
											sb.append("degreeName has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 32:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String subjectName = assignDataString(cell);
											candidateDetails.setSubjectName(subjectName);
											System.out.println("subjectName::"+subjectName);
										}
										else
										{
											sb.append("subjectName has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 33:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String gradUnivRollNo = assignDataString(cell);
											candidateDetails.setGradUniversityRollNo(gradUnivRollNo);
											System.out.println("gradUnivRollNo::"+gradUnivRollNo);
										}
										else
										{
											sb.append("gradUnivRollNo has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 34:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String eduMode = assignDataString(cell);
											candidateDetails.setEduMode(eduMode);
											System.out.println("eduMode::"+eduMode);
										}
										else
										{
											sb.append("eduMode has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 35:
									isError = validateDataNumericInt(cell);
									if (isError != null)
									{
										if (!isError)
										{
											 Integer backPaper = assignDataNumericInt(cell);
											candidateDetails.setBackPaper(backPaper);
											System.out.println("backPaper::"+backPaper);
										}
										else
										{
											sb.append("backPaper has invalid characters/Size greater that 20 characters ");
										}
									}
									break;
								case 36:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String affiliatedTo = assignDataString(cell);
											candidateDetails.setAffiliatedTo(affiliatedTo);
											System.out.println("affiliatedTo::"+affiliatedTo);
										}
										else
										{
											sb.append("affiliatedTo has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								case 37:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											try
											{
													if (cell.getCellType()== Cell.CELL_TYPE_NUMERIC)
													{
														double dv = cell.getNumericCellValue();
														if (HSSFDateUtil.isCellDateFormatted(cell)) 
														{
														    Date date = HSSFDateUtil.getJavaDate(dv);
	
														    String dateFmt = cell.getCellStyle().getDataFormatString();
	
														    String strValue = new CellDateFormatter(dateFmt).format(date);
														    System.out.println("strValue::::"+strValue);
														    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
															Date date1 = sdf.parse(strValue);
															//candidateDetails.setDob(date1);
															System.out.println("date::"+date1);
														}
													}
													else
													{
														sb.append("Invalid Date");
													}
											}
											catch(ParseException pe)
											{
												System.out
														.println("pe"+pe);
												sb.append("Date parse exception");
											}
										}
										else
										{
											sb.append("Date field contains invalid data");
										}
									}
									break;
								case 38:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal englishScore = assignDataNumericBD(cell);
											candidateDetails.setEnglishScore(englishScore);
											System.out.println("englishScore::"+englishScore);
										}
										else
										{
											sb.append("englishScore has invalid characters/Size greater than 50 characters ");
										}
									}
									break;
								case 39:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal englishPercentile = assignDataNumericBD(cell);
											candidateDetails.setEnglishPercentile(englishPercentile);
											System.out.println("englishPercentile::"+englishPercentile);
										}
										else
										{
											sb.append("englishPercentile has invalid characters/Size greater than 50 characters ");
										}
									}
									break;
								case 40:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal vocabulary = assignDataNumericBD(cell);
											candidateDetails.setVocabulary(vocabulary);
											System.out.println("vocabulary::"+vocabulary);
										}
										else
										{
											sb.append("vocabulary has invalid characters/Size greater than 50 characters ");
										}
									}
									break;
								case 41:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal grammer = assignDataNumericBD(cell);
											candidateDetails.setGrammar(grammer);
											System.out.println("grammar::"+grammer);
										}
										else
										{
											sb.append("grammar has invalid characters/Size greater than 50 characters ");
										}
									}
									break;
								case 42:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal comprehension = assignDataNumericBD(cell);
											candidateDetails.setComprehension(comprehension);
											System.out.println("comprehension::"+comprehension);
										}
										else
										{
											sb.append("comprehension has invalid characters/Size greater than 20 characters ");
										}
									}
									break;
								case 43:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal quantitativeAbilityScore = assignDataNumericBD(cell);
											candidateDetails.setQuantitativeAbilityScore(quantitativeAbilityScore);
											System.out.println("quantitativeAbilityScore::"+quantitativeAbilityScore);
										}
										else
										{
											sb.append("quantitativeAbilityScore has invalid characters/Size greater than 50 characters ");
										}
									}
									break;
								case 44:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal quantitativeAbilityPercentage = assignDataNumericBD(cell);
											candidateDetails.setQuantitativeAbilityPercentage(quantitativeAbilityPercentage);
											System.out.println("quantitativeAbilityPercentage::"+quantitativeAbilityPercentage);
										}
										else
										{
											sb.append("quantitativeAbilityPercentage has invalid characters/Size greater than 50 characters ");
										}
									}
									break;
								case 45:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal basicMathematics = assignDataNumericBD(cell);
											candidateDetails.setBasicMathematics(basicMathematics);
											System.out.println("vocabulary::"+basicMathematics);
										}
										else
										{
											sb.append("basicMathematics has invalid characters/Size greater than 50 characters ");
										}
									}
									break;
								case 46:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal enggMaths = assignDataNumericBD(cell);
											candidateDetails.setEngineeringMathematics(enggMaths);
											System.out.println("vocabulary::"+enggMaths);
										}
										else
										{
											sb.append("EngineeringMathematics has invalid characters/Size greater than 50 characters ");
										}
									}
									break;
								case 47:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal appliedMathematics = assignDataNumericBD(cell);
											candidateDetails.setAppliedMathematics(appliedMathematics);
											System.out.println("vocabulary::"+appliedMathematics);
										}
										else
										{
											sb.append("appliedMathematics has invalid characters/Size greater than 50 characters ");
										}
									}
									break;
								case 48:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal logicalAbilityScore = assignDataNumericBD(cell);
											candidateDetails.setLogicalAbilityScore(logicalAbilityScore);
											System.out.println("logicalAbilityScore::"+logicalAbilityScore);
										}
										else
										{
											sb.append("logicalAbilityScore has invalid characters/Size greater than 50 characters ");
										}
									}
									break;
								case 49:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal logicalAbilityPercentage = assignDataNumericBD(cell);
											candidateDetails.setLogicalAbilityPercentage(logicalAbilityPercentage);
											System.out.println("logicalAbilityPercentage::"+logicalAbilityPercentage);
										}
										else
										{
											sb.append("logicalAbilityPercentage has invalid characters/Size greater than 50 characters ");
										}
									}
									break;
								case 50:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal inductiveReasoning = assignDataNumericBD(cell);
											candidateDetails.setInductiveReasoning(inductiveReasoning);
											System.out.println("inductiveReasoning::"+inductiveReasoning);
										}
										else
										{
											sb.append("inductiveReasoning has invalid characters/Size greater than 50 characters ");
										}
									}
									break;
								case 51:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal deductiveReasoning = assignDataNumericBD(cell);
											candidateDetails.setDeductiveReasoning(deductiveReasoning);
											System.out.println("deductiveReasoning::"+deductiveReasoning);
										}
										else
										{
											sb.append("deductiveReasoning has invalid characters/Size greater than 50 characters ");
										}
									}
									break;
								case 52:
									isError = validateDataNumericBD(cell);
									if (isError != null)
									{
										if (!isError)
										{
											BigDecimal abductiveReasoning = assignDataNumericBD(cell);
											candidateDetails.setAbductiveReasoning(abductiveReasoning);
											System.out.println("abductiveReasoning::"+abductiveReasoning);
										}
										else
										{
											sb.append("abductiveReasoning has invalid characters/Size greater than 50 characters ");
										}
									}
									break;
									case 53:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String parentTitle = assignDataString(cell);
											candidateDetails.setParentTitle(parentTitle);
											System.out.println("parentTitle::"+parentTitle);
										}
										else
										{
											sb.append("parentTitle has invalid characters/Greater than 256 characters ");
										}
									}
									break;
									case 54:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String pref1Function = assignDataString(cell);
											candidateDetails.setPref1Function(pref1Function);
											System.out.println("pref1Function::"+pref1Function);
										}
										else
										{
											sb.append("pref1Function has invalid characters/Greater than 256 characters ");
										}
									}
									break;
									case 55:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String prefOfGurgaon1 = assignDataString(cell);
											candidateDetails.setPrefOfGurgaon1(prefOfGurgaon1);
											System.out.println("prefOfGurgaon1::"+prefOfGurgaon1);
										}
										else
										{
											sb.append("affiliatedTo has invalid characters/Greater than 256 characters ");
										}
									}
									break;
									case 56:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String prefOfHyderabad1 = assignDataString(cell);
											candidateDetails.setPrefOfHyderabad1(prefOfHyderabad1);
											System.out.println("prefOfHyderabad1::"+prefOfHyderabad1);
										}
										else
										{
											sb.append("prefOfHyderabad1 has invalid characters/Greater than 256 characters ");
										}
									}
									break;
									case 57:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String prefOfMumbai1 = assignDataString(cell);
											candidateDetails.setPrefOfMumbai1(prefOfMumbai1);
											System.out.println("prefOfMumbai1::"+prefOfMumbai1);
										}
										else
										{
											sb.append("prefOfMumbai1 has invalid characters/Greater than 256 characters ");
										}
									}
									break;
									case 58:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String prefOfBengaluru = assignDataString(cell);
											candidateDetails.setPrefOfBengaluru(prefOfBengaluru);
											System.out.println("prefOfBengaluru::"+prefOfBengaluru);
										}
										else
										{
											sb.append("prefOfBengaluru has invalid characters/Greater than 256 characters ");
										}
									}
									break;
									case 59:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String pref2Function = assignDataString(cell);
											candidateDetails.setPref2Function(pref2Function);
											System.out.println("pref2Function::"+pref2Function);
										}
										else
										{
											sb.append("pref2Function has invalid characters/Greater than 256 characters ");
										}
									}
									break;
									case 60:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String prefOfGurgaon2 = assignDataString(cell);
											candidateDetails.setPrefOfGurgaon2(prefOfGurgaon2);
											System.out.println("prefOfGurgaon2::"+prefOfGurgaon2);
										}
										else
										{
											sb.append("prefOfGurgaon2 has invalid characters/Greater than 256 characters ");
										}
									}
									break;
									case 61:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String prefOfMumbai2 = assignDataString(cell);
											candidateDetails.setPrefOfMumbai2(prefOfMumbai2);
											System.out.println("prefOfMumbai2::"+prefOfMumbai2);
										}
										else
										{
											sb.append("prefOfMumbai2 has invalid characters/Greater than 256 characters ");
										}
									}
									break;
									case 62:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String prefOfHyderabad2 = assignDataString(cell);
											candidateDetails.setPrefOfHyderabad2(prefOfHyderabad2);
											System.out.println("prefOfHyderabad2::"+prefOfHyderabad2);
										}
										else
										{
											sb.append("prefOfHyderabad2 has invalid characters/Greater than 256 characters ");
										}
									}
									break;
									case 63:
									isError = validateDataString(cell);
									if (isError != null)
									{
										if (!isError)
										{
											String prefOfBengaluru2 = assignDataString(cell);
											candidateDetails.setPrefOfBengaluru2(prefOfBengaluru2);
											System.out.println("prefOfBengaluru2::"+prefOfBengaluru2);
										}
										else
										{
											sb.append("prefOfBengaluru2 has invalid characters/Greater than 256 characters ");
										}
									}
									break;
								}
							}
							System.out.println("isError:::::"+sb.toString()+":::::");
							if (!sb.toString().equalsIgnoreCase(""))
							{
								errorMap.put(i, "Row "+i+" "+sb.toString()+"<br/>");
							}
							else
							{
								//Get the ACMAT ID id
								//Candidate Save or Update
								System.out.println("candidateDetails"+candidateDetails);
								System.out.println("ACMAT ID "+candidateDetails.getAmcatID());
								Integer candidateID = candidateDetailsService.getCandidateDetailsAcmatID(candidateDetails.getAmcatID());
								System.out.println("candidateID::"+candidateID);
								if (candidateID != null)
								{
									//Update
									CandidateDetailsDomain cdd = prepareModel(candidateDetails);
									System.out.println("candidateDetails:::::::"+candidateDetails);
									System.out.println("cdd:::::::"+cdd);
									cdd.setCandidateId(candidateID);
									cdd.setUpdatedDt(new Date(System.currentTimeMillis()));
								 	candidateDetailsService.addCandidateDetails(cdd);
								}
								else
								{
									//Save
									CandidateDetailsDomain cdd = prepareModel(candidateDetails);
									cdd.setCreatedDt(new Date(System.currentTimeMillis()));
									cdd.setUpdatedDt(new Date(System.currentTimeMillis()));
								 	candidateDetailsService.addCandidateDetails(cdd);
								}
								
								System.out.println("1::::::::::::::::::in else");
								successMap.put(i,"Row "+i+"inserted successfully! <br/>");
							}
						}
						else
						{
							errorMap.put(i, "Row no "+i+" has empty SI No or aCMAT ID <br/>");
						}
					}
					else
					{
						errorMap.put(i, "Row no "+i+" has empty SI No or aCMAT ID <br/>");
					}
				  }
				  else
					{
						errorMap.put(i, "Row no "+i+" has invalid Campus Name <br/>");
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		StringBuffer sbError = new StringBuffer();
		
		Iterator it = successMap.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pairs = (Map.Entry)it.next();
	        sbError.append(pairs.getValue());
	        it.remove(); // avoids a ConcurrentModificationException
	    }
		
		it = errorMap.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pairs = (Map.Entry)it.next();
	        sbError.append(pairs.getValue());
	        it.remove(); // avoids a ConcurrentModificationException
	    }
	    
		return sbError.toString();
	}

	@RequestMapping(value = "/save", method = RequestMethod.GET)
	@ExceptionHandler({ CareException.class })
	// public ModelAndView saveCandidateDetails(@RequestParam("file")
	// MultipartFile file,@Valid @ModelAttribute("candidatedetails")
	// CandidateDetails canDetails, BindingResult bResult)
	public ModelAndView saveCandidateDetails(
			@Valid @ModelAttribute("candidatedetails") CandidateDetails canDetails,
			BindingResult bResult) throws CareException {
		ModelAndView modelAndView = null;
		try {
			if (bResult.hasErrors()) {
				modelAndView = new ModelAndView(
						"candidatedetails/candidateDetailsAdd");
			} else {
				// Blob blob = Hibernate.createBlob(file.getInputStream());
				// System.out.println("File:" + file.getName());
				// System.out.println("ContentType:" + file.getContentType());
				System.out.println("In saveCandidateDetails1::::::::"
						+ canDetails.getCandidateId());
				System.out.println("In saveCandidateDetails1::::::::"
						+ canDetails.getCampusdomain().getId());
				CandidateDetailsDomain candidateDetailsdomain = prepareModel(canDetails);
				// candidateDetailsdomain.setContent(blob);
				candidateDetailsdomain.setCreatedDt(new Date(System.currentTimeMillis()));
				candidateDetailsdomain.setUpdatedDt(new Date(System.currentTimeMillis()));
				candidateDetailsService
						.addCandidateDetails(candidateDetailsdomain);
				canDetails.setSuccess("success");
				canDetails
						.setSuccessMsg("Candidate Details saved successfully!");
				modelAndView = new ModelAndView(
						"candidatedetails/candidateDetailsShow");
				modelAndView.addObject("candidatedetails", canDetails);

			}
		} catch (CareException e) {
			System.out.println("1::::" + e);
			modelAndView = new ModelAndView(
					"candidatedetails/candidateDetailsAdd");
			canDetails.setSuccess("success");
			canDetails.setSuccessMsg("Candidate Details saved successfully!");
			modelAndView.addObject("candidatedetails", canDetails);
			System.out.println("1::::" + e);
		}
		return modelAndView;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView editCandidateDetails(
			@ModelAttribute("candidatedetails") CandidateDetails canDetails,
			BindingResult bResult) throws CareException {
		System.out.println("In editCandidateDetails::::::::"
				+ canDetails.getCandidateId());
		System.out.println("In editCandidateDetails::::::::"
				+ canDetails.getCampusdomain().getId());
		ModelAndView modelAndView = new ModelAndView(
				"candidatedetails/candidateDetailsAdd");
		// CandidateDetailsDomain candidateDetailsdomain =
		// prepareModel(canDetails);
		// candidateDetailsService.addCandidateDetails(candidateDetailsdomain);
		modelAndView.addObject("candidatedetails", canDetails);
		return modelAndView;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String deleteCandidateDetails(@PathVariable Integer id) throws CareException {
		
		CandidateDetailsDomain cdd = new CandidateDetailsDomain();
		cdd.setCandidateId(id);
		candidateDetailsService.deleteCandidateDetails(cdd);
		
		return "Successfully Deleted" ;
	}

	private CandidateDetails prepareCandidateDetailsBean(
			CandidateDetailsDomain candidateDetailsDomain) {
		CandidateDetails candDetails = new CandidateDetails();
		MapperFacade mapper = mapperFactory.getMapperFacade();
		if (candidateDetailsDomain != null)
			mapper.map(candidateDetailsDomain, candDetails);
		return candDetails;
	}

	private List<CandidateDetails> prepareListofBean(
			List<CandidateDetailsDomain> candidateDetailsDomain) {
		List<CandidateDetails> beans = null;
		if (candidateDetailsDomain != null && !candidateDetailsDomain.isEmpty()) {
			beans = new ArrayList<CandidateDetails>();
			CandidateDetails bean = null;
			for (CandidateDetailsDomain candidateDomain : candidateDetailsDomain) {
				bean = new CandidateDetails();
				MapperFacade mapper = mapperFactory.getMapperFacade();
				// Mapper mapper = new DozerBeanMapper();
				mapper.map(candidateDomain, bean);
				beans.add(bean);
			}
		}
		return beans;
	}

	private CandidateDetailsDomain prepareModel(CandidateDetails canDetails) {
		CandidateDetailsDomain candidateDetailsdomain = new CandidateDetailsDomain();
		MapperFacade mapper = mapperFactory.getMapperFacade();
		// Mapper mapper = new DozerBeanMapper();
		mapper.map(canDetails, candidateDetailsdomain);
		return candidateDetailsdomain;
	}
	
	private Boolean validateDataString(Cell cell)
	{
		String temp = null;
		Boolean isError = null; 
		if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
		{
			temp = String.valueOf(cell.getNumericCellValue());
			isError = false;
		}
		else if(cell.getCellType() == Cell.CELL_TYPE_STRING)
		{
			temp = cell.getStringCellValue();
			isError = false;
		}
		
		if (temp != null)
		{
			temp = temp.trim();
			if (temp.length() > 256)
			{
				isError = true;
			}
		}
		return isError;
	}
	
	private String assignDataString(Cell cell)
	{
		String temp = null;
		if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
		{
			temp = String.valueOf(cell.getNumericCellValue());
		}
		else if(cell.getCellType() == Cell.CELL_TYPE_STRING)
		{
			temp = cell.getStringCellValue();
		}
		return temp;
	}

	
	private Boolean validateDataNumericInt(Cell cell)
	{
		Integer temp = null;
		Boolean isError = null;
		if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
		{
			temp = new Double(cell.getNumericCellValue()).intValue();
			isError = false;
		}
		else if (cell.getCellType() == Cell.CELL_TYPE_STRING)
		{
			isError = true;
		}
		if (temp != null)
		{
			if (temp.toString().length() > 10)
			{
				isError = true;
			}
		}
		return isError;
	}
	
	private BigInteger assignDataNumericBigInt(Cell cell)
	{
		BigInteger temp = null;
		temp =new BigDecimal(cell.getNumericCellValue()).toBigInteger();
		return temp;
	}
	
	private Boolean validateDataNumericBigInt(Cell cell)
	{
		BigInteger temp = null;
		Boolean isError = null;
		if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
		{
			temp =new BigDecimal(cell.getNumericCellValue()).toBigInteger();
			isError = false;
		}
		else if (cell.getCellType() == Cell.CELL_TYPE_STRING)
		{
			isError = true;
		}
		if (temp != null)
		{
			if (temp.toString().length() > 20)
			{
				isError = true;
			}
		}
		return isError;
	}
	
	private Integer assignDataNumericInt(Cell cell)
	{
		Integer temp = null;
		temp = new Double(cell.getNumericCellValue()).intValue();
		return temp;
	}
	
	
	private Boolean validateDataNumericBD(Cell cell)
	{
		BigDecimal temp = null;
		Boolean isError = null;
		if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
		{
			temp = new BigDecimal(cell.getNumericCellValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
			isError = false;
		}
		else if (cell.getCellType() == Cell.CELL_TYPE_STRING)
		{
			isError = true;
		}
		
		if (temp != null)
		{
			if (temp.toString().length() > 20)
			{
				isError = true;
			}
		}
		return isError;
	}
	
	private BigDecimal assignDataNumericBD(Cell cell)
	{
		BigDecimal temp = null;
		temp = new BigDecimal(cell.getNumericCellValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
		return temp;
	}
	
	private Boolean validateDataNumericDouble(Cell cell)
	{
		Double temp = null;
		Boolean isError = null;
		if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
		{
			temp = new Double(cell.getNumericCellValue());
			isError = false;
		}
		else
		{
			isError = true;
		}
		if (temp.toString().length() > 20)
		{
			isError = true;
		}
		return isError;
	}
	
	private Double assignDataNumericDouble(Cell cell)
	{
		Double temp = null;
		temp = new Double(cell.getNumericCellValue());
		return temp;
	}
}
