package com.deloitte.care.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.hibernate.HibernateException;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.DateUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.domain.CampusDomain;
import com.deloitte.care.domain.CandidateDetailsDomain;
import com.deloitte.care.domain.GroupDiscussionDetailsDomain;
import com.deloitte.care.domain.InterviewDetailsDomain;
import com.deloitte.care.domain.PanelistDomain;
import com.deloitte.care.domain.RecruiterDomain;
import com.deloitte.care.domain.UserDomain;
import com.deloitte.care.exception.CareException;
import com.deloitte.care.form.Campus;
import com.deloitte.care.service.CampusDetailsService;
import com.deloitte.care.service.CampusService;
import com.deloitte.care.service.CandidateDetailsService;
import com.deloitte.care.service.GroupDiscussionDetailsService;
import com.deloitte.care.service.InterviewDetailsService;
import com.deloitte.care.service.PanelistService;
import com.deloitte.care.service.RecruiterService;
import com.deloitte.care.service.UserService;
import com.deloitte.care.util.DataTablesData;
import com.google.gson.Gson;


@Controller
@RequestMapping("/admin")
public class AdminController {
	
	static final Logger logger = Logger.getLogger(AdminController.class);
	
	Gson gson= new Gson();
	@Autowired
	private CampusService campusService;
	
	@Autowired
	private PanelistService panelistService;
	
	@Autowired
	private RecruiterService recruiterService;
	
	@Autowired
	private GroupDiscussionDetailsService groupDiscussionDetailsService;
	
	@Autowired
	private InterviewDetailsService interviewDetailsService;
	@Autowired
	private CandidateDetailsService candidateDetailsService;
	
	@Autowired
	private UserService userService;
	
	
	@Autowired
	private CampusDetailsService campusDetailsService;
	
	MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
	
	// Campus related methods
	
	@RequestMapping(value = "/campus/save", method = RequestMethod.POST)
	public @ResponseBody String saveCampus(HttpServletRequest request, HttpServletResponse response) {
		logger.info("saveCampus() : Enter");
		String id = request.getParameter("id");
		String campusName = request.getParameter("campusName");
		String campusDesc = request.getParameter("campusDesc");
		Campus campus = new Campus();
		campus.setId(Integer.parseInt(id));
		campus.setCampusName(campusName);
		campus.setCampusDesc(campusDesc);
		campus.setCreatedDt(new Date(System.currentTimeMillis()));
		campus.setUpdatedDt(new Date(System.currentTimeMillis()));
		
		MapperFacade mapper = mapperFactory.getMapperFacade();
		CampusDomain campusdomain = new CampusDomain();
		mapper.map(campus, campusdomain);
		campusService.addCampus(campusdomain);
		logger.info("saveCampus() : Exit");
		return "success";
	}
		
	@RequestMapping(value = "/get_campus_list", method = RequestMethod.GET, headers = "Accept=*/*")
	public @ResponseBody
	List<Campus> getCampusList() {
		return prepareListofBean(campusService.listCampuses());
	}
	
	@RequestMapping(value = "/get_campus_list_for_panelist", method = RequestMethod.GET, headers = "Accept=*/*")
	public @ResponseBody
	List<Campus> getCampusListForPanelist(@RequestParam("panelistId") int panelistId) {
		return prepareListofBean(campusService.listCampusesForPanelist(panelistId));
	}

	@RequestMapping(value = "/campus/add", method = RequestMethod.GET)
	public @ResponseBody String addCampus(HttpServletRequest request, HttpServletResponse response) {

		String campusName = request.getParameter("campusName");
		String campusDesc = request.getParameter("campusDesc");
		Campus campus = new Campus();
		campus.setCampusName(campusName);
		campus.setCampusDesc(campusDesc);
		campus.setCreatedDt(new Date(System.currentTimeMillis()));
		campus.setUpdatedDt(new Date(System.currentTimeMillis()));
				
		MapperFacade mapper = mapperFactory.getMapperFacade();
		CampusDomain campusdomain = new CampusDomain();
		mapper.map(campus, campusdomain);
		campusService.addCampus(campusdomain);
		return "success" ;
	}

	@RequestMapping(value = "/campus/delete", method = RequestMethod.GET)
	public @ResponseBody String deleteCampus(HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("Inside delete metthod");
		String id = request.getParameter("id");
		Campus campus = new Campus();
		campus.setId(Integer.parseInt(id));
		campusService.deleteCampus(prepareModel(campus));
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("campus", null);
		return "success" ;
	}
	
	private CampusDomain prepareModel(Campus campus){
		CampusDomain campusdomain = new CampusDomain();
		MapperFacade mapper = mapperFactory.getMapperFacade();
		//Mapper mapper = new DozerBeanMapper();
		mapper.map(campus, campusdomain);
		return campusdomain;
	}
	
	private List<Campus> prepareListofBean(List<CampusDomain> campuses){
		List<Campus> beans = null;
		if(campuses != null && !campuses.isEmpty()){
			beans = new ArrayList<Campus>();
			Campus bean = null;
			for(CampusDomain campusdomain : campuses){
				bean = new Campus();
				MapperFacade mapper = mapperFactory.getMapperFacade();
				//Mapper mapper = new DozerBeanMapper();
				mapper.map(campusdomain, bean);
				beans.add(bean);
			}
		}
		return beans;
	}
	
	
	/************************************************************************************************/
	
	@RequestMapping(value = "/panelist/save", method = RequestMethod.POST)
	public @ResponseBody String savePanelist(HttpServletRequest request, HttpServletResponse response) {
		
		String id = request.getParameter("id");
		String panelistName = request.getParameter("panelistName");
		String panelistEmail = request.getParameter("panelist_email_id");
		String serviceLine = request.getParameter("serviceLine");
		String designation = request.getParameter("designation");
		UserDomain userDomain = userService.getUser(panelistEmail);
		PanelistDomain panelistdomain = new PanelistDomain();
		panelistdomain.setId(Integer.parseInt(id));
		panelistdomain.setPanelist_name(panelistName);
		panelistdomain.setPanelist_email_id(panelistEmail);
		panelistdomain.setServiceLine(serviceLine);
		panelistdomain.setDesignation(designation);
		panelistdomain.setCreatedDt(new Date(System.currentTimeMillis()));
		panelistdomain.setUpdatedDt(new Date(System.currentTimeMillis()));
		panelistdomain.setUserDomain(userDomain);
		panelistService.addPanelist(panelistdomain);
		return "success";
	}
		
	@RequestMapping(value = "/get_panelist_list", method = RequestMethod.POST, headers = "Accept=*/*")
	public @ResponseBody
	String getPanelistList() {
		List<PanelistDomain> panelistList = panelistService.listPanelist();
		DataTablesData gridData = null;
		gridData=new DataTablesData(panelistList);
		String testData = gson.toJson(gridData);
		System.out.println("JSONIZED List of Panelists is"+testData);
		return testData;
	}
	
	
	@RequestMapping(value = "/get_groupdiscussion_list/{panelistId}/{gDName}/{campusId}", method = RequestMethod.POST)
	public @ResponseBody String getGroupDiscussionList(@PathVariable Integer panelistId, @PathVariable String gDName, @PathVariable Integer campusId) {
		List<GroupDiscussionDetailsDomain> groupDiscussionList = groupDiscussionDetailsService.getGroupDiscussionDetailsDomain(panelistId, gDName, campusId);
		DataTablesData gridData = new DataTablesData(groupDiscussionList);
		String groupDiscussionJsonData = gson.toJson(gridData);
		return groupDiscussionJsonData;
		}
	@RequestMapping(value = "/list_panelist_gd/{panelistId}/{campusId}", method = RequestMethod.POST)
	public @ResponseBody String getPanelistGroupDiscussionList(@PathVariable Integer panelistId,@PathVariable Integer campusId) {
		List<GroupDiscussionDetailsDomain> groupDiscussionList = groupDiscussionDetailsService.getPanelistGroupDiscussionList(panelistId,campusId);
		DataTablesData gridData = new DataTablesData(groupDiscussionList);
		String groupDiscussionJsonData = gson.toJson(gridData);
		return groupDiscussionJsonData;
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws CareException
	 * @throws JSONException
	 */
	@RequestMapping(value = "/save_gd_result/", method = RequestMethod.POST)
	public @ResponseBody String saveGdResult(HttpServletRequest request, HttpServletResponse response)
			throws CareException, JSONException {
		logger.info("saveGdResult() : Enter");
		String jsonString = request.getParameter("jsondata");
		List<GroupDiscussionDetailsDomain> panelistGDDataList = new ArrayList<GroupDiscussionDetailsDomain>();
		try {
			JSONArray jsonArray = new JSONArray(jsonString);
			for (int i = 0; i < jsonArray.length(); i++) {
				GroupDiscussionDetailsDomain jsonGDData = null;
				JSONObject groupData = jsonArray.getJSONObject(i);

				String gdName = groupData.getString("gdName");
				JSONArray resultSet = (JSONArray) groupData.get("resultSet");
				
				for (int j = 0; j < resultSet.length(); j++) {
					jsonGDData = new GroupDiscussionDetailsDomain();
					String groupIdStr = resultSet.getJSONObject(j).getString("groupId");
					if (groupIdStr != null && !groupIdStr.equals("")) {
						jsonGDData.setId(Integer.parseInt(groupIdStr));
					}
					jsonGDData.setGdName(gdName);
					jsonGDData.setGdFreeze("Y");
					jsonGDData.setLeadership(resultSet.getJSONObject(j)
							.getString("leadership"));
					jsonGDData.setListening(resultSet.getJSONObject(j)
							.getString("listening"));
					jsonGDData.setParticipation(resultSet.getJSONObject(j)
							.getString("participation"));
					jsonGDData.setApproach(resultSet.getJSONObject(j)
							.getString("approach"));
					jsonGDData.setPresentation(resultSet.getJSONObject(j)
							.getString("presentation"));
					jsonGDData.setResponse(resultSet.getJSONObject(j)
							.getString("response"));
					jsonGDData.setComments(resultSet.getJSONObject(j)
							.getString("comments"));
					panelistGDDataList.add(jsonGDData);
				}
			}
			
			groupDiscussionDetailsService.updatePanelistGDData(panelistGDDataList);

		} catch (Exception exp) {
			logger.error("Error ! while saving Group discussion details.");
			exp.printStackTrace();
			throw new CareException(exp.getMessage());
		}

		logger.info("saveGdResult() : Exit");
		return "success";
	}
	
	@RequestMapping(value = "/save_interview_result/", method = RequestMethod.POST)
	public @ResponseBody String saveInterviewResult(HttpServletRequest request, HttpServletResponse response) throws CareException, JSONException, ParseException {
		
		String jsonString = request.getParameter("jsondata") ;
		System.out.println("save_interview_result"+ jsonString);

		InterviewDetailsDomain tempInterviewDomain = null;
		
		JSONArray jsonArray = new JSONArray(jsonString);
		for (int i =0 ; i < jsonArray.length(); i++)
		{
  
			JSONObject temp = jsonArray.getJSONObject(i);
			
			JSONArray resultSet = (JSONArray)temp.get("resultSet");
			
			for (int j =0 ; j < resultSet.length(); j++)
			{
				tempInterviewDomain =  new InterviewDetailsDomain();
				tempInterviewDomain.setId(resultSet.getJSONObject(j).getInt("interviewId"));
				tempInterviewDomain.setService(resultSet.getJSONObject(j).getString("service"));
				tempInterviewDomain.setCommunication(resultSet.getJSONObject(j).getString("communication"));
				tempInterviewDomain.setManagement(resultSet.getJSONObject(j).getString("management"));
				tempInterviewDomain.setLeadership(resultSet.getJSONObject(j).getString("leadership"));
				tempInterviewDomain.setQualification(resultSet.getJSONObject(j).getString("qualification"));
				tempInterviewDomain.setMotivators(resultSet.getJSONObject(j).getString("motivators"));
				tempInterviewDomain.setObjectives(resultSet.getJSONObject(j).getString("objectives"));
				tempInterviewDomain.setShift(resultSet.getJSONObject(j).getString("shift").charAt(0));
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				String date = resultSet.getJSONObject(j).getString("coursecompletiondate");
				if(date.equals("") || date.equals(null) || date.equals("null")){
					tempInterviewDomain.setCoursecompletiondate(dateFormat.parse("1999-22-22"));	
				}
				else
					tempInterviewDomain.setCoursecompletiondate(dateFormat.parse(date));
				
				
				tempInterviewDomain.setApplied('Y');
				tempInterviewDomain.setComments(resultSet.getJSONObject(j).getString("comments"));
				tempInterviewDomain.setInterviewerDecision(resultSet.getJSONObject(j).getString("interviewerDecision"));
			}

		}
		
		interviewDetailsService.updatePanelistInterviewData(tempInterviewDomain);
		
		return "success";
	}
	
	@RequestMapping(value = "/get_interview_list/{panelistId}/{interviewId}", method = RequestMethod.POST)
	public @ResponseBody String getInterviewList(@PathVariable Integer panelistId, @PathVariable String interviewId) throws ParseException {
		InterviewDetailsDomain interviewDomain = interviewDetailsService.getInterviewDetailsDomain(panelistId, interviewId);
		interviewDomain.setPanelistDomain(null);
		interviewDomain.getCandidateDetailsDomain().setCampusdomain(null);
		
		/*long millisInDay = 60 * 60 * 24 * 1000;
		long currentTime = interviewDomain.getCoursecompletiondate().getTime();
		long dateOnly = (currentTime / millisInDay) * millisInDay;
		Date clearDate = new Date(dateOnly);
		
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-DD");
		String date = ft.format(clearDate);
		clearDate = ft.parse(date);
		*/
		
		
		/*
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(interviewDomain.getCoursecompletiondate());
		calendar.set(year, month, date, hourOfDay, minute, second);
		Calendar  cal=Calendar.getInstance();
		cal.setTime(interviewDomain.getCoursecompletiondate());
		cal.set(Calendar.HOUR_OF_DAY,0);
		cal.set(Calendar.MINUTE,0);
		cal.set(Calendar.SECOND,0);
		cal.set(Calendar.MILLISECOND,0);*/
		
		
	//	interviewDomain.setCoursecompletiondate();
		List<InterviewDetailsDomain> interviewList = new ArrayList<InterviewDetailsDomain>();
		interviewList.add(interviewDomain);
		
		DataTablesData gridData = new DataTablesData(interviewList);
		String groupDiscussionJsonData = gson.toJson(gridData);
		return groupDiscussionJsonData;
	}
	
	@RequestMapping(value = "/list_panelist_interview/{panelistId}/{campusId}", method = RequestMethod.POST)
	public @ResponseBody String getPanelistInterviewList(@PathVariable Integer panelistId,@PathVariable Integer campusId) {
		List<InterviewDetailsDomain> groupDiscussionList = interviewDetailsService.getPanelistInterviewList(panelistId,campusId);

		DataTablesData gridData = new DataTablesData(groupDiscussionList);
		String groupDiscussionJsonData = gson.toJson(gridData);
		return groupDiscussionJsonData;
	}
	
	
	@RequestMapping(value = "/panelist/add", method = RequestMethod.GET)
	public @ResponseBody String addPanelist(HttpServletRequest request, HttpServletResponse response) {

		String createdBy="";
		HttpSession session = (HttpSession) request.getSession();
		if(session!=null)
			createdBy = (String) session.getAttribute("userid");
		
	//	@ModelAttribute("panelistdomain") (PanelistDomain)
		
		String panelistName = request.getParameter("panelistName");
		String panelistEmail = request.getParameter("panelist_email_id");
		String serviceLine = request.getParameter("serviceLine");
		String designation = request.getParameter("designation");
		
		createUser(panelistEmail, "ROLE_PANELIST");
		UserDomain userDomain = userService.getUser(panelistEmail);
		PanelistDomain panelistdomain = new PanelistDomain();
		panelistdomain.setPanelist_name(panelistName);
		panelistdomain.setPanelist_email_id(panelistEmail);
		panelistdomain.setServiceLine(serviceLine);
		panelistdomain.setDesignation(designation);
		panelistdomain.setCreatedDt(new Date(System.currentTimeMillis()));
		panelistdomain.setUpdatedDt(new Date(System.currentTimeMillis()));
		panelistdomain.setCreatedBy(createdBy);
		panelistdomain.setUserDomain(userDomain);
		panelistService.addPanelist(panelistdomain);
		return "success" ;
	}

	@RequestMapping(value = "/panelist/delete", method = RequestMethod.GET)
	public @ResponseBody String deletePanelist(HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("Inside delete method");
		String id = request.getParameter("id");
		PanelistDomain panelistDomain = new PanelistDomain();
		panelistDomain.setId(Integer.parseInt(id));
		try{
			panelistService.deletePanelist(panelistDomain);
		}catch(HibernateException h){
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("panelist", null);
			return "failure";
		}
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("panelist", null);
		return "success" ;
	}

	/************************************************************************************************/
	
	@RequestMapping(value = "/recruiter/save", method = RequestMethod.POST)
	public @ResponseBody String saveRecruiter(HttpServletRequest request, HttpServletResponse response) {
		
		String id = request.getParameter("id");
		String recruiterName = request.getParameter("recruiterName");
		String recruiterEmail = request.getParameter("recruiter_email_id");
		UserDomain userDomain = userService.getUser(recruiterEmail);
		RecruiterDomain recruiterdomain = new RecruiterDomain();
		recruiterdomain.setId(Integer.parseInt(id));
		recruiterdomain.setRecruiter_name(recruiterName);
		recruiterdomain.setRecruiter_email_id(recruiterEmail);
		recruiterdomain.setCreatedDt(new Date(System.currentTimeMillis()));
		recruiterdomain.setUpdatedDt(new Date(System.currentTimeMillis()));
		recruiterdomain.setUserDomain(userDomain);
		recruiterService.addRecruiter(recruiterdomain);
		return "success";
	}
		
	@RequestMapping(value = "/get_recruiter_list", method = RequestMethod.POST, headers = "Accept=*/*")
	public @ResponseBody
	String getRecruiterList() {
		List<RecruiterDomain> recruiterList = recruiterService.listRecruiter();
		DataTablesData gridData = null;
		gridData=new DataTablesData(recruiterList);
		String testData = gson.toJson(gridData);
		System.out.println("JSONIZED List of Recruiters is"+testData);
		return testData;
	}

	@RequestMapping(value = "/recruiter/add", method = RequestMethod.GET)
	public @ResponseBody String addRecruiter(HttpServletRequest request, HttpServletResponse response) {

		String recruiterName = request.getParameter("recruiterName");
		String recruiterEmail = request.getParameter("recruiter_email_id");
		String createdBy="";
		HttpSession session = (HttpSession) request.getSession();
		if(session!=null)
			createdBy = (String) session.getAttribute("userid");
		
		createUser(recruiterEmail, "ROLE_RECRUITER");
		UserDomain userDomain = userService.getUser(recruiterEmail);
		
		RecruiterDomain recruiterdomain = new RecruiterDomain();
		recruiterdomain.setRecruiter_name(recruiterName);
		recruiterdomain.setRecruiter_email_id(recruiterEmail);
		recruiterdomain.setCreatedDt(new Date(System.currentTimeMillis()));
		recruiterdomain.setUpdatedDt(new Date(System.currentTimeMillis()));
		recruiterdomain.setCreatedBy(createdBy);
		recruiterdomain.setUserDomain(userDomain);
		recruiterService.addRecruiter(recruiterdomain);
		return "success" ;
	}

	@RequestMapping(value = "/recruiter/delete", method = RequestMethod.GET)
	public @ResponseBody String deleteRecruiter(HttpServletRequest request, HttpServletResponse response) {
		
		String id = request.getParameter("id");
		RecruiterDomain recruiterDomain = new RecruiterDomain();
		recruiterDomain.setId(Integer.parseInt(id));
		recruiterService.deleteRecruiter(recruiterDomain);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("recruiter", null);
		return "success" ;
	}
	
	@RequestMapping(value = "/consensus/finalize/{campusId}", method = RequestMethod.POST)
	@ResponseBody
	public void finalizeConsensus(@PathVariable Integer campusId, HttpServletRequest request, HttpServletResponse response) throws CareException, JSONException{
		String jsonString = request.getParameter("jsondata") ;
		JSONArray jsonArray = new JSONArray(jsonString);
		for (int i =0 ; i < jsonArray.length(); i++)
		{	
			JSONObject temp = jsonArray.getJSONObject(i);
			CandidateDetailsDomain cdd =  candidateDetailsService.getCandidateDetails(temp.getInt("Candidate Id"));
			//cdd.setConsensusComments(temp.getString("Comments"));
			System.out.println(temp.getString("Decision Indicator"));
			cdd.setConsensusResult(temp.getString("Decision Indicator"));
			candidateDetailsService.addCandidateDetails(cdd);
			
		}
		CampusDetailsDomain tempCampusdomain = campusDetailsService.getCampusDetails(campusId);
		tempCampusdomain.setConsensusFreeze("Y");
		campusDetailsService.saveOrUpdateCampusDetails(tempCampusdomain);
	}
	
	public void createUser(String emailId , String role){
		UserDomain ud =  new UserDomain();
		ud.setEnabled(1);
		ud.setPassword(emailId);
		ud.setUsername(emailId);
		userService.addUser(ud , role);

	}
}
