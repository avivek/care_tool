package com.deloitte.care.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.deloitte.care.service.CandidateDetailsService;
import com.deloitte.care.service.PanelistService;
import com.deloitte.care.service.ReportService;

@Controller
@RequestMapping({"/reports"})
public class ReportController
{
  static final Logger logger = Logger.getLogger(ReportController.class);

  @Autowired
  private CandidateDetailsService candidateDetailsService;

  @Autowired
  private PanelistService panelistService;

  
  @Autowired
  private ReportService reportService;

  @RequestMapping(value={"/getFilterReports"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  @ResponseBody
  public void getFilterReportDetails(HttpServletRequest request, HttpServletResponse response)
  {
	  
	  String campusid = request.getParameter("campusid");
	  /*Get details of current campus */
	  Integer campusCountTotal  = reportService.getCountTotalApplicants(campusid);
	  Integer campusCountEligibleGD  = reportService.getCountEligibleGD(campusid);
	  Integer campusCountInterview  = reportService.getCountInterview(campusid);
	  Integer campusCountSelected  = reportService.getCountSelected(campusid);
	  Integer campusWINCountSelected  = reportService.getCountWINSelected(campusid);
	  
	  /*Get details of FY */
	 /* Integer fyCountTotal  = reportService.getCountTotalApplicants(null);
	  Integer fyCountEligibleGD  = reportService.getCountEligibleGD(null);
	  Integer fyCountInterview  = reportService.getCountInterview(null);
	  Integer fyCountSelected  = reportService.getCountSelected(null);*/
	  
   
    JSONObject responseJson = new JSONObject();
    try	
    {
      responseJson.put("funnelDetailsCampus", getFunnelJSON(campusCountTotal,campusCountEligibleGD,campusCountInterview,campusCountSelected));
      responseJson.put("Success", "True");
      responseJson.put("Chart1Vals", campusCountTotal+","+campusCountEligibleGD+","+campusCountInterview+","+campusCountSelected+","+campusWINCountSelected);
      handleJSONResponse(response, responseJson);
    }
    catch (JSONException e)
    {
      e.printStackTrace();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }

  private JSONArray getFunnelJSON(Integer campusCountTotal,
		Integer campusCountEligibleGD, Integer campusCountInterview,
		Integer campusCountSelected) throws JSONException {
	  JSONObject jsonobj = new JSONObject();
	  JSONArray funneldetails_sections = new JSONArray();
      jsonobj.put("title", "Appeared for written test");
      jsonobj.put("value", campusCountTotal);
      funneldetails_sections.put(jsonobj);
      jsonobj = new JSONObject();
      jsonobj.put("title", "Shortlisted for GD");
      jsonobj.put("value", campusCountEligibleGD);
      funneldetails_sections.put(jsonobj);
      jsonobj = new JSONObject();
      jsonobj.put("title", "Shortlisted for interviews");
      jsonobj.put("value", campusCountInterview);
      funneldetails_sections.put(jsonobj);
      jsonobj = new JSONObject();
      jsonobj.put("title", "Final selects");
      jsonobj.put("value", campusCountSelected);
      funneldetails_sections.put(jsonobj);
      return funneldetails_sections;
}


  private void handleJSONResponse(HttpServletResponse response, JSONObject json)
    throws JSONException, IOException
  {
    response.setContentType("text/html;charset=UTF-8");
    PrintWriter out = null;
    try {
      out = response.getWriter();
      out.print(json);
      out.flush();
    } catch (IOException localIOException) {
    }
    out.close();
  }
}