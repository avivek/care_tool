package com.deloitte.care.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.deloitte.care.form.Contact;
import com.deloitte.care.service.ContactService;

@Controller
public class ContactController {

	@ModelAttribute("contact")
	public Contact getContactObject() {
		return new Contact();
	}

	@Autowired
	private ContactService contactService;

	/**
	 * Retrieves the add page
	 * 
	 * @return the name of the JSP page
	 */
	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	public String getAdd(Model model) {
		System.out.println("Received request to show add page");

		// Create new Person and add to model
		// This is the formBackingOBject
		model.addAttribute("contactAttribute", new Contact());

		// This will resolve to /WEB-INF/jsp/addpage.jsp
		return "contact";
	}

	/**
	 * Adds a new person by delegating the processing to PersonService. Displays
	 * a confirmation JSP page
	 * 
	 * @return the name of the JSP page
	 */
	@RequestMapping(value = "/contact", method = RequestMethod.POST)
	public String add(@ModelAttribute("contactAttribute") Contact contact) {
		System.out.println("Received request to add new person");
		System.out.println("First Name:" + contact.getFirstname()
				+ "Last Name:" + contact.getLastname());

		// The "personAttribute" model has been passed to the controller from
		// the JSP
		// We use the name "personAttribute" because the JSP uses that name

		// Call PersonService to do the actual adding

		// This will resolve to /WEB-INF/jsp/addedpage.jsp
		return "home";
	}

	// @RequestMapping("/index-to-be-removed")
	// public String listContacts(Map<String, Object> map) {
	//
	// map.put("contact", new ContactController());
	// map.put("contactList", contactService.listContact());
	//
	// return "contact";
	// }
	//
	// @RequestMapping(value = "/add", method = RequestMethod.GET)
	// public String addContact(@ModelAttribute("contact") Contact contact,
	// BindingResult result) {
	//
	// contactService.addContact(contact);
	//
	// return "redirect:/index";
	// }
	//
	// @RequestMapping("/delete/{contactId}")
	// public String deleteContact(@PathVariable("contactId") Integer contactId)
	// {
	//
	// contactService.removeContact(contactId);
	//
	// return "redirect:/index";
	// }

	// @RequestMapping(value = "/contact", method = RequestMethod.POST)
	// public String addContacts(@ModelAttribute("contact") Contact contact,
	// BindingResult result) {
	//
	// System.out.println("First Name:" + contact.getFirstname()
	// + "Last Name:" + contact.getLastname());
	//
	// return "home";
	// }
	//
	// @RequestMapping("/contacts")
	// public ModelAndView showContacts() {
	//
	// return new ModelAndView("contact", "command", new Contact());
	// }
}
