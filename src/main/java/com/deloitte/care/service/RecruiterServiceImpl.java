package com.deloitte.care.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.deloitte.care.dao.RecruiterDAO;
import com.deloitte.care.domain.RecruiterDomain;


@Service("recruiterService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class RecruiterServiceImpl implements RecruiterService {
	
	@Autowired
	private RecruiterDAO recruiterDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addRecruiter(RecruiterDomain recruiter) {
		recruiterDao.addRecruiter(recruiter);
	}
	
	public List<RecruiterDomain> listRecruiter() {
		return recruiterDao.listRecruiter();
	}

	public RecruiterDomain getRecruiter(int recruiterId) {
		return recruiterDao.getRecruiter(recruiterId);
	}
	
	public void deleteRecruiter(RecruiterDomain recruiter) {
		recruiterDao.deleteRecruiter(recruiter);
	}


}
