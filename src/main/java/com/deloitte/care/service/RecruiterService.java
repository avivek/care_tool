package com.deloitte.care.service;

import java.util.List;

import com.deloitte.care.domain.RecruiterDomain;

public interface RecruiterService {
	
	public void addRecruiter(RecruiterDomain recruiter);

	public List<RecruiterDomain> listRecruiter();
	
	public RecruiterDomain getRecruiter(int recruiterId);
	
	public void deleteRecruiter(RecruiterDomain recruiter);

}
