package com.deloitte.care.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.deloitte.care.dao.CampusDAO;
import com.deloitte.care.dao.CampusDetailsDAO;
import com.deloitte.care.dao.CampusHiringDetailsDAO;
import com.deloitte.care.dao.ContactDAO;
import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.domain.CampusDomain;
import com.deloitte.care.domain.CampusHiringDetailsDomain;
import com.deloitte.care.exception.CareException;

@Service("campusHiringDetailsService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CampusHiringDetailsServiceImpl implements CampusHiringDetailsService {

	@Autowired
	private CampusHiringDetailsDAO campusHiringDetailsDao;

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addCampusHiringDetails(CampusHiringDetailsDomain campusHiringdetailsdomain) throws CareException {
		campusHiringDetailsDao.addCampusHiringDetails(campusHiringdetailsdomain);
	}
	
	@Transactional
	public List<CampusHiringDetailsDomain> listCampusHiringDetails() throws CareException {
		System.out.println("list Campus::::");
		return campusHiringDetailsDao.listCampusHiringDetails();
	}
	
	@Transactional
	public CampusHiringDetailsDomain getCampusHiringDetails(int id, int year) throws CareException{
		return campusHiringDetailsDao.getCampusHiringDetails(id ,year);
	}
	
	@Transactional
	public void deleteCampusHiringDetails(CampusHiringDetailsDomain campusHiringdetailsdomain) throws CareException{
		campusHiringDetailsDao.deleteCampusHiringDetails(campusHiringdetailsdomain);
	}

}
