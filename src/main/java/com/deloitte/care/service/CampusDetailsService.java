package com.deloitte.care.service;

import java.util.List;

import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.exception.CareException;

public interface CampusDetailsService {

	public void saveOrUpdateCampusDetails(CampusDetailsDomain campusdetailsdomain) throws CareException;

	public List<CampusDetailsDomain> listCampusDetails() throws CareException;

	public CampusDetailsDomain getCampusDetails(int campusId) throws CareException;

	public void deleteCampusDetails(CampusDetailsDomain campusdetailsdomain) throws CareException;

}
