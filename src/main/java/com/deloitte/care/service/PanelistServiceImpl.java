package com.deloitte.care.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.deloitte.care.dao.PanelistDAO;
import com.deloitte.care.domain.PanelistDomain;
import com.deloitte.care.domain.PanelistMappingDomain;

	
@Service("panelistService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PanelistServiceImpl implements PanelistService {
	
	@Autowired
	private PanelistDAO panelistDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addPanelist(PanelistDomain panelist) {
		panelistDao.addPanelist(panelist);
	}
	
	public List<PanelistDomain> listPanelist() {
		Set<PanelistDomain> pList =null;
		pList = new HashSet(panelistDao.listPanelist());
		return new ArrayList(pList);
	}

	public PanelistDomain getPanelist(int panelistId) {
		return panelistDao.getPanelist(panelistId);
	}
	
	public void deletePanelist(PanelistDomain panelist) {
		panelistDao.deletePanelist(panelist);
	}

	@Override
	public List<PanelistDomain> getPanelistDomain(int userId) {
		// TODO Auto-generated method stub
		return panelistDao.getPanelistDomain(userId);
	}

	public List<PanelistDomain> listCampusPanelistDetails(int campusId) {
		return panelistDao.listCampusPanelistDetails(campusId);
	}
	
	public void deletePanelistMapping(PanelistMappingDomain pmd){	
		
		List<PanelistMappingDomain> pList =null;
		pList=panelistDao.getPanelistForCampusIdAndPanelistId(pmd);
		if(pList!=null && pList.get(0)!=null)
		panelistDao.deletePanelistMapping(pList.get(0));
	}
	
	public void addPanelistMapping(PanelistMappingDomain pmd){
		panelistDao.addPanelistMapping(pmd);
	}
}
