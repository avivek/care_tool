package com.deloitte.care.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.deloitte.care.dao.ReportDAO;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
public class ReportServiceImpl implements ReportService{
	@Autowired
	private ReportDAO reportDAO;

	@Override
	public Integer getCountTotalApplicants(String campusid) {
		return reportDAO.getCountTotalApplicants(campusid);
	}


	@Override
	public Integer getCountEligibleGD(String campusid) {
		return reportDAO.getCountEligibleGD(campusid);
	}


	@Override
	public Integer getCountInterview(String campusid) {
		return reportDAO.getCountInterview(campusid);
	}


	@Override
	public Integer getCountSelected(String campusid) {
		return reportDAO.getCountSelected(campusid);
	}
	
	@Override
	public Integer getCountWINSelected(String campusid) {
		return reportDAO.getCountWINSelected(campusid);
	}
	
}
