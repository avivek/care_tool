package com.deloitte.care.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.deloitte.care.dao.InterviewDetailsDAO;
import com.deloitte.care.domain.InterviewDetailsDomain;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
public class InterviewDetailsServiceImpl implements InterviewDetailsService{
	@Autowired
	private InterviewDetailsDAO interviewDetailsDAO;

	@Override
	public List<InterviewDetailsDomain> listInterviewDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InterviewDetailsDomain getInterviewDetails(int interviewId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InterviewDetailsDomain getInterviewDetailsDomain(int panelistId, String interviewId) {
		// TODO Auto-generated method stub
		return interviewDetailsDAO.getInterviewDetailsDomain(panelistId, interviewId);
	}
	
	@Override
	public void addInterviewRecord(InterviewDetailsDomain interviewDetailsDomain){
		// TODO Auto-generated method stub
		interviewDetailsDAO.addInterviewRecord(interviewDetailsDomain);
	}

	@Override
	public List<InterviewDetailsDomain> getPanelistInterviewList(int panelistId, int campusId) {
		return interviewDetailsDAO.getPanelistInterviewList(panelistId,campusId);
	}
	@Override
	public void updatePanelistInterviewData(InterviewDetailsDomain panelistInterviewDataList) {
		interviewDetailsDAO.updatePanelistInterviewData(panelistInterviewDataList);
	}
	@Override
	public Integer getElgibleCountOnCutoff(String campusid, String cutoff){
		return interviewDetailsDAO.getElgibleCountOnCutoff(campusid , cutoff);
		
	}
	@Override
	public void setInterviewResultFlag(String campusid, String cutoff, String freeze){
		interviewDetailsDAO.setInterviewResultFlag(campusid , cutoff, freeze);
		
	}

	@Override
	public String getInterviewFreezeFlag(String campusid) {
		
		return interviewDetailsDAO.getInterviewFreezeFlag(campusid);
	}
}
