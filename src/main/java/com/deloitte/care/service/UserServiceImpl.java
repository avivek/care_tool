package com.deloitte.care.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.deloitte.care.dao.UserDAO;
import com.deloitte.care.domain.UserDomain;


@Service("userService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDAO userDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addUser(UserDomain user , String role) {
		userDao.addUser(user , role);
	}
	
	public List<UserDomain> listUser() {
		return userDao.listUser();
	}

	public UserDomain getUser(int userId) {
		return userDao.getUser(userId);
	}
	
	@Override
	public UserDomain getUser(String userName) {
		// TODO Auto-generated method stub
		return userDao.getUser(userName);
	}
	
	public void deleteUser(UserDomain user) {
		userDao.deleteUser(user);
	}


}
