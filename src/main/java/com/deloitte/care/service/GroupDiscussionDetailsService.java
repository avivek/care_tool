package com.deloitte.care.service;

import java.util.List;

import com.deloitte.care.domain.GroupDiscussionDetailsDomain;


public interface GroupDiscussionDetailsService {
	
	public List<GroupDiscussionDetailsDomain> listGroupDiscussionDetails(int campusId);
	
	public GroupDiscussionDetailsDomain getGroupDiscussionDetails(int gdId);
	
	public List<GroupDiscussionDetailsDomain> getGroupDiscussionDetailsDomain(int panelistId, String gdName, int campusId);
	
	public List<GroupDiscussionDetailsDomain> getPanelistGroupDiscussionList(int panelistId, int campusId);
	public void finalize(List<GroupDiscussionDetailsDomain> gdGroupList);

	public void addGroupDisscusionRecord(GroupDiscussionDetailsDomain groupDiscussionDetailsDomain);
	public void updatePanelistGDData(List<GroupDiscussionDetailsDomain> panelistGDDataList);
}
