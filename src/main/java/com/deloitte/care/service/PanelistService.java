package com.deloitte.care.service;

import java.util.List;

import com.deloitte.care.domain.PanelistDomain;
import com.deloitte.care.domain.PanelistMappingDomain;

public interface PanelistService {
	
	public void addPanelist(PanelistDomain panelist);

	public List<PanelistDomain> listPanelist();
	
	public PanelistDomain getPanelist(int panelistId);
	
	public List<PanelistDomain> getPanelistDomain(int userId);
	
	public void deletePanelist(PanelistDomain panelist);

	public List<PanelistDomain> listCampusPanelistDetails(int campusId);

	public void deletePanelistMapping(PanelistMappingDomain pmd);

	public void addPanelistMapping(PanelistMappingDomain pmd);

}
