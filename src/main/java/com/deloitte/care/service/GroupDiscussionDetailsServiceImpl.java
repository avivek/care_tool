package com.deloitte.care.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.deloitte.care.dao.GroupDiscussionDetailsDAO;
import com.deloitte.care.domain.GroupDiscussionDetailsDomain;
import com.deloitte.care.domain.InterviewDetailsDomain;

@Service("groupdiscussiondetailsService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
public class GroupDiscussionDetailsServiceImpl implements GroupDiscussionDetailsService{

	@Autowired
	private GroupDiscussionDetailsDAO groupDiscussionDetailsDAO;
	
	@Override
	public List<GroupDiscussionDetailsDomain> listGroupDiscussionDetails(int campusId) {
		return groupDiscussionDetailsDAO.listGroupDiscussionDetails(campusId);
	}

	@Override
	public GroupDiscussionDetailsDomain getGroupDiscussionDetails(int gdId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<GroupDiscussionDetailsDomain> getGroupDiscussionDetailsDomain(int panelistId, String gdName, int campusId) {
		// TODO Auto-generated method stub
		return groupDiscussionDetailsDAO.getGroupDiscussionDetailsDomain(panelistId, gdName, campusId);
	}

	@Override
	public void finalize(List<GroupDiscussionDetailsDomain> gdGroupList){
		groupDiscussionDetailsDAO.finalize(gdGroupList);
	}

	@Override
	public void addGroupDisscusionRecord(GroupDiscussionDetailsDomain groupDiscussionDetailsDomain){
		// TODO Auto-generated method stub
		groupDiscussionDetailsDAO.addGroupDiscussionRecord(groupDiscussionDetailsDomain);
	}

	@Override
	public List<GroupDiscussionDetailsDomain> getPanelistGroupDiscussionList(int panelistId, int campusId) {
		return groupDiscussionDetailsDAO.getPanelistGroupDiscussionList(panelistId,campusId);
	}
	@Override
	public void updatePanelistGDData(List<GroupDiscussionDetailsDomain> panelistGDDataList) {
		groupDiscussionDetailsDAO.updatePanelistGDData(panelistGDDataList);
	}
}
