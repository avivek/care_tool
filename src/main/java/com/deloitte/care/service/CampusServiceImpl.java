package com.deloitte.care.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.deloitte.care.dao.CampusDAO;
import com.deloitte.care.domain.CampusDomain;


@Service("campusService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CampusServiceImpl implements CampusService {
	
	@Autowired
	private CampusDAO campusDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addCampus(CampusDomain campus) {
		campusDao.addCampus(campus);
	}
	
	public List<CampusDomain> listCampuses() {
		System.out.println("list Campus::::");
		return campusDao.listCampuses();
	}

	public CampusDomain getCampus(int campusId) {
		return campusDao.getCampus(campusId);
	}
	
	public CampusDomain getCampus(String campusName) {
		return campusDao.getCampus(campusName);
	}
	
	public void deleteCampus(CampusDomain campus) {
		campusDao.deleteCampus(campus);
	}

	public List<CampusDomain> listCampusesForPanelist(int panelistId) {
		
		return campusDao.listCampusesForPanelist(panelistId);
	}


}
