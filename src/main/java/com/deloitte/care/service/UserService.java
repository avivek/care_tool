package com.deloitte.care.service;

import java.util.List;

import com.deloitte.care.domain.UserDomain;

public interface UserService {
	
	public void addUser(UserDomain user, String role);

	public List<UserDomain> listUser();
	
	public UserDomain getUser(int userId);
	
	public UserDomain getUser(String userName);
	
	public void deleteUser(UserDomain user);

}
