package com.deloitte.care.service;


public interface ReportService {
	
	public Integer getCountTotalApplicants(String campusid);
	public Integer getCountEligibleGD(String campusid);
	public Integer getCountInterview(String campusid);
	public Integer getCountSelected(String campusid);
	public Integer getCountWINSelected(String campusid);
}
