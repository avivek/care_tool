package com.deloitte.care.service;

import java.util.List;

import com.deloitte.care.domain.InterviewDetailsDomain;

public interface InterviewDetailsService {
	
	public List<InterviewDetailsDomain> listInterviewDetails();
	
	public InterviewDetailsDomain getInterviewDetails(int interviewId);
	
	public InterviewDetailsDomain getInterviewDetailsDomain(int panelistId, String interviewId);

	public void addInterviewRecord(InterviewDetailsDomain interviewDetailsDoman);
	
	public List<InterviewDetailsDomain> getPanelistInterviewList(int panelistId, int campusId);
	public void updatePanelistInterviewData(InterviewDetailsDomain panelistInterviewDataList);

	public Integer getElgibleCountOnCutoff(String campusid, String cutoff);

	public void setInterviewResultFlag(String campusid, String cutoff, String freeze);

	public String getInterviewFreezeFlag(String campusid);
}
