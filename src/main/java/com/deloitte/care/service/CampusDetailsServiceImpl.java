package com.deloitte.care.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.deloitte.care.dao.CampusDAO;
import com.deloitte.care.dao.CampusDetailsDAO;
import com.deloitte.care.dao.ContactDAO;
import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.domain.CampusDomain;
import com.deloitte.care.exception.CareException;

@Service("campusDetailsService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CampusDetailsServiceImpl implements CampusDetailsService {

	@Autowired
	private CampusDetailsDAO campusDetailsDao;

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdateCampusDetails(CampusDetailsDomain campusdetailsdomain) throws CareException {
		campusDetailsDao.saveOrUpdateCampusDetails(campusdetailsdomain);
	}
	
	@Transactional
	public List<CampusDetailsDomain> listCampusDetails() throws CareException {
		System.out.println("list Campus::::");
		return campusDetailsDao.listCampusDetails();
	}
	
	@Transactional
	public CampusDetailsDomain getCampusDetails(int campusId) throws CareException{
		return campusDetailsDao.getCampusDetails(campusId);
	}
	
	@Transactional
	public void deleteCampusDetails(CampusDetailsDomain campusdetailsdomain) throws CareException{
		campusDetailsDao.deleteCampusDetails(campusdetailsdomain);
	}

}
