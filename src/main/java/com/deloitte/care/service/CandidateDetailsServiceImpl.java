package com.deloitte.care.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.deloitte.care.dao.CandidateDetailsDAO;
import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.domain.CandidateDetailsDomain;
import com.deloitte.care.exception.CareException;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CandidateDetailsServiceImpl implements CandidateDetailsService{
	
	@Autowired
	private CandidateDetailsDAO candidateDetailsDAO;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addCandidateDetails(CandidateDetailsDomain candidateDetailsDomain) throws CareException {
		// TODO Auto-generated method stub
		candidateDetailsDAO.addCandidateDetails(candidateDetailsDomain);
	}

	@Override
	public List<CandidateDetailsDomain> listCandidateDetails(int id  , String filterCriteria) throws CareException {
		// TODO Auto-generated method stub
		System.out.println("Calling DAO for getting the list of Candidate details>>>");
		return candidateDetailsDAO.listCandidateDetails(id  , filterCriteria);
	}

	@Override
	public CandidateDetailsDomain getCandidateDetails(int candidateId) throws CareException {
		// TODO Auto-generated method stub
		return candidateDetailsDAO.getCandidateDetails(candidateId);
	}
	
	@Override
	public Integer getCandidateDetailsAcmatID(BigInteger acmatID) throws CareException {
		// TODO Auto-generated method stub
		return candidateDetailsDAO.getCandidateDetailsAcmatID(acmatID);
	}

	@Override
	public void deleteCandidateDetails(CandidateDetailsDomain candidateDetailsDomain) throws CareException{
		// TODO Auto-generated method stub
		candidateDetailsDAO.deleteCandidateDetails(candidateDetailsDomain);
	}

	@Override
	public void updateCandidateDetailsUsingCutOff(Integer campusId,
			BigDecimal aptMin, BigDecimal engMin, BigDecimal logMin) {
		// TODO Auto-generated method stub
		candidateDetailsDAO.updateCandidateDetailsUsingCutOff(campusId,aptMin,engMin,logMin);
	}

	@Override
	public List<CandidateDetailsDomain> listCandidatesOnCampus(
			CampusDetailsDomain campusDetailsDomain) throws CareException {
		// TODO Auto-generated method stub
		return candidateDetailsDAO.listCandidatesOnCampus(campusDetailsDomain);
		 
	}

	@Override
	public void updateCandidateDetails(List<CandidateDetailsDomain> cdd) {		
		candidateDetailsDAO.updateCandidateDetails(cdd);
	}

}
