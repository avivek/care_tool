package com.deloitte.care.service;

import java.util.List;

import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.domain.CampusHiringDetailsDomain;
import com.deloitte.care.exception.CareException;

public interface CampusHiringDetailsService {

	public void addCampusHiringDetails(CampusHiringDetailsDomain campusHiringdetailsdomain) throws CareException;

	public List<CampusHiringDetailsDomain> listCampusHiringDetails() throws CareException;

	public CampusHiringDetailsDomain getCampusHiringDetails(int id , int year) throws CareException;

	public void deleteCampusHiringDetails(CampusHiringDetailsDomain campusHiringdetailsdomain) throws CareException;

}
