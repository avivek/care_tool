package com.deloitte.care.service;

import java.util.List;

import com.deloitte.care.form.Contact;

public interface ContactService {
	
	public void addContact(Contact contact);
	public List<Contact> listContact();
	public void removeContact(Integer id);
}
