package com.deloitte.care.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.domain.CandidateDetailsDomain;
import com.deloitte.care.exception.CareException;

public interface CandidateDetailsDAO {
	
	public void addCandidateDetails(CandidateDetailsDomain candidateDetailsDomain) throws CareException;

	public List<CandidateDetailsDomain> listCandidateDetails(int id, String filterCriteria) throws CareException;

	public CandidateDetailsDomain getCandidateDetails(int candidateId) throws CareException;
	
	public Integer getCandidateDetailsAcmatID(BigInteger acmatId) throws CareException;
	
	public void deleteCandidateDetails(CandidateDetailsDomain candidateDetailsDomain) throws CareException;

	public void updateCandidateDetailsUsingCutOff(Integer campusId,
			BigDecimal aptMin, BigDecimal engMin, BigDecimal logMin);

	public List<CandidateDetailsDomain> listCandidatesOnCampus(CampusDetailsDomain campusDetailsDomain);

	public void updateCandidateDetails(List<CandidateDetailsDomain> cdd);
}
