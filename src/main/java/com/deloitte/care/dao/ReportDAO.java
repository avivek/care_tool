package com.deloitte.care.dao;


public interface ReportDAO {
	
	public Integer getCountTotalApplicants(String campusid);
	public Integer getCountEligibleGD(String campusid);
	public Integer getCountInterview(String campusid);
	public Integer getCountSelected(String campusid);
	public Integer getCountWINSelected(String campusid);
}
