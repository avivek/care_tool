package com.deloitte.care.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.deloitte.care.controller.GroupDiscussionController;
import com.deloitte.care.domain.GroupDiscussionDetailsDomain;
import com.deloitte.care.domain.PanelistDomain;

@Repository("groupdiscussiondetailsDao")
public class GroupDiscussionDetailsDAOImpl implements GroupDiscussionDetailsDAO{

	static final Logger logger = Logger.getLogger(GroupDiscussionDetailsDAOImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<GroupDiscussionDetailsDomain> listGroupDiscussionDetails(int campusId) {

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(GroupDiscussionDetailsDomain.class, "gdDetailDomain");
		criteria.createAlias("gdDetailDomain.campusdomain", "campusdomain");
		criteria.add(Restrictions.eq("campusdomain.id", campusId ));
		return (List<GroupDiscussionDetailsDomain>) criteria.list();
	}

	@Override
	public GroupDiscussionDetailsDomain getGroupDiscussionDetails(int gdId){
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GroupDiscussionDetailsDomain> getGroupDiscussionDetailsDomain(int panelistId, String gDName, int campusId)  {
		// TODO Auto-generated method stub
		List<GroupDiscussionDetailsDomain> gdDomain = null;
		gdDomain= (List<GroupDiscussionDetailsDomain>) sessionFactory
				.getCurrentSession()
				.createCriteria(GroupDiscussionDetailsDomain.class, "gdDetail")
				.add(Restrictions.eq("panelistDomain.id", panelistId))
				.add(Restrictions.eq("gdDetail.gdName", gDName))
				.createAlias("gdDetail.campusdomain", "campusdomain")
				.add(Restrictions.eq("campusdomain.id", campusId ))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return gdDomain;
	}
	

	@Override
	public void finalize(List<GroupDiscussionDetailsDomain> gdGroupList){

		for (GroupDiscussionDetailsDomain groupDiscussionDetailsDomain : gdGroupList) {
			addGroupDiscussionRecord(groupDiscussionDetailsDomain);
		}
	}
	
	@Override
	public void addGroupDiscussionRecord(GroupDiscussionDetailsDomain groupDiscussionDetailsDomain)
	{
		sessionFactory.getCurrentSession().saveOrUpdate(groupDiscussionDetailsDomain);
	}

	@Override
	public List<GroupDiscussionDetailsDomain> getPanelistGroupDiscussionList(int panelistId,int campusId) {
		@SuppressWarnings("unchecked")
		List<GroupDiscussionDetailsDomain> distinctGDList = (List<GroupDiscussionDetailsDomain>)sessionFactory.getCurrentSession()
				.createCriteria(GroupDiscussionDetailsDomain.class)
				.add(Restrictions.eq("panelistDomain.id", panelistId))
				.add(Restrictions.eq("campusdomain.id", campusId))
				.setProjection(Projections.distinct(Projections.property("gdName"))).list();
		return distinctGDList;
	}
	@Override
	public void updatePanelistGDData(List<GroupDiscussionDetailsDomain> panelistGDDataList) {
		Session currentSession = sessionFactory.getCurrentSession();
		logger.info("Entered : updatePanelistGDData()");
		try{
			for(GroupDiscussionDetailsDomain tempPanelistGDDataList : panelistGDDataList)	
			{
				
				GroupDiscussionDetailsDomain currentGDetailDomain = (GroupDiscussionDetailsDomain) currentSession.get(GroupDiscussionDetailsDomain.class, tempPanelistGDDataList.getId());
						
				if(currentGDetailDomain != null)
				{
					//currentGDetailDomain.setId(tempPanelistGDDataList.getId());
					currentGDetailDomain.setLeadership(tempPanelistGDDataList.getLeadership());
					currentGDetailDomain.setListening(tempPanelistGDDataList.getListening());
					currentGDetailDomain.setParticipation(tempPanelistGDDataList.getParticipation());
					currentGDetailDomain.setApproach(tempPanelistGDDataList.getApproach());
					currentGDetailDomain.setPresentation(tempPanelistGDDataList.getPresentation());
					currentGDetailDomain.setResponse(tempPanelistGDDataList.getResponse());
					currentGDetailDomain.setGdFreeze(tempPanelistGDDataList.getGdFreeze());
					int total = (Integer.parseInt(tempPanelistGDDataList.getLeadership()) + 
									Integer.parseInt(tempPanelistGDDataList.getListening()) + 
									Integer.parseInt(tempPanelistGDDataList.getParticipation()) + 
									Integer.parseInt(tempPanelistGDDataList.getApproach()) + 
									Integer.parseInt(tempPanelistGDDataList.getPresentation()) + 
									Integer.parseInt(tempPanelistGDDataList.getResponse()) );
					currentGDetailDomain.setTotal(total);
					currentGDetailDomain.setComments(tempPanelistGDDataList.getComments());
					logger.debug("Updating Group descussion details for Group :: "+currentGDetailDomain.getGdName());
					currentSession.saveOrUpdate(currentGDetailDomain);
				}
			}
		}catch (Exception exp){
			logger.error("Error occured while savign GroupDetails "+exp.getMessage());
			exp.printStackTrace();
			//TO-DO throw back with proper wrapper exception.
		}finally{
			
		}
		logger.info("Exit : updatePanelistGDData()");
	}
}
