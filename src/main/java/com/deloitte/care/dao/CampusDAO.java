package com.deloitte.care.dao;

import java.util.List;

import com.deloitte.care.domain.CampusDomain;

public interface CampusDAO {
	
	public void addCampus(CampusDomain campus);

	public List<CampusDomain> listCampuses();
	
	public List<CampusDomain> listCampusesForPanelist(int panelistId);
	
	public CampusDomain getCampus(int campusId);
	
	public CampusDomain getCampus(String campusName);
	
	public void deleteCampus(CampusDomain campus);

}
