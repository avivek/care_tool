package com.deloitte.care.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.deloitte.care.domain.CampusDomain;
import com.deloitte.care.domain.PanelistMappingDomain;


@Repository("campusDao")
public class CampusDAOImpl implements CampusDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void addCampus(CampusDomain campus) {
		sessionFactory.getCurrentSession().saveOrUpdate(campus);
	}

	@SuppressWarnings("unchecked")
	public List<CampusDomain> listCampuses() {
		List<CampusDomain> temp = (List<CampusDomain>) sessionFactory.getCurrentSession().createCriteria(CampusDomain.class).list();
		return temp ;
	}
	
	public List<CampusDomain> listCampusesForPanelist(int panelistId) {
		List<PanelistMappingDomain> panelistMappingDomainList = (List<PanelistMappingDomain>) sessionFactory.getCurrentSession().createCriteria(PanelistMappingDomain.class).add(Restrictions.eq("panelist_id", panelistId)).list();
		List<Integer> campusIdList = new ArrayList<Integer> ();
		for(PanelistMappingDomain d :panelistMappingDomainList){			
			campusIdList.add(d.getCampus_id());		
		}
		List<CampusDomain> campusDomainList = (List<CampusDomain>) sessionFactory.getCurrentSession().createCriteria(CampusDomain.class).add(Restrictions.in("id", campusIdList)).list();
		return campusDomainList ;
	}

	public CampusDomain getCampus(int campusId) {
		CampusDomain temp = (CampusDomain) sessionFactory.getCurrentSession().get(CampusDomain.class, campusId);
		return (CampusDomain) sessionFactory.getCurrentSession().get(CampusDomain.class, campusId);
	}
	
	public CampusDomain getCampus(String campusName) {
		CampusDomain temp = null;
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CampusDomain.class).add(Restrictions.eq("campusName", campusName));
		if (criteria != null)
		{
			if (!criteria.list().isEmpty())
			{
				temp = (CampusDomain)criteria.list().get(0);
				System.out.println("in DAO:::::"+temp);
			}
		}
		return temp;
	}

	public void deleteCampus(CampusDomain campus) {
		System.out.println("1:::::"+campus.getId());
		sessionFactory.getCurrentSession().createQuery("DELETE FROM CampusDomain WHERE campus_id = "+campus.getId()).executeUpdate();
	}

}
