package com.deloitte.care.dao;

import java.math.BigInteger;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("reportDao")
public class ReportDAOImpl implements ReportDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Integer getCountTotalApplicants(String campusid) {
		Query query = null;
		if(null!=campusid){
			query = sessionFactory.getCurrentSession().createSQLQuery("select count(*) from candidate_details "
					+ "where campus_id = :campusid " )
					.setInteger("campusid", Integer.parseInt(campusid));
		}else{
			query = sessionFactory.getCurrentSession().createSQLQuery("select count(*) from candidate_details ");
		}
		
		List<BigInteger> list = (List<BigInteger>) query.list();
		return ((BigInteger) list.get(0)).intValue() ;
	}


	@Override
	public Integer getCountEligibleGD(String campusid) {
		Query query = null;
		if(null!=campusid){
			query = sessionFactory.getCurrentSession().createSQLQuery("select count(*) from candidate_details "
					+ "where campus_id = :campusid and finalizedForGd ='Y'" )
					.setInteger("campusid", Integer.parseInt(campusid));
		}else{
			query = sessionFactory.getCurrentSession().createSQLQuery("select count(*) from candidate_details " +
					" where finalizedForGd ='Y'");
		}
		
		List<BigInteger> list = (List<BigInteger>) query.list();
		return ((BigInteger) list.get(0)).intValue() ;
	}


	@Override
	public Integer getCountInterview(String campusid) {
		Query query = null;
		if(null!=campusid){
			query = sessionFactory.getCurrentSession().createSQLQuery("select count(*) from candidate_details "
					+ "where campus_id = :campusid and interviewResult ='Y'" )
					.setInteger("campusid", Integer.parseInt(campusid));
		}else{
			query = sessionFactory.getCurrentSession().createSQLQuery("select count(*) from candidate_details " +
					" where interviewResult ='Y'");
		}
		
		List<BigInteger> list = (List<BigInteger>) query.list();
		return ((BigInteger) list.get(0)).intValue() ;
	}


	@Override
	public Integer getCountSelected(String campusid) {
		Query query = null;
		if(null!=campusid){
			query = sessionFactory.getCurrentSession().createSQLQuery("select count(*) from candidate_details "
					+ "where campus_id = :campusid and consensusResult ='Offer'" )
					.setInteger("campusid", Integer.parseInt(campusid));
		}else{
			query = sessionFactory.getCurrentSession().createSQLQuery("select count(*) from candidate_details " +
					" where consensusResult ='Offer'");
		}
		
		List<BigInteger> list = (List<BigInteger>) query.list();
		return ((BigInteger) list.get(0)).intValue() ;
	}
	
	@Override
	public Integer getCountWINSelected(String campusid) {
		Query query = null;
		if(null!=campusid){
			query = sessionFactory.getCurrentSession().createSQLQuery("select count(*) from candidate_details "
					+ "where campus_id = :campusid and consensusResult ='Offer' and gender = 'Female'" )
					.setInteger("campusid", Integer.parseInt(campusid));
		}else{
			query = sessionFactory.getCurrentSession().createSQLQuery("select count(*) from candidate_details " +
					" where consensusResult ='Offer' and gender = 'Female'");
		}
		
		List<BigInteger> list = (List<BigInteger>) query.list();
		return ((BigInteger) list.get(0)).intValue() ;
	}
	

	
}
