package com.deloitte.care.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.deloitte.care.domain.PanelistDomain;
import com.deloitte.care.domain.PanelistMappingDomain;



@Repository("panelistDao")
public class PanelistDAOImpl implements PanelistDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void addPanelist(PanelistDomain panelist) {
		sessionFactory.getCurrentSession().saveOrUpdate(panelist);
	}

	@SuppressWarnings("unchecked")
	public List<PanelistDomain> listPanelist() {
		List<PanelistDomain> temp = (List<PanelistDomain>) sessionFactory.getCurrentSession().createCriteria(PanelistDomain.class).list();
		return temp ;
	}
	
	public List<PanelistDomain> getPanelistDomain(int userId) {
		List<PanelistDomain> panelistDomain = (List<PanelistDomain>) sessionFactory.getCurrentSession().createCriteria(PanelistDomain.class).createAlias("userDomain", "userDomain",
				Criteria.INNER_JOIN).add(Restrictions.eq("userDomain.user_id", userId)).list();
		return panelistDomain ;
	}

	public PanelistDomain getPanelist(int panelistId) {
		PanelistDomain temp = (PanelistDomain) sessionFactory.getCurrentSession().get(PanelistDomain.class, panelistId);
		return temp;
	}
	

	public void deletePanelist(PanelistDomain panelist) {
		System.out.println("1:::::"+panelist.getId());
		sessionFactory.getCurrentSession().createQuery("DELETE FROM PanelistDomain WHERE id = "+panelist.getId()).executeUpdate();
		
		/*PanelistDomain pd = (PanelistDomain ) sessionFactory.getCurrentSession().createCriteria(PanelistDomain.class).add(Restrictions.eq("id", panelist.getId())).uniqueResult();
		sessionFactory.getCurrentSession().delete(pd);*/
		
	}


	public List<PanelistDomain> listCampusPanelistDetails(int campusId) {
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PanelistDomain.class, "panelist");
		criteria.createAlias("panelist.mappings", "mappings");
		criteria.add(Restrictions.eq("mappings.campus_id", campusId ));
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		
	List<PanelistDomain> listCampusPanelistDetails = (List<PanelistDomain>) criteria.list();
	
	return listCampusPanelistDetails;
	}
	
	public void deletePanelistMapping(PanelistMappingDomain pmd){
		sessionFactory.getCurrentSession().createQuery("DELETE FROM PanelistMappingDomain WHERE id = "+pmd.getId()).executeUpdate();
	}
	
	public List<PanelistMappingDomain> getPanelistForCampusIdAndPanelistId(PanelistMappingDomain pmd){
		
		Criteria query = sessionFactory.getCurrentSession().createCriteria(PanelistMappingDomain.class);
		if(pmd.getCampus_id() != 0)
		{
			query.add(Restrictions.eq("campus_id",pmd.getCampus_id()));			
		}
		if(pmd.getPanelist_id() != 0){
			query.add(Restrictions.eq("panelist_id",pmd.getPanelist_id()));
		}
				
		return (List<PanelistMappingDomain>)query.list();		
	}

	public void addPanelistMapping(PanelistMappingDomain pmd){
		sessionFactory.getCurrentSession().saveOrUpdate(pmd);
	}

}
