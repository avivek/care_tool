package com.deloitte.care.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.domain.CandidateDetailsDomain;
import com.deloitte.care.exception.CareException;

@Repository("candidateDetailsDao")
public class CandidateDetailsDAOImpl implements CandidateDetailsDAO{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addCandidateDetails(CandidateDetailsDomain candidateDetailsDomain) throws CareException {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(candidateDetailsDomain);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CandidateDetailsDomain> listCandidateDetails(int campusId  , String filterCriteria) throws CareException {
		// TODO Auto-generated method stub
		
		Criteria query = sessionFactory.getCurrentSession().createCriteria(CandidateDetailsDomain.class);
		if(campusId != 0)
		{
			query.createAlias("campusdomain","campusdomain",Criteria.INNER_JOIN);
			query.add(Restrictions.eq("campusdomain.id",campusId));			
		}
		
		if(filterCriteria != null && !filterCriteria.isEmpty())
		{
			if(filterCriteria.equals("groupDiscussionResult")){
				query.add(Restrictions.eq("groupDiscussionResult","Y"));				
			}
			
			if(filterCriteria.equals("interviewResult")){
				query.add(Restrictions.eq("interviewResult","Y"));				
			}
			
			if(filterCriteria.equals("candidateFreeze")){
				query.add(Restrictions.eq("finalizedForGd","Y"));				
			}
					
		}		
			query.addOrder(Order.asc("firstName"));
		
		return (List<CandidateDetailsDomain>)query.list();
	}

	@Override
	public CandidateDetailsDomain getCandidateDetails(int candidateId) throws CareException {
		// TODO Auto-generated method stub
		return (CandidateDetailsDomain)sessionFactory.getCurrentSession().get(CandidateDetailsDomain.class, candidateId);
	}
	
	@Override
	public Integer getCandidateDetailsAcmatID(BigInteger amcatID) throws CareException {
		CandidateDetailsDomain temp = null;
		Integer candidateID = null;
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CandidateDetailsDomain.class).add(Restrictions.eq("amcatID", amcatID));
		if (criteria != null)
		{
			if (!criteria.list().isEmpty())
			{
				temp = (CandidateDetailsDomain)criteria.list().get(0);
				System.out.println("in DAO:::::"+temp);
				candidateID = temp.getCandidateId();
			}
		}
		return candidateID;
		
	}

	@Override
	public void deleteCandidateDetails(CandidateDetailsDomain candidateDetailsDomain)  throws CareException{
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().createQuery("DELETE FROM CandidateDetailsDomain WHERE candidate_id = "+ candidateDetailsDomain.getCandidateId()).executeUpdate();
	}

	@Override
	public void updateCandidateDetailsUsingCutOff(Integer campusId,
			BigDecimal aptMin, BigDecimal engMin, BigDecimal logMin) {
		
		StringBuffer query= new StringBuffer("update CandidateDetailsDomain set finalizedForGd='Y' where campus_id = ");
		query.append(campusId);
//		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CandidateDetailsDomain.class).add(Restrictions.eq("campus_id", campusId));
		
		if(aptMin.doubleValue()>0){
		//	criteria.add(Restrictions.eq("quantitativeAbilityScore",aptMin));
			query.append(" and quantitativeAbilityScore >= "+aptMin);
			
		}
		if(engMin.doubleValue()>0){
		//	criteria.add(Restrictions.eq("englishScore",engMin));
			query.append(" and englishScore >= "+engMin);
		}
		if(logMin.doubleValue()>0){
	//		criteria.add(Restrictions.eq("logicalAbilityScore",logMin));
			query.append(" and logicalAbilityScore >= "+logMin);
		}
//		if(aptMin.doubleValue()>0||engMin.doubleValue()>0||logMin.doubleValue()>0){
			sessionFactory.getCurrentSession().createQuery(query.toString()).executeUpdate();				
//		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CandidateDetailsDomain> listCandidatesOnCampus(
			CampusDetailsDomain campusDetailsDomain) {
		// TODO Auto-generated method stub
		
		Criteria query = sessionFactory.getCurrentSession().createCriteria(CandidateDetailsDomain.class);
		if(campusDetailsDomain.getId() != 0)
		{
			query.createAlias("campusdomain","campusdomain",Criteria.INNER_JOIN);
			query.add(Restrictions.eq("campusdomain.id",campusDetailsDomain.getId()));			
		}
		return (List<CandidateDetailsDomain>)query.list();
	}

	@Override
	public void updateCandidateDetails(List<CandidateDetailsDomain> cdd) {		
		/*CandidateDetailsDomain []cd = (CandidateDetailsDomain[]) cdd.toArray();
		int i=0;
		while(cdd!=null && cdd.size()>0){
			cd[i].setFinalizedForGd("Y");
			i++;
			sessionFactory.getCurrentSession().saveOrUpdate(cd[i]);
		}	*/
		for (CandidateDetailsDomain candidateDetailsDomain : cdd) {
			candidateDetailsDomain.setFinalizedForGd("Y");
			sessionFactory.getCurrentSession().saveOrUpdate(candidateDetailsDomain);
		}
	}

}
