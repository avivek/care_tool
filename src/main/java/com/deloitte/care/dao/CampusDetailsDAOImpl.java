package com.deloitte.care.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.domain.CampusDomain;
import com.deloitte.care.exception.CareException;

@Repository("campusDetailsDao")
public class CampusDetailsDAOImpl implements CampusDetailsDAO {

	@Autowired
	private SessionFactory sessionFactory;
 
	public void saveOrUpdateCampusDetails(CampusDetailsDomain campusdetailsdomain) throws CareException {
		sessionFactory.getCurrentSession().saveOrUpdate(campusdetailsdomain);
	}

	@SuppressWarnings("unchecked")
	public List<CampusDetailsDomain> listCampusDetails() throws CareException{
		return (List<CampusDetailsDomain>) sessionFactory.getCurrentSession()
				.createCriteria(CampusDetailsDomain.class).list();
	}

	public CampusDetailsDomain getCampusDetails(int campusId) throws CareException{
		CampusDomain temp = (CampusDomain) sessionFactory.getCurrentSession()
				.get(CampusDomain.class, campusId);
		System.out.println("In dao Impl:::" + temp.getCampusName());
		return (CampusDetailsDomain) sessionFactory.getCurrentSession().get(
				CampusDetailsDomain.class, campusId);
	}

	public void deleteCampusDetails(CampusDetailsDomain campusdetailsdomain) throws CareException{
		sessionFactory
				.getCurrentSession()
				.createQuery(
						"DELETE FROM CampusDetailsDomain WHERE id = "
								+ campusdetailsdomain.getId()).executeUpdate();
	}
}
