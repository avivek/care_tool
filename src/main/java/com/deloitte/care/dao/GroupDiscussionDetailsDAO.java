package com.deloitte.care.dao;

import java.util.List;

import com.deloitte.care.domain.GroupDiscussionDetailsDomain;
import com.deloitte.care.exception.CareException;

public interface GroupDiscussionDetailsDAO {

	public List<GroupDiscussionDetailsDomain> listGroupDiscussionDetails(int campusId);
	
	public GroupDiscussionDetailsDomain getGroupDiscussionDetails(int gdId);
	
	public List<GroupDiscussionDetailsDomain> getGroupDiscussionDetailsDomain(int panelistId, String gdName, int campusId);
	
	public void finalize(List<GroupDiscussionDetailsDomain> gdGroupList);

	public void addGroupDiscussionRecord(GroupDiscussionDetailsDomain groupDiscussionDetailsDomain);
	public List<GroupDiscussionDetailsDomain> getPanelistGroupDiscussionList(int panelistId, int campusId);
	public void updatePanelistGDData(List<GroupDiscussionDetailsDomain> panelistGDDataList);

}
