package com.deloitte.care.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.domain.CampusDomain;
import com.deloitte.care.domain.CampusHiringDetailsDomain;
import com.deloitte.care.exception.CareException;

@Repository("campusHiringDetailsDao")
public class CampusHiringDetailsDAOImpl implements CampusHiringDetailsDAO {

	@Autowired
	private SessionFactory sessionFactory;
 
	public void addCampusHiringDetails(CampusHiringDetailsDomain campusHiringdetailsdomain) throws CareException {
		System.out.println("1::::::::::"+campusHiringdetailsdomain.getOfferedStudents());
		System.out.println("1::::::::::"+campusHiringdetailsdomain.getConversionRate());
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select * from campus_hiring_details  where id = :id and yearStat = :yearStat").addEntity(CampusHiringDetailsDomain.class)
				.setInteger("id", campusHiringdetailsdomain.getId())
				.setInteger("yearStat", campusHiringdetailsdomain.getYear());
				List result = query.list();
				if (result.isEmpty())
				{
					Query queryInsert = sessionFactory.getCurrentSession().createSQLQuery("insert into campus_hiring_details (id,offeredStudents, conversionRate ,winRate ,sample ,"+
													"slots, interns, yearStat ,createdBy ,createdDt,updatedBy,updatedDt) values(:id, :offeredStudents, :conversionRate ,:winRate, :sample,"+
													":slots, :interns, :yearStat ,:createdBy ,:createdDt, :updatedBy, :updatedDt)")
							.addEntity(CampusHiringDetailsDomain.class)
							.setInteger("id", campusHiringdetailsdomain.getId())
							.setInteger("offeredStudents", campusHiringdetailsdomain.getOfferedStudents())
							.setFloat("conversionRate", campusHiringdetailsdomain.getConversionRate())
							.setInteger("winRate", campusHiringdetailsdomain.getWinRate())
							.setInteger("sample", campusHiringdetailsdomain.getSample())
							.setInteger("slots", campusHiringdetailsdomain.getSlot())
							.setInteger("interns", campusHiringdetailsdomain.getInterns())
							.setInteger("yearStat", campusHiringdetailsdomain.getYear())
							.setParameter("createdBy", campusHiringdetailsdomain.getCreatedBy())
							.setDate("createdDt", campusHiringdetailsdomain.getCreatedDt())
							.setParameter("updatedBy", campusHiringdetailsdomain.getCreatedBy())
							.setDate("updatedDt", campusHiringdetailsdomain.getCreatedDt());
					queryInsert.executeUpdate();
				}
				else
				{
					
					Query queryUpdate = sessionFactory.getCurrentSession().createSQLQuery("update campus_hiring_details SET  offeredStudents = :offeredStudents, "
							+ "conversionRate =:conversionRate, winRate =:winRate, sample =:sample,"+
							"slots = :slots, interns = :interns ,createdBy = :createdBy, createdDt = :createdDt,"
							+ "updatedBy = :updatedBy, updatedDt =:updatedDt where id = :id and yearStat = :yearStat")
							.addEntity(CampusHiringDetailsDomain.class)
							.setInteger("offeredStudents", campusHiringdetailsdomain.getOfferedStudents())
							.setFloat("conversionRate", campusHiringdetailsdomain.getConversionRate())
							.setInteger("winRate", campusHiringdetailsdomain.getWinRate())
							.setInteger("sample", campusHiringdetailsdomain.getSample())
							.setInteger("slots", campusHiringdetailsdomain.getSlot())
							.setInteger("interns", campusHiringdetailsdomain.getInterns())
							.setInteger("yearStat", campusHiringdetailsdomain.getYear())
							.setParameter("createdBy", campusHiringdetailsdomain.getCreatedBy())
							.setDate("createdDt", campusHiringdetailsdomain.getCreatedDt())
							.setParameter("updatedBy", campusHiringdetailsdomain.getCreatedBy())
							.setDate("updatedDt", campusHiringdetailsdomain.getCreatedDt())
							.setInteger("id", campusHiringdetailsdomain.getId());
					queryUpdate.executeUpdate();
					
				}
	}

	@SuppressWarnings("unchecked")
	public List<CampusHiringDetailsDomain> listCampusHiringDetails() throws CareException{
		return (List<CampusHiringDetailsDomain>) sessionFactory.getCurrentSession()
				.createCriteria(CampusHiringDetailsDomain.class).list();
	}

	public CampusHiringDetailsDomain getCampusHiringDetails(int id, int year) throws CareException{
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select * from campus_hiring_details  where id = :id and yearStat = :yearStat").addEntity(CampusHiringDetailsDomain.class)
				.setInteger("id", id)
				.setInteger("yearStat", year);
				List result = query.list();
				CampusHiringDetailsDomain campusHiringDetailsDomain = null;
				if (!result.isEmpty())
				{
					System.out.println("1:::::::::::"+(CampusHiringDetailsDomain)result.get(0));
					campusHiringDetailsDomain = (CampusHiringDetailsDomain)result.get(0);
				}
				
				return campusHiringDetailsDomain;

	}

	public void deleteCampusHiringDetails(CampusHiringDetailsDomain campusHiringdetailsdomain) throws CareException{
		System.out.println("id::::::::"+campusHiringdetailsdomain.getId());
		sessionFactory.getCurrentSession().createSQLQuery(
						"DELETE FROM campus_hiring_details WHERE id = "+ campusHiringdetailsdomain.getId()).executeUpdate();
	}
}
