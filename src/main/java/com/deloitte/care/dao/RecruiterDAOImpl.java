package com.deloitte.care.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.deloitte.care.domain.RecruiterDomain;


@Repository("recruiterDao")
public class RecruiterDAOImpl implements RecruiterDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void addRecruiter(RecruiterDomain recruiter) {
		sessionFactory.getCurrentSession().saveOrUpdate(recruiter);
	}

	@SuppressWarnings("unchecked")
	public List<RecruiterDomain> listRecruiter() {
		List<RecruiterDomain> temp = (List<RecruiterDomain>) sessionFactory.getCurrentSession().createCriteria(RecruiterDomain.class).list();
		return temp ;
	}

	public RecruiterDomain getRecruiter(int recruiterId) {
		RecruiterDomain temp = (RecruiterDomain) sessionFactory.getCurrentSession().get(RecruiterDomain.class, recruiterId);
		return temp;
	}
	

	public void deleteRecruiter(RecruiterDomain recruiter) {
		System.out.println("1:::::"+recruiter.getId());
		sessionFactory.getCurrentSession().createQuery("DELETE FROM RecruiterDomain WHERE id = "+recruiter.getId()).executeUpdate();
	}



}
