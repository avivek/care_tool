package com.deloitte.care.dao;

import java.util.List;

import com.deloitte.care.domain.PanelistDomain;
import com.deloitte.care.domain.PanelistMappingDomain;

public interface PanelistDAO {
	
	public void addPanelist(PanelistDomain panelist);

	public List<PanelistDomain> listPanelist();
	
	public PanelistDomain getPanelist(int panelistId);
	
	public void deletePanelist(PanelistDomain panelist);
	
	public List<PanelistDomain> getPanelistDomain(int userId);

	public List<PanelistDomain> listCampusPanelistDetails(int campusId);

	public void deletePanelistMapping(PanelistMappingDomain pmd);
	
	public List<PanelistMappingDomain> getPanelistForCampusIdAndPanelistId(PanelistMappingDomain pmd);

	public void addPanelistMapping(PanelistMappingDomain pmd);

}
