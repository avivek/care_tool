package com.deloitte.care.dao;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.domain.CandidateDetailsDomain;
import com.deloitte.care.domain.InterviewDetailsDomain;

@Repository("interviewdetailsDao")
public class InterviewDetailsDAOImpl implements InterviewDetailsDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<InterviewDetailsDomain> listInterviewDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InterviewDetailsDomain getInterviewDetails(int interviewId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InterviewDetailsDomain getInterviewDetailsDomain(int panelistId, String interviewId) {
		// TODO Auto-generated method stub
		return (InterviewDetailsDomain)sessionFactory.getCurrentSession().get(InterviewDetailsDomain.class, new Integer(Integer.parseInt(interviewId)));
	}
	
	@Override
	public void addInterviewRecord(InterviewDetailsDomain interviewDetailsDomain){
		
		sessionFactory.getCurrentSession().saveOrUpdate(interviewDetailsDomain);
	}

	@Override
	public List<InterviewDetailsDomain> getPanelistInterviewList(int panelistId, int campusId) {
		@SuppressWarnings("unchecked")
		List<InterviewDetailsDomain> distinctInterviewList = (List<InterviewDetailsDomain>)sessionFactory.getCurrentSession().createCriteria(InterviewDetailsDomain.class)
		.add(Restrictions.eq("panelistDomain.id", panelistId))
		.add(Restrictions.eq("campusdomain.id", campusId))
		.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		
		return distinctInterviewList;
	}
	@Override
	public void updatePanelistInterviewData(InterviewDetailsDomain panelistInterviewDataList) {
		Session currentSession = sessionFactory.getCurrentSession();
		InterviewDetailsDomain currentInterviewDetailDomain = (InterviewDetailsDomain)currentSession.get(InterviewDetailsDomain.class, new Integer(panelistInterviewDataList.getId()));
		if(currentInterviewDetailDomain != null)
		{
			currentInterviewDetailDomain.setLeadership(panelistInterviewDataList.getLeadership());
			currentInterviewDetailDomain.setService(panelistInterviewDataList.getService());
			currentInterviewDetailDomain.setCommunication(panelistInterviewDataList.getCommunication());
			currentInterviewDetailDomain.setManagement(panelistInterviewDataList.getManagement());
			currentInterviewDetailDomain.setLeadership(panelistInterviewDataList.getLeadership());
			currentInterviewDetailDomain.setQualification(panelistInterviewDataList.getQualification());
			currentInterviewDetailDomain.setMotivators(panelistInterviewDataList.getMotivators());
			currentInterviewDetailDomain.setObjectives(panelistInterviewDataList.getObjectives());
			currentInterviewDetailDomain.setShift(panelistInterviewDataList.getShift());
			currentInterviewDetailDomain.setCoursecompletiondate(panelistInterviewDataList.getCoursecompletiondate());
			currentInterviewDetailDomain.setApplied(panelistInterviewDataList.getApplied());
			currentInterviewDetailDomain.setComments(panelistInterviewDataList.getComments());
			currentInterviewDetailDomain.setInterviewerDecision(panelistInterviewDataList.getInterviewerDecision());
			currentSession.update(currentInterviewDetailDomain);
			
			
			int campusId = currentInterviewDetailDomain.getCampusdomain().getId();
			@SuppressWarnings("unchecked")
			List<InterviewDetailsDomain> fullInterviewList = (List<InterviewDetailsDomain>)sessionFactory.getCurrentSession().createCriteria(InterviewDetailsDomain.class)
			.add(Restrictions.eq("campusdomain.id", campusId)).list();
			boolean flag=true;
			for (InterviewDetailsDomain idd : fullInterviewList) {
				if (idd.getCommunication() == null) {
					flag = false;
					break;
				}
				if (idd.getCommunication().equals("")|| idd.getCommunication().equals("null")) {
					flag = false;
					break;
				}
			}
			
			if(flag==true){				
				@SuppressWarnings("unchecked")
				List<CandidateDetailsDomain> candidateDetailsList = (List<CandidateDetailsDomain>)sessionFactory.getCurrentSession().createCriteria(CandidateDetailsDomain.class)
				.add(Restrictions.eq("campusdomain.id", campusId))
				.add(Restrictions.eq("groupDiscussionResult", "Y")).list();
				
				for(CandidateDetailsDomain cdd: candidateDetailsList){
					cdd.setInterviewResult("Y");
					currentSession.update(cdd);					
				}
				
			}
		}
	}
	@Override
	public Integer getElgibleCountOnCutoff(String campusid, String cutoff){
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select count(*) from (select candidate_id  "
				+ "from group_discussion_details "
				+ "where campus_id = :campusid group by candidate_id having avg(total) >= :cutoff ) a " )
				.setInteger("campusid", Integer.parseInt(campusid))
				.setInteger("cutoff", Integer.parseInt(cutoff));
		List<BigInteger> list = (List<BigInteger>) query.list();
		return ((BigInteger) list.get(0)).intValue() ;
		
	}
	
	@Override
	public void setInterviewResultFlag(String campusid, String cutoff, String freeze){
		Query queryInsert = sessionFactory.getCurrentSession().createSQLQuery("update candidate_details set groupDiscussionResult = 'Y' "
				+ "where candidate_id in (select candidate_id  from group_discussion_details where campus_id = :campusid "
				+ "group by candidate_id having avg(total) >= :cutoff )")
				.setInteger("campusid", Integer.parseInt(campusid))
				.setInteger("cutoff", Integer.parseInt(cutoff));
		queryInsert.executeUpdate();
		
		Session currentSession = sessionFactory.getCurrentSession();
		CampusDetailsDomain campusDetailsUpdate = (CampusDetailsDomain)currentSession.get(CampusDetailsDomain.class,Integer.parseInt(campusid));
		campusDetailsUpdate.setInterviewFreeze(freeze);
		currentSession.update(campusDetailsUpdate);
	}

	@Override
	public String getInterviewFreezeFlag(String campusid) {
		
		Session currentSession = sessionFactory.getCurrentSession();
		CampusDetailsDomain campusDetailsUpdate = (CampusDetailsDomain)currentSession.get(CampusDetailsDomain.class,Integer.parseInt(campusid));
		String interviewFreeze = campusDetailsUpdate.getInterviewFreeze();
		if(interviewFreeze!=null)
			return interviewFreeze;
		else
			return "";
		
	}
	
}
