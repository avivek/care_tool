package com.deloitte.care.dao;

import java.util.List;

import com.deloitte.care.domain.InterviewDetailsDomain;

public interface InterviewDetailsDAO {
	
	public List<InterviewDetailsDomain> listInterviewDetails();
	
	public InterviewDetailsDomain getInterviewDetails(int interviewId);
	
	public InterviewDetailsDomain getInterviewDetailsDomain(int panelistId, String interviewId);

	public void addInterviewRecord(InterviewDetailsDomain interviewDetailsDomain);
	
	public List<InterviewDetailsDomain> getPanelistInterviewList(int panelistId, int campusId);
	public void updatePanelistInterviewData(InterviewDetailsDomain panelistInterviewDataList);

	public Integer getElgibleCountOnCutoff(String campusid, String cutoff);

	public void setInterviewResultFlag(String campusid, String cutoff, String freeze);
	
	public String getInterviewFreezeFlag(String campusid);
}
