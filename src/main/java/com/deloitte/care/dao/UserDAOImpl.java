package com.deloitte.care.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.deloitte.care.domain.UserDomain;


@Repository("userDao")
public class UserDAOImpl implements UserDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void addUser(UserDomain user , String role) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
		Query query = sessionFactory.getCurrentSession().createSQLQuery("insert into user_roles (user_id , authority) values (:user_id , :authority)")
									.setInteger("user_id", user.getUser_id())
									.setString("authority", role);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<UserDomain> listUser() {
		List<UserDomain> temp = (List<UserDomain>) sessionFactory.getCurrentSession().createCriteria(UserDomain.class).list();
		return temp ;
	}

	public UserDomain getUser(int userId) {
		UserDomain temp = (UserDomain) sessionFactory.getCurrentSession().get(UserDomain.class, userId);
		return temp;
	}
	
	
	@Override
	public UserDomain getUser(String userName) {
		// TODO Auto-generated method stub
		List<UserDomain> userDomain = (List<UserDomain>) sessionFactory.getCurrentSession().createCriteria(UserDomain.class).add(Restrictions.eq("username", userName)).list();
		return userDomain.get(0);
	}
	

	public void deleteUser(UserDomain user) {
		System.out.println("1:::::"+user.getUser_id());
		sessionFactory.getCurrentSession().createQuery("DELETE FROM UserDomain WHERE id = "+user.getUser_id()).executeUpdate();
	}


}
