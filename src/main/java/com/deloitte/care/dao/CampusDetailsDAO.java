package com.deloitte.care.dao;

import java.util.List;

import com.deloitte.care.domain.CampusDetailsDomain;
import com.deloitte.care.exception.CareException;

public interface CampusDetailsDAO {
	
	public void saveOrUpdateCampusDetails(CampusDetailsDomain campusdetailsdomain) throws CareException;

	public List<CampusDetailsDomain> listCampusDetails() throws CareException;
	
	public CampusDetailsDomain getCampusDetails(int campusId) throws CareException;
	
	public void deleteCampusDetails(CampusDetailsDomain campusdetailsdomain) throws CareException;
}
