package com.deloitte.care.dao;

import java.util.List;

import com.deloitte.care.domain.CampusHiringDetailsDomain;
import com.deloitte.care.exception.CareException;

public interface CampusHiringDetailsDAO {
	
	public void addCampusHiringDetails(CampusHiringDetailsDomain campusHiringdetailsdomain) throws CareException;

	public List<CampusHiringDetailsDomain> listCampusHiringDetails() throws CareException;
	
	public CampusHiringDetailsDomain getCampusHiringDetails(int id, int year) throws CareException;
	
	public void deleteCampusHiringDetails(CampusHiringDetailsDomain campusdetailsHiringdomain) throws CareException;
}
