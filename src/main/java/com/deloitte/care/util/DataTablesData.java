package com.deloitte.care.util;

import java.util.List;

import com.deloitte.care.form.CandidateDetails;

public class DataTablesData {
	public DataTablesData(List candidateList){
		this.aaData= candidateList;
		this.iTotalDisplayRecords=candidateList.size();
		this.iTotalRecords=candidateList.size();
	}
	
	int iTotalRecords;
	int iTotalDisplayRecords;
	List<CandidateDetails> aaData;
	int messageFlag;
	String sEcho;
	public int getiTotalRecords() {
	return iTotalRecords;
	}
	public void setiTotalRecords(int iTotalRecords) {
	this.iTotalRecords = iTotalRecords;
	}
	public int getiTotalDisplayRecords() {
	return iTotalDisplayRecords;
	}
	
	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
	this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	
	public String getsEcho() {
	return sEcho;
	}
	
	public void setsEcho(String sEcho) {
	this.sEcho = sEcho;
	}
	public int getMessageFlag(){
		return messageFlag;
	}
	public void setMessageFlag(int messageFlag){
		this.messageFlag = messageFlag;
	}
	
	public List<CandidateDetails> getAaData() {
	return aaData;
	}
	
	public void setAaData(List<CandidateDetails> aaData) {
	this.aaData = aaData;
	}

}
