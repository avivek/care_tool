package com.deloitte.care.form;

import java.util.Date;

import javax.validation.constraints.Min;


public class CampusHiringDetails implements java.io.Serializable {
	
	private Integer id;

	private Integer offeredStudents;

	private Float conversionRate;
	
	private Integer winRate;
	
	private Integer slot;
	
	private Integer sample;
	
	private Integer interns;
	
	private Integer year;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOfferedStudents() {
		return offeredStudents;
	}

	public void setOfferedStudents(Integer offeredStudents) {
		this.offeredStudents = offeredStudents;
	}

	public Float getConversionRate() {
		return conversionRate;
	}

	public void setConversionRate(Float conversionRate) {
		this.conversionRate = conversionRate;
	}

	public Integer getWinRate() {
		return winRate;
	}

	public void setWinRate(Integer winRate) {
		this.winRate = winRate;
	}

	public Integer getSlot() {
		return slot;
	}

	public void setSlot(Integer slot) {
		this.slot = slot;
	}

	public Integer getSample() {
		return sample;
	}

	public void setSample(Integer sample) {
		this.sample = sample;
	}

	public Integer getInterns() {
		return interns;
	}

	public void setInterns(Integer interns) {
		this.interns = interns;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "CampusHiringDetails [id=" + id + ", offeredStudents="
				+ offeredStudents + ", conversionRate=" + conversionRate
				+ ", winRate=" + winRate + ", slot=" + slot + ", sample="
				+ sample + ", interns=" + interns + ", year=" + year + "]";
	}
	
}
