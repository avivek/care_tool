package com.deloitte.care.form;

import java.sql.Blob;
import java.util.Date;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.deloitte.care.domain.CampusDomain;

public class DiscussionGroups {

	private List<CandidateDetails> candidateDetails;
	
	private Integer groupNumber;

	public List<CandidateDetails> getCandidateDetails() {
		return candidateDetails;
	}

	public void setCandidateDetails(List<CandidateDetails> candidateDetails) {
		this.candidateDetails = candidateDetails;
	}

	public Integer getGroupNumber() {
		return groupNumber;
	}

	public void setGroupNumber(Integer groupNumber) {
		this.groupNumber = groupNumber;
	}

	
}
