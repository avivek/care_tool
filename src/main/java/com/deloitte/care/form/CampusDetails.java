package com.deloitte.care.form;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

public class CampusDetails implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	@NotEmpty
	@Size(max = 256)
	private String placementOfficer;

	@Range(min = 0, max = 100)
	private Integer talentPool;

	@Range(min = 0, max = 100)
	private Integer winPercentage;

	@Size(max = 256)
	private String brandingEvents;

	@Size(max = 256)
	private String competitiononCampus;

	@Min(value = 0, message = "msg1")
	private Float avgSal;

	@Size(max = 256)
	private String slots;

	@Max(value = 10)
	private Integer pastvisitstoCampus;

	// @Size(max=10)
	@Range(min = 0, max = 100)
	private Integer deloitteTier;

	@Size(max = 256)
	private String serviceLineVisiting;

	@Size(max = 256)
	private String aluminiDetails;

	@Size(max = 256)
	private String otherDetails;

	@Size(max = 256)
	private String ourExperience;

	private String success;
	
	private String error;
	
	private String errorMsg;
	
	private String successMsg;
	
	private String candidateFreeze;
	
	private String panelistFreeze;
	
	private String groupDistributionFreeze;
	
	//imran
	private String ppd;
		
	public String getPpd() {
		return ppd;
	}

	public void setPpd(String ppd) {
		this.ppd = ppd;
	}
	
	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getSuccessMsg() {
		return successMsg;
	}

	public void setSuccessMsg(String successMsg) {
		this.successMsg = successMsg;
	}

	private List<CampusHiringDetails> campusHiringDetails;


	public List<CampusHiringDetails> getCampusHiringDetails() {
		return campusHiringDetails;
	}

	public void setCampusHiringDetails(List<CampusHiringDetails> campusHiringDetails) {
		this.campusHiringDetails = campusHiringDetails;
	}

	public CampusDetails() {
		//ourExperience = "experience";
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPlacementOfficer() {
		return placementOfficer;
	}

	public void setPlacementOfficer(String placementOfficer) {
		this.placementOfficer = placementOfficer;
	}

	public Integer getTalentPool() {
		return talentPool;
	}

	public void setTalentPool(Integer talentPool) {
		this.talentPool = talentPool;
	}

	public Integer getWinPercentage() {
		return winPercentage;
	}

	public void setWinPercentage(Integer winPercentage) {
		this.winPercentage = winPercentage;
	}

	public String getBrandingEvents() {
		return brandingEvents;
	}

	public void setBrandingEvents(String brandingEvents) {
		this.brandingEvents = brandingEvents;
	}

	public String getCompetitiononCampus() {
		return competitiononCampus;
	}

	public void setCompetitiononCampus(String competitiononCampus) {
		this.competitiononCampus = competitiononCampus;
	}

	public Float getAvgSal() {
		return avgSal;
	}

	public void setAvgSal(Float avgSal) {
		this.avgSal = avgSal;
	}

	public String getSlots() {
		return slots;
	}

	public void setSlots(String slots) {
		this.slots = slots;
	}

	public Integer getPastvisitstoCampus() {
		return pastvisitstoCampus;
	}

	public void setPastvisitstoCampus(Integer pastvisitstoCampus) {
		this.pastvisitstoCampus = pastvisitstoCampus;
	}

	public Integer getDeloitteTier() {
		return deloitteTier;
	}

	public void setDeloitteTier(Integer deloitteTier) {
		this.deloitteTier = deloitteTier;
	}

	public String getServiceLineVisiting() {
		return serviceLineVisiting;
	}

	public void setServiceLineVisiting(String serviceLineVisiting) {
		this.serviceLineVisiting = serviceLineVisiting;
	}

	public String getAluminiDetails() {
		return aluminiDetails;
	}

	public void setAluminiDetails(String aluminiDetails) {
		this.aluminiDetails = aluminiDetails;
	}

	public String getOtherDetails() {
		return otherDetails;
	}

	public void setOtherDetails(String otherDetails) {
		this.otherDetails = otherDetails;
	}

	public String getOurExperience() {
		return ourExperience;
	}

	public void setOurExperience(String ourExperience) {
		this.ourExperience = ourExperience;
	}

	@Override
	public String toString() {
		return "CampusDetails [id=" + id + ", placementOfficer="
				+ placementOfficer + ", talentPool=" + talentPool
				+ ", winPercentage=" + winPercentage + ", brandingEvents="
				+ brandingEvents + ", competitiononCampus="
				+ competitiononCampus + ", avgSal=" + avgSal + ", slots="
				+ slots + ", pastvisitstoCampus=" + pastvisitstoCampus
				+ ", deloitteTier=" + deloitteTier + ", serviceLineVisiting="
				+ serviceLineVisiting + ", aluminiDetails=" + aluminiDetails
				+ ", otherDetails=" + otherDetails + ", ourExperience="
				+ ourExperience + ", success=" + success + ", error=" + error
				+ ", campusHiringDetails=" + campusHiringDetails + "]";
	}

	public String getCandidateFreeze() {
		return candidateFreeze;
	}

	public void setCandidateFreeze(String candidateFreeze) {
		this.candidateFreeze = candidateFreeze;
	}

	public String getPanelistFreeze() {
		return panelistFreeze;
	}

	public void setPanelistFreeze(String panelistFreeze) {
		this.panelistFreeze = panelistFreeze;
	}

	public String getGroupDistributionFreeze() {
		return groupDistributionFreeze;
	}

	public void setGroupDistributionFreeze(String groupDistributionFreeze) {
		this.groupDistributionFreeze = groupDistributionFreeze;
	}

}
