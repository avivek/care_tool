package com.deloitte.care.form;

import java.util.Date;

public class Campus {
	
	private int id;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private String campusName;
	
	private String campusDesc;

	public String getCampusName() {
		return campusName;
	}

	public void setCampusName(String campusName) {
		this.campusName = campusName;
	}

	public String getCampusDesc() {
		return campusDesc;
	}

	public void setCampusDesc(String campusDesc) {
		this.campusDesc = campusDesc;
	}
	
	private Date createdDt;
	
	private Date updatedDt;


	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}
	
	
	
}
