package com.deloitte.care.form;

import java.util.Date;

import com.deloitte.care.domain.UserDomain;



public class PanelistDetails {	

	private int id;
	private String panelist_name;
	private String panelist_email_id;
	private String designation;
	private String serviceLine;
	private UserDomain userDomain;
	private Date createdDt;
	private Date updatedDt;
	private String createdBy;
	private String updatedBy;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPanelist_name() {
		return panelist_name;
	}
	public void setPanelist_name(String panelist_name) {
		this.panelist_name = panelist_name;
	}
	public String getPanelist_email_id() {
		return panelist_email_id;
	}
	public void setPanelist_email_id(String panelist_email_id) {
		this.panelist_email_id = panelist_email_id;
	}
	public UserDomain getUserDomain() {
		return userDomain;
	}
	public void setUserDomain(UserDomain userDomain) {
		this.userDomain = userDomain;
	}
	public Date getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public Date getUpdatedDt() {
		return updatedDt;
	}
	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getServiceLine() {
		return serviceLine;
	}
	public void setServiceLine(String serviceLine) {
		this.serviceLine = serviceLine;
	}

	
}
