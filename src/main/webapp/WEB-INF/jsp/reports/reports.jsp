<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<link rel="stylesheet" type="text/css" href="/CareTool/css/funn.css">

<!-- Janki's Part -->

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Simple Sidebar - Start Bootstrap Template</title>

<!-- Bootstrap Core CSS -->
<link href="/CareTool/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="/CareTool/css/simple-sidebar.css" rel="stylesheet">

</head>

<body>
	<!-- <div id="wrapper">
		Sidebar
		<div id="sidebar-wrapper">
			<ul class="sidebar-nav">
				<li class="sidebar-brand"></li>

			<div class="dropdown">
					<button class="btn btn-default dropdown-toggle" type="button"
						id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
						Select Campus <span class="caret"></span>
					</button>
					<div class="campus-dropdown">
						<select class="campus-chosen-select" id="selectCampusFunnel"
							placeholder="Select a Campus"></select>
					</div>
				</div>
			</ul>
		</div>
		/#sidebar-wrapper

		Page Content
		<a href="#menu-toggle" class="btn btn-primary" id="menu-toggle">Filter</a>
	</div> -->

	<div id="reportcontainer">
		<div class="campus-dropdown well well-sm" class="col-md-12">
			<span>Select Campus: </span>
			<select class="campus-chosen-select" id="selectCampusId" placeholder="Select a Campus"></select>
		</div>
		<H2 id="header1">Hiring Funnel</H2>
		<div class="chart" style="display:none">
			<p class="img">
				<img src="/CareTool/images/use.png" /> <label id="c1Ratio1">48.10%</label>
			</p>
			<p class="img">
				<img src="/CareTool/images/use.png" /> <label id="c1Ratio2">30.52%</label>
			</p>
			<p class="img">
				<img src="/CareTool/images/use.png" /> <label id="c1Ratio3">50%</label>
			</p>
			<div id="chartdiv" style="width: 100%;"></div>
			<p class="box">Hit Rate</p>
			<p class="box light" id="hitRate1">7.34%</p>
			<p class="box">WIN Rate</p>
			<p class="box light" id="winRate1">37.9%</p>
		</div>
	</div>
</body>
</html>