<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.deloitte.care.domain.PanelistDomain" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Panelist Interviews</title>
</head>

<% int panelistId = ((PanelistDomain)session.getAttribute("panelistdomain")).getId(); %>
<script type="text/javascript"> var panelistId = <%= panelistId %> </script>
<body>
	<div class="panel panel-info">
	  <div class="panel-heading">Interview</div>
	  <div class="panel-body">
	   <!-- InterviewSection -->
	   		<div id="interviewNoneSection" class="noneSelected">Please Select Candidate</div>
			<div id="interviewPanel" class="table-responsive">
				<!-- Display the data grid  -->
				<table id="interviewHeader" class="table">
					<thead style="background: #e8e8e8;">
						<tr>
							<th>Candidate Name</th>
							<th>Gender</th>
							<th>Interview Date</th>
							<th>Position/Role</th>
							<th>Specialization</th>
							<th>Qualification</th>
							<!-- <th>Interviewer</th> -->
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="candidateIdPL"></td>
							<td id="genderPL"></td>
							<td id="interviewDatePL"></td>
							<td id="positionRolePL"></td>
							<td id="specPL"></td>
							<td id="qualPL"></td>
							<!-- <td id="panelistNamePL"></td> -->
						</tr>
					</tbody>
				</table>
				<div class='interviewFBox' id='service'>
					<div class='ibTitle' id='serviceTitle'>
						<span class='title'>Service - Initiative, Energy</span>
					</div>
					<div class='ibPicker' id='servicePicker'>
						<input type="range" name="servicePoints" class='picker' min="1"
							max="5" onchange="servicePointsValue.value=value">
						<output id="servicePointsValue">3</output>
					</div>
					<div class='ibBlurb' id='serviceBlurb'>
						<textarea id='serviceTextArea' class='blurb'></textarea>
					</div>
				</div>

				<div class='interviewFBox' id='comm'>
					<div class='ibTitle' id='commTitle'>
						<span class='title'>Communication - Articulation,
							Effectiveness</span>
					</div>
					<div class='ibPicker' id='commPicker'>
						<input type="range" name="commPoints" class='picker' min="1"
							max="5" onchange="commPointsValue.value=value">
						<output id="commPointsValue">3</output>
					</div>
					<div class='ibBlurb' id='commBlurb'>
						<textarea id='commTextArea' class='blurb'></textarea>
					</div>
				</div>

				<div class='interviewFBox' id='mgmt'>
					<div class='ibTitle' id='mgmtTitle'>
						<span class='title'>Management - Execution Focus, Decision
							Making</span>
					</div>
					<div class='ibPicker' id='mgmtPicker'>
						<input type="range" name="mgmtPoints" class='picker' min="1"
							max="5" onchange="mgmtPointsValue.value=value">
						<output id="mgmtPointsValue">3</output>
					</div>
					<div class='ibBlurb' id='mgmtBlurb'>
						<textarea id='mgmtTextArea' class='blurb'></textarea>
					</div>
				</div>

				<div class='interviewFBox' id='ldr'>
					<div class='ibTitle' id='ldrTitle'>
						<span class='title'>Leadership - Teaming, Commitment</span>
					</div>
					<div class='ibPicker' id='ldrPicker'>
						<input type="range" name="ldrPoints" class='picker' min="1"
							max="10" onchange="ldrPointsValue.value=value">
						<output id="ldrPointsValue">3</output>
					</div>
					<div class='ibBlurb' id='ldrBlurb'>
						<textarea id='ldrTextArea' class='blurb'></textarea>
					</div>
				</div>
				<div id='verbosePart'>
					<div class="panel panel-default">
					  <div class="panel-heading">Objectives</div>
					  <div class="panel-body">
				    	<div class="form-group">
						    <label for="inputEmail3" class="control-label">What are
										the person's top motivators for joining Deloitte?</label>
						    <input type="text" class="form-control" id="mtvn" placeholder="Enter the person's top motivators for joining Deloitte">
						</div>
						<div class="form-group">
						    <label for="inputEmail3" class="control-label">What
											are the candidate's career objectives?(Clarity in terms of
											growth and aspirations)</label>
						    <input type="text" class="form-control" id="objts" placeholder="Enter the candidate's career objectives">
						</div>
					  </div>
					</div>
					
					<div class="panel panel-default">
					  <div class="panel-heading">General Information</div>
					  <div class="panel-body">
				    	<div class="form-group">
						    <label for="inputEmail3" class="control-label">Ready to work
											shifts?</label>
							<br>
						    <input type="checkbox" id="shiftsCheckbox"
										name="shifts" class='toggler'> <label id="shifts-label"
										for="shiftsCheckbox" class="tog">No</label>
						</div>
						<div class="form-group">
						    <label for="inputEmail3" class="control-label">Date
											of completion of course:</label>
						    <input type="date" class="form-control" id="complDate" placeholder="Pick the person's course completion date">
						</div>
						<div class="form-group">
						    <label for="inputEmail3" class="control-label">Has the candidate
											applied to other organizations?</label>
							<br>				
						    <input type="checkbox" id="compCheckbox"
										name="compCheckbox" class='toggler'> <label
										id="comp-label" for="compCheckbox" class="tog">No</label>
						</div>
						<div class="form-group">
						    <label for="inputEmail3" class="control-label">Other
											comments/Risk areas</label>
						    <input type="text" class="form-control" id="comnts" placeholder="Other comments/Risk areas">
						</div>
						<div class="form-group">
						    <label for="inputEmail3" class="control-label">Interviewers
											Decision</label>
						    <input id="decision" type="range" step="1" max="2" min="1" value="0" class="decisionSlider sm-4" style="width: 150px;">
						    <label class="decisionSliderLabel" for="decisionSlider">No Further Interest</label>
						</div>
						<div class="form-group" style="display: none;">
						    <input type="hidden" id="interviewId"
										name="interviewId">
						</div>
					  </div>
					</div>
	
				</div>
				<div class="control-strip pull-right" id="interview-panel-control-strip">
					<button type="button" id="submtiInterviewFbTrigger" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</div>
	</div>  	
</body>
</html>