<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.deloitte.care.domain.PanelistDomain" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Panelist Home</title>
</head>

<% int panelistId = ((PanelistDomain)session.getAttribute("panelistdomain")).getId(); %>
<script type="text/javascript"> var panelistId = <%= panelistId %> </script>
<body>
	<div class="panel panel-info">
	  <div class="panel-heading">Group Discussion Feedback Form</div>
	  <div class="panel-body" style="overflow-x: auto;">
	  	<div id="gdNoneSection" class="noneSelected">Please Select Campus</div>
	    <div id="panelGdContainer" class="table-responsive">
			<!-- Display the data grid  -->
			<table id="panelSummaryGd" class="table">
		   	<thead>
	          <tr>
	            <th>Group Id</th>
	            <th>Topic of Discussion</th>
	            <th>Panelist Name</th>
	          </tr>
	        </thead>
	        <tbody>
	        <tr class='cloneSeedRow hidden'>
	        	<td class='groupId gdParam'></td>
	        	<td class='topic gdParam'></td>
	        	<td class='panelistName gdParam'></td>
	        </tr>
	        </tbody>
		   </table>
		   <br>
		   <table id="panelGd" class="table">
		   	<thead>
	          <tr>
	            <th>Name</th>
	            <th>Gender</th>
	            <th>Leadership</th>
	            <th>Listening</th>
	            <th>Participation</th>
	            <th>Approach</th>
	            <th>Presentation</th>
	            <th>Response</th>
	            <th>Total</th>
	            <th>Comments</th>
	          </tr>
	        </thead>
	        <tbody>
	        	<tr class='cloneSeedRow hidden'>
	        		<td class="name"></td>
	        		<td class="gender"></td>
	        		<td class="leadership"><select class='leadershipVal gdParam'></select></td>
	        		<td class="listening"><select class='listeningVal gdParam'></select></td>
	        		<td class="participation"><select class='participationVal gdParam'></select></td>
	        		<td class="approach"><select class='approachVal gdParam'></select></td>
	        		<td class="presentation"><select class='presentationVal gdParam'></select></td>
	        		<td class="response"><select class='responseVal gdParam'></select></td>
	        		<td class="total"><input type="text" value="" class='totalVal gdParam' readonly=""></td>
	        		<td class="comments"><textarea class='commentsVal gdParam'></textarea></td>
	        		<td class="groupId" style="display:none;"></td>
	        	</tr>
	        </tbody>
		   </table>
		    <div class="control-strip" id="gd-panel-control-strip">
	          <input id="submtiGdFb" class="hidden"></input>
	          <span id="submtiGdFbTrigger" class="btn btn-primary">Submit</span>
	      </div>
		</div>	
	  </div>
	</div>
</body>
</html>