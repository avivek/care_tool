<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="<%=request.getContextPath()%>/css/elements_popup.css"
	rel="stylesheet" type="text/css" />

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

	
	
	<div id="panelistNone" class="panel panel-info">
	  <div class="panel-heading">Panelist</div>
	  <div class="panel-body">
	    <div class="noneSelected">Please Select Campus</div>
	  </div>
	</div>

	<!-- CurrentPanelistSection -->
	<div id="currentpanelistAdminSection">
		<!-- Display the data grid  -->
		<table id="currentpanelistListProjectTable">
			<thead>
				<tr>
					<th>CurrentPanelist ID</th>
					<th>CurrentPanelist Name</th>
					<th>CurrentPanelist Email Id</th>
					<th>CurrentPanelist Service Line</th>
					<th>CurrentPanelist Designation</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<div class="control-strip" id="panel-control-strip">
			<span id="deletePanel" class="btn btn-warning">Delete</span>
		</div>
	</div>

	<!-- AddPanelistSection -->
	<div id="addpanelistAdminSection">
		<!-- Display the data grid  -->
		<table id="addpanelistListProjectTable">
			<thead>
				<tr>
					<th>AddPanelist ID</th>
					<th>AddPanelist Name</th>
					<th>AddPanelist Email Id</th>
					<th>AddPanelist Service Line</th>
					<th>AddPanelist Designation</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<div class="control-strip" id="panel-control-strip">
			<span id="addPanel" class="btn btn-warning">Add Panelist</span>
		</div>
	</div>

	<div id="panelistFinalize" class="control-strip">
		<!--   <input id="addrowCand" class="hidden"></input>
          <span id="addrowCandTrigger" class="button">Add New Row</span> -->
		<input type="button" class="hidden" id="finalizeCand"> <span
			class="btn btn-success" id="finalizePanelTrigger"
			onclick="div_show()">Finalize</span>
	</div>
	
	<!-- Small modal -->
	<div class="modal fade" id="popupMail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="exampleModalLabel">Send Notification Mail</h4>
	      </div>
	      <div class="modal-body">
	        <form>
	          <div class="form-group">
	            <label class="description popup_label" for="Subject">Subject:</label>
				<input type="text" class="form-control" id="txt"
					name="addSubject" placeholder="Enter subject" value="">
	          </div>
	          <div class="form-group">
	            <label class="description popup_label" for="Subject">Message:</label>
	            <textarea class="form-control" cols="40" rows="5"
					id="txtmsg" name="addMail" placeholder="Enter your message here"
					value=""></textarea>
	          </div>
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary" id="save-campus">Send</button>
	      </div>
	    </div>
	  </div>
	</div>
</body>
</html>