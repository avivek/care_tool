<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.deloitte.care.domain.PanelistDomain"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Panelist Home</title>
</head>

<%
	int panelistId = ((PanelistDomain) session.getAttribute("panelistdomain")).getId();
%>

<body>
	
	<div class="panel panel-info">
	  <div class="panel-heading">
	    <h3 class="panel-title">Panelist Schedule</h3>
	  </div>
	  <div class="panel-body">
	  	<div class="campus-dropdown">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#campusModal">Choose Campus</button>
			<span class="pull-right">Selected Campus <span class="label label-default" id="campus-name">None</span></span>	
		</div>
		
		<div id="gdPanelContainer" class='panelContainer hidePanel'>
			<h5>Click on event to open Group Discussion/Interview feedback form.</h5>
			<div class="panel panel-default">
			  <div class="panel-heading">Group Discussions</div>
			  <div class="panel-body">
			    <ul id="gdContainerUl" class='panelistSchedule'>
					<li class="cloneSeedLi hidden"><a href='#'><span></span></a></li>
				</ul>
			  </div>
			</div>
			
			<div class="panel panel-default">
			  <div class="panel-heading">Interviews</div>
			  <div class="panel-body">
			    <ul id="interviewContainerUl" class='panelistSchedule'>
					<li class="cloneSeedLi hidden"><a href='#'><span></span></a></li>
				</ul>
			  </div>
			</div>
		</div>
		
		
	  </div>
	</div>
	
	<!-- Small modal -->
	<div class="modal fade" id="campusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="exampleModalLabel">Panelist</h4>
	      </div>
	      <div class="modal-body">
	        <form>
	          <div class="form-group">
	            <select id="selectCampusForPanelist" class="form-control"></select>
	          </div>
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary" id="save-campus">Save Changes</button>
	      </div>
	    </div>
	  </div>
	</div>
	<script type="text/javascript">
	var panelistId =
    <%=panelistId%>
	
    </script>

</body>
</html>