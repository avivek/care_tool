<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.deloitte.care.domain.UserDomain"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>Care Portal</title>
<jsp:include page="include.jsp"></jsp:include>
</head>
<body>
	<canvas id="canvas"></canvas>
	<div class="container">
		<div class="row care-container">
			<div class="col-md-3">
				<jsp:include page="header_panel.jsp"></jsp:include>
			</div>
			<div class="col-md-9">
				<div class="care-content">
					<div id="panelistHome" class="page">
						<jsp:include page="panelist/panelistHome.jsp"></jsp:include>
					</div>
					<div id="panelistGD" class="page">
						<jsp:include page="panelist/panelistGD.jsp"></jsp:include>
					</div>
					<div id="panelistInterviews" class="page">
						<jsp:include page="panelist/panelistInterviews.jsp"></jsp:include>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class='push'></div>
	<div id="footer">
		<script>
			//Initial page
			window.location.hash = "#panelistHome";
		</script>
		<jsp:include page="footer.jsp"></jsp:include>
		<!-- Panelist Home -->
		<script src="<%=request.getContextPath()%>/js/pages/panelistHome.js"></script>
		<script src="<%= request.getContextPath() %>/js/pages/campusDetails.js"></script>
		<!-- Panelist Home -->
		
		<!-- Panelist GD -->
		<script src="<%=request.getContextPath()%>/js/pages/panelistGD.js"></script>
		<!-- Panelist GD -->
		
		<!-- Panelist Interview -->
		<script src="<%=request.getContextPath()%>/js/pages/panelistInterview.js"></script>
		<!-- Panelist Interview -->
	</div>
</body>
</html>