<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>Candidate Details</title>
</head>
<body>
<div id="chosen-campus-dropdown">    
        <div id="chosen-campus-text">
        	<span id="chosen-campus">Chosen Campus:</span>
            <span class="chosen-campus-name empty">No campus selected on Campus Details tab</span>
        </div>
	</div>
    <div id="candidate-details"> 
      <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered" id="candidates">
        <thead>
          <tr>
        <!--    <th>Candidate Id</th> -->
            <th>First Name</th>
            <th>Last Name</th>
            <th>Gender</th>
            <th>Aptitude Score</th>
            <th>English Score</th>
            <th>Logical Score</th>
            <th>Degree</th>
            <th>Specialization</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
		  <tr>
		      <th></th>
		      <th></th>
		      <th></th>
		      <th>Cutoff:&nbsp;&nbsp;<input type="text" id="aptMin" class="ceiling"></th>
		      <th>Cutoff:&nbsp;&nbsp;<input type="text" id="engMin" class="ceiling"></th>
		      <th>Cutoff:&nbsp;&nbsp;<input type="text" id="logMin" class="ceiling"></th>
		    </tr>
  </tfoot>
      </table>
      <div id="detail-view" style="display: none;">
      	<form:form id="candidateDetailsForm" commandName="candidatedetails" method="GET">
				<div id="cand-image" class="fspan2">
						<ul>
							<li id="candidate-image">
								<div id="" class="logospace">
                                    <img src=""><h3>Please upload candidate image</h3></div>
                                    <label class="description" for="candimg"></label>
								<div>
									<input id="candimg"  name="candimg" class="hidden" type="file"> 
									<div class="button" id="candimgtrigger">Upload File</div>
								</div>  
							</li>
						</ul>
					</div>
					<div id="left-col" class="fspan3">
					<ul>
						<li id=""><form:label path="candidateId">Candidate Id</form:label>
							<div>
								<form:input path="candidateId" readonly="true"/>
							</div></li>
							
							<li id=""><form:label path="campusdomain.id">Campus Id</form:label>
							<div>
								<form:input path="campusdomain.id" id="cid" readonly="true"/>
							</div></li>

						<li id=""><form:label path="firstName">Name</form:label>
							<div>
								<form:input path="firstName" cssClass="element text medium"  readonly="true"/>
							</div>
							<div>
								<form:errors path="firstName" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="gender">Gender</form:label>
 							<div> 
									<form:select path="gender" readonly="true">
<%--                                              <form:option value="Select" label="Select a value"></form:option> --%>
                                             <form:options items="${genderTypes}" />
                                    </form:select> 
							</div>
							<div>
								<form:errors path="gender" cssStyle="color:red;"></form:errors>
 							</div> 
							</li>

						<li id=""><form:label path="dob">Date of Birth</form:label>
							<div>
								<form:input path="dob" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="dob" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="cAddress">Address</form:label>
							<div>
								<form:input path="cAddress" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="cAddress" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="mobileNumber">Cell Phone Number</form:label>
							<div>
								<form:input path="mobileNumber" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="mobileNumber" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="function">Function</form:label>
							<div>
								<form:input path="function" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="function" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="panelists">Panelists</form:label>
							<div>
								<form:input path="panelists" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="panelists" cssStyle="color:red;"></form:errors>
							</div></li>
					</ul>
				</div>

				<div id="right-col" class="fspan3">
					<ul>
						<li id=""><form:label path="interviewers">Interviewers</form:label>
							<div>
								<form:input path="interviewers" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="interviewers" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="observers">Observers</form:label>
							<div>
								<form:input path="observers" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="observers" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="quantitativeAbilityScore">Aptitude</form:label>
							<div>
								<form:input path="quantitativeAbilityScore" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="quantitativeAbilityScore" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="englishScore">English Score</form:label>
							<div>
								<form:input path="englishScore" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="englishScore" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="collegePecentage">Academic Score</form:label>
							<div>
								<form:input path="collegePecentage" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="collegePecentage" cssStyle="color:red;"></form:errors>
							</div></li>

					</ul>
				</div>

				<!-- <input type="button" value="Edit" id="editBtClk" onclick="editCandidateBtClk()" class="primary-button" /> -->
				<!-- <input type="button" value="Cancel" id="cancelBtClk" onclick="cancelCandidateBtClk()" class="primary-button" />
				<input type="button" value="Delete" id="deleteBtClk" onclick="deleteCandidateBtClk()" class="primary-button" /> -->
			</form:form>
		</div><!--End Detail View-->
	
      <div class="control-strip row" id="cand-control-strip">
        <!--   <input id="addrowCand" class="hidden"></input>
          <span id="addrowCandTrigger" class="button">Add New Row</span> -->
          <input id="cancelCand" class="hidden" type="button"></input>
          <span id="cancelCandTrigger" class="button">Cancel</span>
          <form id='cand-upload-form'>
	          <input type="file" name="bulkUpload" id="bulkUpload" class="hidden"/>
			  <span id="bulkUploadTrigger" class="btn btn-warning">Bulk Upload File</span>
			  <input type="submit" id="bulkUploadButton" class="hidden">
		      <span class="btn btn-warning" id="bulkUploadButtonTrigger">Upload</span>
		  </form>		  
		 
          <input id="finalizeCand" class="hidden" type="button"></input>
          <span id="finalizeCandTrigger" class="btn btn-success">Finalize</span>
          <span id="saveCandTrigger" class="btn btn-success">Save</span>
      </div>
      <div id="progressbox" >
		  	<div id="progressbar"></div >
		  	<div id="statustxt">0%</div>
	 </div>
	  <div id="output"></div>
    </div>
	<%-- <div>
	<table id="projectTable"></table>
		<div id="pagingDiv"></div>
	</div>
	
  	<div id="campus-form-container" class="span7">
			<form:form id="candidateDetailsForm" commandName="candidatedetails" method="POST" enctype="multipart/form-data">
			</form:form> 
			<input type="button" value="Add new Candidate" id="addCandidate" onclick="addCandidateBtClk()" class="primary-button" hidden = "true" />
	</div>  --%>
</body>
</html>