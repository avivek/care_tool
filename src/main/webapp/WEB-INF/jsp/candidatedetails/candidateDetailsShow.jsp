<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<c:set var="successVar" value="${candidatedetails.success}"/>
<c:if test="${successVar == 'success'}">
<div id="success" class="success">${candidatedetails.successMsg}</div>
</c:if>
<c:set var="errorVar" value="${candidatedetails.success}"/>
<c:if test="${errorVar == 'error'}">
<div id="error" class="error">${candidatedetails.errorMsg}</div>
</c:if>
			<form:form id="candidateDetailsForm" commandName="candidatedetails" method="GET">
				<div id="cand-image" class="fspan2">
						<ul>
							<li id="candidate-image">
								<div id="" class="logospace">
                                    <img src=""><h3>Please upload candidate image</h3></div>
                                    <label class="description" for="candimg"></label>
								<div>
									<input id="candimg"  name="candimg" class="hidden" type="file"> 
									<div class="button" id="candimgtrigger">Upload File</div>
								</div>  
							</li>
						</ul>
					</div>
					<div id="left-col" class="fspan3">
					<ul>
						<li id=""><form:label path="candidateId">Candidate Id</form:label>
							<div>
								<form:input path="candidateId" readonly="true"/>
							</div></li>
							
							<li id=""><form:label path="campusdomain.id">Campus Id</form:label>
							<div>
								<form:input path="campusdomain.id" id="cid" readonly="true"/>
							</div></li>

						<li id=""><form:label path="firstName">Name</form:label>
							<div>
								<form:input path="firstName" cssClass="element text medium"  readonly="true"/>
							</div>
							<div>
								<form:errors path="firstName" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="gender">Gender</form:label>
 							<div> 
									<form:select path="gender" readonly="true">
<%--                                              <form:option value="Select" label="Select a value"></form:option> --%>
                                             <form:options items="${genderTypes}" />
                                    </form:select> 
							</div>
							<div>
								<form:errors path="gender" cssStyle="color:red;"></form:errors>
 							</div> 
							</li>

						<li id=""><form:label path="dob">Date of Birth</form:label>
							<div>
								<form:input path="dob" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="dob" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="cAddress">Address</form:label>
							<div>
								<form:input path="cAddress" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="cAddress" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="mobileNumber">Cell Phone Number</form:label>
							<div>
								<form:input path="mobileNumber" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="mobileNumber" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="function">Function</form:label>
							<div>
								<form:input path="function" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="function" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="panelists">Panelists</form:label>
							<div>
								<form:input path="panelists" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="panelists" cssStyle="color:red;"></form:errors>
							</div></li>
					</ul>
				</div>

				<div id="right-col" class="fspan3">
					<ul>
						<li id=""><form:label path="interviewers">Interviewers</form:label>
							<div>
								<form:input path="interviewers" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="interviewers" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="observers">Observers</form:label>
							<div>
								<form:input path="observers" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="observers" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="quantitativeAbilityScore">Aptitude</form:label>
							<div>
								<form:input path="quantitativeAbilityScore" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="quantitativeAbilityScore" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="englishScore">English Score</form:label>
							<div>
								<form:input path="englishScore" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="englishScore" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="collegePecentage">Academic Score</form:label>
							<div>
								<form:input path="collegePecentage" cssClass="element text medium" readonly="true"/>
							</div>
							<div>
								<form:errors path="collegePecentage" cssStyle="color:red;"></form:errors>
							</div></li>

					</ul>
				</div>

				<!-- <input type="button" value="Edit" id="editBtClk" onclick="editCandidateBtClk()" class="primary-button" /> -->
				<!-- <input type="button" value="Cancel" id="cancelBtClk" onclick="cancelCandidateBtClk()" class="primary-button" />
				<input type="button" value="Delete" id="deleteBtClk" onclick="deleteCandidateBtClk()" class="primary-button" /> -->
			</form:form>
			<script>
			$j(document).ready(function(){
				$j("#candidateDetailsForm").on("click", ".element", function(){
					$j("input#camp-cancel-button").show().fadeIn("slow");
					editCandidateBtClk();
					}); 
			});
			</script>
</body>
</body>
</html>