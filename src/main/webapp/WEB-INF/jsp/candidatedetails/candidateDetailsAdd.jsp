<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http//www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"></head>
<body>
<c:set var="successVar" value="${candidatedetails.success}"/>
<c:if test="${successVar == 'success'}">
<div id="success" class="success">${candidatedetails.successMsg}</div>
</c:if>
<c:set var="errorVar" value="${candidatedetails.error}"/>
<c:if test="${errorVar == 'error'}">
<div id="error" class="error">${candidatedetails.errorMsg}</div>
</c:if>

<form:form id="candidateDetailsForm" commandName="candidatedetails" method="POST" >
			<div id="cand-image" class="fspan2">
						<ul>
							<li id="candidate-image">
								<div id="" class="logospace">
                                    <img src=""><h3>Please upload candidate image</h3></div>
                                    <label class="description" for="candimg"></label>
								<div>
									<input id="candimg"  name="candimg" class="hidden" type="file"> 
									<div class="button" id="candimgtrigger">Upload File</div>
								</div>  
							</li>
						</ul>
					</div>
			<div id="left-col" class="fspan3">
					<ul>
						<li id=""><form:label path="candidateId">Candidate Id</form:label>
							<div>
								<form:input path="candidateId" readonly="true"/>
							</div></li>
							
							<li id=""><form:label path="campusdomain.id">Campus Id</form:label>
							<div>
								<form:input path="campusdomain.id" id="cid" readonly="true"/>
							</div></li>

						<li id=""><form:label path="firstName">Name</form:label>
							<div>
								<form:input path="firstName" cssClass="element text medium"  />
							</div>
							<div>
								<form:errors path="firstName" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="gender">Gender</form:label>
 							<div> 
									<form:select path="gender" >
<%--                                              <form:option value="Select" label="Select a value"></form:option> --%>
                                             <form:options items="${genderTypes}" />
                                    </form:select> 
							</div>
							<div>
								<form:errors path="gender" cssStyle="color:red;"></form:errors>
 							</div> 
							</li>

						<li id=""><form:label path="dob">Date of Birth</form:label>
							<div>
								<form:input path="dob" cssClass="element text medium" />
							</div>
							<div>
								<form:errors path="dob" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="cAddress">Address</form:label>
							<div>
								<form:input path="cAddress" cssClass="element text medium" />
							</div>
							<div>
								<form:errors path="cAddress" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="mobileNumber">Cell Phone Number</form:label>
							<div>
								<form:input path="mobileNumber" cssClass="element text medium" />
							</div>
							<div>
								<form:errors path="mobileNumber" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="function">Function</form:label>
							<div>
								<form:input path="function" cssClass="element text medium" />
							</div>
							<div>
								<form:errors path="function" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="panelists">Panelists Add</form:label>
							<div>
								<form:input path="panelists" cssClass="element text medium" />
							</div>
							<div>
								<form:errors path="panelists" cssStyle="color:red;"></form:errors>
							</div></li>
					</ul>
				</div>

				<div id="right-col" class="fspan3">
					<ul>
						<li id=""><form:label path="interviewers">Interviewers</form:label>
							<div>
								<form:input path="interviewers" cssClass="element text medium" />
							</div>
							<div>
								<form:errors path="interviewers" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="observers">Observers</form:label>
							<div>
								<form:input path="observers" cssClass="element text medium" />
							</div>
							<div>
								<form:errors path="observers" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="quantitativeAbilityScore">Aptitude</form:label>
							<div>
								<form:input path="quantitativeAbilityScore" cssClass="element text medium" />
							</div>
							<div>
								<form:errors path="quantitativeAbilityScore" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="englishScore">English Score</form:label>
							<div>
								<form:input path="englishScore" cssClass="element text medium" />
							</div>
							<div>
								<form:errors path="englishScore" cssStyle="color:red;"></form:errors>
							</div></li>

						<li id=""><form:label path="collegePecentage">Academic Score</form:label>
							<div>
								<form:input path="collegePecentage" cssClass="element text medium" />
							</div>
							<div>
								<form:errors path="collegePecentage" cssStyle="color:red;"></form:errors>
							</div></li>
							
							<form:input path="amcatID" cssClass="element text medium" hidden="true"/>
							<form:input path="lastName" cssClass="element text medium" hidden="true"/>
							<form:input path="middleName" cssClass="element text medium" hidden="true"/>
							<form:input path="emailID" cssClass="element text medium" hidden="true"/>
							<form:input path="alternateEmailID" cssClass="element text medium" hidden="true"/>
							<form:input path="landLine" cssClass="element text medium" hidden="true"/>
							<form:input path="fatherMobileNo" cssClass="element text medium" hidden="true"/>
							<form:input path="pAddress" cssClass="element text medium" hidden="true"/>
							<form:input path="pCity" cssClass="element text medium" hidden="true"/>
							<form:input path="pState" cssClass="element text medium" hidden="true"/>
							<form:input path="pZipCode" cssClass="element text medium" hidden="true"/>
							<form:input path="cCity" cssClass="element text medium" hidden="true"/>
							<form:input path="cState" cssClass="element text medium" hidden="true"/>
							<form:input path="cZipCode" cssClass="element text medium" hidden="true"/>
							<form:input path="boardName10" cssClass="element text medium" hidden="true"/>
							<form:input path="passingYear10" cssClass="element text medium" hidden="true"/>
							<form:input path="percentage10" cssClass="element text medium" hidden="true"/>
							<form:input path="boardName12" cssClass="element text medium" hidden="true"/>
							<form:input path="passingYea12" cssClass="element text medium" hidden="true"/>
							<form:input path="percentage12" cssClass="element text medium" hidden="true"/>
							<form:input path="completionYear" cssClass="element text medium" hidden="true"/>
							<form:input path="collegName" cssClass="element text medium" hidden="true"/>
							<form:input path="collegeCity" cssClass="element text medium" hidden="true"/>
							<form:input path="collegeState" cssClass="element text medium" hidden="true"/>
							<form:input path="degreeName" cssClass="element text medium" hidden="true"/>
							<form:input path="subjectName" cssClass="element text medium" hidden="true"/>
							<form:input path="gradUniversityRollNo" cssClass="element text medium" hidden="true"/>
							<form:input path="eduMode" cssClass="element text medium" hidden="true"/>
							<form:input path="backPaper" cssClass="element text medium" hidden="true"/>
							<form:input path="affiliatedTo" cssClass="element text medium" hidden="true"/>
							<form:input path="testAttemptTime" cssClass="element text medium" hidden="true"/>
							<form:input path="englishPercentile" cssClass="element text medium" hidden="true"/>
							<form:input path="vocabulary" cssClass="element text medium" hidden="true"/>
							<form:input path="grammar" cssClass="element text medium" hidden="true"/>
							<form:input path="comprehension" cssClass="element text medium" hidden="true"/>
							<form:input path="quantitativeAbilityPercentage" cssClass="element text medium" hidden="true"/>
							<form:input path="basicMathematics" cssClass="element text medium" hidden="true"/>
							<form:input path="engineeringMathematics" cssClass="element text medium" hidden="true"/>
							<form:input path="appliedMathematics" cssClass="element text medium" hidden="true"/>
							<form:input path="logicalAbilityScore" cssClass="element text medium" hidden="true"/>
							<form:input path="logicalAbilityPercentage" cssClass="element text medium" hidden="true"/>
							<form:input path="inductiveReasoning" cssClass="element text medium" hidden="true"/>
							<form:input path="deductiveReasoning" cssClass="element text medium" hidden="true"/>
							<form:input path="abductiveReasoning" cssClass="element text medium" hidden="true"/>
							<form:input path="parentTitle" cssClass="element text medium" hidden="true"/>
							<form:input path="parentName" cssClass="element text medium" hidden="true"/>
							<form:input path="pref1Function" cssClass="element text medium" hidden="true"/>
							<form:input path="prefOfGurgaon1" cssClass="element text medium" hidden="true"/>
							<form:input path="prefOfHyderabad1" cssClass="element text medium" hidden="true"/>
							<form:input path="prefOfMumbai1" cssClass="element text medium" hidden="true"/>
							<form:input path="prefOfBengaluru" cssClass="element text medium" hidden="true"/>
							<form:input path="pref2Function" cssClass="element text medium" hidden="true"/>
							<form:input path="prefOfGurgaon2" cssClass="element text medium" hidden="true"/>
							<form:input path="prefOfMumbai2" cssClass="element text medium" hidden="true"/>
							<form:input path="prefOfHyderabad2" cssClass="element text medium" hidden="true"/>
							<form:input path="prefOfBengaluru2" cssClass="element text medium" hidden="true"/>


					</ul>
				</div>
			</form:form>	
</body>
</html>