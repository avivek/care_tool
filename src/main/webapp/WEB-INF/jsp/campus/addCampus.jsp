<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Spring MVC Form Handling</title>
</head>
<body>
	<h2>Add Employee Data</h2>
	<form:form method="POST" action="/CareTool/admin/campus/save">
		<table>
			<tr>
				<td><form:label path="id">Campus ID:</form:label></td>
				<td><form:input path="id" value="${campus.id}"
						readonly="true" /></td>
			</tr>
			<tr>
				<td><form:label path="campusName">Campus Name:</form:label></td>
				<%-- 			        <td><form:input path="campusName" /></td> --%>
				<td><form:input path="campusName" value="${campus.campusName}" /></td>
			</tr>
			<tr>
				<td><form:label path="campusDesc">Campus Description:</form:label></td>
				<%-- 			        <td><form:input path="campusDesc" /> --%>
				<td><form:input path="campusDesc" value="${campus.campusDesc}" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>

	<c:if test="${!empty campuses}">
		<h2>List Campuses</h2>
		<table align="left" border="1">
			<tr>
				<th>Campus ID</th>
				<th>Campus Name</th>
				<th>Campus Description</th>
				<th>Actions on Row</th>
			</tr>

			<c:forEach items="${campuses}" var="campus">
				<tr>
					<td><c:out value="${campus.id}" /></td>
					<td><c:out value="${campus.campusName}" /></td>
					<td><c:out value="${campus.campusDesc}" /></td>
					<td align="center"><a
						href="/CareTool/admin/campus/edit?id=${campus.id}">Edit</a> | <a
						href="/CareTool/admin/campus/delete?id=${campus.id}">Delete</a></td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
</body>
</html>