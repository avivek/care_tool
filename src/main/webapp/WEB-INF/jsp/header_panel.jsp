<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

</head>
<body>
	<div class="care-sidebar">
		<!-- SIDEBAR USERPIC -->
		<div class="care-userpic">
			<img src="images/care-new-logo.png" class="img-responsive" alt="">
		</div>
		<!-- END SIDEBAR USERPIC -->
		<!-- SIDEBAR USER TITLE -->
		<div class="care-usertitle">
			<div class="care-usertitle-name">Recruitment Process Tool</div>
			<div class="care-usertitle-job">Powered by Deloitte</div>
		</div>
		<!-- END SIDEBAR USER TITLE -->
		<!-- END SIDEBAR BUTTONS -->
		<!-- SIDEBAR MENU -->
		<div class="care-usermenu">
			<ul class="nav">
				<li data-url="panelistHome"><a href="#panelistHome"> <i
						class="glyphicon glyphicon-home"></i> Home
				</a></li>
				<li data-url="panelistGD"><a href="#panelistGD"> <i class="glyphicon glyphicon-bullhorn"></i>
						Group Discussion
				</a></li>
				<li data-url="panelistInterviews"><a href="#panelistInterviews"> <i class="glyphicon glyphicon-blackboard"></i>
						Interview
				</a></li>
				<li>
					<a href="<%=request.getContextPath()%>/login"><i class="glyphicon glyphicon-log-out"></i>Logout</a>
				</li>
			</ul>
		</div>
		<!-- END MENU -->
	</div>
	<!--End logo div-->
</body>
</html>