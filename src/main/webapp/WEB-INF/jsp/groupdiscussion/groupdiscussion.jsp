<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>Group Discussion</title>
</head>

<body>
	<div id="chosen-campus-dropdown">
		<div id="chosen-campus-text">
			<span id="chosen-campus">Chosen Campus:</span> <span
				class="chosen-campus-name empty">No campus selected on Campus
				Details tab</span>
		</div>
	</div>
	<div id="groupdiscussion-form-container" class="fspan8">
		<div id='gd-param-bar' class="bg-info">
			<label id='sizeOfGdGroupLabel' for='sizeOfGdGroup'>Enter Group Size</label>
			<input id='sizeOfGdGroup' type='text' placeholder="5"></input>
			<button id="genGdTrigger" class="btn btn-success">Generate GD Groups</button>
		</div>
		<div id="tableContainer" style="display: none;">
			<table id="gdMasterTable">
				<thead>
				<tr class="gdMasterHeader">
					<th id="groupId">Group ID</th>
				</tr>
				</thead>
				<tbody>
				<tr class="gdMasterRow">
					<td><span class="groupId">No Data</span></td>
				</tr>
				</tbody>
			</table>
			<table id="gdCandTable">
				<thead>
					<tr class="gdCandHeader">
							<th id="candidatetId">Candidate ID</th>
							<th id="candidateName">Candidate Name</th>
							<th id="groupId">Group ID</th>
					</tr>
				</thead>
				<tbody>
					<tr class="gdCandRow">
						<td><span class="candidatetId">No Data</span></td>
						<td><span class="candidateName">No Data</span></td>
						<td><span class="groupId">No Data</span></td>
					</tr>
				</tbody>
			</table>
			<table id="gdPanelistTable">
					<thead>	
						<tr class="gdPanelistHeader">
							<th id="panelistId">Panelist ID</th>
							<th id="panelistName">Panelist Name</th>
							<th id="groupId">Group ID</th>
						</tr>
				<tbody>
					<tr class="gdPanelistRow">
						<td><span class="panelistId">No Data</span></td>
						<td><span class="panelistName">No Data</span></td>
						<td><span class="groupId">No Data</span></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="gd-accordion">
			<ul class='gd-parent-ul'>
				<li class='gd-title-li'><a href="#"><span>Group
							Discussion Teams</span></a></li>
				<li class='gd-empty-group-title-li'><span>No Groups to Display</span>
					</li>
			</ul>
		</div>
		<div class="control-strip" id="gd-control-strip">
           <input id="finalizeGd" class="hidden" type="button"></input>
          <button id="finalizeGdTrigger" class="btn btn-success">Finalize</span>
      </div>
		
	</div>
</body>
</html>