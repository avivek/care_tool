<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
	<div id="campus-details" class="row">
		<div class="campus-dropdown bg-info" class="col-md-12">
			<span>Choose Campus -- </span> <select class="campus-chosen-select"
				id="selectCampus" placeholder="Select a Campus"></select>
			<button id="add-campus" type="button"
				class="btn btn-primary pull-right">Add Campus</button>
		</div>
		<div id="campus-details-container">
			<!-- Pop for addCampus action -->
			<!-- TODO: Not Sure Where it is used ??? -->
			<div id="addCampusPopup" style="display: none;">
				<span id="addCampusError" class="error"></span>
				<table>
					<tr>
						<td><label class="description" for="addCampusName">Campus
								Name</label> <label id="addCampusNameError" class="error"
							for="addCampusName"></label></td>
					</tr>
					<tr>
						<td><input type="text" id="addCampusName"
							name="addCampusName" class="form-control"
							placeholder="Enter Campus name" value=""></td>
					</tr>
					<tr>
						<td><label class="description" for="addCampusDesc">Campus
								Description</label> <label id="addCampusDescError" class="error"
							for="addCampusDesc"></label></td>
					</tr>
					<tr>
						<td><textarea type="text" id="addCampusDesc"
								name="addCampusDesc"
								placeholder="Enter Brief Campus Description"
								class="form-control" rows="3" value=""></textarea></td>
					</tr>
				</table>
				<span class="btn btn-warning pull-left" id="addCampusTrigger">Add
					Campus</span>
			</div>
			<div id="campus-form-container" class="row">
				<!-- BEGIN Tab Container-->
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption caption-red">
							<i class="glyphicon glyphicon-pushpin"></i> <span
								class="caption-subject bold font-yellow-crusta uppercase">
								Campus Info </span> <span class="caption-helper"></span>
						</div>
						<ul class="nav nav-tabs">
							<li><a href="#portlet_tab4" data-toggle="tab">
									Experience </a></li>
							<li><a href="#portlet_tab3" data-toggle="tab"> Upload </a></li>
							<li><a href="#portlet_tab2" data-toggle="tab"> Details </a></li>
							<li class="active"><a href="#portlet_tab1" data-toggle="tab">
									Stats </a></li>
						</ul>
					</div>
					<div class="portlet-body">
						<div class="tab-content">
							<div class="tab-pane active" id="portlet_tab1">
								<!-- <h4>Campus Hiring Statistics</h4> -->
								<div id="right-col">
									<ul>
										<select id="hire-years" class="chosen-select">
										</select>
										</li>
										<div id="hiring-stats-container" class="emptyForm">
											<!--Use this div ONLY IF div level refresh is required when option is changed in the dropdown. Else, delete-->
											<li id=""><label class="description" for="offers">Offered
													Students </label>
												<div>
													<input id="offers" name="offers" readonly
														class="form-control" type="text" maxlength="255" value="" />
												</div></li>
											<li id=""><label class="description" for="conv-rate">Conversion
													Rate </label>
												<div>
													<input id="conv-rate" name="conv-rate" readonly
														class="form-control" type="text" maxlength="255" value="" />
												</div></li>
											<li id=""><label class="description" for="win-ratio">Win
													Ratio </label>
												<div>
													<input id="win-ratio" name="win-ratio" readonly
														class="form-control" type="text" maxlength="255" value="" />
												</div></li>
											<li id=""><label class="description" for="attrition">Attrition
											</label>
												<div>
													<input id="attrition" name="attrition" readonly
														class="form-control" type="text" maxlength="255" value="" />
												</div></li>
											<li id=""><label class="description" for="slot-hist">Slot
											</label>
												<div>
													<input id="slot-hist" name="slot-hist" readonly
														class="form-control" type="text" maxlength="255" value="" />
												</div></li>
											<li id=""><label class="description" for="int-ppo">Interns
													| PPO </label>
												<div>
													<input id="int-ppo" name="int-ppo" readonly
														class="form-control" type="text" maxlength="255" value="" />
												</div></li>
										</div>
										<!--End cam-stats-data div-->

										<li id="stud-det"><h3 class="header-highlight">Student
												Details</h3></li>
										<li id=""><label class="description" for="alumni">Alumni
												Details </label>
											<div>
												<input id="alumni" name="alumni" class="form-control"
													type="text" maxlength="255" value="" />
											</div></li>
										<li id=""><label class="description" for="ppd">P/P/D
												from this Campus </label>
											<div>
												<input id="ppd" name="ppd" class="form-control" type="text"
													maxlength="255" value="" />
											</div></li>
									</ul>
								</div>
								<!--End Right Column-->
							</div>
							<div class="tab-pane" id="portlet_tab2">
								<div id="left-col">
									<ul>
										<li id=""><label class="description" for="campusId">Campus
												Id </label>
											<div>
												<input id="campusId" name="campusId" class="form-control"
													type="text" readonly="true" disabled="true" />
											</div></li>
										<li id=""><label class="description" for="plcmtofficer">Placement
												officer </label>
											<div>
												<input id="plcmtofficer" name="plcmtofficer"
													class="form-control" type="text" maxlength="255" value="" />
											</div></li>
										<li id=""><label class="description" for="talentpool">Talent
												Pool </label>
											<div>
												<input id="talentpool" name="talentpool"
													class="form-control" type="text" maxlength="255" value="" />
											</div></li>
										<li id=""><label class="description" for="win">Win
												% </label>
											<div>
												<input id="win" name="win" class="element text medium float"
													type="text" maxlength="255" value="" />
											</div></li>
										<li id=""><label class="description" for="branding">Branding
												Events </label>
											<div>
												<input id="brandingevents" name="branding"
													class="form-control" type="text" maxlength="255" value="" />
											</div></li>
										<li id=""><label class="description" for="competition">Competition
												on Campus </label>
											<div>
												<input id="competition" name="competition"
													class="form-control" type="text" maxlength="255" value="" />
											</div></li>
										<li id=""><label class="description" for="avgsalary">Average
												Salary (Previous Batch - LPA) </label>
											<div>
												<input id="avgsalary" name="avgsalary" class="form-control"
													type="text" maxlength="255" value="" />
											</div></li>
										<li id=""><label class="description" for="slots">Slots
										</label>
											<div>
												<!-- <input id="slots" name="slots" class="element text medium" type="text" maxlength="255" value=""/> -->
												<select id='slots' class="form-control">
													<option value='0'></option>
													<option value='1'>Dream</option>
													<option value='2'>Regular</option>
													<option value='3'>Standard</option>
												</select>
											</div></li>

										<li id="other-plct">
											<h3 class="header-highlight">Other Placement Details</h3>
										</li>
										<li id=""><label class="description" for="pastvisits">Past
												Visits to Campus </label>
											<div>
												<input id="pastvisits" name="pastvisits"
													class="form-control" type="text" maxlength="255" value=""
													pattern="[0-9]" />
											</div></li>
										<li id=""><label class="description" for="tier">Deloitte
												Tier </label>
											<div>
												<!-- <input id="tier" name="tier" class="element text medium" type="text" maxlength="255" value=""/> -->
												<select id='tier' class="form-control">
													<option value='0'></option>
													<option value='1'>Tier 1</option>
													<option value='2'>Tier 2</option>
													<option value='3'>Tier 3</option>
												</select>
											</div></li>
										<li id=""><label class="description" for="slvisiting">Service
												Lines Visiting </label>
											<div>
												<input id="slvisiting" name="slvisiting"
													class="form-control" type="text" maxlength="255" value="" />
											</div></li>
									</ul>
								</div>
							</div>
							<div class="tab-pane" id="portlet_tab3">
								<!-- <h4>Campus Logo</h4> -->
								<div id="campus-logo">
									<ul>
										<li id="logo">
											<div id="logospace">
												<img id="campusLogoImg" src="#"
													alt="Please upload logo image" />
												<h3 id="imgAltHdr">Please upload logo image</h3>
											</div> <label class="description" for="campuslogo"></label>
											<div>
												<input id="campuslogo" name="campuslogo" class="hidden"
													type="file" />
												<div class="btn btn-primary" id="logotrigger">Upload
													File</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="tab-pane" id="portlet_tab4">
								<h4>Our Experience this Year</h4>
								<div id="bottom-col" class="span6">
									<ul>
										<li id="">
											<div>
												<textarea id="our-exp" name="our-exp" class="form-control"
													rows="3" value=""></textarea>
											</div>
										</li>
									</ul>
								</div>
								<!--End Bottom Column-->
								<br>

							</div>
						</div>
						<div class="control-strip" id="campus-control-strip" style="float:right">
							<!-- <span id='cancelBtClkTrigger' class='button'>Save</span> -->
							<span id='saveBtClkTrigger' class="btn btn-warning">Save</span>
						</div>
					</div>
				</div>
			</div>
			<!--End Campus Form Container-->
		</div>
		<!--End Campus details container div-->
	</div>
	<!--End Campus details div-->
</body>
</html>