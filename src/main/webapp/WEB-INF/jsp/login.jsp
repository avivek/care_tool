<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Login Page</title>
<jsp:include page="include.jsp"></jsp:include>
<!-- Page Specific Styling -->
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/pages/login.css">
</head>
<body>
	<canvas id="canvas"></canvas>
	<div id="container" class="container">
		<div class="row vertical-offset-100">
			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row-fluid user-row">
							<img src="images/care-new-logo-small.png" class="img-responsive"
								alt="Care Logo" />
						</div>
					</div>
					<div class="panel-body">
						<form accept-charset="UTF-8" role="form" class="form-signin"
							name='login' action="<c:url value='j_spring_security_check' />"
							method='POST'>
							<fieldset>
								<label class="panel-login">
									<div class="login_result">
										<c:if test="${not empty error}">
											<div class="errorblock" id="loginerror">
												${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</div>
										</c:if>
									</div>
								</label> <input class="form-control" placeholder="Username"
									id="username" type="text" name='j_username'> <input
									class="form-control" placeholder="Password" id="password"
									type="password" name='j_password'> <br></br> <input
									class="btn btn-lg btn-success btn-block" type="submit"
									id="login" value="Sign In"> 
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /Login Form -->
	</div>
	<!-- Page Specific Script -->
	<!-- Load Common Scripts -->
	<script src="<%=request.getContextPath()%>/js/lib/jquery-2.1.4.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/lib/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/lib/TweenMax.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/lib/tween-bg-animate.js"></script>
	<script src="<%=request.getContextPath()%>/js/care.js"></script>
	<script src="<%=request.getContextPath()%>/js/chosen.jquery.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery.dataTables.js"></script>
	<script src="<%=request.getContextPath()%>/js/datepicker.js"></script>
	<script src="<%=request.getContextPath()%>/js/messages.js"></script>
	<script src="<%=request.getContextPath()%>/js/pages/login.js"></script>=
</body>
</html>