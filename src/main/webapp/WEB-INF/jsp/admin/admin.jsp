<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin</title>
</head>

<script src="<%= request.getContextPath() %>/js/admin.js"></script>
<body>
	<select id="selectAdminOption">
		<option value="0" selected="selected">Select One</option>
		<!-- <option value="campus">Campus</option> -->
		<option value="panelist">Panelist</option>
		<option value="recruiter">Recruiter</option>
	</select>
	<br>
	<br>
	
	<!-- PanelistSection -->
	<div id="panelistAdminSection" style="display: none;">
		<!-- Display the data grid  -->
	   <table id="panelistListProjectTable">
	   	<thead>
          <tr>
            <th>Panelist ID</th>
            <th>Panelist Name</th>
            <th>Panelist Email Id</th>
            <th>Service Line</th>
            <th>Designation</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
	   </table>
		<div id="panelistListPagingDiv"></div>
		
		<button id="addPanelistButton" style="display: none;" class="btn btn-success">Add Panelist</button>
		
		<!-- Display detailed info for the selected row -->
		<br>
		<br>
		<div id="displayRowDetailsPanelist" style="display: none;">
		
		</div>
	
		<!-- Pop for addPanelist action -->
		<div id="addPanelistPopup" style="display: none;">
			<table class="table table-striped">
				<tr><td>Panelist Name</td><td><input type="text" id="addPanelistName" value=""></td></tr>
				<tr><td>Panelist Email Address</td><td><input type="text" id="addPanelistEmail" value=""></td></tr>
				<tr><td>Service Line</td><td><input type="text" id="serviceLine" value=""></td></tr>
				<tr><td>Designation</td><td><input type="text" id="designation" value=""></td></tr>
			</table>
			<input class="btn btn-success" type="submit" value="Save" onclick="addPanelist()">
		</div>
	</div>
	
    <!-- RecruiterSection -->
    <div id="recruiterAdminSection" style="display: none;">
        <!-- Display the data grid  -->
       <table id="recruiterListProjectTable">
        <thead>
          <tr>
            <th>Recruiter ID</th>
            <th>Recruiter Name</th>
            <th>Recruiter Email Id</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
       </table>
        <div id="recruiterListPagingDiv"></div>
        
        <button id="addRecruiterButton" style="display: none;" class="btn btn-success">Add Recruiter</button>
        
        <!-- Display detailed info for the selected row -->
        <br>
        <br>
        <div id="displayRowDetailsRecruiter" style="display: none;">
        
        </div>
    
        <!-- Pop for addRecruiter action -->
        <div id="addRecruiterPopup" style="display: none;" >
            <table class="table table-striped">
                <tr><td>Recruiter Name</td><td><input type="text" id="addRecruiterName" value=""></td></tr>
                <tr><td>Recruiter Email Address</td><td><input type="text" id="addRecruiterEmail" value=""></td></tr>
            </table>
            <input class="btn btn-primary" type="submit" value="Save" onclick="addRecruiter()">
        </div>
    </div>
	
</body>
</html>