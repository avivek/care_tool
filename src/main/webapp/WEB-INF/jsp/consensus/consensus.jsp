<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head></head>
<link href="<%= request.getContextPath() %>/css/style1.css" rel="stylesheet" type="text/css" />
<link href="<%= request.getContextPath() %>/css/jq.css" rel="stylesheet" type="text/css" />

<body>
<div id="consensusContainer">
	<div id='consensusListContainer'>
	<table>
              <tr>
                     <td>Count of candidates On Hold</td>
                     <td>:</td>
                     <td id='countOnHold'>0</td>
              </tr>
              <tr>
                     <td>Count of candidates Offers</td>
                     <td>:</td>
                     <td id='countOffer'>0</td>
              </tr>
       </table>
		<table id="consensusTable"  class="tablesorter">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Candidate Name</th>
                    <th>Gender</th>
                    <th class='hidden'>Candidate Id</th>
                    <!-- <th>Aptitude Score</th>
                    <th>English Score</th>
                    <th>Logical Score</th> -->
                    <th>Average Score</th>
                    <th>Group Discussion Selection</th>
                    <th>Interviewer Decision</th>
                    <th>Decision Picker</th>
                    <th>Decision Indicator</th>
                   <!--  <th>Comments</th> -->
                </tr>
            </thead>
            <tbody>
                <tr class='cloneSeedTr hidden'>
                    <td class='slNo'></td>
                    <td class='candidateName'></td>
                    <td class='gender'></td>
                    <td class='candidateId hidden'></td>
                    <!-- <td class='aptitudeScore'></td>
                    <td class='englishScore'></td>
                    <td class='logicalScore'></td> -->
                    <td class='averageScore'></td>
                    <td class='englishScore'></td>
                    <td class='englishScore'></td>
                    <td class='decisionSlider'></td>
                    <td class='decision'></td>
                   <!--  <td class='comments'></td> -->
                </tr>               
            </tbody>
            
        </table>
          <div class="control-strip" id="consensus-control-strip">
          <span id="submitConsensusTrigger" class="btn btn-success">Finalize</span>
      </div>
	</div>
</div>
</body>
</html>
