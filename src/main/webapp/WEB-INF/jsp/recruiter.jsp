<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>Care Portal</title>
<jsp:include page="include.jsp"></jsp:include>
</head>
<body>
	<canvas id="canvas"></canvas>
	<div class="container">
		<div class="row care-container">
			<div class="col-md-3">
				<jsp:include page="header.jsp"></jsp:include>
			</div>
			<div class="col-md-9">
				<div class="care-content">
					<div id="campusdetails" class="page">
						<jsp:include page="campusdetails/campusdetails.jsp"></jsp:include>
					</div>
					<div id="candidatedetails" class="page">
						<jsp:include page="candidatedetails/candidatedetails.jsp"></jsp:include>
					</div>
					<div id="panelist" class="page">
						<jsp:include page="panelist/panelist.jsp"></jsp:include>
					</div>
					<div id="groupdiscussion" class="page">
						<jsp:include page="groupdiscussion/groupdiscussion.jsp"></jsp:include>
					</div>
					<div id="interview" class="page">
						<jsp:include page="interview/interview.jsp"></jsp:include>
					</div>
					<div id="reports" class="page">
						<jsp:include page="reports/reports.jsp"></jsp:include>
					</div>
					<div id="consensus" class="page">
						<jsp:include page="consensus/consensus.jsp"></jsp:include>
					</div>
					<div id="admin" class="page">
						<jsp:include page="admin/admin.jsp"></jsp:include>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class='push'></div>
	<div id="footer">
		<script>
			//Initial page
			window.location.hash = "#campusdetails";
		</script>
		<jsp:include page="footer.jsp"></jsp:include>
		
		<!-- Campus Details -->
		<script src="<%=request.getContextPath()%>/js/pages/campusDetails.js"></script>
		<!-- Campus Details -->
		
		<!-- Candidate Details -->
		<script src="<%=request.getContextPath()%>/js/pages/candidateDetails.js"></script>
		<script src="<%=request.getContextPath()%>/js/jquery.form.js"></script>
		<script src="<%=request.getContextPath()%>/js/jquery.tablesorter.js"></script>
		<!-- /Candidate Details -->
		
		<!-- Consensus -->
		<script src="<%=request.getContextPath()%>/js/pages/consensus.js"></script>
		<!-- /Consensus -->
		
		<!-- GD -->
		<script src="<%=request.getContextPath()%>/js/pages/gd.js"></script>
		<!-- /GD -->
		
		<!-- Interview -->
		<script src="<%=request.getContextPath()%>/js/pages/interview.js"></script>
		<!-- /Interview -->
		
		<!-- Panelist -->
		<script src="<%=request.getContextPath()%>/js/pages/panelist.js"></script>
		<script src="<%=request.getContextPath()%>/js/popup_js.js"></script>
		<!-- /Panelist -->
		
		<!-- Reports -->
		<script src="<%=request.getContextPath()%>/js/amchart.js"></script>
		<script src="<%=request.getContextPath()%>/js/funnel1.js"></script>
		<script src="<%=request.getContextPath()%>/js/themes.js"></script>
		<!-- /Reports -->
		
		<!-- Menu Toggle Script -->
		<script src="<%=request.getContextPath()%>/js/pages/reports.js"></script>
		<script src="<%=request.getContextPath()%>/js/pages/admin.js"></script>
	</div>
</body>
</html>