<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

</head>
<body>
	<div class="care-sidebar">
		<!-- SIDEBAR USERPIC -->
		<div class="care-userpic">
			<img src="images/care-new-logo.png" class="img-responsive" alt="">
		</div>
		<!-- END SIDEBAR USERPIC -->
		<!-- SIDEBAR USER TITLE -->
		<div class="care-usertitle">
			<div class="care-usertitle-name">Recruitment Process Tool</div>
			<div class="care-usertitle-job">Powered by Deloitte</div>
		</div>
		<!-- END SIDEBAR USER TITLE -->
		<!-- END SIDEBAR BUTTONS -->
		<!-- SIDEBAR MENU -->
		<div class="care-usermenu">
			<ul class="nav">
				<li class="active" data-url="campusdetails"><a href="#campusdetails"> <i
						class="glyphicon glyphicon-education"></i> Campus Details
				</a></li>
				<li data-url="candidatedetails"><a href="#candidatedetails"> <i class="glyphicon glyphicon-user"></i>
						Candidate Details
				</a></li>
				<li data-url="panelist"><a href="#panelist"> <i
						class="glyphicon glyphicon-list-alt"></i> Panelist
				</a></li>
				<li data-url="groupdiscussion"><a href="#groupdiscussion"> <i class="glyphicon glyphicon-bullhorn"></i>
						Group Discussion
				</a></li>
				<li data-url="interview"><a href="#interview"> <i class="glyphicon glyphicon-blackboard"></i>
						Interview
				</a></li>
				<li data-url="consensus"><a href="#consensus"> <i class="glyphicon glyphicon-certificate"></i>
						Consensus
				</a></li>
				<li data-url="reports"><a href="#reports"> <i class="glyphicon glyphicon-duplicate"></i>
						Reports
				</a></li>
				<li data-url="admin"><a href="#admin"> <i class="glyphicon glyphicon-eye-open"></i>
						Admin
				</a></li>
				<li>
					<a href="<%=request.getContextPath()%>/login"><i class="glyphicon glyphicon-log-out"></i>Logout</a>
				</li>
			</ul>
		</div>
		<!-- END MENU -->
	</div>
	<!--End logo div-->
</body>
</html>