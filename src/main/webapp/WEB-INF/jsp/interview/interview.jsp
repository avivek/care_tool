<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
	<div id="interviewContainer">
		<div id="chosen-campus-dropdown">
			<div id="chosen-campus-text">
				<span id="chosen-campus">Chosen Campus:</span> <span
					class="chosen-campus-name empty">No campus selected on
					Campus Details tab</span>
			</div>
		</div>
		<div class="inteview-cutoff bg-info clearfix">
			<div class="cutoff-container row">
				<label class="col-md-5">Enter the cutoff number:</label> <span
					class="col-md-4"><input type="text"
					class="element text medium"></span>
				<button class="btn btn-success col-md-3">Check</button>
			</div>

			<div class="eligible-container row">
				<label class="col-md-5">Eligible number of candidates:</label> <span
					class="col-md-4"><input type="text"
					class="element text medium" disabled=""></span>
				<button class="btn btn-success col-md-3">Submit</button>
			</div>
		</div>
		<div class="row">
			<div id="freeStudents" class="fspan2 col-md-6">
				<div id="freeStudentsTitle" class="sideTitle">
					<p>Unallocated Students</p>
				</div>
				<ul id="freeStudentsList" class="interviewList">
					<li class="firstListItem">No Free Candidates</li>
				</ul>
			</div>
			<div id="freePanelists" class="fspan2 col-md-6">
				<div id="freePanelistsTitle" class="sideTitle">
					<p>Unallocated Panelists</p>
				</div>
				<ul id="freePanelistsList" class="interviewList">
					<li class="firstListItem">No Free Panelists</li>
				</ul>
			</div>
		</div>
		<div id="interviewMain" class="fspan4">
			<div id="interview-accordion">
				<ul class='interview-parent-ul'>
					<li class='interview-title-li'>
						<h3>Interview Panels</h3>
					</li>
					<li class='interview-panel-title-li cloneSeed'><a href="#"><span>Panel
								1</span></a>
						<ul class='interview-content-ul'>
							<li class='interview-panel-li'>
								<ul class="panelList">
									<li class="emptyPanel"><span>No panelists chosen</span></li>
								</ul>
							</li>
							<li class='interview-attendee-li'>
								<ul class="attendeeList">
									<li class="emptyAttendee"><span>No attendee chosen</span>
									</li>
								</ul>
							</li>
							<li class='interview-control-li'>
								<div class="control-strip" id="interview-control-strip">
									<input id="savePanel" class="hidden"></input>
									<button id="savePanelTrigger" class="btn btn-success">Save
										Group</button>
									<input id="clearPanel" class="hidden" type="button"></input>
									<button id="clearPanelTrigger" class="btn btn-warning">
										Cancel
										</buton>
								</div>
							</li>
						</ul></li>
				</ul>
			</div>
		</div>

		<!-- <div class="spacer fspan2">&nbsp;</div>
	<div id="finalize" class="fspan4">
		<div class="control-strip" id="interview-finalize-strip">
          <span id="interviewFinalzizeTrigger" class="button">Finalize</span>
 		</div>
	</div>
	<div class="spacer fspan2">&nbsp;</div> -->
	</div>

</body>
</html>