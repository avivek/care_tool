<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12">&copy; 2015 Deloitte Pvt Ltd | All
				Rights Reserved | Best viewed in IE9+, Mozilla, Chrome and Safari</div>
		</div>
	</div>
</footer>
<!-- Load Common Scripts -->
<script src="<%=request.getContextPath()%>/js/lib/jquery-2.1.4.min.js"></script>
<script src="<%=request.getContextPath()%>/js/lib/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/lib/TweenMax.min.js"></script>
<script src="<%=request.getContextPath()%>/js/lib/tween-bg-animate.js"></script>
<script src="<%=request.getContextPath()%>/js/care.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery.dataTables.js"></script>
<script src="<%=request.getContextPath()%>/js/datepicker.js"></script>
<script src="<%=request.getContextPath()%>/js/messages.js"></script>
<script src="<%=request.getContextPath()%>/js/pages/common.js"></script>
</body>
</html>
