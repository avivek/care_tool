$j(document).ready(function() {
    	window.isGDSelected = false;
    	window.isInterviewSelected = false;
	$j(".hidePanel").hide();
	loadCampusesForPanelist();
	var selectedCampus;
	
	$j("#selectCampusForPanelist").on('change',function(){		
	    selectedCampus = $j('#selectCampusForPanelist').find(":selected").val();			
	});
	
	jQuery('#campusModal').modal('show');
	
	jQuery("#save-campus").on("click", function () {
	    if (selectedCampus) {
		$j("ul#gdContainerUl.panelistSchedule").empty();
		listPanelistGD(selectedCampus);		
		$j("ul#interviewContainerUl.panelistSchedule").empty();
		listPanelistInterview(selectedCampus);
		$j(".hidePanel").show();
		jQuery("#campus-name").text("Campus " + selectedCampus);
	    }
	    jQuery('#campusModal').modal('hide');
	});
	
	
//	listPanelistGD();
//	listPanelistInterview();
	//showGdTable('Technical GD');  //Need to call this function on click of any of the listed GDs in Panelist home page and pass the selected GD name as the function parameter.
	//showInterviewPanel("1");     //Need to call this function on click of any of the listed Interviews in Panelist home page and pass the selected interview id as the function parameter.
	$j("li#panelistLanding"/*"+sessionStorage.getItem("active-link")*/).trigger("click");
	
	$j("body").on('click', 'ul#gdContainerUl li a', function(e){
		e.preventDefault();
		var gdName = $j.trim($j(this).text());
		if(gdName != ''){
		    	window.isGDSelected = true;
			showGdTable(gdName,selectedCampus);
			$j("li#panelistGd").trigger("click");
			//Set active menu
			jQuery("li[data-url='panelistGD']").trigger("click");
		}		
	});
	$j("body").on('click', 'ul#interviewContainerUl li a', function(e){
		e.preventDefault();
		window.isInterviewSelected = false;
		var intId = $j.trim($j(this).find('span.hidden').text());
		showInterviewPanel(intId);
		$j("li#panelistInterview").trigger("click");
		interviewerDecision();
		//Set active menu
		jQuery("li[data-url='panelistInterviews']").trigger("click");
	});
});

