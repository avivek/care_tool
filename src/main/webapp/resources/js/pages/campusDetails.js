var careToolVar = {};

jQuery("#add-campus").on("click", function () {
    $j("#campus-details-container p").empty();
    $j("div#campus-form-container").hide();
    $j("div#campus-details-container").addClass("addCampusTrigger");
    $j("#addCampusPopup").show();
});

function validateNumericField(numericals, numericalsError) {
	if (numericals != null && numericals !== '') {
		var regex = new RegExp(/^\+?[0-9(),.-]+$/);
		if (!numericals.match(regex)) {
			$j('#' + numericalsError).val('Should contain only numeric values');
			$j('#' + numericalsError).show();
			return false;
		} else {
			return true;
		}
	}
}

function validateRange(min, max, value, numericalsError) {
	if (value != null && value != '') {
		if (value < min || value > max) {
			$j('#' + numericalsError)
					.val(
							'Should contain only values between ' + min
									+ ' and ' + max);
			$j('#' + numericalsError).show();
			return false;
		} else {
			return true;
		}
	}
}

function save() {
	var offeredStudents = $j('#offeredStudents').val().trim();
	var conversionRate = $j('#conversionRate').val().trim();
	var winRate = $j('#winRate').val().trim();
	var slot = $j('#slot').val().trim();
	var sample = $j('#sample').val().trim();
	var interns = $j('#interns').val().trim();

	var validateOs = validateNumericField(offeredStudents, "offeredStudentsError");
	var validateCr = validateNumericField(conversionRate, "conversionRateError");
	var validateWr = validateNumericField(winRate, "winRateError");
	var validateSl = validateNumericField(slot, "slotError");
	var validateSa = validateNumericField(sample, "sampleError");
	var validateIn = validateNumericField(interns, "internsError");

	var validateOsr = validateRange(0, 10000, offeredStudents,
			"offeredStudentsError");
	var validateCrr = validateRange(0, 100, conversionRate,
			"conversionRateError");
	var validateWrr = validateRange(0, 100, winRate, "winRateError");
	var validateSlr = validateRange(0, 100, slot, "slotError");
	var validateSar = validateRange(0, 10000, sample, "sampleError");
	var validateInr = validateRange(0, 10000, interns, "internsError");
	var saveAjax = $j.ajax({
		url : "/CareTool/campusdetails/save/?id="
				+ $j('select#selectCampus').val(),
		// data : $j("#campus-form").find("select,textarea,input").serialize(),
		data : $j("#campus-form").find("select,textarea,input").serialize()
				+ "&offeredStudents=" + $j("#offeredStudents").val()
				+ "&conversionRate=" + $j("#conversionRate").val()
				+ "&winRate=" + $j("#winRate").val() + "&slot="
				+ $j("#slot").val() + "&sample=" + $j("#sample").val()
				+ "&interns=" + $j("#interns").val() + "&year="
				+ $j("#hire-years option:selected").val(),
		type : "GET",
		success : function(response) {
			// saveCampusHiringDetails();
			$j("#campus-form").html(response);
			var yearSelected = $j("#hire-years option:selected").val();
			if (yearSelected != 0 || yearSelected != null) {
				getval($j('#id').val());
			}
			else{
				$j("div#hiring-stats-container").addClass("emptyForm");
				$j("div#hiring-stats-container").children().hide();
				$j("div#hiring-stats-container").append("<p>No Year Selected</p>"); 
			}
		}
	});

	if (validateOs == false || validateCr == false || validateWr == false
			|| validateSl == false || validateSa == false
			|| validateIn == false || validateOsr == false
			|| validateCrr == false || validateWrr == false
			|| validateSlr == false || validateSar == false
			|| validateInr == false) {
		saveAjax.abort();
	}
}

function edit() {
	$j.ajax({
		url : "/CareTool/campusdetails/editCampusDetailsdisplay/?id="+ $j('select#selectCampus').val(),
		data : $j("#campus-form").find("select,textarea,input").serialize(),
		type : "GET",
		success : function(response) {
			// alert('In Sucess'+response);
			$j("#campus-form").html(response);
			var campusID = $j('#id').val();
			var yearSelected = $j("#hire-years option:selected").val();
			// alert('yearSelected:::'+yearSelected)
			if (yearSelected != 0 || yearSelected !=null) {
				getval($j('#id').val());
			}
			else{
				$j("div#hiring-stats-container").addClass("emptyForm");
				$j("div#hiring-stats-container").children().hide();
				$j("div#hiring-stats-container").append("<p>No Year Selected</p>");
			}
			
		}
	});
}

function deleteCD() {
	if (confirm("Are you sure?")) {
		// your deletion code
		$j.ajax({
			url : "/CareTool/campusdetails/delete/?id="
					+ $j('select#selectCampus').val(),
			data : $j("#campus-form").serialize(),
			type : "GET",
			success : function(response) {
				// alert('In delete');
				$j("#campus-form").html(response);
			},
			error : function(response) {
				alert('Error occured' + response);
			}
		});
	}
	return false;
}

function getval(sel) {
	if (($j("select#hire-years").val() == 0)
			|| ($j("select#hire-years").val() == null)) {
		$j("div#hiring-stats-container").addClass("emptyForm");
		$j("div#hiring-stats-container").children().hide();
		$j("div#hiring-stats-container").append("<p>No Year Selected</p>");
	} else {
		$j.ajax({
			url : "/CareTool/campusdetails/getHiringStats/?id="
					+ $j('#id').val() + "&year="
					+ $j("#hire-years option:selected").text(),
			type : "GET",
			success : function(response) {

				$j("div#hiring-stats-container").removeClass("emptyForm");
				$j("div#hiring-stats-container p").hide();
				$j('#hiring-stats-container').show();
				// alert('get val success');
				// alert('In Sucess'+response.id);
				// alert('In Sucess'+response.offeredStudents);
				$j('#offeredStudents').val(response.offeredStudents);
				$j('#conversionRate').val(response.conversionRate);
				$j('#winRate').val(response.winRate);
				$j('#slot').val(response.slot);
				$j('#sample').val(response.sample);
				$j('#interns').val(response.interns);
				$j('#hiring-stats-container').children("li").show();
			}
		});
	}
   
}
function loadCampusList(firstLoad) {
	// alert("static called");
	if(firstLoad){
			$j.ajax({
			url : "/CareTool/admin/get_campus_list/",
			type : "GET",
			success : function(data) {
				$j('select#selectCampus').append(
						new Option("Select a Campus", 0));
				for ( var i = 0; i < data.length; i++) {
					$j('select#selectCampus').append(
							new Option(data[i].campusName, data[i].id));
				}
				$j('select#selectCampus').append(
				new Option("Add a Campus", "addCampus"));
				$j("select#selectCampus option:first-child").addClass(
						"placeholder");
//				$j(".campus-chosen-select").chosen();
			},
			error : function(response) {
				console.log("Error occured while fetching Campus List"
						+ response);
			}
		});
	}
	else{
		if($j('select#selectCampus option').length!=0){
			$j('select#selectCampus').empty();
		}
		$j.ajax({
			url : "/CareTool/admin/get_campus_list/",
			type : "GET",
			success : function(data) {
				$j('select#selectCampus').append(
						new Option("Select a Campus", 0));
				for ( var i = 0; i < data.length; i++) {
					$j('select#selectCampus').append(
							new Option(data[i].campusName, data[i].id));
				}
				$j('select#selectCampus').append(
				new Option("Add a Campus", "addCampus"));
				$j("select#selectCampus option:first-child").addClass(
						"placeholder");
				$j(".campus-chosen-select").trigger('chosen:updated');
				$j('div#selectCampus_chosen chosen-results ul li:nth-last-child(2)').click();
			},
			error : function(response) {
				console.log("Error occured while fetching Campus List"
						+ response);
			}
		});
	}
}

	function addCampus(){  
		var campusName =  $j("#addCampusName").val();
		var campusDesc =  $j("#addCampusDesc").val();
		$j.ajax({
			type : "GET",
			url : "/CareTool/admin/campus/add",
			async : true,
			data : "campusName=" + campusName + "&campusDesc=" + campusDesc ,
			success : function(event) {	
				loadCampusList(false);
				$j("span.chosen-campus-name").removeClass('empty').text(campusName);
				$j("#addCampusPopup").hide();
				$j('div#campus-details-container').append("<p class=\"newCampus\">New Campus: "+campusName+" Added Successfully</p>");
			},
			error : function(response) {
				alert('Error occured'+response);
			}
		});	
	}
	
	function loadCampusesForPanelist(){
		
		loadCampusListForPanelist();
		
	}
	function loadCampusListForPanelist(){
		
		$j.ajax({
			url : "/CareTool/admin/get_campus_list_for_panelist/",
			type : "GET",
			data : "panelistId=" + panelistId ,
			success : function(data) {
				$j('select#selectCampusForPanelist').append(
						new Option("Select a Campus", 0));				
				for ( var i = 0; i < data.length; i++) {
					$j('select#selectCampusForPanelist').append(
							new Option(data[i].campusName, data[i].id));
				}
				$j("select#selectCampusForPanelist option:first-child").addClass(
						"placeholder");
				$j(".campus-chosen-select").chosen();
			},
			error : function(response) {
				console.log("Error occured while fetching Campus List"
						+ response);
			}
		});
		
	}
	
	function intParser(value) {
		var intValue = parseInt(value)
		return isNaN(intValue) ? "" : intValue;
	}
	
	 
function loadCampus() {
	loadCampusList(true);
	//alert('calleed inner child111:::');
	$j('#campus-form').hide();
	$j('select#selectCampus').change(function(event) {
					if (($j(this).val() == 0)
							|| ($j(this).val() == null)) {
						$j("div#campus-details-container").addClass("emptyForm");
						$j("div#campus-details-container").removeClass("addCampusTrigger");
						$j("div#campus-details-container").remove("div#plusContainer");
						$j("div#campus-details-container").append("<p>No Campus Selected</p>");
						$j("#campus-form").empty();
						$j("div#addCampusPopup").hide();						
						$j("span.chosen-campus-name").removeClass('empty').text($j("#selectCampus option:selected").text());
					}
					else
					{	
						$j("div#campus-form-container").show();
						$j("div#addCampusPopup").hide();
						$j('#campus-form').show();
						var campusid = $j(this).val();
						$j.ajax({
							type : "GET",
							url : "/CareTool/campusdetails/get/" + $j(this).val(),
							async : true,
							success : function(response) {
								var campusJson = JSON.parse(response);
								var campusFieldList = campusJson.campusDetails;
								$j('input#campusId').val(campusJson.campusId);
								$j('input#alumni').val(campusFieldList.aluminiDetails);
								$j('input#plcmtofficer').val(campusFieldList.placementOfficer);
								$j('input#talentpool').val(campusFieldList.talentPool);
								$j('input#win').val(campusFieldList.winPercentage);
								$j('input#brandingevents').val(campusFieldList.brandingEvents);
								$j('input#competition').val(campusFieldList.competitiononCampus);
								$j('input#avgsalary').val(campusFieldList.avgSal);
								$j('select#slots').val(parseInt(campusFieldList.slots)).trigger('chosen:updated');
								$j('input#pastvisits').val(campusFieldList.pastvisitstoCampus);
								$j('select#tier').val(campusFieldList.deloitteTier).trigger('chosen:updated');
								$j('input#slvisiting').val(campusFieldList.serviceLineVisiting);
								$j('textarea#our-exp').val(campusFieldList.ourExperience);								
								//Imran
								$j("input#ppd").val(campusFieldList.ppd);
								
								// alert('in sucess response');
								$j("div#campus-details-container").removeClass(
										"emptyForm");
								$j("div#campus-details-container").removeClass("addCampusTrigger");
								$j("div.plusContainer").hide();
								$j("div#campus-details-container p").hide();
								/*$j('#campus-form').html(response);*/
								// createCandidateList();
								$j('form#cand-upload-form').hide();
								populateCandidateList();
								currentCampusPanelistMapping();
//								$j("select#hire-years").chosen({
//									disable_search_threshold : 10
//								});
//								$j("select#hire-years").val('0').trigger('chosen:updated');
								$j("div#hiring-stats-container").addClass("emptyForm");
									$j("div#hiring-stats-container").children().hide();
									$j("div#hiring-stats-container").append(
											"<p>No Year Selected</p>");
								$j("span.chosen-campus-name").removeClass('empty').text($j("#selectCampus option:selected").text());
								
								//New code starts - Author: DKN		
								careToolVar.panelistFreeze = campusJson.panelistFreeze,
						  		careToolVar.groupDistributionFreeze = campusJson.groupDistributionFreeze,
						  		careToolVar.candidateFreeze = campusJson.candidateFreeze; 			
								careToolVar.groupDiscussionFinalize = false;
								
								var finalizeCandDetails = false;
								
						  		//DKN - Disable buttons on permissions	
							 	if(careToolVar.candidateFreeze == 'Y') {
							 	    finalizeCandDetails = true;
							 	    $j('span#finalizeCandTrigger').removeClass("btn-success active").addClass("btn-default");
							 	} else {
							 	   finalizeCandDetails = false;
							 	    $j('span#finalizeCandTrigger').removeClass("btn-default").addClass("btn-success active");
							 	}
							 	
							 	if(careToolVar.panelistFreeze == 'Y') {
							 	    $j('span#finalizePanelTrigger').removeClass("btn-success active").addClass("btn-default");
							 	} else {
							 	    $j('span#finalizePanelTrigger').removeClass("btn-default").addClass("btn-success active");
							 	}
							 	
							 	if(careToolVar.panelistFreeze == 'Y' && careToolVar.candidateFreeze == 'Y' && campusJson.groupDistributionFreeze == 'N') $j('#finalizeGdTrigger, #genGdTrigger').addClass('active');
								else $j('#finalizeGdTrigger, #genGdTrigger').removeClass('active');

								//default hide Interview Tab
								$j('li#interview').hide();
								$j('li#interviewInactive').show();

								if(careToolVar.panelistFreeze == 'Y' && careToolVar.candidateFreeze == 'Y'){									
									if(careToolVar.groupDistributionFreeze == 'Y'){									
										finalizeInterview(campusid);
									}
									$j('li#gd').show();
									$j('li#gdInactive').hide();
								}
								else{
									$j('li#gd').hide();
									$j('li#gdInactive').show();
								}			

							/*	if(careToolVar.groupDiscussionFinalize || careToolVar.groupDistributionFreeze){
									$j('li#interview').show();
									$j('li#interviewInactive').hide();					
								}else{
									$j('li#interview').hide();
									$j('li#interviewInactive').show();
								}
*/
								consensusTable();	//Call for consensus table	
								//finalizeGD();		
								updatePanelBtns();	
							},
							error : function(response) {
								$j('input#campusId').val($j(this).val());
								$j("div#campus-details-container").removeClass(
								"emptyForm");
								$j("div#campus-details-container").removeClass("addCampusTrigger");
								$j("div.plusContainer").hide();
								$j("div#campus-details-container p").hide();
								populateCandidateList();
								currentCampusPanelistMapping();
//								$j("select#hire-years").chosen({
//									disable_search_threshold : 10
//								});
//								$j("select#hire-years").val('0').trigger('chosen:updated');
								$j("div#hiring-stats-container").addClass("emptyForm");
									$j("div#hiring-stats-container").children().hide();
									$j("div#hiring-stats-container").append(
											"<p>No Year Selected</p>");
								$j("span.chosen-campus-name").removeClass('empty').text($j("#selectCampus option:selected").text());
								
							}
						});
					}

				event.preventDefault();
			});
}
function finalizeInterview(id){
	genGdGroup('disable');
	var gdGroupTitle = $j('li.gd-group-title-li');
	var gdMasterFObject = [];
	$j.each(gdGroupTitle, function(i, item){
		gdMasterFObject.push(prepareGdJSON((i+1), item));
	});
	$j.ajax({
		type : "GET",
		url : "/CareTool/groupdiscussion/fetchFinalized/"+id,
		dataType: 'text',
		data : { jsondata : JSON.stringify(gdMasterFObject)},
		success : function(response) {
			updatePanelistList();
			updateCandidateList();
			$j('li#interview').show();
			$j('li#interviewInactive').hide();
		},
		error: function(response){
			care.popup(care.messages.candidateDetailsSaveErrorTitle, care.messages.candidateDetailsSaveErrorBody,"");	
			$j('li#interview').hide();
			$j('li#interviewInactive').show();				
		}
	});
}
function initializePage() {
	//First 
	if (($j("select#selectCampus").val() == 0)
			|| ($j("select#selectCampus").val() == null)) {
		$j("div#campus-details-container").addClass("emptyForm");
		$j("div#campus-details-container").append("<p>No Campus Selected</p>");
		$j("#campus-form").empty();
	}
}

function editFeatureFunction(){
	/*$j("div#campus-details").on("keydown", ".element", function() {
		$j("span#cancelBtClkTrigger").show().fadeIn("slow");
	});*/
}
function saveCampus() {
	var validationFlag = true;
	
	if(!validateElementsWithClass("numeric")) {
		validationFlag = false;
	}
	
	if( !validateElementsWithClass("float")) {
		validationFlag = false;
	}
	
	if(!validationFlag) {
		return false;
	}
	$j('span#saveBtClkTrigger').addClass('loading');
	var campusMasterObject = {};
	campusMasterObject.campusId=$j('select#selectCampus').val();
	campusMasterObject.campusDetails = [];
	var campusInputs, campusSelects, campusTextareas = [];
	
	campusInputs = $j('#hiring-stats-container > li > div > input');	
	campusInputs.push.apply(campusInputs, $j('#right-col > ul > li > div > input'));	
	campusInputs.push.apply(campusInputs, $j('#left-col > ul > li > div > input'));
	campusInputs.push.apply(campusInputs, $j('#campus-logo > ul > li > div > input'));
	
	campusSelects = $j("#left-col > ul > li > div > select");			
//	campusSelects.push.apply(campusSelects, $j("#right-col > ul > select"));
	campusTextareas = $j("#our-exp");

	/*campusInputs = $j('#campus-form-container > div > ul > li > div > input');
	campusSelects = $j('#campus-form-container > div > ul > li > div > select');
	campusTextareas = $j('#campus-form-container > div > ul > li > div > textarea');*/
	var index = 0;
	$j.each(campusInputs, function(i, input){
		campusMasterObject.campusDetails[index] = {};
		if($j(input).val()!=null && $j(input).val()!=""){
			campusMasterObject.campusDetails[index][$j(input).attr('id')] = $j(input).val();
			index++;
		}
		else{
			campusMasterObject.campusDetails[index][$j(input).attr('id')] = 0;
			index++;
		}
	});
	$j.each(campusSelects, function(i, input){
		campusMasterObject.campusDetails[index] = {};
		if($j(input).val()!=null && $j(input).val()!=""){
			campusMasterObject.campusDetails[index][$j(input).attr('id')] = $j(input).val();
			index++;
		}
		else{
			campusMasterObject.campusDetails[index][$j(input).attr('id')] = 0;
			index++;
		}
	});
	$j.each(campusTextareas, function(i, input){
		campusMasterObject.campusDetails[index] = {};
		if($j(input).val()!=null && $j(input).val()!=""){
			campusMasterObject.campusDetails[index][$j(input).attr('id')] = $j(input).val();
			index++;
		}
		else{
			campusMasterObject.campusDetails[index][$j(input).attr('id')] = 0;
			index++;
		}
	});
	var campusSet = JSON.stringify(campusMasterObject);
	var saveAjax = $j.ajax({
		url : "/CareTool/campusdetails/save?campusId="+$j("select#selectCampus").val(),
		datatype: 'text',
		data : {jsondata:campusSet},
		type : "POST",
		success : function(response) {
			console.log(response);
			$j('span#saveBtClkTrigger').removeClass('loading');
			alert("Campus details saved successfully.");
		}
	});
	
}

care.popup = function(title, message, footer){
	var pop = $j('#carePopup');
	pop.find('#popupTitle').html(title);
	pop.find('#popupBody').html(message);
	pop.find('#popupFooter').html(footer);
	if(!$j('.overlayPop').length) $j('body').append('<div class="overlayPop"></div>');
	$j('.overlayPop').show().after(pop.show());
	//pop.show();
}


/*
 * @Auhor Imran 21/05/2015
 * 
 */
function previewImage(imageInput) {
	
	if(imageInput.files && imageInput.files[0]) {
		var reader = new FileReader();
		reader.readAsDataURL(imageInput.files[0]);
		
		reader.onload = function(e) {
			//$j('#campusLogoImg').attr(src, e.target.result);
			document.getElementById("campusLogoImg").src = e.target.result;
			$("#campusLogoImg").css('display', 'block');
			$("#campusLogoImg").css('width', '100px');
			$("#campusLogoImg").css('height', '100px');
			$("#imgAltHdr").hide();
		}
		
	}
	
}

/*
 * Dynamically populates years into the select element for hire years.
 * Starts from startYear till current year. 
 */
function populateYears() {
	var today = new Date();
	var currentYear = today.getFullYear();
	var startYear = 2000;
	var options = '<option value=""></option>';
	for(year = currentYear; year >= startYear ; year--) {
		options += '<option value="'+year+'">'+year+'</option>';
	}
	$j('#hire-years').html(options);
}


function validateRegex(pattern, value) {
	return pattern.test(value);
}

/*
 * Reg ex for number validation
 */
function isNumeric(value) {
	var pattern = new RegExp('^[0-9]+$');
	return pattern.test(value);
}


/*
 * Reg ex for float validation
 */
function isFloat(value) {
	var pattern = new RegExp('^[-+]?[0-9]*\.?[0-9]+$');
	return pattern.test(value);
}

/*
 * show generic error message just each HTML element 
 */
function showErrorMessage(element, message) {
	$j(element).before('<div id="errorMsg" class="error-msg">'+message+'</div>');
	$j(element).addClass("validation-error");
}

/*
 * Removes error message over the element passed. 
 */
function clearErrorMessage(element) {
	$j(element).prev('div #errorMsg').remove();
	$j(element).removeClass("validation-error");
}


/*
 * Generic code that calls a function determined by the class of the HTML element passed.
 * callbackFunction w.r.t the element class is called for validation.
 * If validation fails, error message passed is appended just before the element.  
 */
function validate(inputElement, callbackFunction, errorMessage) {
	var result = true;
	if($j(inputElement).val().trim() != "" && !callbackFunction($j(inputElement).val().trim())) {
		clearErrorMessage(inputElement);
		showErrorMessage(inputElement,errorMessage);
		//$j(inputElement).focus();
		result = false;
	} else {
		clearErrorMessage(inputElement);
	}
	return result;
}


function validateElementsWithClass(clasz) {
	var isValid = true;
	var isFocusSet = false;
	var callbackFunction = null;
	var errorMessage = null;
	if(clasz == "numeric") {
		callbackFunction = isNumeric;
	}
	switch(clasz) {
	case "numeric":
		callbackFunction = isNumeric;
		errorMessage = "Please enter a valid number";
		break;
	case "float":
		callbackFunction = isFloat;
		errorMessage = "Please enter a valid decimal number";
		break;
	default:
		callbackFunction = null;
	
	}
	
	$j('.'+clasz).each(function(i,obj) {
		if(callbackFunction != null && !validate($j(this), callbackFunction, errorMessage)) {
			isValid = false;
		}
		if(!isValid && !isFocusSet) {
			isFocusSet = true;
			$j(this).focus();
		}
	});
	
	return isValid;
}
$j(document).ready(function() {
	editFeatureFunction();
	populateYears();
	$j("#logotrigger").click(function() {
		$j("#campuslogo").click();
	});
	$j("#candimgtrigger").click(function() {
		$j("#candimg").click();
	});
	$j("#addrowCandTrigger").click(function() {
		$j("#addrowCand").click();
		$j('select#selectCampus').val('2').trigger('chosen:updated');
	});
	$j("#cancelCandTrigger").click(function() {
		$j("#cancelCand").click();
	});
	/*$j("#saveCandTrigger").click(function() {
		$j("#saveCand").click();
	});*/
	$j("#finalizeCandTrigger").click(function() {
		$j("#finalizeCand").click();
	});
	$j("#addCampusTrigger").click(function(){
		addCampus();
	});
	$j('#saveBtClkTrigger').click(function(){
		saveCampus();
	});
	/*$j('#cancelBtClkTrigger').click(function(){
		clearChanges();
	});*/
	loadCampus();
	initializePage();
//	$j('div#campus-form-container select#tier,div#campus-form-container select#slots ').chosen({
//		disable_search_threshold : 10,
//		width: "77%"
//	});
	$j('select#hire-years').change(function(){
		var hireYear = $j(this).val();
		var campusId = $j('select#selectCampus').val();
		$j.ajax({
			type : "GET",
			url : "/CareTool/campusdetails/getHiringStats/",
			async : true,
			data : "campusId=" + campusId + "&hireYear=" + hireYear,
			success : function(response) {
				var campusHiringJSONObject = JSON.parse(response);
				if(campusHiringJSONObject.success){
					var campusHiringFieldList = campusHiringJSONObject.hiringStats[0];
					$j('input#offers').val(campusHiringFieldList.offeredStudents);
					$j('input#conv-rate').val(campusHiringFieldList.conversionRate);
					$j('input#win-ratio').val(campusHiringFieldList.winRate);
					$j('input#int-ppo').val(campusHiringFieldList.interns);
					$j('input#slot-hist').val(campusHiringFieldList.slot);
					$j('input#attrition').val(campusHiringFieldList.sample);
					$j('div#hiring-stats-container').removeClass('emptyForm');
					$j('div#hiring-stats-container > li').show();
					$j('div#hiring-stats-container p').remove();
				}
				else{
					$j("div#hiring-stats-container").addClass(
							"emptyForm");
					$j("div#hiring-stats-container").children().hide();
					$j("div#hiring-stats-container").append("<p>No Data for Selected Year</p>");
				}
			},
			error : function(response) {
				alert('Error occured'+response);
			}
		});
	});
	if($j('li#gd').length){
		var disGd = $j('li#gd').clone().attr('id', 'gdInactive').hide();
		$j('li#gd').after(disGd);
	}
	$j('#gdInactive').on('click', function(e){
		return false;
	});
	if($j('li#interview').length){
		var disInterview = $j('li#interview').clone().attr('id', 'interviewInactive').hide();
		$j('li#interview').after(disInterview);
	}
	$j('#interviewInactive').on('click', function(e){
		return false;
	});
	$j('#campuslogo').on('change', function(){
		//alert("right here");
		previewImage(this);
	});
});
