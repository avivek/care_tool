var candidateTable = null;
var candidateListBlob = null;
var campusID = null;
var options = null;
function getCandidatetails(candidateId) {
	//alert("get candidate details");
	$j.ajax({
		url : "/CareTool/candidatedetails/getCanDetail/" + candidateId,
		type : "GET",
		success : function(response) {
			//alert('in here' + response);
			$j('div#detail-view').removeClass('loading');
			$j("div#detail-view").html(response);
		},
		error: function(response){
			care.popup(care.messages.candidateDetailsErrorTitle, care.messages.candidateDetailsErrorBody,"");
		}
	});
}

function addCandidateBtClk() {
	$j.ajax({
		url : "/CareTool/candidatedetails/savepage/",
		type : "GET",
		success : function(response) {
			//alert('In Sucess' + response);
			$j("#candidateDetailsForm").html(response);
			$j("#addCandidate").hide();
			$j("#dob").datepicker();
			$j('.cid').val($j('#selectCampus').val());
			$j('#cid').val($j('#selectCampus').val());
			//alert('2:::::::' + $j('#cid').val());
		}
	});
}

function saveCandidateBtClk() {
	$j.ajax({
		url : "/CareTool/candidatedetails/save/",
		data : $j("#candidateDetailsForm").find("select,textarea,input,img")
				.serialize(),
		type : "GET",
		success : function(response) {
			$j('div#detail-view').removeClass('loading');
			alert("Information saved successfully");
			setTimeout(function(){
				populateCandidateList();
			}, 200);
			$j("#dob").DatePicker({
				flat: true,
				date: $j("#dob").val(),
				current: $j("#dob").val(),
				calendars: 1,
				starts: 1
			});
			 $j("div#detail-view").hide();
			 $j('#addrowCandTrigger,#finalizchosen-campus-texteCandTrigger').show();
			 $j('#saveCandTrigger,#cancelCandTrigger').hide();
			 $j('#addrowCandTrigger,#finalizeCandTrigger').show();
		},
		error: function(response){
			care.popup(care.messages.candidateDetailsSaveErrorTitle, care.messages.candidateDetailsSaveErrorBody,"");
		}
	});
}

function editCandidateBtClk() {
	var cndId = $j('#candidateId').val();
	//alert('cndId' + cndId);
	$j.ajax({
		url : "/CareTool/candidatedetails/getCanDetailForEdit/" + cndId,
		type : "GET",
		success : function(response) {
			//alert('In edit' + response);
			$j("div#detail-view").empty();
			$j("div#detail-view").html(response);
			$j("#dob").DatePicker({
					flat: true,
					date: $j("#dob").val(),
					current: $j("#dob").val(),
					calendars: 1,
					starts: 1
					});
			/*$j('.cid').val($j('#selectCampus').val());
			$j('#cid').val($j('#selectCampus').val());*/
		}
	});
}
function handleAjaxError( xhr, textStatus, error ) {
    if ( textStatus === 'timeout' ) {
    	care.popup("Server Timeout", "");
    }
    else {
    		care.popup(care.messages.candidateDetailsFetchServerTimeoutTitle, 
    				care.messages.candidateDetailsFetchServerTimeoutBody, care.messages.candidateDetailsFetchServerTimeoutFooter);
    }
}
function populateCandidateList() {
	campusID = $j('select#selectCampus').val();
	options = { 
		    target:   '#output',   // target element(s) to be updated with server response 
		    beforeSubmit:  beforeSubmit,  // pre-submit callback 
		    success:       afterSuccess,  // post-submit callback 
		    uploadProgress: OnProgress, //upload progress callback 
		    resetForm: true ,       // reset the form after successful submit 
		    url: '/CareTool/candidatedetails/massupload'  ,
		    data: { id : campusID },
		    type: 'POST'
		};
	
	 $j('table#candidates').dataTable( {
        "bProcessing": false,
        "bDestroy": true,
        "bAutoWidth": true, 
        "iDisplayLength": 10,
        "sAjaxSource": "/CareTool/candidatedetails/get/" + $j('select#selectCampus').val(),
        "fnServerData": function (sSource, aoData, fnCallback ) {
            $j.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": "/CareTool/candidatedetails/get/"+$j('select#selectCampus').val(),
                "success": fnCallback,
                "timeout": 15000,   // optional if you want to handle timeouts (which you should)
                "error": function(error){
                	console.log("Data error"+JSON.stringify(error));
                } // this sets up jQuery to give me errors
            } );
        },
        "oLanguage": {
            "sZeroRecords": "No records to display"
          },
        "aoColumns": [
	                     /* { "mData":"candidateId",  "bVisible": false},*/
	                      { "mData":"firstName", "sWidth":"auto"},
	                      { "mData":"lastName", "sWidth":"auto"},
	                      { "mData":"gender", "sWidth":"auto"},
	                      { "mData":"quantitativeAbilityScore", "sWidth":"auto"},
	                      { "mData":"englishScore", "sWidth":"auto"},
	                      { "mData":"logicalAbilityScore", "sWidth":"auto"},
	                      { "mData":"degreeName", "sWidth":"auto"},
	                      { "mData":"subjectName", "sWidth":"auto"}
                      ]
    } );
	$j('form#cand-upload-form').show();
}

function makeViewDetailsButton_html(cellvalue, options, rowObject) {
	return '<input type="button" onclick="getCandidatetails(' + cellvalue
			+ ')"  id="btn' + cellvalue + '" value="View Details"/>';
}

function appendPercent(cellvalue, options, rowObject) {
	if (cellvalue != null)
		return cellvalue + '%';
	else
		return '';
}


function afterSuccess(){
	$j('form#cand-upload-form').hide();
	candidateTable.fnReloadAjax();
	$j('div#output,div#progressbox').hide();
	return true;
}
/* Custom filtering function which will filter data in column four between two values */
$j.fn.dataTableExt.afnFiltering.push(
    function( oSettings, aData, iDataIndex ) {
    	var iApt = 3;
        var iAptMin = document.getElementById('aptMin').value * 1;
        var iAptMax = 2000;
         
        var iAptitude = aData[iApt] == "-" ? 0 : aData[iApt]*1;
        if ( iAptMin == "" && iAptMax == "" )
        {
            return true;
        }
        else if ( iAptMin == "" && iAptitude < iAptMax )
        {
            return true;
        }
        else if ( iAptMin <= iAptitude && "" == iAptMax )
        {
            return true;
        }
        else if ( iAptMin <= iAptitude && iAptitude < iAptMax )
        {
            return true;
        }
        return false;
        
    }
);
$j.fn.dataTableExt.afnFiltering.push(
	    function( oSettings, aData, iDataIndex ) {
	        var iEng = 4;
	        var iEngMin = document.getElementById('engMin').value * 1;
	        var iEngMax = 2000;
	         
	        var iEnglish = aData[iEng] == "-" ? 0 : aData[iEng]*1;
	        if ( iEngMin == "" && iEngMax == "" )
	        {
	            return true;
	        }
	        else if ( iEngMin == "" && iEnglish < iEngMax )
	        {
	            return true;
	        }
	        else if ( iEngMin <= iEnglish && "" == iEngMax )
	        {
	            return true;
	        }
	        else if ( iEngMin <= iEnglish && iEnglish < iEngMax )
	        {
	            return true;
	        }
	        return false;
	    }
);
$j.fn.dataTableExt.afnFiltering.push(
	function( oSettings, aData, iDataIndex ) {
		var iLog = 5;
        var iLogMin = document.getElementById('logMin').value * 1;
        var iLogMax = 2000;
         
        var iLogical = aData[iLog] == "-" ? 0 : aData[iLog]*1;
        if ( iLogMin == "" && iLogMax == "" )
        {
            return true;
        }
        else if ( iLogMin == "" && iLogical < iLogMax )
        {
            return true;
        }
        else if ( iLogMin <= iLogical && "" == iLogMax )
        {
            return true;
        }
        else if ( iLogMin <= iLogical && iLogical < iLogMax )
        {
            return true;
        }
        return false;
	}
);
$j.fn.dataTableExt.oApi.fnReloadAjax = function ( oSettings, sNewSource, fnCallback, bStandingRedraw )
{
    // DataTables 1.10 compatibility - if 1.10 then versionCheck exists.
    // 1.10s API has ajax reloading built in, so we use those abilities
    // directly.
    if ( $j.fn.dataTable.versionCheck ) {
        var api = new $j.fn.dataTable.Api( oSettings );
 
        if ( sNewSource ) {
            api.ajax.url( sNewSource ).load( fnCallback, !bStandingRedraw );
        }
        else {
            api.ajax.reload( fnCallback, !bStandingRedraw );
        }
        return;
    }
 
    if ( sNewSource !== undefined && sNewSource !== null ) {
        oSettings.sAjaxSource = sNewSource;
    }
 
    // Server-side processing should just call fnDraw
    if ( oSettings.oFeatures.bServerSide ) {
        this.fnDraw();
        return;
    }
 
    this.oApi._fnProcessingDisplay( oSettings, true );
    var that = this;
    var iStart = oSettings._iDisplayStart;
    var aData = [];
 
    this.oApi._fnServerParams( oSettings, aData );
 
    oSettings.fnServerData.call( oSettings.oInstance, oSettings.sAjaxSource, aData, function(json) {
        /* Clear the old information from the table */
        that.oApi._fnClearTable( oSettings );
 
        /* Got the data - add it to the table */
        var aData =  (oSettings.sAjaxDataProp !== "") ?
            that.oApi._fnGetObjectDataFn( oSettings.sAjaxDataProp )( json ) : json;
 
        for ( var i=0 ; i<aData.length ; i++ )
        {
            that.oApi._fnAddData( oSettings, aData[i] );
        }
         
        oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
 
        that.fnDraw();
 
        if ( bStandingRedraw === true )
        {
            oSettings._iDisplayStart = iStart;
            that.oApi._fnCalculateEnd( oSettings );
            that.fnDraw( false );
        }
 
        that.oApi._fnProcessingDisplay( oSettings, false );
 
        /* Callback user function - for event handlers etc */
        if ( typeof fnCallback == 'function' && fnCallback !== null )
        {
            fnCallback( oSettings );
        }
    }, oSettings );
};
function beforeSubmit(){
	   //check whether client browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
	    {
	       var fsize = $j('#bulkUpload')[0].files[0].size; //get file size
	           var ftype = $j('#bulkUpload')[0].files[0].type; // get file type
	        //allow file types 
	           if(!(ftype=='.csv' || ftype=='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || ftype=='application/vnd.ms-excel')){
	             $j("#output").html("<b>"+ftype+"</b> Unsupported file type!");
	             return false;
	           }
	    
	       //Allowed file size is less than 5 MB (1048576 = 1 mb)
	           else if(fsize>1048576) 
		       {
	        	   $j("#output").html("<b>"+fsize +"</b>"+care.messages.candidateDetailsFileTooBig);
	        	   return false;
		       }
	           else 
	        	   return true;
	        }
	        else
	    {
	       //Error for older unsupported browsers that doesn't support HTML5 File API
	       alert("Please upgrade your browser, because your current browser lacks some new features we need!");
	           return false
	    }
	}
function OnProgress(event, position, total, percentComplete)
{
    //Progress bar
    $j('#progressbox').show();
    $j('#progressbar').width(percentComplete + '%') //update progressbar percent complete
    $j('#statustxt').html(percentComplete + '%'); //update status text
    if(percentComplete>50)
        {
            $j('#statustxt').css('color','#000'); //change status text to white after 50%
        }
}
$j(document).ready(function() {
	candidateTable = $j("table#candidates").dataTable( {
        scrollX:        true,
        scrollCollapse: true,
        fixedColumns:   {
            leftColumns: 2
        }
	} );
	$j("#candidateDetailsForm").on("click", ".element", function(){
		$j("input#camp-cancel-button").show().fadeIn("slow");
		editCandidateBtClk();
		}); 
	$j("table#candidates tbody").on('click', 'tr', function(){
		if(!finalizeCandDetails){
			var row = candidateTable.fnGetData(this);
			$j('div#detail-view').show().slideDown('slow');
			$j('div#detail-view').addClass('loading');
			getCandidatetails(row.candidateId);
			$j('#addrowCandTrigger,#finalizeCandTrigger').hide();
			$j('form#cand-upload-form').hide();
			$j('#saveCandTrigger,#cancelCandTrigger').show();
		}
		else
			{return true;}
		
	});
	$j('input#bulkUpload').on('change',function(){
		$j('span#bulkUploadTrigger').text($j('input#bulkUpload').val());
	});
	$j("#bulkUploadTrigger").on('click',function() {
		$j("#bulkUpload").trigger('click');
	});
	$j("#bulkUploadButtonTrigger").on('click',function() {
		$j("#bulkUploadButton").trigger('click');
	});
	$j("#bulkUploadButton").on('click',function() {
		 $j('#cand-upload-form').trigger('submit');
	});
	
	$j("li.button-row").on("click", "input#camp-cancel-button", function(){
		$j(this).hide().fadeOut("slow");
		$j("div#campus-details .element").each(function(){
		$j(this).val($j(this).attr("data-currentvalue"));
		});
		}); 
		$j('#addrowCandTrigger').on('click', this, function(){
			$j('div#detail-view').show().slideDown('slow');
			$j('#addrowCandTrigger,#finalizeCandTrigger').hide();
			$j('#saveCandTrigger,#cancelCandTrigger').show();
			$j('form#cand-upload-form').hide();
			newRowFlag = true;
		}); 
		$j('#cancelCandTrigger').on('click', this, function(){
			 $j('#addrowCandTrigger,#finalizchosen-campus-texteCandTrigger').show();
			 $j('#saveCandTrigger,#cancelCandTrigger').hide();
			 $j("#detail-view input").val('');
			 $j(this).parents('#candidate-details').children('#detail-view').hide().slideUp('slow');
			 $j('#addrowCandTrigger,#finalizeCandTrigger').show();
			 $j('form#cand-upload-form').show();
		}); 
		$j("#saveCandTrigger").on('click',this,function(){
			$j('div#detail-view').addClass('loading');
			saveCandidateBtClk();
		});    
		 $j('#cand-upload-form').submit(function() { 
			    $j(this).ajaxSubmit(options);            
			    return false; 
			}); 
		 $j('div#cand-control-strip').on('click','span#finalizeCandTrigger.active',function(){
			 
			 var aptMin = $('#aptMin').val();
			 var engMin = $('#engMin').val();
			 var logMin = $('#logMin').val();
			 
			 if(aptMin==""){
				 aptMin=0;
			 }
			 if(engMin==""){
				 engMin=0;
			 }
			 if(logMin==""){
				 logMin=0;
			 }
			 
			 
			 	$j.ajax({
					type : "POST",
					url : "/CareTool/candidatedetails/freezeCandidates/" + campusID,
					dataType: 'text',
					data :{aptMin:aptMin, engMin:engMin, logMin:logMin },
					success : function(response) {						
				//		alert("Candidates Freezed !!");
					},
					error: function(response){
						care.popup(care.messages.candidateDetailsSaveErrorTitle, care.messages.candidateDetailsSaveErrorBody,"");
					}
				});
			 	
				$j.ajax({
					type : "POST",
					url : "/CareTool/campusdetails/finalize/candidateFreeze/" + campusID,
					dataType: 'text',
					data : true,
					success : function(response) {
						finalizeCandDetails = true;
						$j('table#candidates tbody tr').off('click');
						$j('div#cand-control-strip span.button').remove();
						$("#finalizeCandTrigger").removeClass("active");
						$("#finalizeCandTrigger").removeClass("btn-success").addClass("btn-default");
						alert("Successfully Finalized Candidate List. This page is now read only.");
					},
					error: function(response){
						care.popup(care.messages.candidateDetailsSaveErrorTitle, care.messages.candidateDetailsSaveErrorBody,"");
					}
				});

			});	
		 	$j('#aptMin, #engMin, #logMin').keyup( function() { candidateTable.fnDraw(); } );		
});
