$j(document)
	.ready(
		function() {

		    $j('div#consensus-control-strip')
			    .on(
				    'click',
				    'span#submitConsensusTrigger',
				    function() {
					var consensusJSON = [];
					var $headers = $j("table#consensusTable thead th");
					var $rows = $j(
						"table#consensusTable tbody tr:not('.cloneSeedTr')")
						.each(
							function(index) {
							    $cells = $j(this)
								    .find("td");
							    consensusJSON[index] = {};
							    $cells
								    .each(function(
									    cellIndex) {
									// 9th
									// index
									// is
									// decision
									// indicator.
									var val = $j
										.trim(cellIndex == 9 ? $j(
											this)
											.find(
												'textarea')
											.val()
											: $j(
												this)
												.text());
									if (cellIndex != 7)// 7th
											    // index
											    // is
											    // for
											    // interviewer
											    // decision
									    consensusJSON[index][$j(
										    $headers[cellIndex])
										    .html()] = val;
								    });
							});
					consensusJSON.candidateId = "";
					var consensusJSONObject = {};
					$j('#consensus-control-strip').hide();
					consensusJSONObject.consensusJSON = consensusJSON;
					$j
						.ajax({
						    "dataType" : 'text',
						    "type" : "POST",
						    "url" : "/CareTool/admin/consensus/finalize/"
							    + campusID,
						    "data" : {
							jsondata : JSON
								.stringify(consensusJSONObject.consensusJSON)
						    },
						    "success" : function(
							    response) {
							alert("Consensus has been saved to database.");
						    },
						    // "timeout": 15000, //
						    // optional if you want to
						    // handle timeouts (which
						    // you should)
						    "error" : function handleAjaxError(
							    xhr, textStatus,
							    error) {
							alert('The server took too long to send the data.'
								+ textStatus);
							alert('The server took too long to send the data.'
								+ error);
						    }
						// this sets up jQuery to give
						// me errors
						});
				    });
		    // caseconsensusTable();
		});

function consensusTable() {

    var url = "/CareTool/consensus/getConsensusEligibleCandidates/" + campusID;
    $j('#consensusTable tbody').html('');
    $j
	    .getJSON(
		    url,
		    function(response) {
			if (response !== null && response != '') {
			    var clasz = "";
			    $j
				    .each(
					    response,
					    function(i, item) {
						i++;

						var sliderVal = 0;
						var label = $j(this).parents(
							'tr').find(
							'.decisionSliderLabel');
						var consensusResultVal = "";
						switch (item.consensusResult) {
						case "No Further Interest":
						    sliderVal = 1;
						    clasz = "noFurtherInterest";
						    consensusResultVal = "No Further Interest";
						    break;
						case "On Hold":
						    sliderVal = 2;
						    clasz = "onHold";
						    consensusResultVal = "On Hold";
						    break;
						case "Offer":
						    sliderVal = 3;
						    clasz = "offer";
						    consensusResultVal = "Offer";
						    break;
						default:
						    clasz = "";
						    consensusResultVal = "No Further Interest";
						    break;
						}

						if (item.consensusComments == null) {
						    item.consensusComments = "";
						}
						$j('#consensus-control-strip').show();
						if(item.consensusResult != null){
							$j('#consensus-control-strip').hide();
						}
						else
							$j('#consensus-control-strip').show();

						// Calculate average score.
						var averageScore = (item.logicalAbilityScore
							+ item.englishScore + item.quantitativeAbilityScore) / 3;
						var interviewResultDecision = item.interviewResult == 'Y' ? "Yes"
							: "No";
						var groupDiscussionResDecision = item.groupDiscussionResult == 'Y' ? "Yes"
							: "No";
						var tr = '<tr>'
							+ '<td class="slNo"><span>'
							+ i
							+ '</span></td>'
							+ '<td class="candidateName"><span>'
							+ item.firstName
							+ '</span></td>'
							+ '<td class="gender"><span>'
							+ item.gender
							+ '</span></td>'
							+ '<td class="candidateId hidden"><span>'
							+ item.candidateId
							+ '</span></td>'
							// + '<td
							// class="aptitudeScore"><span>'
							// +
							// item.quantitativeAbilityScore
							// + '</span></td>'
							// + '<td
							// class="englishScore"><span>'
							// + item.englishScore +
							// '</span></td>'
							// + '<td
							// class="logicalScore"><span>'
							// +
							// item.logicalAbilityScore
							// + '</span></td>'
							+ '<td class="averageScore"><span>'
							+ averageScore
								.toFixed(1)
							+ '</span></td>'
							+ '<td class="gDScore"><span>'
							+ groupDiscussionResDecision
							+ '</span></td>'
							+ '<td class="interviewerDecision"><span>'
							+ interviewResultDecision
							+ '</span></td>'
							+ '<td class="decisionSlider"><span><input type="range" step="1" max="3" min="1" value="'
							+ sliderVal
							+ '" class="decisionSlider"></span></td>'
							+ '<td class="decision '
							+ clasz
							+ '"><span><label class="decisionSliderLabel" for="decisionSlider">'
							+ consensusResultVal
							+ '</label></span></td>'
							/*
							 * + '<td class="comments"><span><textarea>'+
							 * item.consensusComments +'</textarea></span></td>'
							 */
							+ '</tr>';
						$j('#consensusTable tbody')
							.append(tr);
					    });

			    // Populate "Count of candidates On Hold" and "Count
			    // of candidates Offers"
			    $j('#countOnHold').html($j('.onHold').length);
			    $j('#countOffer').html($j('.offer').length);

			} else if (response == '') {
			    var tr = '<tr><td colspan="6"><span>No Data Available</span></td></tr>';
			    $j('#consensusTable tbody').append(tr);
			}
		    })
	    .done(
		    function() {
			$j(".decisionSlider[type=range]")
				.on(
					'change',
					function(e) {
					    var label = $j(this)
						    .parents('tr')
						    .find(
							    '.decisionSliderLabel');
					    switch ($j(this).val()) {
					    case "1":
						label
							.text("No Further Interest");
						break;
					    case "2":
						label.text("On Hold");
						break;
					    case "3":
						label.text("Offer");
						break;
					    }
					    var decision = $j(this).parents(
						    'tr').find('td.decision');
					    decision
						    .removeClass('offer')
						    .removeClass('onHold')
						    .removeClass(
							    'noFurtherInterest');
					    if (decision.text() == 'On Hold') {
						decision.addClass('onHold');
					    } else if (decision.text() == 'No Further Interest') {
						decision
							.addClass('noFurtherInterest');
					    } else if (decision.text() == 'Offer') {
						decision.addClass('offer');
					    }
					    $j('#countOnHold').html(
						    $j('.onHold').length);
					    $j('#countOffer').html(
						    $j('.offer').length);

					});
		    });
};


jQuery(document).ready(function() {
    jQuery("#consensusTable thead th:eq(7)").data("sorter", false);
    jQuery("#consensusTable").tablesorter();
});