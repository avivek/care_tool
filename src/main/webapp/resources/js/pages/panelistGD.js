$j(document).ready(function() {
     	if (!isGDSelected) {
     	    $j("#gdNoneSection").show();
     	    $j("#panelGdContainer").hide();
     	}
	$j('input.totalVal').val(6);
	$j('#panelGd').on('change', 'select.gdParam', function(){
		var parentArray = $j(this).parents('tr').find('select.gdParam');
		var total= 0;
		$j.each(parentArray, function(i, item){
			var itemVar = 0;
			if(parseInt(item.value)==NaN || parseInt(item.value)==""){
				return;
			}
			else{
				total = total+parseInt(item.value);
			}
		});
		$j(this).parents('tr').find('input.totalVal').val(total);
	});
	
	var selectMenus = $j('select.gdParam');
	$j.each(selectMenus, function(i, item){
		for(var i=1; i<6; i++){
			$j(item).append($j("<option></option>").attr("value",i).text(i)); 
		}
	});
	
	$j("#gd-panel-control-strip").on('click','#submtiGdFbTrigger',function() {
        var action,
            r = confirm("Do you want to Submit?");
        if (r == true) 
        	action = true;
        else
        	action = false;
        if(action) {
			$j(this).addClass('loading');
			var panelistGDDataToSave = [];
			var panelResultMasterObject = {};
			var gdId=$j('#panelSummaryGd tr:eq(2) td:eq(0)').text();
			panelResultMasterObject.gdId=gdId;
			panelResultMasterObject.gdName='';
			panelResultMasterObject.gdName=$j('table#panelSummaryGd').find('td.topic').text();
			panelResultMasterObject.resultSet = [];
			var interviewGroupTitle = $j('li.interview-panel-title-li');
			var controlStrip = $j(this).parents('#gd-panel-control-strip');
			var table =$j(this).parents('div#panelGdContainer').find('table#panelGd tbody tr:visible');
			$j.each(table, function(i, row){
				panelResultMasterObject.resultSet[i] = {};
				$j.each($j(row).children('td'), function(j, cell){
                    if($j(cell).attr('class') != 'name' && $j(cell).attr('class') != 'groupId'){
                        if($j(cell).attr('class') != 'comments') 
                        	panelResultMasterObject.resultSet[i][$j(cell).attr('class')] = $j.trim($j(cell).find('select').val());
                        else 
                        	panelResultMasterObject.resultSet[i][$j(cell).attr('class')] = $j.trim($j(cell).find('textarea').val());
                    }
                    else panelResultMasterObject.resultSet[i][$j(cell).attr('class')] = $j.trim($j(cell).text());
				});
			});
			//console.info(panelResultMasterObject);
			panelistGDDataToSave.push(panelResultMasterObject);
			var panelResult = JSON.stringify(panelistGDDataToSave);
			console.info("panelResult "+panelResult);
			$j.ajax({
				url : "/CareTool/admin/save_gd_result/",
				type : "POST",
				async: false,
				data: {jsondata:panelResult},
				datatype: 'text',
				success : function(data) {
					$j("#submtiGdFbTrigger").removeClass('loading').hide();
					alert("Feedback has been submitted. Navigating to homepage")
					$j('div#panelGdContainer').find('input, select, textarea').attr('disabled', true).trigger("chosen:updated");
					$j("li#panelistLanding"/*"+sessionStorage.getItem("active-link")*/).trigger("click");					
				},
				error: function(xhr, status, errorThrown){
					alert("Error occured"+ xhr +status+errorThrown);	
					$j("#submtiGdFbTrigger").removeClass('loading');
				}
			});
        }
	});
});
function listPanelistGD(campusId)
{
	$j.ajax( {
        "dataType": 'json',
         "type": "POST",
         "url": "/CareTool/admin/list_panelist_gd/"+ panelistId+"/"+campusId,
         "success": function(response){
         	var grps = response.aaData;
         	$j(grps).each(function(i){
         		$j('#gdContainerUl').append('<li><a href="#"><span>'+grps[i]+'</span></a></li>');
         	});
        	 /*var ul = $j('#gdContainerUl li.cloneSeedLi').clone('true').insertAfter($j('#gdContainerUl li.cloneSeedLi'));
        	 ul.addClass('loading');
        	 var li = $j('#gdContainerUl li:last-child');
      		li.removeClass('cloneSeedLi').removeClass('hidden');
      		 li.find('span').text(response.aaData[0]);
      		 console.info("Distinct GDs For Panelist:: "+response.aaData);*/
      		/* var addLi;
      		for(var i=0;i<1;i++){
     			addLi = $j('#gdContainerUl li:last-child').insertAfter($j('#gdContainerUl li:last-child'));
     			addLi.removeClass('cloneSeedLi').removeClass('hidden');
         		addLi.find('span').text("Group "+i);
         	}
     		ul.removeClass('loading');
     		*/
         },
         //"timeout": 15000,   // optional if you want to handle timeouts (which you should)
         "error": function handleAjaxError( xhr, textStatus, error ) {
                 alert( 'The server took too long to send the data.'+textStatus);
                 alert( 'The server took too long to send the data.'+error);
         } // this sets up jQuery to give me errors
     } );
}
function showGdTable(gDName,campusId){
    	$j("#gdNoneSection").hide();
	$j("#panelGdContainer").show();
	$j.ajax( {
        "dataType": 'json',
         "type": "POST",
         "url": "/CareTool/admin/get_groupdiscussion_list/"+ panelistId+"/"+gDName+"/"+campusId,
         "success": function(response){
      		console.info(response);
        	
        	 var panelistName = response.aaData[0].panelistDomain.panelist_name;
        	 var topicOfDisc = response.aaData[0].gdName;
        	 var freeze = response.aaData[0].gdFreeze;
        	 var candidateName = response.aaData[0].candidateDetailsDomain.firstName+" "+response.aaData[0].candidateDetailsDomain.lastName;
        	//To add Group ID, Discussion topic and Panelist name.
        	$j('#panelSummaryGd tr.cloneSeedRow').clone('true').insertAfter($j('#panelSummaryGd tr.cloneSeedRow'));
      		row = $j("#panelSummaryGd tbody tr:last-child");
      		row.removeClass('cloneSeedRow').removeClass('hidden');
      	
      		row.find('td.panelistName').text(panelistName);
      		row.find('td.topic').text(topicOfDisc);
      		$j("#panelGd").find("tr:gt(1)").remove();
        	$j('#panelGd tr.cloneSeedRow').clone('true').insertAfter($j('#panelGd tr.cloneSeedRow'));
        	for(var i=0;i<response.aaData.length;i++){
        		var groupId = response.aaData[i].id;
     			var addRow = $j('#panelGd tbody tr:last-child').clone('true').insertAfter($j('#panelGd tbody tr:last-child'));
         		addRow.removeClass('cloneSeedRow').removeClass('hidden');
     			var candidateName=response.aaData[i].candidateDetailsDomain.firstName+" "+response.aaData[i].candidateDetailsDomain.lastName;
     			addRow.find('td.name').text(candidateName);
     			addRow.find('td.gender').text(response.aaData[i].candidateDetailsDomain.gender);
     			addRow.find('td textarea.commentsVal').text(response.aaData[i].comments);
     			addRow.find('td input.totalVal').val(response.aaData[i].total);
     			addRow.find('td.groupId').text(groupId);
     			
     			addRow.find(".leadership option").filter(function() {
     			    
     			    return $j(this).text() == response.aaData[i].leadership; 
     			}).attr('selected', true);
     			addRow.find(".listening option").filter(function() {
     			    
     			    return $j(this).text() == response.aaData[i].listening; 
     			}).attr('selected', true);
     			addRow.find(".participation option").filter(function() {
     			    
     			    return $j(this).text() == response.aaData[i].participation; 
     			}).attr('selected', true);
     			addRow.find(".approach option").filter(function() {
     			    
     			    return $j(this).text() == response.aaData[i].approach; 
     			}).attr('selected', true);
     			
     			addRow.find(".presentation option").filter(function() {
     			    
     			    return $j(this).text() == response.aaData[i].presentation; 
     			}).attr('selected', true);
     			
     			addRow.find(".response option").filter(function() {
	    
     				return $j(this).text() == response.aaData[i].response; 
     			}).attr('selected', true);
     			
     			
     			/*addRow.find("td.leadership span").html(response.aaData[i].leadership);
     			addRow.find("td.listening span").html(response.aaData[i].listening);
     			addRow.find("td.participation span").html(response.aaData[i].participation);
     			addRow.find("td.approach span").html(response.aaData[i].approach);
     			addRow.find("td.presentation span").html(response.aaData[i].presentation);
     			addRow.find("td.response span").html(response.aaData[i].response);*/
     			
     		//	var selectMenus = $j('select.gdParam');
/*     			$j('select.listeningVal').attr('value',response.aaData[i].listening);
     			$j('select.listeningVal').text(response.aaData[i].listening);*/
     			
     			
     	/*		$j.each(selectMenus, function(i, item){
     				for(var i=1; i<6; i++){
     					$j(item).append($j("<option></option>").attr("value",i).text(i)); 
     					$j(item).append($j("<option></option>").attr("value",response.aaData[i].listening).text(response.aaData[i].listening));
     					$j(item).append($j("<option></option>").attr("value",response.aaData[i].participation).text(response.aaData[i].participation));
     					$j(item).append($j("<option></option>").attr("value",response.aaData[i].approach).text(response.aaData[i].approach));
     					$j(item).append($j("<option></option>").attr("value",response.aaData[i].presentation).text(response.aaData[i].presentation));
     					$j(item).append($j("<option></option>").attr("value",response.aaData[i].response).text(response.aaData[i].response));
     					$j(item).append($j("<option></option>").attr("value",response.aaData[i].total).text(response.aaData[i].total));
     					
     				}
     			});*/
     			
     			if(freeze =='Y'){
     				$j('div#panelGdContainer').find('input, select, textarea').attr('disabled', true).trigger("chosen:updated");
     				$j("#submtiGdFbTrigger").removeClass('loading').hide();
     			}
     			else{
     				$j('div#panelGdContainer').find('input, select, textarea').attr('disabled', false);
     				$j("#submtiGdFbTrigger").addClass('btn btn-primary').show();
     			}

     		}
//     		$j('select.gdParam').chosen({
//     			disable_search: true,
//     			width: "60%"
//     		});
         },
         "error": function handleAjaxError( xhr, textStatus, error ) {
                 alert( 'The server took too long to send the data.'+textStatus);
                 alert( 'The server took too long to send the data.'+error);
         } // this sets up jQuery to give me errors
     } );
}            	   


