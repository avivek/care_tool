var $ = jQuery;
$(document).ready(function() {
	
	$('#selectAdminOption').change(function(event) {
		var selectedOption =  $('#selectAdminOption').val();
		if (selectedOption ==  'recruiter')
			{	
				$("#panelistAdminSection").hide();
				$("#recruiterAdminSection").show();	
				showRecruiterListTable();
				$("#addRecruiterButton").show();
				$('#displayRowDetailsPanelist').html('');
			}
		if(selectedOption == 'panelist')
			{
				$("#panelistAdminSection").show();
				$("#recruiterAdminSection").hide();
				showPanelistListTable();
				$("#addPanelistButton").show();
				$('#displayRowDetailsRecruiter').html('');			
			}
	});
	$('#addPanelistButton').on('click', function(e){
		e.preventDefault();
		$('#displayRowDetailsPanelist').html('');
		$('#addPanelistPopup').show();
	});
	
	$('#addRecruiterButton').on('click', function(e){
		e.preventDefault();
		$('#displayRowDetailsRecruiter').html('');
		$('#addRecruiterPopup').show();
	});	
});

function showRecruiterListTable() {
			recruiterListProjectTable =  $("table#recruiterListProjectTable").dataTable({
					"bProcessing": true,
			        "bDestroy": true,
			        "bAutoWidth": true, 
			        "iDisplayLength": 10,
			        "bServerSide": true,
			        "sAjaxSource": "/CareTool/admin/get_recruiter_list/",
			        /*"fnServerData": function ( sSource, aoData, fnCallback ) {
			            $.getJSON( sSource, aoData, function (json) {
			            	//Here you can do whatever you want with the additional data
			                status=json.messageFlag;
			                //Call the standard callback to redraw the table
			                fnCallback(json);
			            } );
			        },*/
			        "fnServerData": function (sSource, aoData, fnCallback ) {
			            $.ajax( {
			               "dataType": 'json',
			                "type": "POST",
			                "url": sSource,
			                "success": fnCallback,
			                async : false,
			                "timeout": 15000,   // optional if you want to handle timeouts (which you should)
			                "error": handleAjaxError // this sets up jQuery to give me errors
			            } );
			        },
			        "oLanguage": {
			            "sZeroRecords": "No records to display"
			          },
			        "aoColumns": [
			                     /* { "mData":"candidateId",  "bVisible": false},*/
			                      { "mData":"id", "sWidth":"auto"},
			                      { "mData":"recruiter_name", "sWidth":"auto"},
			                      { "mData":"recruiter_email_id", "sWidth":"auto"}
			                      ]
							});
							/*var rowData  = $("#recruiterListProjectTable").fnGetData(this);
							showRecruiterInformation(rowData);*/
			
					$("table#recruiterListProjectTable").on('click', 'tr', function(){
						var row = recruiterListProjectTable.fnGetData(this);
						showRecruiterInformation(row);
					});
}

function showRecruiterInformation(data){
		
	var strHTML = '';
	strHTML = '<table class="table table-striped">';
	strHTML += '<tr><td class="labelname">Recruiter Name</td><td><input type="text" id="recruiterName" value=""></td></tr>';
	strHTML += '<tr><td class="labelname">Recruiter EmailID</td><td><input type="text" id="recruiter_email_id" value=""></td></tr>';
	strHTML += '</table>';
	strHTML += '<input type="submit" class="btn btn-success" value="Save" onclick="modifyRecruiterRecord(' + data.id + ')">';
	strHTML += '<input type="submit" class="btn btn-warning" value="Delete" onclick="deleteRecruiterRecord('+ data.id +')">';
	
	$("#displayRowDetailsRecruiter").html(strHTML);
	$("#displayRowDetailsRecruiter").css({"display":""});
	$("#addRecruiterPopup").css({"display":"none"});
	$("#recruiterName").val(data.recruiter_name);
	$("#recruiter_email_id").val(data.recruiter_email_id);
	
}

function deleteRecruiterRecord(id){
	
	$.ajax({
		type : "GET",
		url : "/CareTool/admin/recruiter/delete",
		async : false,
		data : "id=" + id, 
		success : function(event) {
			
		},
		error : function(response) {
			alert('Error occured'+response);
		}
	});
	showRecruiterListTable();
}

function modifyRecruiterRecord(id){
	
	var recruiterName =  $("#recruiterName").val();
	var recruiter_email_id =  $("#recruiter_email_id").val();
	$.ajax({
		type : "POST",
		url : "/CareTool/admin/recruiter/save",
		async : false,
		data : "id=" + id + "&recruiterName=" + recruiterName + "&recruiter_email_id=" + recruiter_email_id , 
		success : function(event) {
			
		},
		error : function(response) {
			alert('Error occured'+response);
		}
	});
	showRecruiterListTable();	
}


function addRecruiter(){	
	var recruiterName =  $("#addRecruiterName").val();
	var recruiter_email_id =  $("#addRecruiterEmail").val();
	if(recruiterName != '' || recruiter_email_id != ''){
		$.ajax({
			type : "GET",
			url : "/CareTool/admin/recruiter/add",
			async : false,
			data : "recruiterName=" + recruiterName + "&recruiter_email_id=" + recruiter_email_id ,
			success : function(event) {
				console.log(event);
			},
			error : function(response) {
				alert('Error occured'+response);
			},
			completed: function(){
				$("#addRecruiterPopup").html('Added Successfully');
			}
		});		
		setTimeout(function(){
			showRecruiterListTable();
		}, 200);
	}
	else{
		alert('Please enter the required detials');
	}	
}

function showPanelistListTable() {
			panelistListProjectTable =  $("table#panelistListProjectTable").dataTable({
					"bProcessing": true,
			        "bDestroy": true,
			        "bAutoWidth": true, 
			        "iDisplayLength": 10,
			        "bServerSide": true,
			        "sAjaxSource": "/CareTool/admin/get_panelist_list/",
			        /*"fnServerData": function ( sSource, aoData, fnCallback ) {
			            $.getJSON( sSource, aoData, function (json) {
			            	//Here you can do whatever you want with the additional data
			                status=json.messageFlag;
			                //Call the standard callback to redraw the table
			                fnCallback(json);
			            } );
			        },*/
			        "fnServerData": function (sSource, aoData, fnCallback ) {
			            $.ajax( {
			            	async : false,
			               "dataType": 'json',
			                "type": "POST",
			                "url": sSource,
			                "success": fnCallback,
			                "timeout": 15000,   // optional if you want to handle timeouts (which you should)
			                "error": handleAjaxError // this sets up jQuery to give me errors
			            } );
			        },
			        "oLanguage": {
			            "sZeroRecords": "No records to display"
			          },
			        "aoColumns": [
			                     /* { "mData":"candidateId",  "bVisible": false},*/
			                      { "mData":"id", "sWidth":"auto"},
			                      { "mData":"panelist_name", "sWidth":"auto"},
			                      { "mData":"panelist_email_id", "sWidth":"auto"},
			                      { "mData":"serviceLine", "sWidth":"auto"},
			                      { "mData":"designation", "sWidth":"auto"}
			                      ]
							});
							/*var rowData  = $("#panelistListProjectTable").fnGetData(this);
							showPanelistInformation(rowData);*/
			
					$("table#panelistListProjectTable").on('click', 'tr', function(){
						var row = panelistListProjectTable.fnGetData(this);
						showPanelistInformation(row);
					});
}

function showPanelistInformation(data){
	var strHTML = '';
	strHTML = '<table class="table table-striped">';
	strHTML += '<tr><td class="labelname">Panelist Name</td><td><input type="text" id="panelist_Name" value=""></td></tr>';
	strHTML += '<tr><td class="labelname">Panelist EmailID</td><td><input type="text" id="panelist_email_id" value=""></td></tr>';
	strHTML += '<tr><td class="labelname">Service Line</td><td><input type="text" id="serviceLine" value=""></td></tr>';
	strHTML += '<tr><td class="labelname">Designation</td><td><input type="text" id="designation" value=""></td></tr>';
	strHTML += '</table>';
	strHTML += '<input type="submit" class="btn btn-success" value="Save" onclick="modifyPanelistRecord(' + data.id + ')">';
	strHTML += '<input type="submit" class="btn btn-warning" value="Delete" onclick="deletePanelistRecord('+ data.id +')">';
	
	$("#displayRowDetailsPanelist").html(strHTML);
	$("#displayRowDetailsPanelist").css({"display":""});
	$("#addPanelistPopup").css({"display":"none"});
	$("#panelist_Name").val(data.panelist_name);
	$("#panelist_email_id").val(data.panelist_email_id);
	$("#serviceLine").val(data.serviceLine);
	$("#designation").val(data.designation);
	
}

function deletePanelistRecord(id){
	
	$.ajax({
		type : "GET",
		url : "/CareTool/admin/panelist/delete",
		async : false,
		data : "id=" + id, 
		success : function(event) {
			
		},
		error : function(response) {
			alert('Error occured'+response);
		}
	});
	showPanelistListTable();
}

function modifyPanelistRecord(id){
	
	var panelistName =  $("#panelist_Name").val();
	var panelist_email_id =  $("#panelist_email_id").val();
	var serviceLine =  $("#serviceLine").val();
	var designation =  $("#designation").val();
	$.ajax({
		type : "POST",
		url : "/CareTool/admin/panelist/save",
		async : false,
		data : "id=" + id + "&panelistName=" + panelistName + "&panelist_email_id=" + panelist_email_id + "&serviceLine=" + serviceLine + "&designation=" + designation, 
		success : function(event) {
			
		},
		error : function(response) {
			alert('Error occured'+response);
		}
	});
	showPanelistListTable();
	
}


function addPanelist(){	
	var panelistName =  $("#addPanelistName").val();
	var panelist_email_id =  $("#addPanelistEmail").val();
	var serviceLine =  $("#serviceLine").val();
	var designation =  $("#designation").val();
	if(panelistName != '' || panelist_email_id != ''){
		$.ajax({
			type : "GET",
			url : "/CareTool/admin/panelist/add",
			async : false,
			data : "panelistName=" + panelistName + "&panelist_email_id=" + panelist_email_id + "&serviceLine=" + serviceLine + "&designation=" + designation ,
			success : function(event) {
				
			},
			error : function(response) {
				alert('Error occured'+response);
			},
			completed: function(){
				$("#addPanelistPopup").html('Added Successfully');
			}
		});
		setTimeout(function(){
			showPanelistListTable();
		}, 200);	
	}else alert('Please enter required displayRowDetailsPanelist');
	
}