
var interviewCount = 0;
var interviewMasterObject = [];
var panelistsJSON = "[{\"panelistId\":1,\"panelistName\":\"Name1\"},"+
"{\"panelistId\":2,\"panelistName\":\"Name2\"},"+
"{\"panelistId\":3,\"panelistName\":\"Name3\"},"+
"{\"panelistId\":4,\"panelistName\":\"Name4\"},"+
"{\"panelistId\":5,\"panelistName\":\"Name5\"},"+
"{\"panelistId\":6,\"panelistName\":\"Name6\"},"+
"{\"panelistId\":7,\"panelistName\":\"Name7\"}]";
function updateCandidateList(){
	$j('ul#freeStudentsList').addClass('loading');
	$j.ajax({
		url : "/CareTool/interview/getInterviewEligibleStudents/" + $j("#selectCampus").val(),
		type : "GET",
		async:false,
		datatype: 'json',
		success : function(data) {
				$j('ul#freeStudentsList').empty();
				$j.each(data, function(i, item){
					$j('ul#freeStudentsList firstListItem').hide();
					$j('ul#freeStudentsList').append('<li class=\'studentListItem\'><a href=\'#\'>'+
					'<span class=\'candidateName\'>'+item.firstName+'&nbsp;'+item.lastName+'</span><span class=\"candidateId hidden\">'+item.candidateId+'</span></a></li>').slideDown('slow');
				});
			$j('ul#freeStudentsList').delay(2000).removeClass('loading');
		},
		error: function(xhr, status, errorThrown){
			console.log("Error occured"+ xhr +status+errorThrown);	
		}
	});
	}

function updatePanelistList(){
	$j('ul#freePanelistsList').addClass('loading');
	$j.ajax({
		url : "/CareTool/interview/getInterviewEligiblePanelist/" + $j("#selectCampus").val(),
		type : "GET",
		async:false,
		datatype: 'json',
		success : function(data) {		
			var data1 = $j.parseJSON(data);
			if(data1.length){
				$j('ul#freePanelistsList').empty();
				$j.each(data1, function(i, item){
					$j('ul#freePanelistsList firstListItem').hide();
					$j('ul#freePanelistsList').append('<li class=\'studentListItem\'><a href=\'#\'><span class=\'panelistName\'>'+item.panelist_name+'</span><span class=\'panelistId hidden\'>'+item.id+'</span></a></li>').slideDown('slow');
				});
			}
			else
				{
					return false;
				}
			$j('ul#freePanelistsList').delay(2000).removeClass('loading');				
		},
		error: function(xhr, status, errorThrown){
			console.log("Error occured"+ xhr +status+errorThrown);	
		}
	});
	
}
function clone(){
	$j('li.cloneSeed').clone('true').insertAfter($j('li.cloneSeed'));
	var interviewChildLi = $j('ul.interview-parent-ul > li');
	$j.each(interviewChildLi, function(){
		if($j(this).hasClass('active')){
			$j(this).removeClass('active');
		}
	});
	$j('ul.interview-parent-ul > li:nth-child(3)').removeClass('cloneSeed').addClass('interview-panel-title-li').addClass('active');
	$j('ul.interview-parent-ul > li:nth-child(3)').find('a').trigger('click');
}
function prepareInterviewJSON(i, node){
	var returnVal = {
			"interviewId": i,
			"campusID":$j("#selectCampus").val(),
			"candidateId":$j(node).find('span.attendeeId').text(),
			"listOfPanelists":""
	};
	returnVal.listOfPanelists = [];
	returnVal.listOfPanelists.panelistId = "";
	var panelList = $j(node).find('li.panelist');
	$j.each(panelList, function(){
		var str = $j(this).children('span.panelistId').text();
		returnVal.listOfPanelists.push({panelistId:str});
	});
	return returnVal;
}
$j(document).ready(function(){
	$j('#interview-accordion > ul.interview-parent-ul').on('click','li.interview-panel-title-li a',function() {
		var checkElement = $j(this).next();
		$j('#interview-accordion li.interview-panel-title-li').removeClass('active');
		$j(this).closest('li').addClass('active');	
		if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
			$j(this).closest('li').removeClass('active');
			checkElement.slideUp('normal');
		}
		if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
			$j('#interview-accordion ul.interview-content-ul:visible').slideUp('normal');
			checkElement.slideDown('normal');
		}
		if (checkElement.is('ul')) {
			return false;
		} else {
			return true;	
		}
	});
	/*var list = setInterval(function(){
		updatePanelistList();
		updateCandidateList();
	}, 10000);*/
	$j("#interview-control-strip").on('click','#savePanelTrigger',function() {
		var act = $j(this).parents('.interview-panel-title-li'),
			actPanel = act.find('.interview-panel-li > .panelList'),
			actAttend = act.find('.interview-attendee-li > .attendeeList');
		if(actPanel.find('li.panelist').length == 0 || actAttend.find('li.attendee').length == 0){
			alert("Please add at least one Panelist and One Candidate");
			return false;
		}
		else{
			var interviewGroupTitle = $j('li.interview-panel-title-li');
			var controlStrip = $j(this).parents('#interview-control-strip');
			var item =$j(this).parents('ul.interview-content-ul');
			interviewMasterObject = [];
			interviewMasterObject.push(prepareInterviewJSON((interviewCount++), item));
			console.log(JSON.stringify(interviewMasterObject));
			$j.ajax({
				url : "/CareTool/interview/saveInterviewPanel/",
				type : "POST",
				async: false,
				data: {jsondata:JSON.stringify(interviewMasterObject)},
				datatype: 'text',
				success : function(data) {
						/*$j('ul#freeStudentsList').empty();*/
						$j.each(data, function(i, item){
							$j('ul#freeStudentsList firstListItem').hide();
							/*$j('ul#freeStudentsList').append('<li class=\'studentListItem\'><a href=\'#\'><span>'+item.firstName+'&nbsp;'+item.lastName+'</span></a></li>').slideDown('slow');*/
						});
					$j('ul#freeStudentsList').delay(2000).removeClass('loading');
					$j(controlStrip).remove();
				},
				error: function(xhr, status, errorThrown){
					console.log("Error occured"+ xhr +status+errorThrown);	
				}
			});
			clone();
			$j('li.interview-panel-title-li.active > a > span').text('Panel '+($j('ul.interview-parent-ul > li').size()-2));
		}
	
	});
	$j("#interview-control-strip").on('click','#clearPanelTrigger', function(e) {
		e.preventDefault();
		var attendeeListitem = $j('ul#freeStudentsList li.studentListItem');
		var panelistListitem = $j('ul#freePanelistsList li.studentListItem');
		var candId = $j(this).parents('ul.interview-content-ul').find('span.attendeeId').text();
		$j.each(attendeeListitem, function(i, item){
			if(!($j(item).is(':visible'))){
				if($j(item).find('span.candidateId').text()==candId){
					$j(item).show();
				}
					
			}
		});
		var panelIds=panelistIds = $j(this).parents('ul.interview-content-ul').find('span.panelistId');
		$j.each(panelistListitem, function(i, item){
			if(!($j(item).is(':visible'))){
				$j.each(panelIds, function(i, subItem){
					if($j(subItem).text()==$j(item).find('span.panelistId').text()){
						$j(item).show();
					}
				});
					
			}
		});
		$j(this).parents('ul.interview-content-ul').find('li.panelist').remove();
		$j(this).parents('ul.interview-content-ul').find('li.emptyPanel').show();
		$j(this).parents('ul.interview-content-ul').find('li.attendee').remove();
		$j(this).parents('ul.interview-content-ul').find('li.emptyAttendee').show();
	
		
	});
	$j('ul#freeStudentsList').on('click','li.studentListItem a', function(e){
		var freePoolItem = $j(this);
		e.preventDefault();
		var emptyPanel = $j('li.interview-panel-title-li.active').find('li.emptyAttendee')
				if(emptyPanel.is(':visible')){
					emptyPanel.hide();
					freePoolItem.parent().hide();	
					emptyPanel.parents('ul.attendeeList').append('<li class=\'attendee\'>'+'<span class=\'attendeeName\'>'+$j(this).children('span.candidateName').text()+'</span><span class=\'attendeeId hidden\'>'+$j(this).children('span.candidateId').text()+'</span></li>');
				}
				else{
					var studentListItem = $j('ul#freeStudentsList > li.studentListItem');
					$j.each(studentListItem, function(){
						if($j(this).find('span').text()==emptyPanel.parents('ul.attendeeList').find('li.attendee span').text()){
							$j(this).show();
						}
					});
					emptyPanel.parents('ul.attendeeList').find('li.attendee').remove();
					freePoolItem.parent().hide();					
					emptyPanel.parents('ul.attendeeList').append('<li class=\'attendee\'>'+'<span class=\'attendeeName\'>'+$j(this).children('span.candidateName').text()+'</span><span class=\'attendeeId hidden\'>'+$j(this).children('span.candidateId').text()+'</span></li>');
				}
			});
	$j('ul#freePanelistsList').on('click','li.studentListItem a', function(e){
		e.preventDefault();
		var freePoolItem = $j(this);
		var emptyPanel = $j('li.interview-panel-title-li.active').find('li.emptyPanel');
				if(emptyPanel.is(':visible')){
					emptyPanel.hide();
					//freePoolItem.parent().hide();	
					emptyPanel.parents('ul.panelList').append('<li class=\'panelist\'>'+'<span class=\'panelistName\'>'+
							$j(this).children('.panelistName').text()+'</span><span class=\'panelistId hidden\'>'+$j(this).children('.panelistId').text()+'</span></li>');
				}
				else{
					//freePoolItem.parent().hide();
					var panelistList = emptyPanel.parents('ul.panelList').children('li.panelist');
					var duplicateFlag = false;
					$j.each(panelistList, function(i,item){
						if(($j(freePoolItem).find('span').text())==($j(item).find('span').text())){
							duplicateFlag = true;
						}
					});
					if(!duplicateFlag){
					emptyPanel.parents('ul.panelList').append('<li class=\'panelist\'>'+'<span class=\'panelistName\'>'+
							$j(this).children('.panelistName').text()+'</span><span class=\'panelistId hidden\'>'+$j(this).children('.panelistId').text()+'</span></li>');
					}
				}
				if($j('ul#freePanelistsList li.studentListItem').size()==0){
					emptyPanel.show();
				}
			});
	clone();

	$j(".inteview-cutoff").on('click','button',function() {
		var cuttoff = $j.trim($j('.cutoff-container').find('input.text').val()) || 0,
			eligible =$j('.eligible-container').find('input.text');	
			$j.ajax({
				url : "/CareTool/interview/getElgibleCountOnCutoff?campusid="+campusID+"&cutoff="+cuttoff,
				type : "GET",
				async: false,
				datatype: 'text',
				success : function(data) {
					var response = $j.trim(data);
					eligible.val(response);
					
				},
				error: function(xhr, status, errorThrown){
					console.log("Error occured"+ xhr +status+errorThrown);	
				}
			});		
	});
	$j('.eligible-container').on('click','button', function(){
		
		var action=true;
		$j.ajax({
			url : "/CareTool/interview/getInterviewFreezeFlag?campusid="+campusID,
			type : "GET",
			async: false,
			datatype: 'text',
			success : function(data) {
				if(data=='Y'){
					//	$j('.inteview-cutoff').hide();
					alert("The candidates have already been finalized.")
					action=false;
				}
			},
			error: function(xhr, status, errorThrown){
				console.log("Error occured"+ xhr +status+errorThrown);	
			}
		});
		
		
		if(action){
			var cuttoff = $j.trim($j('.cutoff-container').find('input.text').val()) || 0,
				eligible =$j('.eligible-container').find('input.text');	
			var freeze='Y';
				$j.ajax({
					url : "/CareTool/interview/setInterviewResultFlag?campusid="+campusID+"&cutoff="+cuttoff+"&interviewFreeze="+freeze,
					type : "GET",
					async: false,
					datatype: 'text',
					success : function(data) {
						updateCandidateList();
					},
					error: function(xhr, status, errorThrown){
						console.log("Error occured"+ xhr +status+errorThrown);	
					}
				});
			}
		});
});
