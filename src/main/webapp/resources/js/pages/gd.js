var freePanelList = [];
var freeCandList = [];
var gdMasterObject = [];

function genGdGroup(flag){
	var sizeofgroup =  $j("#sizeOfGdGroup").val();
	if(sizeofgroup==0 || sizeofgroup==null){
		sizeofgroup=5;
	}
	var id =  $j("#selectCampus").val();	
	$j('li.gd-empty-group-title-li span').hide();
	$j('li.gd-empty-group-title-li').addClass('loading');
	$j.ajax({
		type : "GET",
		url : "/CareTool/groupdiscussion/generateGdGroups/",
		async : true,
		data : "sizeofgroup=" + sizeofgroup + "&id=" + id , 
		success : function(response) {
			$j('li.gd-group-title-li').remove();
			// alert('In Sucess'+response);
			gdMasterList = response;
			var data = $j.parseJSON(response);	
			gdCandMasterList = [];
			gdPanelMasterList = [];
			$j('tr.gdPanelistRow').remove();
			$j('tr.gdCandRow').remove();
			$j('tr.gdMasterRow').remove();
			 $j('li.gd-empty-group-title-li').show();
				$j.each(data.listofGdGroups, function(i, item){
					$j.each(item.listOfCandidates, function(l,subItem){
						var candidateTable = $j('table#gdCandTable');
						candidateTable.append('<tr class=\'gdCandRow\'>'+
								'<td><span class=\'candidateID\'>'+subItem.candidateID+'</span></td>'+
								'<td><span class=\'candidateName\'>'+subItem.candidateName+'</span></td>'+
								'<td><span class=\'groupId\'>'+(i+1)+'</span></td>'+
								'</tr>');
						});
				});

				var counter = 1;
				for(var a=0;a<(((data.listofGdGroups.length)/(data.listOfPanelists.length))+1);a++){
					$j.each(data.listOfPanelists, function(j,item){
						var panelistTable = $j('table#gdPanelistTable');
						panelistTable.append('<tr class=\'gdPanelistRow\'>'+
								'<td><span class=\'panelistID\'>'+item.panelistID+'</span></td>'+
								'<td><span class=\'panelistName\'>'+item.panelistName+'</span></td>'+
								'<td><span class=\'groupId\'></span></td>'+
								'</tr>');
						counter++;
						});
				}
				var array = new Array();
				/*console.log(counter);*/
				for(var a=1,c=0;(a<(counter/2));a++){
					for(var b=a;b<(a+2);b++){
						array[c]=a;
						c++;
					}
				}
				//console.log(array);
				var panelistRow = $j('table#gdPanelistTable tr.gdPanelistRow');
				 $j.each(panelistRow,function (i, row){
					 $j(row).find('span.groupId').text(array[i]);
				 });
				 
				 for(d=0;d<(array.length)/2;d++){
					 $j('table#gdMasterTable').append('<tr class=\'gdMasterRow\'>'+
						'<td><span class=\'groupId\'>'+(d+1)+'</span></td>'+
						'</tr>');
				 }
				 var masterRow = $j('table#gdMasterTable tr.gdMasterRow');
				 $j.each(masterRow, function(i,row){
					 var id=$j(row).find('span.groupId').text();
					 $j('ul.gd-parent-ul').append('<li class=\'gd-group-title-li\'><a href=\'#\'><span>Group'+'&nbsp;'+id+'</span></a>').slideDown('slow');
					 $j('ul.gd-parent-ul li.gd-group-title-li:last-child').append('<ul class=\'gd-content-ul\'>'
								+'<li class=\'gd-panel-li\'>'
									+'<span class=\'gd-sub-title\'>Panelists&nbsp;</span>'
									+'<select id=\'panelist-list_'+id+'\'class=\'gd-panel-list\' multiple=\'\' placeholder=\'Choose Panelists\'></select></li>'
								+'<li class=\'gd-group-li\'>'
									+'<span class=\'gd-sub-title\'>Group&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'
									+'<select id=\'group-list_'+id+'\' class=\'gd-cand-list\' multiple=\'\' placeholder=\'Choose Candidates\'></select></li></ul>');
				 		});	
					 
					 var panelistRow = $j('table#gdPanelistTable tr.gdPanelistRow');
										 
					var gdPanelList = $j('.gd-group-title-li select.gd-panel-list');
					 $j(data.listOfPanelists).each(function(i, item){
					 	gdPanelList.append('<option class=panelist_'+item.panelistID+'><span class=\'hidden\'>'+item.panelistID+'.&nbsp;'+'</span><span>'+item.panelistName+'</span></option>');
					 });

				    $j.each(panelistRow, function(i,row){
						var gdPanelList = $j('select.gd-panel-list'),
							ids = $j(this).find('.groupId').text();
							panIds = $j(this).find('.panelistID').text();
						$j('select#panelist-list_'+ids).find('.panelist_'+panIds).attr('selected','selected');
					});			

					 var candidateRow = $j('table#gdCandTable tr.gdCandRow');
					 $j.each(candidateRow, function(i,row){
						var gdCandList = $j('select.gd-cand-list');
						$j.each(gdCandList,function(i,select){
							if('group-list_'+$j(row).find('span.groupId').text()==$j(select).attr('id')){
								$j(this).append('<option selected=\'\'>'+'<span class=\'hidden\'>'+$j(row).find('span.candidateID').text()+'.&nbsp;'+'</span><span>'+$j(row).find('span.candidateName').text()+'</span></option>')
							}
						});
					 });
					
					//console.log("Item is"+(i+1));		
//			$j('select.gd-cand-list, select.gd-panel-list').chosen();
			$j('li.gd-empty-group-title-li').hide();
			$j('select#group-list_1').parents('.gd-group-title-li').children('a').click();
		},
		error : function(response) {
			$j('li.gd-empty-group-title-li span').empty().text(response);
			$j('li.gd-empty-group-title-li').removeClass('loading');
		}
	});
	if(flag == 'disable') $j('#genGdTrigger, #finalizeGdTrigger').removeClass('active');		
	else $j('#genGdTrigger, #finalizeGdTrigger').addClass('active');
}

function prepareGdJSON(i, node){
	var gdPanelChoices = $j(node).find('ul.gd-content-ul').find('li.gd-panel-li').find('option:selected');
	var gdGroupChoices = $j(node).find('ul.gd-content-ul').find('li.gd-group-li').find('option:selected');
	var returnVal = {
			"gdName": "Group "+i,
			"campusID":$j("#selectCampus").val(),
			"listOfCandidates":"",
			"listOfPanelists":""
	};
	returnVal.listOfCandidates = [];
	returnVal.listOfCandidates.candidateID = "";
	returnVal.listOfPanelists = [];
	returnVal.listOfPanelists.panelistID = "";
	returnVal.groupID = i;
//	var panelSearchChoice = $j(gdPanelChoices).children('li.search-choice');
//	var groupSearchChoice = $j(gdGroupChoices).children('li.search-choice');
	$j.each(gdPanelChoices, function(){
		//var str = $j(this).children('span').text();
		var str = $j(this).text();
		var index = str.indexOf(".");
		returnVal.listOfPanelists.push(str.substring(0,index));
		/*if (!returnVal.hasOwnProperty("nodes")) {
			returnVal.nodes = [];
        }
		returnVal.nodes.push(str.substring(0,index));*/
	});
	$j.each(gdGroupChoices, function(){
		var str = $j(this).text();// $j(this).children('span').text();
		var index = str.indexOf(".");
		returnVal.listOfCandidates.push(str.substring(0,index));
		/*if (!returnVal.hasOwnProperty("nodes")) {
			returnVal.nodes = [];
        }
		returnVal.nodes.push(str.substring(0,index));*/
	});
	return returnVal;
}

function finalizeGD(){
	var gdGroupTitle = $j('li.gd-group-title-li');
	$j.each(gdGroupTitle, function(i, item){
		gdMasterObject.push(prepareGdJSON((i+1), item));
	});
	//console.log("group data"+gdMasterObject);
	$j.ajax({
		type : "POST",
		url : "/CareTool/groupdiscussion/finalize/",
		dataType: 'text',
		data : { jsondata : JSON.stringify(gdMasterObject)},
		success : function(response) {
			$j('div#gd-accordion a.search-choice-close').remove();
			$j('span#finalizeGdTrigger').remove();
			$j('span#genGdTrigger').remove();
			$j('div.chosen-drop').remove();
			updatePanelistList();
			updateCandidateList();
			careToolVar.groupDiscussionFinalize = true;
				$j('li#interview').show();
				$j('li#interviewInactive').hide();
		},
		error: function(response){
			care.popup(care.messages.candidateDetailsSaveErrorTitle, care.messages.candidateDetailsSaveErrorBody,"");
			careToolVar.groupDiscussionFinalize = false;		
				$j('li#interview').hide();
				$j('li#interviewInactive').show();				
		}
	});
}


$j(document).ready(function(){
	// Accordion Code
	/* $j('#gd-accordion > ul > li:has(ul)').addClass("has-sub"); */
	$j('#gd-accordion > ul.gd-parent-ul').on('click','li.gd-group-title-li a',function() {
		var checkElement = $j(this).next();
		$j('#gd-accordion li.gd-group-title-li').removeClass('active');
		$j(this).closest('li').addClass('active');	
		if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
			$j(this).closest('li').removeClass('active');
			checkElement.slideUp('normal');
		}
		if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
			$j('#gd-accordion ul.gd-content-ul:visible').slideUp('normal');
			checkElement.slideDown('normal');
		}
		if (checkElement.is('ul')) {
			return false;
		} else {
			return true;	
		}
	});
	
//	$j('select.gd-panel-list').chosen().on('change', this, function(event, params){
//		//console.log("Deselected"+params.deselected);
//		var chosenChoices = $j(this).parents('li.gd-group-li').find('ul.chosen-choices');
//		var chosenResults = $j(this).parents('li.gd-group-li').find('ul.chosen-results');
//	});
//	$j('select.gd-cand-list').chosen().on('change', this, function(event, params){
//			//console.log("Selected"+params.selected);
//			//console.log("Deselected"+params.deselected);
//			var chosenChoices = $j(this).parents('li.gd-panel-li').find('ul.chosen-choices');
//			var chosenResults = $j(this).parents('li.gd-panel-li').find('ul.chosen-results');
//	});
//	$j('select.gd-cand-list, select.gd-panel-list').chosen();
	$j('#genGdTrigger').on('click', this, function(){
		if($j(this).hasClass('active')) genGdGroup();		
	});
	$j('input#sizeOfGdGroup').on('focus', this, function(){
		$j('p#groupSizePrompt').show().animate({opacity: 1},'slow');
	}).on('blur', this, function(){
		$j('p#groupSizePrompt').hide().animate({opacity: 0},'slow');
	});
	$j('input#sizeOfGdGroup').bind('input propertychange', function() {
		
		$j(this).val($j(this).val().replace(/[^0-9\.]+/g,''));
		if($j(this).val()>10){
			$j(this).val(10);
			$j('input#sizeOfGdGroup').addClass('error');
		}
		else if($j(this).val()<=10){
			$j('input#sizeOfGdGroup').removeClass('error');
			}
		
	});
	
	$j('#gd-control-strip').on('click','#finalizeGdTrigger.active',function(){
		var action,
			r = confirm("Do you want to finalize?");
		if (r == true) action = true;
		else  action = false;
		if(action){			
			finalizeGD();
			alert("Successfully Finalized GD GRoups. This page is now read only.");
		}
	});
});