$j(document).ready(function() {
    $j('div#panelistFinalize').on('click','#finalizePanelTrigger.btn-success',function(){		
      var panelistJSON = [];
      var $headers = $j("table#currentpanelistListProjectTable thead th");
      var $rows = $j("table#currentpanelistListProjectTable tbody tr").each(function(index) {
          $cells = $j(this).find("td");
          panelistJSON[index] = {};
          $cells.each(function(cellIndex) {
            var val = $j.trim(cellIndex == 5 ? $j(this).find('textarea').val() : $j(this).text());
            if(cellIndex != 3) panelistJSON[index][$j($headers[cellIndex]).html()] = val;
          }); 
      });
      panelistJSON.candidateId = "";
      var panelistJSONObject = {};
      panelistJSONObject.panelistJSON = panelistJSON;
      $j.ajax({
             "dataType": 'text',
             "type": "POST",
             "url": "/CareTool/campusdetails/finalize/panelistFreeze/"+$j("select#selectCampus").val(),
             "data": JSON.stringify(panelistJSONObject),
             "success": function(response){ 
        	$j('span#finalizePanelTrigger').removeClass("btn-success active").addClass("btn-default"); 
               jQuery('#popupMail').modal('show');
               $j('li#gd').show();
               $j('li#gdInactive').hide();
               $j('#genGdTrigger, #finalizeGdTrigger').addClass('active');
             },
             //"timeout": 15000,   // optional if you want to handle timeouts (which you should)
             "error": function handleAjaxError( xhr, textStatus, error ) {
                alert( 'Error occurred');
               $j('li#gd').hide();
               $j('li#gdInactive').show();
               $j('#genGdTrigger, #finalizeGdTrigger').removeClass('active');
                    /* alert( 'The server took too long to send the data.'+textStatus);
                     alert( 'The server took too long to send the data.'+error);*/
             } // this sets up jQuery to give me errors
         });
    });    

   updatePanelBtns();

    $j('#currentpanelistAdminSection').on('click','span#deletePanel.active', function(){
        var del = $j.trim($j('#currentpanelistListProjectTable tr.selected').find('td:first-child').text());       
        $j.ajax( {
             "dataType": 'text',
             "type": "POST",
             "url": "/CareTool/panelist/deletePanelistFromCampus?campus_id="+campusID+"&panelist_id="+del,
             "success": function(response){               
               setTimeout(function(){
                  currentCampusPanelistMapping();
                  availableCampusPanelistMapping();
                  alert("Panelists has been removed.");
               }, 200);
             },
             //"timeout": 15000,   // optional if you want to handle timeouts (which you should)
             "error": function handleAjaxError( xhr, textStatus, error ) {
                       alert( 'Error occurred');
               /*      alert( 'The server took too long to send the data.'+textStatus);
                     alert( 'The server took too long to send the data.'+error);*/
             } // this sets up jQuery to give me errors
         });
    });

    $j('#addpanelistAdminSection').on('click', 'span#addPanel.active', function(){
        var sel = $j.trim($j('#addpanelistAdminSection tr.selected').find('td:first-child').text());       
        $j.ajax( {
             "type": "POST",
             "url": "/CareTool/panelist/addPanelistToCampus?campus_id="+campusID+"&panelist_id="+sel,
             "success": function(response){               
               setTimeout(function(){
                  currentCampusPanelistMapping();
                  availableCampusPanelistMapping();
              //    alert("Panelists has been Added.");
               }, 200);
             },
             //"timeout": 15000,   // optional if you want to handle timeouts (which you should)
             "error": function handleAjaxError( xhr, textStatus, error ) {
                    alert( 'Error occurred');
                    /* alert( 'The server took too long to send the data.'+textStatus);
                     alert( 'The server took too long to send the data.'+error);*/
             } // this sets up jQuery to give me errors
         });
    });
    $j('#mailsend').on('click', function(){
    	//alert("clicked "+$j('#txt').val()+"\n"+$j('#txtmsg').val());
    	
    	var items=[];

    	//Iterate all td's in current panelist email id
    	$j('#currentpanelistListProjectTable tbody tr td:nth-child(3)').each( function(){
    	   //add item to array
    	   items.push( $j(this).text() );       
    	});

    	//restrict array to unique items
    	var items = $j.unique(items);
    	
    	//alert(items);
    	
        $j.ajax( {
             "type": "POST",
             "url": "/CareTool/panelist/sendNotificationMail?campus_id="+campusID+"&txt="+$j('#txt').val()+"&txtmsg="+$j('#txtmsg').val()+"&emailId="+items,
             "success": function(response){               
            	 alert( 'Email has been sent to the panelists');
            	 div_hide();
             },
             //"timeout": 15000,   // optional if you want to handle timeouts (which you should)
             "error": function handleAjaxError( xhr, textStatus, error ) {
                    alert( 'Error occurred');
                    /* alert( 'The server took too long to send the data.'+textStatus);
                     alert( 'The server took too long to send the data.'+error);*/
             } // this sets up jQuery to give me errors
         });
    });    
});

$j("#currentpanelistAdminSection").hide();
$j('#addpanelistAdminSection').hide();
$j("#panelistFinalize").hide();
$j("#panelistNone").show();

function currentCampusPanelistMapping() { 
  $j("#panelistNone").hide();  
  $j("#currentpanelistAdminSection").show();
  $j('#addpanelistAdminSection').show();
  $j("#panelistFinalize").show();
  $j("table#currentpanelistListProjectTable").dataTable({
        "bProcessing": true,
        "bDestroy": true,
        "bAutoWidth": true, 
        "bServerSide": true,
        "iDisplayLength": 10,
        "sAjaxSource": "/CareTool/campusdetails/get_panelist_list/" + campusID,
        "fnServerData": function (sSource, aoData, fnCallback ) {
            $j.ajax( {
               "dataType": 'json',
                "type": "GET",
                "url": sSource,
                "success": fnCallback,
                "timeout": 15000,   // optional if you want to handle timeouts (which you should)
                "error": handleAjaxError // this sets up jQuery to give me errors
            } );
        },
        "fnRowCallback": function(nRow, aaData){
            $j(nRow).on('click', function(){
              if(!$j('#deletePanel').hasClass('inactive')){
                  $this = $j(this);
                  $this.siblings('tr').removeClass('selected');
                  if($this.hasClass('selected')){
                    $this.removeClass('selected');
                    $j('#deletePanel').removeClass('active');
                  }
                  else {
                    $this.addClass('selected');
                    $j('#deletePanel').addClass('active');
                  }
              }              
            });
        },
        "oLanguage": {
            "sZeroRecords": "No records to display"
          },
        "aoColumns": [                     
                      { "mData":"id", "sWidth":"auto"},
                      { "mData":"panelist_name", "sWidth":"auto"},
                      { "mData":"panelist_email_id", "sWidth":"auto"},
                      { "mData":"serviceLine", "sWidth":"auto"},
                      { "mData":"designation", "sWidth":"auto"}
        ]
        });
        availableCampusPanelistMapping();
  }

function availableCampusPanelistMapping() {
  $j("table#addpanelistListProjectTable").dataTable({
      "bProcessing": true,
      "bDestroy": true,
      "bAutoWidth": true, 
      "bServerSide": true,
      "iDisplayLength": 10,
      "sAjaxSource": "/CareTool/panelist/availablePanelist/" + campusID,
      "fnServerData": function (sSource, aoData, fnCallback ) {
          $j.ajax( {
             "dataType": 'json',
              "type": "GET",
              "url": sSource,
              "success": fnCallback,
              "timeout": 15000,   // optional if you want to handle timeouts (which you should)
              "error": handleAjaxError // this sets up jQuery to give me errors
          } );
      },
      "oLanguage": {
          "sZeroRecords": "No records to display"
        },
        "fnRowCallback": function(nRow, aaData){
            $j(nRow).on('click', function(){
              if(!$j('#addPanel').hasClass('inactive')){
                  $this = $j(this);
                  $this.siblings('tr').removeClass('selected');
                  if($this.hasClass('selected')){
                    $this.removeClass('selected');
                    $j('#addPanel').removeClass('active');
                  }
                  else{
                    $this.addClass('selected');
                    $j('#addPanel').addClass('active');
                  }
              }              
            });
        },
      "aoColumns": [                     
                    { "mData":"id", "sWidth":"auto"},
                    { "mData":"panelist_name", "sWidth":"auto"},
                    { "mData":"panelist_email_id", "sWidth":"auto"},
                    { "mData":"serviceLine", "sWidth":"auto"},
                    { "mData":"designation", "sWidth":"auto"}
                    ]
  });
}

// Function to remove popup when the button is inactive
function rempopup() {
    
}

function updatePanelBtns(){
  if($j('#finalizePanelTrigger').hasClass('active')){
    $j('span#deletePanel, span#addPanel').addClass('active').removeClass('inactive');
  }else{
    $j('span#deletePanel, span#addPanel').removeClass('active').addClass('inactive');
  }
}
function addCampusPanelistMapping() {
  
}
function removeCampusPanelistMapping() {
  
}