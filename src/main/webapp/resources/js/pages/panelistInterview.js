$j(document).ready(function() {
	//alert('panelistId in interviews:: '+panelistId);
	/*showRecruiterListTable1();*/
    	
        if(!isInterviewSelected) {
            $j("#interviewNoneSection").show();
     	    $j("#interviewPanel").hide();
        }
	$j('div').on('change','input.toggler', function(){
		if($j(this).parent().children('label.tog').text()=='Yes'){
			$j(this).parent().children('label.tog').text('No');
			return false;
		}
		else {
			$j(this).parent().children('label.tog').text('Yes');
			return false;
		}
	});
	
	$j("#interview-panel-control-strip").on('click','#submtiInterviewFbTrigger',function() {
			$j(this).addClass('loading');
			var panelistInterviewDataToSave = [];
			//var panelResultMasterObject = {};
			var panelResultMasterObject = { 
					resultSet : [] 
			};
			var jsonData = {};
			//Need to create a JSON Object with tthe below format to update the interview data.
			jsonData["interviewId"] = $j.trim($j('#candidateIdPL').next('.canId ').text()) || 0;
			jsonData["service"] = $j('#servicePicker > input').val() || 0;
			jsonData["servicecmnts"] = $j('#serviceTextArea').val();
			jsonData["communication"] = $j('#commPicker > input').val() || 0;			
			jsonData["communicationcmnts"] = $j('#commTextArea').val();
			jsonData["management"] =  $j('#mgmtPicker > input').val() || 0;
			jsonData["managementcmnts"] = $j('#mgmtTextArea').val();
			jsonData["leadership"] =  $j('#ldrPicker > input').val() || 0;
			jsonData["leadershipcmnts"] = $j('#ldrTextArea').val();
			jsonData["qualification"] = $j.trim($j('input#quals').val());
			jsonData["motivators"] = $j.trim($j('input#mtvn').val());
			jsonData["objectives"] = $j.trim($j('input#objts').val());
			jsonData["shift"] = $j('#shiftsCheckbox').next('label').text() == 'Yes' ? 'Y' : 'N';
			jsonData["coursecompletiondate"] = $j.trim($j('input#complDate').val());
			jsonData["applied"] = $j('#compCheckbox').next('label').text() == 'Yes' ? 'Y' : 'N';
			jsonData["comments"] = $j.trim($j('input#comnts').val());
			jsonData["interviewerDecision"] = $j('#decision').next('label').text() == 'No Further Interest' ? 'No Further Interest' : 'Offer';
			panelResultMasterObject.resultSet.push(jsonData);
			/*panelResultMasterObject.interviewId='';
			panelResultMasterObject.interviewId=$j('table#panelSummaryGd').find('td.groupId').text();
			panelResultMasterObject.resultSet = [];
			var interviewGroupTitle = $j('li.interview-panel-title-li');
			var controlStrip = $j(this).parents('#gd-panel-control-strip');
			var table =$j(this).parents('div#panelGdContainer').find('table#panelGd tbody tr:visible');
			$j.each(table, function(i, row){
				panelResultMasterObject.resultSet[i] = {};
				$j.each($j(row).children('td'), function(j, cell){
					panelResultMasterObject.resultSet[i][$j(cell).attr('class')]=$j(cell).text();
				});
			});*/
			panelistInterviewDataToSave.push(panelResultMasterObject);
			var panelResult = JSON.stringify(panelistInterviewDataToSave);
			console.info("panelistInterviewDataToSave: "+panelResult);
			$j.ajax({
				url : "/CareTool/admin/save_interview_result/",
				type : "POST",
				async: false,
				data: {jsondata:panelResult},
				datatype: 'text',
				success : function(data) {
					$j("#submtiInterviewFbTrigger").removeClass('loading');//.remove();
					alert("Feedback has been submitted. Navigating to homepage")
					$j('div#interviewPanel').find('input, select, textarea').val('');//.attr('disabled', true);
					//$j("li#panelistLanding"/*"+sessionStorage.getItem("active-link")*/).trigger("click");
					location.reload(true);					
				},
				error: function(xhr, status, errorThrown){
					console.log("Error occured"+ xhr +status+errorThrown);	
				}
			});
	});
});

function listPanelistInterview(campusId)
{
	$j.ajax( {
        "dataType": 'json',
         "type": "POST",
         "url": "/CareTool/admin/list_panelist_interview/"+ panelistId+"/"+campusId,
         "success": function(response){
         	var pnls = response.aaData;
         	$j(pnls).each(function(i){
         		$j('#interviewContainerUl').append('<li><a href="#"><span>'+pnls[i].candidateDetailsDomain.firstName+' '+pnls[i].candidateDetailsDomain.lastName+'</span><span class="hidden">'+pnls[i].id+'</span></a></li>');
         	});
        	 /*var ul = $j('#interviewContainerUl li.cloneSeedLi').clone('true').insertAfter($j('#interviewContainerUl li.cloneSeedLi'));
        	 ul.addClass('loading');
        	 var li = $j('#interviewContainerUl li:last-child');
      		 li.removeClass('cloneSeedLi').removeClass('hidden');
      		 li.find('span').text(response.aaData[0].interviewName);
     		 ul.removeClass('loading');*/
         },
         //"timeout": 15000,   // optional if you want to handle timeouts (which you should)
         "error": function handleAjaxError( xhr, textStatus, error ) {
                 alert( 'The server took too long to send the data.'+textStatus);
                 alert( 'The server took too long to send the data.'+error);
         } // this sets up jQuery to give me errors
     } );
}
function showInterviewPanel(interviewId) {
    	  $j("#interviewNoneSection").hide();
	  $j("#interviewPanel").show();
	  $j.ajax( {
          "dataType": 'json',
           "type": "POST",
           "url": "/CareTool/admin/get_interview_list/"+panelistId+"/"+interviewId,
           "success": function(response){
        	   var candidateName = response.aaData[0].candidateDetailsDomain.firstName+" "+response.aaData[0].candidateDetailsDomain.lastName;
        	   var interviewId = response.aaData[0].id;
        	   var interviewStartDate = response.aaData[0].interviewStartDate;
        	   var position = response.aaData[0].position;
        	   var gender = response.aaData[0].candidateDetailsDomain.gender;
        	   var specialization = response.aaData[0].candidateDetailsDomain.subjectName;
        	   var degree = response.aaData[0].candidateDetailsDomain.degreeName;
        	   /*var panelistName = response.aaData[0].panelistDomain.panelist_name;*/
        	   var candidateId = response.aaData[0].candidateDetailsDomain.candidateId;
/*        	   $j('#interviewContainerUl li.cloneSeedLi').clone('true').insertAfter($j('#interviewContainerUl li.cloneSeedLi'));
        		var li = $j('#interviewContainerUl li:last-child');
        		li.removeClass('cloneSeedLi').removeClass('hidden');
        		li.find('span').text("Interview "+interviewId);*/
        		var row = $j("table#interviewHeader tbody tr");
        		row.find('#candidateIdPL').text(candidateName).after('<span class="canId hidden">'+interviewId+'</span>');
        		row.find('#interviewDatePL').text(interviewStartDate);
        		row.find('#positionRolePL').text(position);
        		row.find('#genderPL').text(gender);
        		row.find('#qualPL').text(degree);
        		row.find('#specPL').text(specialization);
        		
        		/*row.find('#panelistNamePL').text(panelistName);*/
        		row = $j("table#interviewHeader tbody tr");   
        		getdata = response.aaData[0];
        		//setTimeout(function(){        			
        			$j('#candidateIdPL').next('.canId ').text(getdata.interviewId);
					$j('#servicePicker > input').val(getdata.service);
					$j('#serviceTextArea').val(getdata.servicecmnts);
					$j('#commPicker > input').val(getdata.communication);
					$j('#commTextArea').val(getdata.communicationcmnts);
					$j('#mgmtPicker > input').val(getdata.management);
					$j('#mgmtTextArea').val(getdata.managementcmnts);
					$j('#ldrPicker > input').val(getdata.leadership);
					$j('#ldrTextArea').val(getdata.leadershipcmnts);
					$j('input#quals').val(getdata.qualification);
					$j('input#mtvn').val(getdata.motivators);
					$j('input#objts').val(getdata.objectives);
					$j('input#complDate').val(getdata.coursecompletiondate);
					$j('input#comnts').val(getdata.comments);
					$j('#decision').next('label').text(getdata.interviewerDecision);
					var isOffer = (getdata.interviewerDecision === "Offer") ? 2 : 0;
					$j(".decisionSlider").val(isOffer);
					/*var label = $j('.decisionSliderLabel');
					$j(".decisionSlider[type=range]").trigger("change");
					if(getdata.interviewerDecision=='Offer'){						
						$j(".decisionSlider[type=range]").val()
					}
					else if(getdata.interviewerDecision=='No Further Interest'){
						label.addClass('noFurtherInterest');
					}*/
					if(getdata.shift == 'Y') {
						$j('#shiftsCheckbox').attr('checked', true);
						$j('#shiftsCheckbox').next('label').text('Yes');
					}
					else{
						$j('#shiftsCheckbox').removeAttr('checked');
						$j('#shiftsCheckbox').next('label').text('No');
					} 
					if(getdata.applied == 'Y') {
						$j('#compCheckbox').attr('checked', true);
						$j('#compCheckbox').next('label').text('Yes');
					}
					else{
						$j('#compCheckbox').removeAttr('checked');
						$j('#compCheckbox').next('label').text('No');
					} 
					//jsonData["shift"] = $j('#shiftsCheckbox').next('label').text() == 'Yes' ? 'Y' : 'N';					
					//jsonData["applied"] = $j('#compCheckbox').next('label').text() == 'Yes' ? 'Y' : 'N';					 
					$j('input.picker').trigger('change');
        		//},1000);
        		
           },
           //"timeout": 15000,   // optional if you want to handle timeouts (which you should)
           "error": function handleAjaxError( xhr, textStatus, error ) {
               alert( 'The server took too long to send the data.'+textStatus);
               alert( 'The server took too long to send the data.'+error);
       }  // this sets up jQuery to give me errors
       } );
	  /*
		panelistInterviewListProjectTable =  $j("table#panelistInterviewListProjectTable").dataTable({
					"bProcessing": true,
			        "bDestroy": true,
			        "bAutoWidth": true, 
			        "iDisplayLength": 50,
			        "sAjaxSource": "/CareTool/admin/get_interview_list/",
			        "fnServerData": function ( sSource, aoData, fnCallback ) {
			            $j.getJSON( sSource, aoData, function (json) {
			            	//Here you can do whatever you want with the additional data
			                status=json.messageFlag;
			                //Call the standard callback to redraw the table
			                fnCallback(json);
			            } );
			        },
			        "fnServerData": function (sSource, aoData, fnCallback ) {
			            $j.ajax( {
			               "dataType": 'json',
			                "type": "POST",
			                "url": sSource,
			                "data": "panelistid=" + panelistId,
			                "success": fnCallback,
			                //"timeout": 15000,   // optional if you want to handle timeouts (which you should)
			                "error": function handleAjaxError( xhr, textStatus, error ) {
		                        alert( 'The server took too long to send the data.'+textStatus);
		                        alert( 'The server took too long to send the data.'+error);
		                }  // this sets up jQuery to give me errors
			            } );
			        },
			        "oLanguage": {
			            "sZeroRecords": "No records to display"
			          },
			        "aoColumns": [
			                      { "mData":"candidateId",  "bVisible": false},
			                      { "mData":"interviewName", "sWidth":"auto"},
			                      { "mData":"interviewStartDate", "sWidth":"auto"},
			                      { "mData":"interviewEndDate", "sWidth":"auto"}
			                      ]
							});
							var rowData  = $j("#recruiterListProjectTable").fnGetData(this);
							showRecruiterInformation(rowData);
			
					$j("table#recruiterListProjectTable").on('click', 'tr', function(){
						var row = recruiterListProjectTable.fnGetData(this);
						//showRecruiterInformation(row);
					});
*/}

function showRecruiterInformation(data){
		
	var strHTML = '';
	strHTML = '<table>';
	strHTML += '<tr><td>Interview</td><td><input type="text" id="interviewName" value=""></td></tr>';
	strHTML += '<tr><td>Start Date</td><td><input type="text" id="interviewStartDate" value=""></td></tr>';
	strHTML += '<tr><td>End Date</td><td><input type="text" id="interviewEndDate" value=""></td></tr>';
	strHTML += '</table>';
	
	$j("#displayRowDetailsRecruiter").html(strHTML);
	$j("#displayRowDetailsRecruiter").css({"display":""});
	$j("#addRecruiterPopup").css({"display":"none"});
	$j("#recruiterName").val(data.recruiter_name);
	$j("#recruiter_email_id").val(data.recruiter_email_id);
	
}

function interviewerDecision() {
	$j(".decisionSlider[type=range]").on('change', function(e) {
		var label = $j('.decisionSliderLabel');
		switch ($j(this).val()) {
			case "1":
				label.text("No Further Interest");
				break;
				break;
			case "2":
				label.text("Offer");
				break;
		}
		var decision = $j(this).parents('tr').find('td.decision');
		decision.removeClass('offer').removeClass('onHold').removeClass('noFurtherInterest');
		
		if (decision.text() == 'No Further Interest') {
			decision.addClass('noFurtherInterest');
		} else if (decision.text() == 'Offer') {
			decision.addClass('offer');
		}
	});
}