jQuery("#menu-toggle").click(function(e) {
	e.preventDefault();
	jQuery("#wrapper").toggleClass("toggled");
});

var loadCampusListData = function() {
	$j.ajax({
		url : "/CareTool/admin/get_campus_list/",
		type : "GET",
		success : function(data) {
			$j('select#selectCampusId')
					.append(new Option("Select a Campus", 0));
			for (var i = 0; i < data.length; i++) {
				$j('select#selectCampusId').append(
						new Option(data[i].campusName, data[i].id));
			}
			$j('select#selectCampusId').append(
					new Option("Add a Campus", "addCampus"));
			$j("select#selectCampusId option:first-child").addClass(
					"placeholder");
			// $j(".campus-chosen-select").chosen();
		},
		error : function(response) {
			console.log("Error occured while fetching Campus List" + response);
		}
	});
}

loadCampusListData();

$j('select#selectCampusId').change(function generateFunnel() {
	$("#reportcontainer .chart").show();
	var campusID = $j(this).val();
	var reportFilterJSON = [];
	var reportFilterJSONObject = {};
	reportFilterJSONObject.reportFilterJSON = reportFilterJSON;

	// funnle ajax call start
	$j
			.ajax({
				"dataType" : 'json',
				"type" : "POST",
				"url" : "/CareTool/reports/getFilterReports?campusid="
						+ campusID,
				"data" : {
					jsondata : JSON
							.stringify(reportFilterJSONObject.reportFilterJSON)
				},
				"success" : function(response) {
					$("#container").show();
					var c1Vals = response.Chart1Vals.split(',');
					$("#c1Ratio1").text(
							(c1Vals[1] * 100 / c1Vals[0]).toFixed(2) + '%');
					$("#c1Ratio2").text(
							(c1Vals[2] * 100 / c1Vals[1]).toFixed(2) + '%');
					$("#c1Ratio3").text(
							(c1Vals[3] * 100 / c1Vals[2]).toFixed(2) + '%');

					$("#hitRate1").text(
							(c1Vals[3] * 100 / c1Vals[0]).toFixed(2) + '%');
					$("#winRate1").text(
							(c1Vals[4] * 100 / c1Vals[3]).toFixed(2) + '%');

					var chart = AmCharts.makeChart("chartdiv", {
						"type" : "funnel",
						"baseWidth" : "100%",
						"theme" : "light",
						"dataProvider" : response.funnelDetailsCampus,
						"addClassNames" : true,
						"marginBottom" : 130,
						"marginRight" : 50,
						"marginLeft" : 75,
						"funnelAlpha" : 0.9,
						"titleField" : "title",
						"valueField" : "value",
						"startX" : 0,
						"startAlpha" : 0,
						"neckWidth" : "20%",
						"outlineThickness" : 2,
						"outlineColor" : "#002776",
						"neckHeight" : "50%",
						"balloonText" : "[[title]]:<b>[[value]]</b>",
						"responsive" : {
							"enabled" : true,
							"addDefaultRules" : false,
							"rules" : [ {
								"maxWidth" : 600,
								"overrides" : {
									"labelText" : ""
								}
							} ]
						}
					});

				},
				// "timeout": 15000, // optional if you want to
				// handle timeouts (which you should)
				"error" : function handleAjaxError(xhr, textStatus, error) {
					alert('The server took too long to send the data.'
							+ textStatus);
					alert('The server took too long to send the data.' + error);
				} // this sets up jQuery to give me errors
			});
});