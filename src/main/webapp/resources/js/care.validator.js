var care.validator.validateNumericField = function (input, error) {
	if (numericals != null && numericals !== '') {
		var regex = new RegExp(/^\+?[0-9(),.-]+$/);
		if (!numericals.match(regex)) {
			$j('#' + numericalsError).val('Should contain only numeric values');
			$j('#' + numericalsError).show();
			return false;
		} else {
			return true;
		}
	}
}

var care.validator.validateTextField = function (input, error) {
	if (numericals != null && numericals !== '') {
		var regex = new RegExp(/^\+?[0-9(),.-]+$/);
		if (!numericals.match(regex)) {
			$j('#' + numericalsError).val('Should contain only numeric values');
			$j('#' + numericalsError).show();
			return false;
		} else {
			return true;
		}
	}
}

var care.validator.validateAlphanumericField = function (input, error) {
	if (numericals != null && numericals !== '') {
		var regex = new RegExp(/^\+?[0-9(),.-]+$/);
		if (!numericals.match(regex)) {
			$j('#' + numericalsError).val('Should contain only numeric values');
			$j('#' + numericalsError).show();
			return false;
		} else {
			return true;
		}
	}
}
