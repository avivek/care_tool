var $j = jQuery.noConflict();
var care = care || {};
//create a general purpose namespace method
//this will allow us to create namespace a bit easier
care.createNS = function (namespace) {
 var nsparts = namespace.split(".");
 var parent = care;

 // we want to be able to include or exclude the root namespace 
 // So we strip it if it's in the namespace
 if (nsparts[0] === "care") {
     nsparts = nsparts.slice(1);
 }
 // loop through the parts and create 
 // a nested namespace if necessary
 for (var i = 0; i < nsparts.length; i++) {
     var partname = nsparts[i];
     // check if the current parent already has 
     // the namespace declared, if not create it
     if (typeof parent[partname] === "undefined") {
         parent[partname] = {};
     }
     // get a reference to the deepest element 
     // in the hierarchy so far
     parent = parent[partname];
 }
 // the parent is now completely constructed 
 // with empty namespaces and can be used.
 return parent;
};
care.topNavigation = function(currentNavElmt){
	if(!($j(currentNavElmt).hasClass("active-link"))){
		$j.each($j("ul#topnav-bar li"), function(){
		if($j(this).hasClass("active-link")){
			$j(this).removeClass("active-link");
			var image=$j(this).find("div.topnav-linkimg img");
			image.attr('src',$j(this).find("img").attr('src').replace("-white.png",".png"));
			}
		});
		$j(currentNavElmt).addClass("active-link");
		var image=$j(currentNavElmt).find("div.topnav-linkimg img");
		image.attr('src',$j(currentNavElmt).find("img").attr('src').replace(".png","-white.png"));
		if(typeof(Storage)=="undefined"){
			  console.log("Local Storage is not supported on this browser. Please use Internet Explorer 8+, Mozilla Firefox, Google Chrome, Opera or Safari");
			}	
		sessionStorage.setItem("active-link", $j(currentNavElmt).attr("id"));
		$j.each($j("div.mod-container"), function(){
			if($j(this).hasClass("active-module")){	
					$j(this).removeClass("active-module");
					$j(this).addClass("inactive-module");
			}
		});
		var activeModule=$j("div#"+$j(currentNavElmt).attr("id"));
		activeModule.removeClass("inactive-module");
		activeModule.addClass("active-module");
	}
	else{
		return true;
	}
}
//Create the namespace for products
care.createNS("care.campusDetails");
care.createNS("care.candidateDetails");
care.createNS("care.gd");
care.createNS("care.interview");
care.createNS("care.consensus");
care.createNS("care.reports");
care.createNS("care.admin");
care.createNS("care.messages");
care.createNS("care.campusDetails.validator");
care.createNS("care.candidateDetails.validator");
care.createNS("care.gd.validator");
care.createNS("care.consensus.validator");
care.createNS("care.reports.validator");
care.createNS("care.admin.validator");

$j(document).ready(function(){
	$j(document).on("click", function() {
	    $j("div#carePopup").hide("fast");	
	    $j('.overlayPop').hide('fast');
	  });
	$j("ul#topnav-bar").on("click", "li", function(){
		care.topNavigation(this);
	});
	$j("li#campusDetails").trigger("click");	
	$j("li.button-row").on("click", "input#camp-cancel-button", function(){
		$j(this).hide().fadeOut("slow");
		$j("div#campus-details .element").each(function(){
			$j(this).val($j(this).attr("data-currentvalue"));
		});
	});
});