//Append all messages that need to go into the CaRe Tool here
care.messages  = {"emptyCampusTitle":"No campus has been selected in the Campus Details tab",
					"emptyFormField":"This field cannot be left blank",
					"candidateDetailsFetchErrorTitle":"Error fetching candidate details",
					"candidateDetailsFetchErrorBody":"There seems to be an error in our database. Please contact the System Admin",
					"candidateDetailsSaveErrorTitle":"Error saving candidate details",
					"candidateDetailsSaveErrorBody":"There seems to be an error in our database. Please contact the System Admin",
					"candidateDetailsFetchServerTimeoutTitle":"Server Timeout",
					"candidateDetailsFetchServerTimeoutBody":"The damn server has taken too long to respond. Please try after some time. Sorry!", 
					"candidateDetailsFetchServerTimeoutFooter":"Click anywhere outside this care.popup to close.",
					"candidateDetailsFileTooBig":"Too big file! <br />File is too big, it should be less than 10 MB."
};
