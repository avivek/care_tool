package com.deloitte.care.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "group_discussion_details")
public class GroupDiscussionDetailsDomain implements java.io.Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique=true,nullable=false)
	private int id;

	@Column(name = "gdName")
	private String gdName;

	@Column(name = "gdStartDate")
	private Date gdStartDate;

	@Column(name = "gdEndDate")
	private Date gdEndDate;
	
	@Column(name = "leadership")
	private String leadership;
	@Column(name = "listening")
	private String listening;
	@Column(name = "participation")
	private String participation;
	@Column(name = "approach")
	private String approach;
	@Column(name = "presentation")
	private String presentation;
	@Column(name = "response")
	private String response;
	@Column(name = "total")
	private Integer total;
	@Column(name = "comments")
	private String comments;
	@ManyToOne
	@JoinColumn(name="campus_id")
	private CampusDomain campusdomain;
	
	@ManyToOne
	@JoinColumn(name="candidate_id")
	private CandidateDetailsDomain candidateDetailsDomain;

	@ManyToOne
	@JoinColumn(name="panelist_id")
	private PanelistDomain panelistDomain;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdDt", updatable = false)
	private Date createdDt;

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updatedDt", insertable = false)
	private Date updatedDt;

	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "updatedBy")
	private String updatedBy;
	
	@Column(name = "gdFreeze")
	private String gdFreeze;

	
	public String getGdFreeze() {
		return gdFreeze;
	}

	public void setGdFreeze(String gdFreeze) {
		this.gdFreeze = gdFreeze;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getGdName() {
		return gdName;
	}

	public void setGdName(String gdName) {
		this.gdName = gdName;
	}

	public Date getGdStartDate() {
		return gdStartDate;
	}

	public void setGdStartDate(Date gdStartDate) {
		this.gdStartDate = gdStartDate;
	}

	public Date getGdEndDate() {
		return gdEndDate;
	}

	public void setGdEndDate(Date gdEndDate) {
		this.gdEndDate = gdEndDate;
	}

	public CampusDomain getCampusdomain() {
		return campusdomain;
	}

	public void setCampusdomain(CampusDomain campusdomain) {
		this.campusdomain = campusdomain;
	}

	public CandidateDetailsDomain getCandidateDetailsDomain() {
		return candidateDetailsDomain;
	}

	public void setCandidateDetailsDomain(
			CandidateDetailsDomain candidateDetailsDomain) {
		this.candidateDetailsDomain = candidateDetailsDomain;
	}

	public PanelistDomain getPanelistDomain() {
		return panelistDomain;
	}

	public void setPanelistDomain(PanelistDomain panelistDomain) {
		this.panelistDomain = panelistDomain;
	}

	public String getLeadership() {
		return leadership;
	}
	public void setLeadership(String leadership) {
		this.leadership = leadership;
	}
	public String getListening() {
		return listening;
	}
	public void setListening(String listening) {
		this.listening = listening;
	}
	public String getParticipation() {
		return participation;
	}
	public void setParticipation(String participation) {
		this.participation = participation;
	}
	public String getApproach() {
		return approach;
	}
	public void setApproach(String approach) {
		this.approach = approach;
	}
	public String getPresentation() {
		return presentation;
	}
	public void setPresentation(String presentation) {
		this.presentation = presentation;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
}
